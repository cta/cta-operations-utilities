#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Interactive operator tool for tape media unmount tasks.
"""

import sys
import argparse
from argparse import RawTextHelpFormatter

import tapeadmin

from ctautils import log_utils
from tapeadmin import tape_drive, tape_medium
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME

config, logger = None, None
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]


def load_args():
    """
    Parse command line flags
    :return: Parsed argparse args
    """
    usage = "\ntape-unmount [-v <vid>] [-D <drive_name>] [-c]\n" \
            "tape-unmount [--vid <vid>] [--drivename <drive_name>] [--verbose] " \
            "[--check] [-C | --config-file <path>] [-h | --help]"

    parser = argparse.ArgumentParser(
        description = "A script to unmount a tape on the local drive",
        usage = usage,
        formatter_class = RawTextHelpFormatter
    )

    # VID is needed, except if '--local' is given
    parser.add_argument(
        "-v",
        "--vid",
        type = str,
        help = "The volume ID to unmount"
    )
    parser.add_argument(
        "-l",
        "--local",
        action = 'store_true',
        help = "Unmount all tapes on all host-local drives"
    )
    parser.add_argument(
        "-D",
        "--drive",
        type = str,
        help = "The drive to mount the tape on"
    )
    parser.add_argument(
        "-c",
        "--check",
        action = 'store_true',
        help = "Check if the correct tape is mounted by reading the VOL1 label."
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    # Set up config holder
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return args


def unmount_local_drives():
    """
    Identify local drives + their mounted tapes, then unmount them.
    """
    logger.info("Unmounting all locally mounted tapes.")
    drives_dict_list = tape_drive.get_local_drives(config, logger)

    # TODO: Perhaps handle VIDs more explicitly here, instead of relying on
    # the under-the-hood actions of do_unmount?
    n_success = 0
    for drive_dict in drives_dict_list:
        drive_name = drive_dict['DriveName']

        unmount_success = tape_medium.do_unmount(
            vid = None,  # Free check performed by unmount call
            drive_name = drive_name,
            config = config,
            logger = logger
        )
        if unmount_success:
            n_success += 1

    if n_success == len(drives_dict_list):
        logger.info("Unmount successful, all drives are free.")
        return True

    return False


def main():
    """
    Unmount a user-specified tape from a user-specified drive,
    or unmount all tapes in all host-local drives.
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    if args.local:
        # Unmount tapes from all local drives
        unmount_success = unmount_local_drives()
        if not unmount_success:
            log_utils.log_and_exit(
                logger,
                "Failed to unmount tapes from all local drives, please investigate."
            )

    else:
        # Unmount a single tape from a single drive
        unmount_success = tape_medium.do_unmount(
            vid = args.vid,
            drive_name = args.drive,
            config = config,
            logger = logger
        )
        if not unmount_success:
            log_utils.log_and_exit(
                logger,
                f"Tape {args.vid} was not correcly unmounted from drive " \
                f"{args.drive}. Aborting."
            )


if __name__ == '__main__':
    main()
