#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Shows the following information of a given tape:
VID, NB_MASTER_FILES, MASTER_DATA_IN_BYTES
"""

import sys
import json
import argparse

from argparse import RawTextHelpFormatter

import tapeadmin
from ctautils import log_utils, cmd_utils, table_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]

config, logger = None, None

cta_statistics_update_cmd = None

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "A script to show queues of requests",
        formatter_class = RawTextHelpFormatter
    )

    # Required arguments
    required_args = parser.add_argument_group("required arguments")
    required_args.add_argument(
        "-v",
        "--vid",
        type = str,
        help = "The volume ID from which to get the summary",
        required = True
    )

    # Optional arguments
    parser.add_argument(
        "--refresh",
        action = 'store_true',
        help = "If True, updates previously the CTA Catalogue using the "
            "'cta-statistics-update' script"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )
    settings_group.add_argument(
        "-j",
        "--json",
        action = 'store_true',
        help = "Write output as JSON (default disabled)"
    )

    # Assign the variables to the options
    args = parser.parse_args()

    if not args.vid:
        parser.print_help()
        sys.exit(0)

    # Set up config holder
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    global cta_statistics_update_cmd
    cta_statistics_update_cmd = config.get(my_name, 'cta_statistics_update_cmd')

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return args


def main():
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    # Update the CTA Catalogue
    if args.refresh:
        cmd_call = cmd_utils.run_cmd_popen(cta_statistics_update_cmd)
        if cmd_call.returncode != 0:
            log_utils.log_and_exit(
                logger,
                {log_utils.format_stderr(cmd_call.stderr)}
            )
        else:
            logger.info(cmd_call.stdout.rstrip())
    cmd = f'{tapeadmin.cta_admin_json_tape_ls_vid} {args.vid}'
    try:
        tape_info = json.loads(cmd_utils.run_cmd_popen(cmd).stdout)[0]
    except IndexError:
        if args.json:
            print('[]')
        log_utils.log_and_exit(
            logger,
            f'Tape {args.vid} not found in the output of "cta-admin tape"'
        )

    tape_usage_percentage = 100 * int(tape_info['masterDataInBytes']) / int(tape_info['capacity'])
    # Getting only the fields we need for our tape summary
    tape_summary = {
        'vid': tape_info['vid'],
        'master_files': tape_info['nbMasterFiles'],
        'master_data_bytes': tape_info['masterDataInBytes'],
        'usage': f'{tape_usage_percentage:.2f}%'
    }
    # Print only JSON if option is specified
    if args.json:
        print(json.dumps(tape_summary, indent=4, sort_keys=True))
        sys.exit(0)
    # Print to console in table format, if JSON option is not specified
    headers = ['VOLUME_ID', 'MASTER_FILES', 'MASTER_DATA_IN_BYTES', 'TAPE_USAGE']
    rows = [
        [
            tape_summary['vid'],
            tape_summary['master_files'],
            tape_summary['master_data_bytes'],
            tape_summary['usage']
        ]
    ]
    print(table_utils.get_table(headers, rows, config))


if __name__ == '__main__':
    main()
