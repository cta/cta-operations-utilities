#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Shows the following information of a given tape: args.vid, LIBRARY, LOCATION,
[ELEMENT_ADDRESS], [ADDITIONAL_INFO]
"""

import sys
import json
import argparse

from collections import OrderedDict
from argparse import RawTextHelpFormatter

import tapeadmin
from ctautils import log_utils, cmd_utils, table_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME

my_name = sys.argv[0].split('/')[-1]    # Get rid of "/usr/local/bin" and take only the final part

config, logger = None, None

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = 'Uses "/usr/bin/cta-smc" to show the physical location of a given tape',
        formatter_class = RawTextHelpFormatter
    )

    # Required arguments
    required_args = parser.add_argument_group("required arguments")
    required_args.add_argument(
        "-v",
        "--vid",
        type = str,
        help = "The volume ID from which to get the summary",
        required=True
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )
    settings_group.add_argument(
        "-j",
        "--json",
        action = 'store_true',
        help = "Write output as JSON (default disabled)"
    )

    # Assign the variables to the options
    args = parser.parse_args()

    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return args


def main():
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    # Check if cta-smc utility is available
    if cmd_utils.run_cmd_popen('ls /usr/bin/cta-smc').returncode != 0:
        log_utils.log_and_exit(
            logger,
            'CTA-SMC utility not found in /usr/bin/cta-smc'
        )
    # Get tape info from cta-admin
    cmd_cta_admin_tape_ls_json = f'{tapeadmin.cta_admin_json_tape_ls_vid} {args.vid}'
    try:
        tape_info = json.loads(cmd_utils.run_cmd_popen(cmd_cta_admin_tape_ls_json).stdout)[0]
    except IndexError:
        log_utils.log_and_exit(
            logger,
            f'Tape {args.vid} not found in cta-admin'
        )
    # Check if tape has a library assigned
    if 'logicalLibrary' in tape_info.keys():
        library = tape_info['logicalLibrary']
    else:
        log_utils.log_and_exit(
            logger,
            f"Library not found for tape {args.vid} in cta-admin."
        )
    # Check if the type of library is amongst the expected ones
    cmd_cta_smc_query_tape_json = f'{tapeadmin.cta_smc_query_json} V -V {args.vid}'
    # Attempt cta-smc call several times if necessary (sometimes it fails and a retry is needed)
    smc_call = cmd_utils.run_cmd_popen(cmd_cta_smc_query_tape_json, n_attempts=3)
    if smc_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            f'Command "{cmd_cta_smc_query_tape_json}" failed {log_utils.format_stderr(smc_call.stderr)}'
        )
    # Get the physical tape info from the cta-smc call and organise it in a dict for the output
    try:
        smc_info = json.loads(smc_call.stdout)[0]
    except IndexError:
        log_utils.log_and_exit(
            logger,
            f"Tape {args.vid} not found in cta-smc (please note that tape is in library {library})"
        )
    tape_location_info = OrderedDict(
        {
            'args.vid': args.vid,
            'LIBRARY': library
        }
    )
    # Some other fields depend on what the location is
    if smc_info['elementType'] == 'drive':
        tape_location_info['LOCATION'] = 'DRIVE'
        tape_location_info['ELEMENT_ADDRESS'] = smc_info['elementAddress']
    elif smc_info['elementType'] == 'slot':
        tape_location_info['LOCATION'] = 'HOME'
        tape_location_info['ELEMENT_ADDRESS'] = smc_info['elementAddress']
    else:
        tape_location_info['LOCATION'] = 'UNKNOWN'
        tape_location_info['ADDITIONAL_INFO'] = smc_info['elementType']
    # Print only JSON if option is specified
    if args.json:
        print(json.dumps(tape_location_info, indent=4, sort_keys=True))
        sys.exit(0)
    # Print to console in table format, if JSON option is not specified
    headers = tape_location_info.keys()
    rows = [tape_location_info.values()]
    print(table_utils.get_table(headers, rows, config))

if __name__ == '__main__':
    main()
