#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Interactive operator tool for tape media mount tasks.
"""

import os
import sys
import argparse
from argparse import RawTextHelpFormatter

import tapeadmin

from ctautils import cmd_utils, log_utils
from tapeadmin import tape_drive, tape_medium
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME

config, logger = None, None
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]


def load_args():
    """
    Parse command line flags
    :return: Parsed argparse args
    """
    usage = "\ntape-mount -v <vid> -D <drive_name> [-c]\n" \
            "tape-mount --vid <vid> --drivename <drive_name> [--verbose] " \
            "[--check] [-C | --config-file <path>] [-h | --help]"

    parser = argparse.ArgumentParser(
        description = 'A script to mount a tape on the local drive',
        usage = usage,
        formatter_class = RawTextHelpFormatter
    )
    required_named = parser.add_argument_group('required arguments')
    required_named.add_argument(
        "-v",
        "--vid",
        type = str,
        help = "The volume ID to mount"
    )
    required_named.add_argument(
        "-D",
        "--drive",
        type = str,
        required = True,
        help = "The drive to mount the tape on"
    )
    parser.add_argument(
        "-c",
        "--check",
        action = 'store_true',
        help = "Check if the correct tape is mounted by reading the VOL1 label."
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    # Set up config holder
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    if not args.vid:
        parser.print_help()
        sys.exit(0)

    return args


def do_VOL1_check_if_mount(vid:str, drive_name:str):
    """
    Double check that the tape just mounted on the drive is in fact the one we
    are expecting by looking at the on-tape data.
    :param vid: VID of the tape we just mounted
    :param drive_name: Name of the drive it was mounted on
    """
    # Rewind
    drive = tape_drive.get_local_drive(drive_name, config, logger)
    device = drive['DriveDevice']
    cmd = f'{tapeadmin.mt} -f {device} rewind'
    if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
        log_utils.log_and_exit(logger, 'Rewind failed')
    logger.debug(f'Rewind of {vid}: OK')

    # Read VOL1
    tmp_file_path = f'/tmp/{vid}-{str(os.getuid())}vol1'
    # Remove past temp file
    cmd = f'rm -f {tmp_file_path} >/dev/null 2>&1'
    cmd_utils.run_cmd(cmd, logger=logger)

    if tape_drive.do_VOL1_check(device, tmp_file_path, vid, logger=logger):
        logger.debug(f'VOL1 contains {vid}: OK')
    else:
        logger.warning(
            f'VOL1 check FAILED for tape {vid} after mount operation'
        )
    # Remove temp file
    cmd_utils.run_cmd(f'rm -f {tmp_file_path} >/dev/null 2>&1', logger=logger)


def main():
    """
    Mount a user-specified tape on a user-specified drive
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    mount_success = tape_medium.do_mount(
        vid = args.vid,
        drive_name = args.drive,
        config = config,
        logger = logger
    )
    if not mount_success:
        log_utils.log_and_exit(
            logger,
            f"Tape {args.vid} was not correcly mounted in drive " \
            f"{args.drive}. Aborting."
        )
    if args.check:
        do_VOL1_check_if_mount(args.vid, args.drive)


if __name__ == '__main__':
    main()
