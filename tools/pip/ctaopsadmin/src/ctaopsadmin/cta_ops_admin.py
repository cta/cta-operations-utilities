#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Wrapper for executing cta-admin and additional tape operator commands.
The purpose of this tool is to:
1. Provide a consistent (cmd args, cmd flags, logging, etc.) wrapper for both
   cta-admin and the operator-specific tools that supplement it.
2. Provide custom output rendering/table-formatting around cta-admin's json
   output.
3. Allow for operator-specific overrides to cta-admin (cta-cli) config.
"""

import sys
import json
import argparse

from datetime import datetime, timedelta
from argparse import RawTextHelpFormatter
from pathlib import Path

import tapeadmin

from ctautils import cmd_utils, log_utils, table_utils
from tapeadmin import tape_drive

from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctaopsadmin import TOOL_NAME

TIME_KEYWORDS = ['time', 'creationTime']
TIMEDELTA_KEYWORDS = [
    'driveStatusSince', 'timeSinceLastUpdate', 'sleepTime',
    'archiveMinRequestAge', 'retrieveMinRequestAge']
BYTE_KEYWORDS = [
    'bytes', 'Bytes', 'capacity', 'occupancy', 'size', 'Size',
    'targetedFreeSpace', 'freeSpace', 'capacityBytes', 'dataBytes'
]
MOUNT_TYPE_KEYWORDS = ['mountType']
STATUS_KEYWORDS = ['driveStatus']

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].rsplit('/', maxsplit=1)[-1]

logger = None


def script_parameters_to_list(args):
    """
    Converts script parameters from argparse.Namespace to a list for subprocess execution
    :param args: argparse.Namespace object containing the parsed arguments
    :return: List of script parameters
    """
    params = []

    for key, value in args.__dict__.items():
        if value is True:
            params.append(f'--{key}')
        elif value is not None:
            params.extend([f'--{key}', str(value)])

    return params


def execute_tapeutils_script(handler, args:list, config):
    """
    Executes the corresponding cta-cern-tape-utils script if specified in the
    command line.
    :param handler: The script to be executed
    :param args: The arguments for the script to pass
    :param config: CtaOpsConfig
    """
    cmd = f'{handler} {" ".join(args)}'
    logger.debug(f'Executing tape utility script: {cmd}')
    try:
        cmd_utils.run_cmd_interactive(cmd, logger)

    except Exception as e:
        log_utils.log_and_exit(
            logger,
            f"An error occurred while executing {cmd}: {e}"
        )

def run_cta_admin_cmd(parser, args, remaining_args, config):
    """
    Run a cta-admin command and return the command execution.
    This function facilitiates running multiple cta-admin commands and grouping
    their output before displaying them.
    :param parser: The argparse ArgumentParser
    :param args: The parsed arguments
    :param remaining_args: Args after removal of special wrapper-only args
    :param config: CtaOpsConfig
    :return: The subprocess call result
    """
    base_cmd = tapeadmin.cta_admin_cmd

    # Special case for no-subcommand commands (version),
    # which don't support json output.
    if 'subcmd' in args and args.subcmd is None:
        args.subcmd = ''
        base_cmd = base_cmd.replace('--json', '')

    # Print the correct help text on each level
    if args.cmd is None:
        parser.print_help()
        return False
    if args.cmd is not None and args.subcmd is None:
        # Print subcommand help text, if the command has a defined subcommand
        if 'subcommands' in config.get(my_name, 'commands', args.cmd):
            for action in parser._actions:
                if isinstance(action, argparse._SubParsersAction):
                    for choice in action.choices.items():
                        if choice[0] == args.cmd:
                            choice[1].print_help()
            return False

    # Construct the final cta-admin command
    remaining_str = " ".join(f'"{arg}"' if " " in arg else arg for arg in remaining_args)
    real_cmd = f'{base_cmd} {args.cmd} {args.subcmd} {remaining_str}'
    logger.debug(f'Executing cta-admin command: {real_cmd}')

    try:
        # If the subcommand is 'ls' we have to capture the output instead of
        # printing it. For others we just don't want to show empty [] prints.
        if args.subcmd in ['ls', 'up', 'down']:
            cta_execution = cmd_utils.run_cmd(real_cmd, config=config, logger=logger)
        # Normal 'wrapper' mode, where we print cta-admin output as it arrives.
        else:
            cta_execution = cmd_utils.run_cmd_interactive(real_cmd, logger)

    except Exception as e:
        log_utils.log_and_exit(logger, f"An error occurred while executing {real_cmd}: {e}")

    return cta_execution


def execute_cta_admin_cmd(parser, args, remaining_args, config):
    """
    If no operations-side handler is assigned, attempt to delegate directly to
    cta-admin instead.
    'cta-admin X ls' output is captured and displayed as a table
    :param parser: The argparse ArgumentParser
    :param args: The parsed arguments
    :param remaining_args: Args after removal of special wrapper-only args
    :param config: CtaOpsConfig
    :return: True if command was executed successfully, otherwise False
    """

    # Set the good command line args for the cta-admin help
    if "-h" in remaining_args or "--help" in remaining_args:
        remaining_args = ["help"]
        args.subcmd = None

    cta_execution = run_cta_admin_cmd(parser, args, remaining_args, config)

    if args.subcmd == 'ls':
        if cta_execution.returncode == 0:
            raw_ls_json = cta_execution.stdout
            if args.cmd == 'drive':
                # In case of a drive ls, we need to determine if we are
                # on a tape server or not. To make the
                # We need to fill the PID field
                local_drives=[]
                drive_facts_path = Path(config.get('tape', 'drive_facts'))
                if cmd_utils.effectively_readable(drive_facts_path):
                    local_drives = tape_drive.get_local_drives(config, logger)
                raw_ls_json = _set_pid(raw_ls_json, local_drives)
                raw_ls_json = _resolve_device_path(raw_ls_json, local_drives)

            render_table(args.cmd, raw_ls_json, config)

    if cta_execution.returncode == 0:
        return True

    return False


def execute_wrapped_cmd_drive(parser, args, preparsed_args, config):
    """
    Wrapper for 'cta-admin drive' commands, to extend them with custom flags.
    Extends the command with the '--local' flag, to operate on all locally
    configured drives.
    """
    custom_parser = argparse.ArgumentParser(
        prog = f"{my_name} {args.cmd} {args.subcmd}",
        epilog = "========================" +
                f"Extra details help of cta-admin {args.cmd} {args.subcmd}" +
                "========================"
    )

    custom_parser.add_argument(
        "-l",
        "--local",
        action = 'store_true',
        help = "Perform the action on all locally configured drives."
    )

    # Managed help to have details of cta-admin help
    if '-h' in preparsed_args or '--help' in preparsed_args:
        custom_parser.print_help()
        execute_cta_admin_cmd(parser, args, preparsed_args, config)
        return

    custom_args, remaining_args = custom_parser.parse_known_args(preparsed_args)

    # Commands like 'cta-admin drive down' are very particular about the
    # placement of its args.
    # Here we assume that the --local flag will be given in the position
    # where we want to put the drive name(s)
    local_flag_index = -1
    if '--local' in preparsed_args:
        local_flag_index = preparsed_args.index('--local')
    elif '-l' in preparsed_args:
        local_flag_index = preparsed_args.index('-l')

    if custom_args.local:
        # Run the command once for each drive
        drives = tape_drive.get_local_drives(config, logger)
        if args.subcmd == 'ls':
            # If the command is 'ls', then the result should be aggregated before tabulation
            aggregate_stdout = []
            for drive in drives:
                preparsed_args.pop(local_flag_index)
                preparsed_args.insert(local_flag_index, drive['DriveName'])
                remaining_args_with_drive = preparsed_args
                call = run_cta_admin_cmd(parser, args, remaining_args_with_drive, config)
                if call.returncode == 0:
                    aggregate_stdout.extend(json.loads(call.stdout))
            
            # Before rendering the table we need to get the PIDs.
            aggregate_stdout= _resolve_device_path(json.dumps(aggregate_stdout), drives)
            aggregate_stdout= _set_pid(aggregate_stdout, drives)
            render_table(args.cmd, aggregate_stdout, config)
        else:
            for drive in drives:
                preparsed_args.pop(local_flag_index)
                preparsed_args.insert(local_flag_index, drive['DriveName'])
                remaining_args_with_drive = preparsed_args
                execute_cta_admin_cmd(parser, args, remaining_args_with_drive, config)
    else:
        execute_cta_admin_cmd(parser, args, remaining_args, config)


def create_base_parser():
    """
    Define the argument parser for the basic execution of the script.
    Mainly just needs to get the config file.
    :return: Argparse ArgumentParser
    """
    parser = argparse.ArgumentParser(
        formatter_class = RawTextHelpFormatter,
        add_help = False  # Needed for parser merge later on
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Print more details on operations."
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the cta-ops config file (file has to be in YAML format)"
    )
    # TODO: Json output

    return parser


def create_cmd_parser(config, parent_parsers=[]):
    """
    After having read the config file, construct a parser for the defined
    commands.
    :param config: CtaOpsConfig object
    :param parent_parsers: Parser to inherit arguments from
    :return: Argparse ArgumentParser
    """
    parser = argparse.ArgumentParser(
        parents = parent_parsers,
        description = "Combined execution tool for cta-admin and additional " \
        "operator commands"
    )
    subparsers = parser.add_subparsers(
        title = "commands",
        description = "first level commands of cta-ops-admin",
        help = "use -h after the cmd to have a detailled usage of the cmd",
        dest = 'cmd'  # Allows us to remember which command was invoced
    )                 # i.e. drive/tape/...

    commands_config = config.get(my_name, 'commands')

    # Iterate through the commands config yaml,
    # determine which subcommands are available/enabled
    for cmd, cmd_info in commands_config.items():
        new_subparser = subparsers.add_parser(
            f'{cmd}',
            aliases = [cmd_info['alias']],  # Only single alias supported for now
            help = cmd_info['help'] if 'help' in cmd_info else "",
            description= cmd_info['help'] if 'help' in cmd_info else ""
        )
        sub_subparser = new_subparser.add_subparsers(
            title = "subcommands",
            description = f"Subcommands of cta-ops-admin {cmd} command",
            help= "Use -h after the subcmd to have a detailled usage of the subcmd",
            dest = 'subcmd'  # Allows us to remember the used subcmd        
        )
        if 'subcommands' in cmd_info:
            for subcmd, subcmd_info in cmd_info['subcommands'].items():
                aliases = []
                help_str = ""
                if subcmd_info is not None and 'alias' in subcmd_info:
                    aliases = subcmd_info['alias']
                if subcmd_info is not None and 'help' in subcmd_info:
                    help_str = subcmd_info['help']
                new_subsubparser = sub_subparser.add_parser(
                    f'{subcmd}',
                    aliases = aliases,
                    help = help_str,
                    add_help = False
                    # TODO: Help formatter?
                )
                if subcmd_info is not None:
                    if 'handler' in subcmd_info:
                        new_subsubparser.set_defaults(func=subcmd_info['handler'])

    return parser


def _process_nested_headers(cta_admin_object:dict, header, config):
    """
    Recursively process the desired values from the CTA object dict.
    Apply special display rules to the resulting data.
    :param cta_admin_object: Single cta-admin output element dict
    :param header: Headers definition from the config file
    :param config: CtaOpsConfig
    :return: A column row as a list of strings
    """
    def format_timedelta(delta:timedelta):
        """
        Create a short, human-friendly time delta str approximation.
        :param delta: A time delta object
        :return: Approximate time delta string
        """
        if delta.days > 0:
            return f"{delta.days}d"
        elif delta.seconds > 60**2:
            hours = round(delta.seconds / (60**2))
            return f"{hours}h"
        elif delta.seconds > 60:
            minutes= round(delta.seconds / 60)
            return f"{minutes}m"
        else:
            return f"{delta.seconds}s"

    row = []
    for key, header_val in header.items():
        #if isinstance(header_val, dict):
        #    row += _process_nested_headers(cta_admin_object, header_val, config)
        if key in cta_admin_object:
            cta_val = cta_admin_object[key]
            if isinstance(cta_val, dict):
                row += _process_nested_headers(cta_val, header_val, config)
            else:
                # Special case: Empty string is translated to '-' for visibility
                if cta_val == '':
                    cta_val = config.get(my_name, 'empty_table_item_char')
                # Print time deltas with sensible unit
                elif any(delta_word == key for delta_word in TIMEDELTA_KEYWORDS):
                    since = timedelta(seconds=int(cta_val))
                    cta_val = format_timedelta(since)
                # Print timestamps as formatted time string
                elif any(time_word == key for time_word in TIME_KEYWORDS):
                    datetime_val = datetime.fromtimestamp(int(cta_val))
                    cta_val = datetime_val.strftime(
                        config.get(my_name, 'date_format', default='%Y-%m-%d %H:%M:%S')
                    )
                # Format byte quantities to sensible units
                elif any(byte_word == key for byte_word in BYTE_KEYWORDS):
                    cta_val = cmd_utils.convert_byte_unit(int(cta_val))
                # Make Mount Types easier on the eyes
                elif any(mount_word == key for mount_word in MOUNT_TYPE_KEYWORDS):
                    if cta_val == 'NO_MOUNT':
                        cta_val = config.get(my_name, 'empty_table_item_char', default = '-')
                elif any(status_word == key for status_word in STATUS_KEYWORDS):
                    if cta_val.endswith('RING'):
                        cta_val = cta_val[:-4]
                    elif cta_val.endswith('ING'):
                        cta_val = cta_val[:-3]
                row.append(cta_val)
        else:
            # Insert the correct number of empty columns for missing header leaf entries
            if isinstance(header_val, dict):
                row += _process_nested_headers(cta_admin_object, header_val, config)
            else:
                row.append(config.get(my_name, 'empty_table_item_char'))
    return row


def _get_pretty_headers(headers:dict):
    """
    Process a nested header dict, in order to return a list of the headers
    for the table to display.
    :param headers: Dict of json_key:pretty_text pairs
    """
    header_list = []
    for val in headers.values():
        if isinstance(val, dict):
            header_list += _get_pretty_headers(val)
        elif isinstance(val, str):
            header_list.append(val)
    return header_list


def _set_pid(raw_cta_admin_json, localDrives):
    """
    Add the PID field for every drive entry in the raw JSON output.
    And sets the PID for the local drives.
    :param raw_cta_admin_json: The json output from cta-admin, string or list of strings
    :param localDrives: List of local drives .
    :return: A modified version of raw_cta_admin_json
    """
    raw_cta_admin_json = json.loads(raw_cta_admin_json)
    for idx in range(len(raw_cta_admin_json)):
        pid_val = ""
        drive_entry = raw_cta_admin_json[idx]['driveName']
        if drive_entry in [drive["DriveName"] for drive in localDrives]:
            cmd_str = ("ps --no-headers -u cta -o pid,comm,args | " # Get cta processes
                       "grep -v -E 'master|rmcd|maint' | "          # Exclude master, rmcd and maintenance processes
                       f"grep {drive_entry} | "                     # Get the drive we are looking for (needed in multi-drive configuraitons)
                       "awk '{print $1}'")                          # We only want the PID
            pid_fetch_call = cmd_utils.run_cmd(cmd_str)
            if pid_fetch_call.returncode == 0:
                pid_val = pid_fetch_call.stdout.strip() # Remove new line set by run_cmd

        raw_cta_admin_json[idx]["pid"] = pid_val

    return json.dumps(raw_cta_admin_json)


def _resolve_device_path(raw_cta_admin_json, localDrives):
    raw_cta_admin_json = json.loads(raw_cta_admin_json)
    for drive in raw_cta_admin_json:
        if drive["driveName"] in [d["DriveName"] for d in localDrives]:
            drive["devFileName"] = Path(drive["devFileName"].strip()).resolve().as_posix() 

    return json.dumps(raw_cta_admin_json)


def render_table(command:str, raw_cta_admin_json, config):
    """
    Takes takes cta-admin raw json output and displays a table to the user.
    :param command: The executed cta-admin command (drive, tape, ...)
    :param raw_cta_admin_json: The json output from cta-admin, string or list of strings
    :param config: CtaOpsConfig object
    """
    # List headers to display
    pretty_headers = []
    header_dict = config.get(my_name, 'commands', command, 'table_headers')
    pretty_headers = _get_pretty_headers(header_dict)
    col_align = config.get(
        my_name, 'commands', command, 'header_alignment',
        default = []
    )
    # List rows to display
    rows = []
    if isinstance(raw_cta_admin_json, str):
        cta_admin_cmd_output = json.loads(raw_cta_admin_json)
        for cta_admin_object in cta_admin_cmd_output:
            rows.append(_process_nested_headers(cta_admin_object, header_dict, config))
    elif isinstance(raw_cta_admin_json, list):
        for call_output in raw_cta_admin_json:
            cta_admin_cmd_output = json.loads(call_output)
            for cta_admin_object in cta_admin_cmd_output:
                rows.append(_process_nested_headers(cta_admin_object, header_dict, config))

    table = table_utils.get_table(
        rows = rows,
        headers = pretty_headers,
        col_align = col_align,
        config = config
    )

    print(table)


def main():
    """
    Execute a cta-admin or tape operator command.
    Formats the output into a table for 'ls'-type commands.
    """
    base_parser = create_base_parser()
    args, remaining_args = base_parser.parse_known_args()

    # Set up config holder
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    # Create the full arg parser now that we have access to the config
    parser = create_cmd_parser(config, parent_parsers=[base_parser])
    args, remaining_args = parser.parse_known_args()

    # Support aliases, including help/usage
    commands_conf = config.get(my_name, 'commands')
    if args.cmd not in commands_conf:
        for defined_cmd, cmd_conf in commands_conf.items():
            if 'alias' in cmd_conf.keys():
                if cmd_conf['alias'] == args.cmd:
                    args.cmd = defined_cmd

    # Execute the chosen subcommand
    if hasattr(args, 'func'):
        if 'custom' in args.func:
            if 'drive' in args.func:
               execute_wrapped_cmd_drive(parser, args, remaining_args, config)
            # Extend with more custom flag wrappers if needed here:
        else:
            execute_tapeutils_script(args.func, remaining_args, config)
    else:
        execute_cta_admin_cmd(parser, args, remaining_args, config)

if __name__ == '__main__':
    main()
