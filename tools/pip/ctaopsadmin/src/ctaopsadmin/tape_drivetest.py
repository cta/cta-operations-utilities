#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
The drive-test script performs a health check for a drive, which may be used to
diagnose problems with it.
"""

import os
import re
import sys
import time
import atexit
import calendar
import argparse

from datetime import datetime
from pathlib import Path

import tapeadmin

from ctautils import cmd_utils, log_utils
from tapeadmin import tape_drive, tape_medium, tape_library, tape_pool
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME


shm_available_capacity_bytes = None

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None

device = None

utilities = (
    tapeadmin.mt,
    tapeadmin.cta_tape_label
)

hostname            = cmd_utils.run_cmd('hostname', logger=logger).stdout.rstrip()
server_name         = hostname.split('.')[0]

random_file_name = ''
checksum_random_file = None


def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = 'A script to test the drives',
    )

    required_named = parser.add_argument_group('required arguments')
    required_named.add_argument(
        '-v',
        '--vid',
        required = True,
        type = str,
        help = 'The volume ID'
    )

    parser.add_argument(
        '-c',
        '--checkfiles-path',
        type = str,
        help = 'The directory name where all the check files will be stored. '
                'It will be created as a subdirectory of /dev/shm'
    )
    parser.add_argument(
        '-D',
        '--drive',
        type = str,
        required = True,
        help = 'The drive to test.'
    )
    parser.add_argument(
        '-f',
        '--files-number',
        type = int, help = 'Number of files to write/read'
    )
    parser.add_argument(
        '-s',
        '--size-of-files',
        type = str,
        help = 'Size of the file to write/read. It can be specified without suffix '
                '(bytes) of with suffix (k|K, '
                'm|M, g|G) for kibibytes, mebibytes and gibibytes respectively'
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()


    # Set up config and logging
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    args.checkfiles_path = args.checkfiles_path or config.get(my_name, 'default_file_path')
    args.files_number = args.files_number or config.get(my_name, 'default_number_of_files')
    args.size_of_files = args.size_of_files or config.get(my_name, 'default_file_size')


    args.path = '/dev/shm' + args.checkfiles_path

    # Correct paths, adding a slash at the end if they don't have one already
    if not args.path.endswith('/'):
        args.path += '/'

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    # Check if file size has been specified with suffix for KiB, MiB or GiB
    regex_kb = re.compile('^\d+k$')
    regex_mb = re.compile('^\d+m$')
    regex_gb = re.compile('^\d+g$')
    try:
        if regex_kb.match(args.size_of_files.lower()):
            args.size_of_files = int(args.size_of_files.lower().replace('k', ''))
            args.size_of_files *= 1024
        elif regex_mb.match(args.size_of_files.lower()):
            args.size_of_files = int(args.size_of_files.lower().replace('m', ''))
            args.size_of_files *= 1024**2
        elif regex_gb.match(args.size_of_files.lower()):
            args.size_of_files = int(args.size_of_files.lower().replace('g', ''))
            args.size_of_files *= 1024**3
    except AttributeError:
        # File size was already specified as an integer
        args.size_of_files = int(args.size_of_files)

    # Check if number of files is a positive integer
    if args.files_number < 1:
        log_utils.log_and_exit(
            logger,
            "Number of files to write/read must be greater than 0"
        )

    # Check if file size is a positive integer greater or equal than 256K
    try:
        args.size_of_files = int(args.size_of_files)
    except ValueError:
        log_utils.log_and_exit(
            logger,
            f"Can't convert file size ({args.size_of_files}) to integer, " \
            "or wrong suffix for the size specified"
        )
    if not args.size_of_files >= 1024:
        log_utils.log_and_exit(
             logger,
            "File size must be greater or equal than 1024 (bytes)."
        )

    local_drives = tape_drive.get_local_drives(config, logger)
    if args.drive not in [drive['DriveName'] for drive in local_drives]:
        log_utils.log_and_exit(
            logger,
            f"Drive {args.drive} not found in set of locally configured drives."
        )

    return args


def check_or_create_file_path(opt_path):
    """
    Checks if the path given for the files exists. If it doesn't,
    tries to create it. If none given, it takes the default one.
    :param opt_path: Working directory path, string
    """
    # Try to create the file directory. If already exists, do nothing
    if cmd_utils.run_cmd(
            f'find {opt_path} >/dev/null 2>&1',
            logger = logger
        ).returncode != 0:
        cmd = f'mkdir -p {opt_path} >/dev/null 2>&1'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode != 0:
            log_utils.log_and_exit(
                logger,
                f"Directory {opt_path} could not be created " \
                f"{log_utils.format_stderr(cmd_call.stderr)}"
            )
        logger.info(f"File directory created: {opt_path}")

        cmd = 'chmod a+rwx ' + opt_path + ' >/dev/null 2>&1'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode != 0:
            log_utils.log_and_exit(
                logger,
                f"Could not change the rights of directory {opt_path} " \
                f"{log_utils.format_stderr(cmd_call.stderr)}"
            )
        logger.debug(f"Changed permissions (a+rwx) for file directory: {opt_path}")


def search_utility(script):
    """
    Looks for an utility that will be needed to run tape-drivetest
    :param script: The name of the utility script to search for
    :return: True if utility found; False otherwise
    """
    the_script = Path(script)
    if the_script.is_file():
        return True
    return False


def perform_initial_drive_and_utility_checks(drive:dict):
    """
    Performs the following checks in the following order:
        1) Status of the drive (must be FREE)
        2) Check if all the binary and tape utilities necessary for
           the script exist and are available to use
    :param: drive: Drive configuration dict, from cta-ops-drive-config-generate
    """
    # 1)
    drive_name = drive['DriveName']
    logger.info("Checking if drive status is free...")
    if not tape_drive.is_drive_free(drive):
        log_utils.log_and_exit(
            logger,
            f'Drive {drive_name} is not free (can be caused by server ' \
            'not idle, or cartridge stuck/not unloaded)'
        )
    logger.info("Is drive free: OK")

    # 2)
    logger.info("Checking availability of binaries and tape utilities...")
    for script in utilities:
        if not search_utility(script):
            log_utils.log_and_exit(
                logger,
                f"{script} does not exist or it is unavailable."
            )
    logger.info("Binaries and tape utilities available: OK")


def check_tape_queryvolume(vid):
    """
    Calls tape_queryvolume in module tapeadmin to check the location
    of the desired tape.
    :param vid: Tape VID string
    """
    tape_location = tape_medium.tape_queryvolume(vid=vid, logger=logger)

    if 'UNKNOWN' in tape_location:
        log_utils.log_and_exit(
            logger,
            f"Unknown location of volume {vid} [{tape_location}]"
        )

    elif 'DRIVE' in tape_location:
        log_utils.log_and_exit(
            logger,
            f"Tape {vid} is in drive [{tape_location}]"
        )

    elif 'HOME' in tape_location:
        logger.info("Tape is in home slot.")

    else:
        log_utils.log_and_exit(logger, "Undefined tape-queryvolume output.")


def rewind(vid, opt_path, skip_VOL1=False):
    """
    Rewinds the tape inside the current device.
    :param opt_path: Working directory path, string
    :return: True if operation succesful; False otherwise
    """
    cmd = f'{tapeadmin.mt} -f {device} rewind'
    cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            f"Rewind of tape {vid} inside device {device} failed. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )
    logger.info(f"Rewind of tape {vid} inside device {device}: OK")

    if skip_VOL1:
        logger.info("Skipping VOL1 for READ.")
        cmd = f'/usr/bin/dd if={device} of={opt_path}{vid}.before-read.VOL1 '\
            'bs=80 count=1 > /dev/null 2>&1 ; sync'
        if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
            log_utils.log_and_exit(logger, "Failed to skip VOL1.")


def checksum(file_name, path):
    """
    Calculates the checksum of a file
    :param file_name: The name of the file
    :param path: The path of the file
    :return: The checksum file in TXT format
    """
    logger.info(f"Calculating checksum of file {path}{file_name} .")
    cmd = f"{tapeadmin.adler32} {path}{file_name} 2>&1 | " \
        f"/bin/cut -f1 -d' ' > {path}{file_name}-checksum.txt"

    the_checksum = None
    full_path = f'{path}{file_name}-checksum.txt'
    if not cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
        with open(full_path, mode='r', encoding='utf-8') as cksum_file:
            for line in cksum_file:
                the_checksum = line
                break
            the_checksum = the_checksum.rstrip()
            # Check the format of the checksum is correct
            regex = re.compile('\w{8}')
            if not regex.match(the_checksum):
                logger.warning(
                    f"Incorrect checksum: {the_checksum} of file {file_name} " \
                    f"calculated with this command:\n {cmd}"
                )
                return 'fail'
            logger.info(f"Checksum of file {path}{file_name} is: {the_checksum}")
    else:
        logger.warning(
            f'Could not calculate checksum of file {path}{file_name} " \
            f"with this command:\n {cmd}'
        )
        return 'fail'

    return full_path


def perform_VOL1_check(vid, opt_path):
    """
    Checks if the VOL1 of the tape contains the desired volume ID,
    it is indeed the tape we want. Also skips VOL1.
    :param vid: Tape VID string
    :param opt_path: Working directory path, string
    """
    # Check VOL1 if we have an AUL tape
    # if tape_info['LBL'] == 'aul':
    input_file = device
    output_file = f'{opt_path}{vid}.before-write.VOL1'
    if not tape_medium.do_VOL1_check(input_file, output_file, vid, logger=logger):
        msg = f'Wrong VOL1 check for tape {vid}'
        log_utils.log_and_exit(logger, msg)


def create_random_file_on_shm(opt_path, opt_filesize):
    """
    Create a random file in the specified subdirectory inside /dev/shm
    :param opt_path: Working directory path, string
    """
    global random_file_name

    if os.path.isdir(opt_path):      # If subdirectory already exists, remove it
        if cmd_utils.run_cmd(
                f'/bin/rm -r {opt_path}',
                logger=logger
            ).returncode != 0:
            log_utils.log_and_exit(
                logger,
                f"Found already the directory {opt_path} but could not remove it."
            )

    # 1) First create the subdirectory
    cmd = f'/bin/mkdir {opt_path}'
    if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
        log_utils.log_and_exit(logger, f"{cmd} failed")
    else:
        logger.info(f"Directory {opt_path} created.")

    # 2) Create the random file in the directory
    record_count = int(opt_filesize / 1024)
    random_file_name = f'randomfile-{str(opt_filesize)}.bin'
    path_name = opt_path + random_file_name
    cmd = f'/usr/bin/dd if=/dev/urandom of={path_name} bs=1024 count={str(record_count)}'

    logger.info(f"Creating {str((record_count / 1024))} MB random file {path_name}")

    cmd_call = cmd_utils.run_cmd(f'{cmd} 2>&1', logger=logger)
    cmd_call_result = cmd_call.returncode

    if cmd_call_result != 0:
        log_utils.log_and_exit(
            logger,
            f"Could not create random file {path_name} . " \
            "The DD error has been the following: " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )
    else:
        logger.info(f"Random file {path_name} successfully created")

    # 3) Change random file permissions to 777
    cmd = f'chmod 777 {path_name}'
    cmd_call = cmd_utils.run_cmd(f'{cmd} 2>&1', logger=logger)
    cmd_call_result = cmd_call.returncode

    if cmd_call_result != 0:
        msg = f"Could not change permissions of random file {path_name} to " \
            f"777 {log_utils.format_stderr(cmd_call.stderr)}"
        log_utils.log_and_exit(logger, msg)


def check_if_tape_exists(vid:str):
    """
    Checks if the tape to use exists. Aborts otherwise
    :param vid: Tape VID string
    """
    tape_info_dict = tape_medium.get_tape_info(vid, logger)
    if not tape_info_dict:
        log_utils.log_and_exit(
            logger,
            f'No tape info available for tape {vid} in cta-admin.'
        )


def check_if_correct_tape_pool_for_drivetest(vid:str):
    """
    Determines if the tape belongs to any of the correct pools for tape-drivetest
    (Tape belongs to the 'test_*' or 'tolabel_*' or 'erase_*' tape pool)
    :param vid: Tape VID string
    """
    tape_info = tape_medium.get_tape_info(vid, logger)
    pool_name = tape_info['tapepool']
    valid_pools = config.get('tape', 'pool_prefixes_ok_for_labeling')
    pool_ok_for_label = tape_pool.is_pool_name_ok_to_label(
        pool_name = pool_name,
        config = config,
        logger = logger
    )
    if not pool_ok_for_label:
        log_utils.log_and_exit(
            logger,
            f"Incorrect pool of tape {vid}: {tape_info['tapepool']} " \
            f"(should start with one of {valid_pools})"
        )
    logger.info(f"Correct tape pool name [{tape_info['tapepool']}] - OK")


def check_total_size_to_write(vid:str, opt_numberfiles, opt_filesize):
    """
    Checks if the total data size to write is going to be higher than the
    tape capacity. If so, aborts.
    :param vid: Tape VID string
    """
    tape_info = tape_medium.get_tape_info(vid, logger)
    capacity = int(tape_info['capacity'])           # We just leave the capacity in bytes

    total_size_to_write = opt_numberfiles * opt_filesize
    if total_size_to_write > capacity:
        log_utils.log_and_exit(
            logger,
            f'Total data size to write on tape ({str(opt_filesize)} * {str(opt_numberfiles)} = ' \
            f'{str(total_size_to_write)} bytes) is higher than tape capacity ({str(capacity)} bytes)'
        )
    logger.info(
        f"Total size to write less than tape capacity ({str(opt_filesize)} * " \
        f"{str(opt_numberfiles)} = {str(total_size_to_write)} bytes < {str(capacity)} bytes) - OK"
    )


def do_write(vid:str, drive_name:str, opt_numberfiles, opt_path):
    """
    Performs the write operation in the specified tape
    :param vid: Tape VID string
    :param drive_name: Drive name string
    :param opt_path: Working directory path, string
    """
    for file_count in range(1, opt_numberfiles + 1):
        cmd = f'LANG=C /usr/bin/dd if={opt_path}{random_file_name} of={device} bs=256K 2>&1'
        logger.info(
            f'{cmd} ({str("%.2f" % round((file_count / opt_numberfiles) * 100, 2))}% done)'
        )

        dd_result = cmd_utils.run_cmd(cmd)
        dd_output = dd_result.stdout.rstrip()
        dd_return_code = dd_result.returncode

        logger.info(f'DD_OUTPUT:\n{dd_output}')

        if dd_return_code != 0:  # Command failed
            if dd_output and 'No space left on device' in dd_output:
                log_utils.log_and_exit(
                    logger,
                    "EOT has been reached. Unexpected script behaviour."
                )
            else:
                log_utils.log_and_exit(
                    logger,
                    f'Unexpected error when writing file {str(file_count)} ' \
                    f'with command:\n{cmd}\nDD_OUTPUT = {str(dd_output)}'
                )
                rewind(vid, opt_path)
                unmounted = tape_medium.do_unmount(vid, drive_name, config, logger)
                if not unmounted:
                    log_utils.log_and_exit(
                        logger,
                        f"Unmounting of tape {vid} failed, aborting."
                    )
                break
        else:
            if 'copied' in dd_output:
                logger.info(f'Written file {str(file_count)} .')
                if not file_count % 100:  # Print line every 100th file
                    logger.info(
                        f'Written file {str(file_count)} (' \
                        f'{str("%.2f" % round((file_count / opt_numberfiles) * 100,2))}% done)'
                    )
    # We don't need the random file anymore, so we erase it to free space in /dev/shm/
    os.unlink(f'{opt_path}{random_file_name}')


def do_read(vid, opt_path, opt_numberfiles):
    """
    Performs the read operation of the files that were written, comparing the checksum
    :param vid: Tape VID string
    :param opt_path: Working directory path, string
    """
    logger.info(f"Starting to READ {str(opt_numberfiles)} files from tape {vid} ...")
    for file_count in range(1, opt_numberfiles + 1):
        cmd = f'LANG=C /usr/bin/dd if={device} of={opt_path}{random_file_name}' \
              f'-{str(file_count)} bs=256K 2>&1'

        dd_result = cmd_utils.run_cmd(cmd, logger=logger)
        dd_output = dd_result.stdout.rstrip()
        dd_return_code = dd_result.returncode

        logger.info(f"DD_OUTPUT:\n{dd_output}")

        if dd_return_code != 0:  # Command failed
            log_utils.log_and_exit(
                logger,
                f'Unexpected error when reading file {str(file_count)} with ' \
                f'command:\n{cmd}\n{str(dd_output)}'
            )
            break
        else:
            if ' copied, ' in dd_output:
                logger.info(
                    f"Read file {str(file_count)} (" \
                    f'{str("%.2f" % round((file_count / opt_numberfiles) * 100, 2))}% done)'
                )

        # Compare the checksums
        checksum_read_file = checksum(random_file_name + '-' + str(file_count), opt_path)
        if checksum_read_file == 'fail':
            break
        logger.info(
            f"Compare checksum files - random file: {str(checksum_random_file)}" \
            f" - read file: {str(checksum_read_file)}"
        )
        cmd = f'/usr/bin/diff {checksum_random_file} {checksum_read_file} 2>&1'

        dd_result = cmd_utils.run_cmd(cmd, logger=logger)
        dd_output = dd_result.stdout.rstrip()
        dd_return_code = dd_result.returncode

        if dd_return_code != 0:  # Command failed
            log_utils.log_and_exit(
                logger,
                f"Unexpected error when comparing checksums with command:\n" \
                f"{cmd}\n{str(dd_output)}"
            )
            break
        else:
            logger.info("Checksums are equal.")

        # Remove the file we just got from tape
        os.unlink(opt_path + random_file_name + '-' + str(file_count))

    # Have we read all files back correctly?
    if file_count == opt_numberfiles:
        logger.info(
            f"Read back all {str(opt_numberfiles)} files from tape, all checksums match."
        )
    else:
        log_utils.log_and_exit(
            logger,
            f'Expected to read {str(opt_numberfiles)} ' \
            f'files from tape, but got {str(file_count)}'
        )


def do_tape_label(vid:str, drive_name:str):
    """
    Performs the re-labeling of the tape to check the correctness of the drive on
    doing that operation.
    :param vid: Tape VID string
    :param drive_name: Drive name string
    """
    logger.info(f"Starting to re-label tape {vid} using drive {drive_name}")

    tape_labeled, error_msg = tape_medium.label_tape(vid, drive_name, config, logger)
    if not tape_labeled:
        log_utils.log_and_exit(
            logger,
            f"Failed to label tape {vid}: {error_msg}"
        )
    logger.info("Finished tape label - OK")


def remove_working_directory(opt_path:str):
    """
    Calls remove_directory_recursively() to remove the working
    directory (the one under /dev/shm).
    :param opt_path: Working directory path, string
    """
    if cmd_utils.remove_directory_recursively(opt_path) != 0:
        logger.error(f"Could not remove random working directory {opt_path} .")
    else:
        logger.info(f"Removed random working directory {opt_path}")


def is_ok_check_total_size_to_write_on_shm(opt_filesize):
    """
    Checks if the total size to write under the /dev/shm/ directory
    is more than its free capacity.
    """
    global shm_available_capacity_bytes
    cmd = 'df -k /dev/shm'
    shm_available_capacity_bytes = int(
        cmd_utils.run_cmd(cmd, logger=logger).stdout.split()[10]
    ) * 1024

    if opt_filesize >= shm_available_capacity_bytes:
        logger.warning(
            "Available free space under /dev/shm " \
            f"({shm_available_capacity_bytes} bytes) is less than the " \
            f"specified file size ({opt_filesize} bytes); shrinking the file."
        )
        return False
    return True


def main():
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    cmd_utils.abort_if_root()

    logger.info(
        f"Log file can be found in: {log_utils.get_logger_file_path(logger)}"
    )
    drive = tape_drive.get_local_drive(args.drive, config, logger)

    drive_correctly_down = tape_drive.check_drive_down_with_reason(args.drive, logger)
    if not drive_correctly_down:
        date = datetime.now().strftime('%Y%m%d')
        username = cmd_utils.get_username()
        hint = f'cta-admin drive down {args.drive} --reason "{username} ({date}): ' \
            f'tape-drivetest with tape {args.vid}"'
        cmd_utils.display_hint(hint)

    logger.info(f"Using working file path: {args.path}")
    logger.info(f"Using number of files: {str(args.files_number)}")

    if is_ok_check_total_size_to_write_on_shm(args.size_of_files):
        logger.info(
            f"Using file size: {args.size_of_files} bytes"
        )
    else:
        args.size_of_files = int(shm_available_capacity_bytes - shm_available_capacity_bytes*0.1)
        logger.info(
            "Using file size (90% of the total capacity available in /dev/shm): " \
            f"{args.size_of_files} bytes"
        )

    check_if_tape_exists(args.vid)

    # Are tape and drive in the same lib?
    logger.info(
        f"Checking if tape {args.vid} and local drive {args.drive} are in the same lib ... "
    )
    same_lib, lib_tape, lib_drive = \
        tape_library.are_tape_and_drive_in_the_same_lib(
            args.vid,
            args.drive,
            logger
        )
    if same_lib:
        logger.info(
            f"Same library ({tape_drive.get_library(args.drive, config, logger)}): OK"
        )
    else:
        log_utils.log_and_exit(
            logger, f"Different libraries (drive={lib_drive} | tape={lib_tape})"
        )

    # Check file path and create a new one if it doesn't exist
    check_or_create_file_path(args.path)

    atexit.register(lambda: remove_working_directory(args.path))

    check_if_correct_tape_pool_for_drivetest(args.vid)

    check_total_size_to_write(args.vid, args.files_number, args.size_of_files)

    # Get the appropriate device for the given drive
    global device
    device = drive['DriveDevice']

    perform_initial_drive_and_utility_checks(drive)

    check_tape_queryvolume(args.vid)

    # Mount tape
    logger.info(f'Mounting tape {args.vid} .')
    mount_success = tape_medium.do_mount(args.vid, args.drive, config, logger)
    if not mount_success:
        log_utils.log_and_exit(
            logger,
            f"Mount of {args.vid} failed, aborting."
        )
    logger.info(f"Tape {args.vid} mounted inside drive {args.drive} .")

    # Rewind tape
    logger.info(f"Rewinding tape {args.vid} .")
    rewind(args.vid, args.path)

    perform_VOL1_check(args.vid, args.path)

    # Create the random file for the writing
    create_random_file_on_shm(args.path, args.size_of_files)

    # Calculate the checksum of the random file
    global checksum_random_file
    checksum_random_file = checksum(random_file_name, args.path)
    if checksum_random_file == 'fail':
        log_utils.log_and_exit(
            logger,
            f'Failed to calculate the checksum of {args.path}{random_file_name}'
        )

    # -------------------------------- Start the WRITE --------------------------------
    logger.info("Starting to WRITE. Can take several minutes...")
    time_start_seconds = calendar.timegm(time.gmtime())
    do_write(args.vid, args.drive, args.files_number, args.path)
    time_taken_seconds = calendar.timegm(time.gmtime()) - time_start_seconds
    time_taken_minutes = time_taken_seconds / 60
    logger.info(
        f"Time taken for WRITE operation: {str(time_taken_seconds)} seconds (" \
        f"{str('%.3f' % time_taken_minutes)} minutes)"
    )

    total_size_to_write = (args.files_number * args.size_of_files) / 1024**2
    write_speed = total_size_to_write / time_taken_seconds
    logger.info(
        f"Average speed of WRITE operation: {str('%.3f' % write_speed)} MB/s"
    )

    # -------------------------------- Finalize the WRITE --------------------------------
    rewind(args.vid, args.path, skip_VOL1=True)

    # -------------------------------- Start the READ --------------------------------
    time_start_seconds = calendar.timegm(time.gmtime())
    do_read(args.vid, args.path, args.files_number)
    time_taken_seconds = calendar.timegm(time.gmtime()) - time_start_seconds
    time_taken_minutes = time_taken_seconds / 60
    logger.info(
        f"Time taken for READ operation: {str(time_taken_seconds)} seconds (" \
        f"{str('%.3f' % time_taken_minutes)} minutes)"
    )

    try:
        read_speed = total_size_to_write / time_taken_seconds
        logger.info(
            f"Average speed of READ operation: {str('%.3f' % read_speed)} MB/s"
        )
    except ZeroDivisionError:
        logger.warning(
            "Average speed of READ operation: (too low to calculate)"
        )

    # -------------------------------- Try TAPE-LABEL --------------------------------
    unmounted = tape_medium.do_unmount(args.vid, args.drive, config, logger)
    if not unmounted:
        log_utils.log_and_exit(
            logger,
            f"Unmounting of tape {args.vid} failed, aborting."
        )
    do_tape_label(args.vid, args.drive)

    # -------------------------------- ALL FINISHED --------------------------------
    logger.info(f'Read write operation inside drive {args.drive} - OK')

    logger.info(f'Tape-label operation inside drive {args.drive} - OK')

    logger.info(f'All tape-drivetest operations of drive {args.drive} finished - OK')

if __name__ == '__main__':
    main()
