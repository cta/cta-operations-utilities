#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
The tape-mediacheck script performs a health check on tape media.
"""

import os
import re
import sys
import time
import json
import atexit
import tempfile
import argparse

from datetime import datetime
from argparse import RawTextHelpFormatter

import tapeadmin

from ctautils import cmd_utils, log_utils
from tapeadmin import tape_pool, tape_medium, tape_drive, tape_library
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME


# random_file_size = 5 * (1024 ** 3)  # Means 5 * 1024^3
random_file_name = None

# Variable for the command args

tempdir = None

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None


# Lists of tapes
to_check    = []
checked     = []
unchecked   = []

# Dict of messages for the summary
tape_mediacheck_summary = {}


def define_directories():
    """
    Defines and creates a random directory to work on
    """
    global tempdir
    tempdir = tempfile.mkdtemp(dir='/dev/shm', prefix=my_name + '-')
    msg = f'using random working directory {tempdir}'
    logger.info(msg)


def is_pool_valid_for_mediacheck(tape_pool):
    """
    Check if a pool is valid for tape-mediacheck, depending on its name
    TODO: Make config option
    :param tape_pool: The tape pool's name
    :return: True if valid; False otherwise
    """
    if 'erase_' in tape_pool or 'test_' in tape_pool:
        return True
    return False


def abort_if_wrong_srcpool(opt_srcp):
    if not tape_pool.pool_exists(opt_srcp):
        msg = f"Source pool [{opt_srcp}] does not exist"
        log_utils.log_and_exit(logger, msg)
    if not is_pool_valid_for_mediacheck(opt_srcp):
        msg = f"Source pool [{opt_srcp}] is not a valid pool (must start with 'erase_' or 'test_')"
        log_utils.log_and_exit(logger, msg)
    else:
        logger.info("Source pool checked: OK")


def assemble_tapes_to_be_checked(opt_vid, opt_file, opt_srcp):
    global to_check

    # Verify volume
    if opt_vid is not None:
        to_check.append(opt_vid)

    # Verify file
    if opt_file is not None:
        try:
            with open(opt_file, mode='r', encoding="utf-8") as the_file:
                for line in the_file:
                    if not line.startswith('#'):  # If it is not a comment
                        volume_id = line.rstrip()
                        to_check.append(volume_id)
            the_file.close()
        except FileNotFoundError:
            msg = f"File not found: {opt_file}"
            log_utils.log_and_exit(logger, msg)

    # Verify sourcepool
    if opt_srcp is not None:
        if not tape_pool.pool_exists(opt_srcp):
            msg = f"Source pool not found: {opt_srcp}"
            log_utils.log_and_exit(logger, msg)

        cmd = f'{tapeadmin.cta_admin_tape_json} ls --tapepool {opt_srcp}'
        cta_admin_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cta_admin_call.returncode != 0:
            msg = f"Execution of command [{cmd}] failed. {log_utils.format_stderr(cta_admin_call.stderr)}"
            logger.error(msg)
        for the_tape_info in json.loads(cta_admin_call.stdout):  # For tape_info (dict) in the list of tapes
            a_tape_vid = the_tape_info['vid']
            to_check.append(a_tape_vid)

    if not to_check:  # Is empty
        msg = "No tapes to perform media check found"
        log_utils.log_and_exit(logger, msg)


def load_args():
    """
    Load the options for all possible args
    """
    parser = argparse.ArgumentParser(
        description='A wrapper script to check if the media of a tape is usable',
        usage='\ntape-mediacheck -v <vid> -D <drive_name> | -f <filename> | -s <poolname>  [-l <limit>]'  # Short arguments example
              '\ntape-mediacheck --vid <vid> --drive <drive_name> | --file <filename> | --srcpool <poolname> '
              '[--limit <limit>] [--verbose] [--skip-simple-errors]'  # Long arguments example
              '[-C | --config-file <path>]' '[-h | --help]',
        formatter_class=RawTextHelpFormatter
    )

    # Required arguments
    required_named = parser.add_argument_group("Required arguments")
    required_named.add_argument(
        "-v",
        "--vid",
        type = str,
        help = "A volume ID to check the media.",
    )
    required_named.add_argument(
        '-D',
        '--drive',
        type = str,
        required = True,
        help = "Name of the drive to use for the mediacheck"
    )
    required_named.add_argument(
        "-f",
        "--file",
        type = str,
        help = "A file containing a list of volume IDs to be checked. "
            "The format of the file is one VID per line.",
    )
    required_named.add_argument(
        "-s",
        "--srcpool",
        type = str,
        help = "The source pool from which tapes "
            "are taken for media checking.",
    )
    parser.add_argument(
        "-l",
        "--limit",
        type = int,
        help = "The maximum number of tapes to perform the media check."
    )
    parser.add_argument(
        "--skip-simple-errors",
        action = 'store_true',
        help="Do not abort the script if a non-critical failure occurs in "
            "checking one of the tapes. Instead, continue and check all the"
            "specified tapes remaining"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "print more details on operations."
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )

    args = parser.parse_args()

    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    global random_file_size
    random_file_size = config.get(my_name, 'random_file_size')

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    if not any((args.vid, args.file, args.srcpool)):
        parser.print_usage()
        sys.exit(0)

    if args.limit and args.limit < 1:
        msg = f"Wrong limit value ({str(args.limit)}). Must be higher than 0"
        log_utils.log_and_exit(logger, msg)

    return args


def checksum(drive, device, file_name, path, volume_id, opt_skip_simple_errors):
    """
    Calculates the checksum of a file.
    :param drive: The drive name
    :param device: the device name
    :param file_name: The name of the file
    :param path: The path of the file
    :param volume_id: The ID of the current volume to check
    :return: The checksum file in TXT format
    """
    global tape_mediacheck_summary
    msg = f"Calculating checksum of file {path}/{file_name}"
    logger.info(msg)
    cmd = f"{tapeadmin.adler32} {path}/{file_name} 2>&1 | " \
        f"/bin/cut -f1 -d' ' > {path}/{file_name}-checksum.txt"

    the_checksum_result = None
    if not cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
        for line in open(f'{path}/{file_name}-checksum.txt', mode='r'):
            the_checksum_result = line
            break
        the_checksum = the_checksum_result.rstrip()
        # Check the format of the checksum is correct
        regex = re.compile('\w{8}')
        if not regex.match(the_checksum):
            msg = f"Incorrect checksum: {the_checksum} " \
                    f"of file {file_name} calculated with this command:\n{cmd}"
            if opt_skip_simple_errors:
                logger.error(msg)
            else:
                logger.critical(msg)
            tape_mediacheck_summary[volume_id] = f"Incorrect checksum: {the_checksum}" \
                                f" of file {file_name} calculated with this command:\n{cmd}"
            if not opt_skip_simple_errors:
                msg = f"Incorrect checksum: {the_checksum} " \
                      f"of file {file_name} calculated with this command:\n{cmd}"
                log_utils.log_and_exit(logger, msg)
            rewind(device)
            unmounted = tape_medium.do_unmount(volume_id, drive, config, logger)
            if not unmounted:
                msg = f"Unmounting of tape {volume_id} failed"
                tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                log_utils.log_and_exit(logger, msg)
            return "fail"

        msg = f"Checksum of file {path}/{file_name} is: {the_checksum_result}"
        logger.info(msg)

    else:
        msg = f"Could not calculate checksum of file " \
              f"{path}/{file_name} with this command:\n{cmd}"
        if opt_skip_simple_errors:
            logger.error(msg)
        else:
            logger.critical(msg)
        tape_mediacheck_summary[volume_id] = f"Could not calculate checksum of file {path}/" \
                            f"{file_name} with this command:\n{cmd}"
        if not opt_skip_simple_errors:
            log_utils.log_and_exit(
                logger,
                f"Could not calculate checksum of file {path}/" \
                f"{file_name} with this command:\n{cmd}"
            )
        rewind(device)
        unmounted = tape_medium.do_unmount(volume_id, drive, config, logger)
        if not unmounted:
            msg = f"Unmounting of tape {volume_id} failed"
            tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
            log_utils.log_and_exit(logger, msg)
        return "fail"

    return f"{path}/{file_name}-checksum.txt"


def create_random_file_on_shm():
    """
    Create a random file inside a subdirectory of the /dev/shm directory (RAM storage)
    """
    global random_file_name

    record_count = int(random_file_size / 1024)
    random_file_name = f"randomfile-{str(random_file_size)}.bin"
    path_name = f"{tempdir}/{random_file_name}"
    cmd = f'/usr/bin/dd if=/dev/urandom of={path_name} bs=1024 count={str(record_count)}'

    msg = f"Creating {str((record_count / 1024))} MB random file {path_name}"
    logger.info(msg)

    if cmd_utils.run_cmd(f'{cmd} > /dev/null 2>&1', logger=logger).returncode != 0:
        msg = f"Could not create random file {path_name}"
        log_utils.log_and_exit(logger, msg)

    msg = f"Random file {path_name} created successfully"
    logger.info(msg)


def rewind(drive:dict):
    """
    Rewinds the tape inside the device and logs the operation
    :param drive: The tape drive dict, as given by cta-ops-drive-config-generate
    """
    global tape_mediacheck_summary
    device = drive['DriveDevice']
    cmd = f'{tapeadmin.mt} -f {device} rewind'
    if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
        volume_id = tape_medium.get_tape_vid_inside_drive(drive)
        msg = f"Rewind of tape {volume_id} inside device {device} failed"
        tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
        log_utils.log_and_exit(logger, msg)


def show_tape_mediacheck_summary():
    """
    Logs the summary of the tape mediacheck for all the tapes checked
    """
    if len(tape_mediacheck_summary) > 0:
        message = '\n' + '-' * 50 + ' TAPE MEDIACHECK SUMMARY ' + '-' * 50
        logger.info(message)
        for num, key in enumerate(tape_mediacheck_summary):
            message = f'{str(num + 1)} [{key}] - {tape_mediacheck_summary[key]}'
            logger.info(message)


def remove_working_directory():
    """
    Calls remove_directory_recursively() to remove the working
    directory (the one under /dev/shm)
    """
    if cmd_utils.remove_directory_recursively(tempdir) != 0:
        logger.error(f"Could not remove random working directory {tempdir}")
    else:
        logger.info(f"Removed random working directory {tempdir}")


def main():
    global tape_mediacheck_summary

    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    cmd_utils.abort_if_root()

    define_directories()

    logger.info(
        f"Log file can be found in: {log_utils.get_logger_file_path(logger)}"
    )

    drive = tape_drive.get_local_drive(args.drive, config, logger)
    drive_name = drive['DriveName']

    # At the end of the script, print and log summary
    atexit.register(show_tape_mediacheck_summary)
    atexit.register(remove_working_directory)

    # Initial checks
    drive_correctly_down = tape_drive.check_drive_down_with_reason(drive_name, logger)
    if not drive_correctly_down:
        date = datetime.now().strftime('%Y%m%d')
        username = cmd_utils.get_username()
        hint = f'{tapeadmin.cta_admin_cmd} drive down {drive_name} ' \
            f'--reason "{username} ({date}): ' \
            f'tape-mediacheck with tape {args.vid}"'
        cmd_utils.display_hint(hint)

    if args.srcpool:
        abort_if_wrong_srcpool(args.srcpool)

    assemble_tapes_to_be_checked(args.vid, args.file, args.srcpool)

    device = drive['DriveDevice']

    tape_count = 0
    for volume_id in to_check:
        tape_count += 1
        if args.limit is not None and tape_count > args.limit:
            break

        logger.info('#' * 50 + '( TAPE ' + str(tape_count) + ' out of ' + str(len(to_check)) + ' )' + '#' * 50)
        logger.info(f"Processing tape {volume_id}")

        # Check if tape exists
        tape_info_dict = tape_medium.get_tape_info(volume_id, logger)
        if not tape_info_dict:
            msg = f"No tape info available for {volume_id}: SKIPPING"
            logger.error(msg)
            tape_mediacheck_summary[volume_id] = f"ERROR: {msg}"
            continue

        all_operations_ok_until_now = True

        # Pool name OK?
        pool_name = tape_info_dict['tapepool']
        if not is_pool_valid_for_mediacheck(pool_name):
            msg = f"Pool of {volume_id} ({pool_name})" \
                  " is not a valid pool (must start with 'erase_' or 'test_')"
            tape_mediacheck_summary[volume_id] = msg
            if not args.skip_simple_errors:
                log_utils.log_and_exit(logger, msg)
            logger.warning(msg)
            rewind(drive)
            unmounted = tape_medium.do_unmount(volume_id, drive_name, config, logger)
            if not unmounted:
                msg = f"Unmounting of tape {volume_id} failed"
                tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                log_utils.log_and_exit(logger, msg)
            continue
        else:
            logger.info("Source pool checked: OK")

        # Are tape and drive in the same lib?
        msg = f"Checking if tape {volume_id} and local drive are in the same lib ..."
        logger.info(msg)
        same_lib, lib_tape, lib_drive = tape_library.are_tape_and_drive_in_the_same_lib(
            volume_id,
            drive_name,
            logger
        )
        if same_lib:
            logger.info(f"Same library ({lib_tape}): OK")
        else:
            msg = f'Different libraries (drive={lib_drive} | tape={lib_tape}): SKIPPING'
            logger.error(msg)
            tape_mediacheck_summary[volume_id] = f'ERROR: {msg}'
            continue

        # Is tape empty?
        logger.info(f"Checking if tape [{volume_id}] is empty...")
        if not tape_medium.is_tape_empty(volume_id):
            msg = f'Tape [{volume_id}] is not empty: SKIPPING'
            logger.error(msg)
            tape_mediacheck_summary[volume_id] = f'ERROR: {msg}'
            continue
        else:
            logger.info(f"Tape [{volume_id}] is empty: OK")

        # Get the cartridge capacity (capacity in cta-admin JSON is in bytes)
        capacity_bytes = int(tape_info_dict['capacity'])
        # Convert the cartridge capacity into GB
        capacity = int(capacity_bytes / 1e9)

        # Add 10% on the cartridge to be sure that we hit end of tape
        files_to_fill_cartridge = int((capacity * 1.1) / (random_file_size / (1024 ** 3)))
        logger.info(
            f"Cartridge {volume_id} has capacity of {str(capacity)} GB " \
            f"and it will be filled with {str(files_to_fill_cartridge)} files" \
            f"(each with size of {str(random_file_size)} bytes)"
        )

        # Is tape at home?
        msg = f"Checking if tape {volume_id} is in home slot..."
        logger.info(msg)
        tape_location = tape_medium.tape_queryvolume(vid=volume_id, logger=logger)
        if tape_location != 'HOME':
            msg = f"Tape {volume_id} is not in home slot. " \
                    f"Its location is {tape_location}: SKIPPING"
            logger.error(msg)
            tape_mediacheck_summary[volume_id] = msg
            continue

        else:
            logger.info(f"Tape {volume_id} is in home slot: OK")

        # Mount the tape
        mount_success = tape_medium.do_mount(volume_id, drive_name, config, logger)
        if not mount_success:
            msg = f"Mounting of tape {volume_id} failed, aborting."
            tape_mediacheck_summary[volume_id] = msg
            logger.error(msg)
            continue

        # Is tape write protected?
        if tape_medium.is_tape_write_protected(device, logger):
            logger.error(f"{volume_id} is write protected")
            all_operations_ok_until_now = False
        else:
            logger.info(f"{volume_id} is not write protected: OK")

        # Rewind the tape
        if all_operations_ok_until_now:
            cmd = f'{tapeadmin.mt} -f {device} rewind'
            if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
                logger.error("Rewind failed")
                all_operations_ok_until_now = False
            else:
                logger.info(f"Rewind of {volume_id}: OK")

        # Check VOL1 if we have an AUL tape
        if all_operations_ok_until_now:
            input_file = device
            output_file = f'{tempdir}/{volume_id}.before-write.VOL1'
            if not tape_medium.do_VOL1_check(
                    input_file,
                    output_file,
                    volume_id,
                    logger):
                all_operations_ok_until_now = False
                tape_mediacheck_summary[volume_id] = "VOL1 read failed"

        # Do the actual media check
        if all_operations_ok_until_now:
            # Create random file
            if random_file_name is None:
                create_random_file_on_shm()
            checksum_random_file = checksum(drive_name, device, random_file_name, tempdir, volume_id, args.skip_simple_errors)
            if checksum_random_file == 'fail':
                continue

            # Set the block size (to get variable block length support)
            cmd = f'{tapeadmin.mt} -f {device} setblk 0'
            cmd_utils.run_cmd(cmd, logger=logger)

            # Switch compression off
            cmd = f'{tapeadmin.mt} -f {device} compression 0'
            cmd_utils.run_cmd(cmd, logger=logger)

            # WRITE
            logger.info("Starting to WRITE on tape...")
            dd_output = None
            files_to_read_back = 0
            for file_count in range(1, files_to_fill_cartridge + 1):
                cmd = f'LANG=C /usr/bin/dd if={tempdir}/{random_file_name} of={device} bs=256K 2>&1'
                msg = f'{cmd}' \
                        f' ({str("%.2f" % round((file_count / files_to_fill_cartridge) * 100, 2))} % done)'
                logger.info(msg)

                dd_result = cmd_utils.run_cmd(cmd, logger=logger)
                dd_output = dd_result.stdout.rstrip()
                dd_return_code = dd_result.returncode

                logger.info(f"DD_OUTPUT:\n{dd_output}")

                if dd_return_code != 0:  # Command failed
                    if dd_output and 'No space left on device' in dd_output:
                        msg = f"WRITE finished - EOT hit - written {str(file_count)} files"
                        logger.info(msg)
                        break
                    else:
                        msg = f"Unexpected error when writing file " \
                                 f"{str(file_count)} with command:\n" \
                                 f"{cmd}\nDD_OUTPUT = {str(dd_output)}"

                        tape_mediacheck_summary[volume_id] = msg
                        if not args.skip_simple_errors:
                            log_utils.log_and_exit(logger, msg)
                        logger.error(msg)
                        rewind(drive)
                        unmounted = tape_medium.do_unmount(volume_id, drive_name, config, logger)
                        if not unmounted:
                            msg = f"Unmounting of tape {volume_id} failed"
                            tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                            log_utils.log_and_exit(logger, msg)
                        break
                else:
                    if 'copied' in dd_output:
                        logger.info(f"Written file {str(file_count)}")

                        if not file_count % 100 and not args.verbose:  # Print line every 100th file
                            logger.info(
                                f"Written file {str(file_count)}  (" \
                                f"{str('%.2f' % round((file_count / files_to_fill_cartridge) * 100, 2))}% done)"
                            )

            # We don't need the random file anymore, so we erase it to free space in the tempdir
            os.unlink(f'{tempdir}/{random_file_name}')

            if volume_id not in tape_mediacheck_summary:    # If tape is in the summary it is because of an error
                files_to_read_back = file_count - 1     # We skip the last one because it will be wrong

                # Rewind
                cmd = f'sync ; {tapeadmin.mt} -f {device} rewind ; sync'
                logger.info(cmd)

                if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
                    msg = f"Rewind of tape {volume_id} inside device {device} failed"
                    tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                    log_utils.log_and_exit(logger, msg)
                time.sleep(10)

                msg = "Skipping VOL1 for READ"
                logger.info(msg)

                cmd_utils.run_cmd(
                    f'/usr/bin/dd if={device} of={tempdir}/{volume_id}' \
                    f'.before-read.VOL1 bs=80 count=1 > /dev/null 2>&1 ; sync',
                    logger = logger
                )
                time.sleep(10)

                # READ
                logger.info(
                    f"Starting to READ {str(files_to_read_back)} files from tape {volume_id} ..."
                )

                for file_count in range(1, files_to_read_back + 1):     # +1 because of the range() function
                    cmd = f'LANG=C /usr/bin/dd if={device} of={tempdir}/{random_file_name}' \
                          f'-{str(file_count)} bs=256K 2>&1'

                    dd_result = cmd_utils.run_cmd(cmd, logger=logger)
                    dd_output = dd_result.stdout.rstrip()
                    dd_return_code = dd_result.returncode

                    logger.info(f"DD_OUTPUT:\n{dd_output}")

                    if dd_return_code != 0:  # Command failed
                        msg = f"Unexpected error when reading file " \
                                f"{str(file_count)} with command:\n{cmd}\n{str(dd_output)}"
                        tape_mediacheck_summary[volume_id] = msg
                        if not args.skip_simple_errors:
                            log_utils.log_and_exit(logger, msg)
                        logger.error(msg)
                        rewind(drive)
                        unmounted = tape_medium.do_unmount(volume_id, drive_name, config, logger)
                        if not unmounted:
                            msg = f"Unmounting of tape {volume_id} failed"
                            tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                            log_utils.log_and_exit(logger, msg)
                        break
                    else:
                        if ' copied, ' in dd_output:
                            logger.info(
                                f"Read file {str(file_count)} (" \
                                f"{str('%.2f' % round((file_count / files_to_read_back) * 100, 2))}% done)"
                            )

                            if not file_count % 100 and not args.verbose:
                                logger.info(f"Read file {str(file_count)}")

                    # Compare the checksums
                    checksum_read_file = checksum(drive_name, device, random_file_name + '-' + str(file_count), tempdir, volume_id, args.skip_simple_errors)
                    if checksum_read_file == 'fail':
                        break
                    logger.info(
                        f"Compare checksum files - random file: " \
                        f"{str(checksum_random_file)} - read file: {str(checksum_read_file)}"
                    )

                    cmd = f'/usr/bin/diff {checksum_random_file} {checksum_read_file} 2>&1'

                    dd_result = cmd_utils.run_cmd(cmd, logger=logger)
                    dd_output = dd_result.stdout.rstrip()
                    dd_return_code = dd_result.returncode

                    if dd_return_code != 0:  # Command failed
                        msg = f"Unexpected error when comparing checksums with command:\n" \
                                f"{cmd}\n{str(dd_output)}"

                        tape_mediacheck_summary[volume_id] = msg
                        if not args.skip_simple_errors:
                            log_utils.log_and_exit(logger, msg)
                        logger.error(msg)
                        rewind(drive)
                        unmounted = tape_medium.do_unmount(volume_id, drive_name, config, logger)
                        if not unmounted:
                            msg = f'Unmounting of tape {volume_id} failed'
                            tape_mediacheck_summary[volume_id] = f'ABORT: {msg}'
                            log_utils.log_and_exit(logger, msg)
                        break
                    else:
                        logger.info("Checksums are equal")

                    # Remove the file we just got from tape
                    os.unlink(f'{tempdir}/{random_file_name}-{str(file_count)}')

                if volume_id not in tape_mediacheck_summary:    # If tape is in the summary it is because of an error
                    # Have we read all files back correctly?
                    if file_count == files_to_read_back:
                        logger.info(
                            f"Read back all {str(files_to_read_back)} files from tape, " \
                            "all checksums match."
                        )
                        logger.info(
                            f"Media on tape {volume_id} checked, " \
                            "EVERYTHING WENT FINE - MEDIA LOOKS GOOD."
                        )

                    else:
                        msg = f"Expected to read {str(files_to_read_back)}" \
                              f" files from tape, but got {str(file_count)}"

                        tape_mediacheck_summary[volume_id] = msg
                        if not args.skip_simple_errors:
                            log_utils.log_and_exit(
                                logger,
                                f"{msg}. Something went wrong - aborting."
                            )
                        logger.error(msg)
                        rewind(drive)
                        unmounted = tape_medium.do_unmount(volume_id, drive_name, config, logger)
                        if not unmounted:
                            msg = f"Unmounting of tape {volume_id} failed"
                            tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                            log_utils.log_and_exit(logger, msg)
                        break

            if volume_id not in tape_mediacheck_summary:  # If tape is in the summary it is because of an error
                # Rewind
                rewind(drive)

        if volume_id not in tape_mediacheck_summary:
            # Unmount the tape cartridge (needs fixing if there multiple drives)
            unmounted = tape_medium.do_unmount(volume_id, drive_name, config, logger)
            if not unmounted:
                msg = f"Unmounting of tape {volume_id} failed"
                tape_mediacheck_summary[volume_id] = f"ABORT: {msg}"
                log_utils.log_and_exit(logger, msg)
            # Add to tape_mediacheck_summary that the media check of the current tape has been OK
            tape_mediacheck_summary[volume_id] = "SUCCESS"

if __name__ == '__main__':
    main()
