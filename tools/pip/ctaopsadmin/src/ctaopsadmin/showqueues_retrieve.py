#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN


"""
Handler for cta-ops-admin to call showqueues retrieve subcommand.
"""

import sys
import argparse

from argparse import RawTextHelpFormatter
from ctautils import tape_showqueues, log_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME

my_name = sys.argv[0].split('/')[-1]    # Get rid of "/usr/local/bin" and take only the final part

config, logger = None, None

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "A script that summarizes queued retrieve requests",
        formatter_class = RawTextHelpFormatter
    )

    # Required arguments
    parser.add_argument(
        "-g",
        "--group-by",
        type = str,
        default = "tapepool",
        help = "The requests grouped come from this item",
        choices = ["library", "tapepool", "vo"],
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )
    settings_group.add_argument(
        "-j",
        "--json",
        action = 'store_true',
        help = "Write output as JSON (default disabled)"
    )

    # Assign the variables to the options
    args = parser.parse_args()

    # Set up config holder and logger
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)


    return args

def main():
    args = load_args()

    logger.debug(f"Executing {my_name}")

    mount_type = "RETRIEVE"
    tape_showqueues.do_showqueues(args, mount_type, config, logger)
