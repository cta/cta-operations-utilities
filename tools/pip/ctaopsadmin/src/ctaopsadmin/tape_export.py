#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

import re
import sys
import time
import json
import atexit
import argparse

import tapeadmin

from ctautils import log_utils, cmd_utils
from tapeadmin import tape_pool, tape_medium, tape_drive, tape_library
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME


config, logger = None, None

my_name = sys.argv[0].split('/')[-1]    # Get rid of "/usr/local/bin" and take only the final part

lib = None

number_of_export_attempts = None
sleep_time_between_export_attempts = None  # In seconds

def load_args():
    """
    Loads the options for all possible script arguments
    """
    global config, logger

    parser = argparse.ArgumentParser(
        description = "A wrapper script to export tapes into the I/O station " \
            "of a library",
    )

    required_named = parser.add_argument_group('required arguments')
    required_named.add_argument(
        "-v",
        "--vid",
        type = str,
        help = "The volume ID to export"
    )
    required_named.add_argument(
        "-D",
        "--drive",
        type = str,
        required = True,
        help = "The drive to mount the tape on"
    )
    required_named.add_argument(
        "-f",
        "--file",
        type = str,
        help = "A file containing a list of volume IDs to be exported. "
            "The format of the file is one VID per line")
    required_named.add_argument(
        "-s",
        "--srcpool",
        type = str,
        help = "The source pool from which tapes are taken "
            "for export."
    )

    parser.add_argument(
        "-c",
        "--ioport-capacity",
        type = int,
        help = "The number of tapes that can be exported in one go. "
            "If this option is given, the cap (option -c) must "
            "be specified as well."
    )
    parser.add_argument(
        "-l",
        "--limit",
        type = int,
        help = "The maximum number of tapes to export"
    )
    parser.add_argument(
        "--skip-simple-errors",
        action = 'store_true',
        help = "Do not abort the script if a non-critical failure occurs in exporting one of the "
            "tapes. Instead, continue and export all the specified tapes remaining."
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    # Set up config
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    global number_of_export_attempts, sleep_time_between_export_attempts
    number_of_export_attempts = config.get(my_name, 'number_of_export_attempts')
    sleep_time_between_export_attempts = config.get(
        my_name,
        'sleep_time_between_export_attempts'
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    if not args.vid and not args.file and not args.srcpool:
        parser.print_help()
        sys.exit(0)

    if args.ioport_capacity is not None:
        if args.ioport_capacity < 1:
            msg = 'ioport-capacity must be greater or equal than 1'
            log_utils.log_and_exit(logger, msg)

    if args.limit is not None and args.limit < 1:
        msg = 'limit must be greater or equal than 1'
        log_utils.log_and_exit(logger, msg)
    return args

class TapeExport:
    def __init__(self, args):
        self.vid = args.vid
        self.drive_name = args.drive
        self.file = args.file
        self.srcpool = args.srcpool
        self.ioport_capacity = args.ioport_capacity
        self.limit = args.limit
        self.verbose = args.verbose
        self.skip_simple_errors = args.skip_simple_errors
        self.config_file_path = args.config_file

        self.tapes_to_export = []
        self.tapes_exported = []
        self.tapes_not_exported = {}

    def gather_tapes_for_exporting(self):
        """
        Gathers the list of tapes to export based on the options specified in
        the script (single tape, file or source pool).
        """

        # Add single tape if specified
        if self.vid:
            self.tapes_to_export.append(self.vid)

        # Check if file exists and gather its tapes
        if self.file:
            try:
                with open(self.file, mode='r', encoding='utf-8') as the_file:
                    for line in the_file:
                        if not line.startswith('#'):  # If it is not a comment
                            volume_id = line.rstrip()
                            self.tapes_to_export.append(volume_id)
                the_file.close()
            except FileNotFoundError:
                msg = f"File not found: {self.file}"
                log_utils.log_and_exit(logger, msg)

        # Check if source pool exists and gather its tapes
        if self.srcpool:
            if not tape_pool.pool_exists(self.srcpool):
                msg = f"Source pool not found: {self.srcpool}"
                log_utils.log_and_exit(logger, msg)
            cmd = f"{tapeadmin.cta_admin_tape_json} ls --tapepool {self.srcpool}"
            cta_admin_call = cmd_utils.run_cmd(cmd, logger=logger)
            if cta_admin_call.returncode != 0:
                msg = f"Execution of command [{cmd}] failed with result: " \
                    f"{log_utils.format_stderr(cta_admin_call.stderr)}"
                logger.error(msg)
            # For tape_info (dict) in the list of tapes
            for tape_info in json.loads(cta_admin_call.stdout):
                a_tape_vid = tape_info['vid']
                self.tapes_to_export.append(a_tape_vid)

        if not self.tapes_to_export:
            msg = 'No tapes to export found'
            log_utils.log_and_exit(logger, msg)


    def check_tape_exists_in_cta_admin(self, the_tape):
        """
        Checks if there is any info about the tape in cta-admin, and aborts
        if --skip-simple-errors is not specified.
        :param the_tape: The tape to check
        """
        if not tape_medium.get_tape_info(the_tape, logger):
            msg = f"No info found for tape {the_tape} in cta-admin"
            if not self.skip_simple_errors:
                log_utils.log_and_exit(logger, msg)
            logger.error(msg)
            return False
        return True


    def determine_lib(self):
        """
        Determines the type of lib that is being used (e.g. IBM).
        Does it by checking the lib of the first tape in the list.
        """
        global lib
        tape_info = tape_medium.get_tape_info(self.tapes_to_export[0], logger)
        if not tape_info:
            msg = f"No info found for tape {self.tapes_to_export[0]} in cta-admin"
            log_utils.log_and_exit(logger, msg)
        lib = tape_info['logicalLibrary']
        logger.info(f"Tape library used: {lib}")


    def abort_or_skip(self, volume_id, message):
        """
        Aborts the script, or skips the current tape if --abort-simple-errors is specified
        :param volume_id: The tape that caused the abort or skip
        :param message: The cause for the abort or skip of the tape
        """
        self.tapes_to_export.remove(volume_id)
        self.tapes_not_exported[volume_id] = message

        if not self.skip_simple_errors:
            log_utils.log_and_exit(logger, f"Tape {volume_id}: {message}")
        logger.error(f"Tape {volume_id}: {message}")


    def determine_ioport_capacity(self):
        """
        Assign the default I/O port capacity for the lib if the
        --ioport-capacity option is not specified; otherwise, assign the
        specified capacity and show a warning if it's higher than the default one.
        """

        # Ask the library how many I/O slots are available in total (physical + virtual)
        smc_call = cmd_utils.run_cmd(
            tapeadmin.cta_smc_ioslot_query_json,
            number_of_attempts = 3,
            logger = logger
        )
        if smc_call.returncode != 0:
            msg = f"Command failed [{tapeadmin.cta_smc_ioslot_query_json}]. " \
                f"{log_utils.format_stderr(smc_call.stderr)}"
            log_utils.log_and_exit(logger, msg)

        max_ioport_capacity = json.loads(smc_call.stdout)[0]['port']['count']
        logger.info(
            f"Maximum I/O port capacity including virtual slots is: {max_ioport_capacity}"
        )

        if self.ioport_capacity:
            if self.ioport_capacity > max_ioport_capacity:
                msg = f"The specified I/O port capacity ({self.ioport_capacity}) " \
                    f"is higher than the maximum one ({max_ioport_capacity})"
                log_utils.log_and_exit(logger, msg)
            logger.info(f"Using specified I/O port capacity: {self.ioport_capacity}")
        else:
            self.ioport_capacity = max_ioport_capacity
            logger.info(f"Using default maximum I/O port capacity: {max_ioport_capacity}")


    def do_IBM_export(self, volume_id):
        """
        Perform the export operation over an IBM lib
        :param volume_id: The tape to export (single one)
        :return: (<result>, <error_message>) => result is True if operation was
        successful; False otherwise. And <error_message> returns the error
        message in the smc operation if occurred; None otherwise.
        """
        cmd = f'{tapeadmin.cta_smc_export_tape} {volume_id}'

        exported = False

        for attempt in range(number_of_export_attempts):
            # Try to export the tape
            msg = f"Executing command: {cmd} (attempt number {str(attempt + 1)} " \
                f"out of {str(number_of_export_attempts)} )"
            logger.info(msg)
            smc_call = cmd_utils.run_cmd(
                cmd,
                number_of_attempts = 3,
                logger = logger
            )
            if smc_call.returncode != 0:
                msg = f"Command failed: {cmd} . {log_utils.format_stderr(smc_call.stderr)}"
                if 'Import or Export Element Accessed' in smc_call.stderr:
                    logger.error(msg)
                else:
                    log_utils.log_and_exit(logger, msg)

            # Check if tape has been exported
            the_tape_location = tape_medium.tape_queryvolume(vid=volume_id, logger=logger)
            if 'UNKNOWN' in the_tape_location:
                exported = True
                break
            else:
                msg = f"Tape {volume_id} has not been exported (location: {the_tape_location}). " \
                    f"Sleeping {sleep_time_between_export_attempts} seconds..."
                logger.error(msg)
                time.sleep(sleep_time_between_export_attempts)

        # End of the process
        if exported:
            return True, None  # Operation OK, no error code
        else:
            return False, f'Last smc error message was: "{smc_call.stderr}"'


    def show_tape_export_summary(self):
        """
        Shows as a summary the tapes that:
        1) Have been exported successfully
        2) Have failed in exporting
        3) Have not even attempted to export yet (pending)
        """
        text = ''

        if self.tapes_exported:
            text += f"\n{'-' * 37} (TAPES EJECTED)" \
                f" [either physical or virtual I/O port] {'-' * 37}\n"
            for i, vid in enumerate(self.tapes_exported):
                text += str(i + 1) + ') ' + vid
                if i != len(self.tapes_exported) - 1:
                    text += '\n'

        if self.tapes_not_exported:
            text += f"\n{'-' * 37} (TAPES NOT EJECTED) {'-' * 37}\n"
            for i, vid in enumerate(self.tapes_not_exported):
                text += f"{i + 1}) {vid} ({self.tapes_not_exported[vid]})"
                if i != len(self.tapes_not_exported) - 1:
                    text += '\n'

        if self.tapes_to_export:
            text += f"\n{'-' * 37} (TAPES TO EJECT) {'-' * 37}\n"
            for i, vid in enumerate(self.tapes_to_export):
                text += str(i + 1) + ') ' + vid
                if i != len(self.tapes_to_export) - 1:
                    text += '\n'

        if self.tapes_exported or self.tapes_not_exported or self.tapes_to_export:
            logger.info(f'\n {"#" * 35} (TAPE EJECT SUMMARY) {"#" * 35} {text} \n')


    def check_ioport_capacity(self):
        """
        If the I/O port capacity is reached, prompts the user to continue the script after its input
        """
        if len(self.tapes_exported) != 0 and len(self.tapes_exported) % self.ioport_capacity == 0:
            msg = '*' * 50 + '\nReached I/O port capacity of ' + str(
                self.ioport_capacity) + ' (physical or virtual) slots.\n\n' \
                                    'Please remove the exported tapes from the I/O port (several extractions might be' \
                                    ' needed depending on the number of physical slots).\n'
            logger.info(msg)
            if not self.verbose:
                print(msg)

            msg = "Do you wish to continue with the execution of the script? (yes|no): "
            pattern_yes = re.compile('^y$|^ye$|^yes$', re.IGNORECASE)
            pattern_no = re.compile('^n$|^no$', re.IGNORECASE)
            response = ''
            while not pattern_yes.match(response) and not pattern_no.match(response):
                response = input(msg).rstrip()
                logger.info(msg + str(response))
                if pattern_yes.match(response):
                    msg = f"User has chosen to continue with the execution of " \
                            f"the script (response = \'{response}\')"
                    logger.info(msg)
                    break
                elif pattern_no.match(response):
                    msg = f"User has decided to stop the execution of the script " \
                            f"(response = \'{response}\') "
                    logger.info(msg)
                    sys.exit(0)

    def main(self):
        # New line separating the executions of the script

        logger.info("EXECUTED COMMAND: ".join(str(cmd_item) for cmd_item in sys.argv))

        cmd_utils.abort_if_root()

        logger.info("Gathering specified tapes for exporting...")
        self.gather_tapes_for_exporting()

        atexit.register(self.show_tape_export_summary)

        self.determine_lib()

        self.determine_ioport_capacity()

        # -------------------------- Start the EJECT procedure --------------------------

        # Make a copy of self.tapes_to_export and use it down here.
        # Because we are going to be removing tapes from the array,
        # and that would screw up the execution
        aux_tapes_to_export = self.tapes_to_export.copy()

        drive = tape_drive.get_local_drive(self.drive_name, config, logger)
        for tape_counter, tape in enumerate(aux_tapes_to_export):
            # Stop if limit has been reached
            if self.limit is not None and tape_counter == self.limit:
                break

            # Check that we haven't exceeded the I/O port capacity,
            # either the default one or the specified one
            self.check_ioport_capacity()

            # Show the name of the current tape and number of tapes remaining
            tapes_remaining = len(aux_tapes_to_export) - (tape_counter + 1)
            if self.limit is not None and self.limit < len(aux_tapes_to_export):
                tapes_remaining = self.limit - (tape_counter + 1)
            logger.info(
                f"Processing tape {tape} - tapes remaining after this one: " \
                f"{str(tapes_remaining)}"
            )

            # Check if tape exists
            if not self.check_tape_exists_in_cta_admin(tape):
                continue  # Will only be excuted if --skip-simple-errors

        # Check if the tape is in the same lib as the local drive
            logger.info(
                f"Checking if tape {tape} and local drive are in the same " \
                "library."
            )
            same_lib, lib_tape, lib_drive = tape_library.are_tape_and_drive_in_the_same_lib(
                tape,
                drive['DriveName'],
                logger
            )
            if same_lib:
                logger.info(f"Same library ({lib_tape}): OK")
            else:
                if not self.skip_simple_errors:
                    log_utils.log_and_exit(
                        logger,
                        f"Tape {tape} - different libraries (drive={lib_drive} | " \
                        f"tape={lib_tape})"
                    )
                logger.error(
                    f"Tape {tape} - different libraries (drive={lib_drive} | " \
                    f"tape={lib_tape}): SKIPPING"
                )
                continue  # Will only be excuted if --skip-simple-errors

            # Is tape at home?
            tape_location = tape_medium.tape_queryvolume(
                vid = tape,
                logger = logger,
                skip_simple_errors = self.skip_simple_errors
            )
            if tape_location != 'HOME':
                self.abort_or_skip(
                    tape,
                    f"Location is not HOME, it is: {tape_location}"
                )
                continue

            # Do the actual export
            export_result = self.do_IBM_export(tape)
            if export_result[0]:  # Tape exported OK
                self.tapes_exported.append(tape)
                self.tapes_to_export.remove(tape)
                logger.info(f"Tape {tape} successfuly moved to I/O slot")
            else:  # Tape NOT exported
                self.abort_or_skip(tape, export_result[1])
                logger.error(f"Tape {tape} was NOT moved to I/O slot")
                continue


def main():
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    tape_exporter = TapeExport(args=args)
    tape_exporter.main()

if __name__ == '__main__':
    main()
