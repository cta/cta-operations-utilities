#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
The tape-import script acts as an operator wrapper for the import of tape media to CTA.
"""

import sys
import argparse

from argparse import RawTextHelpFormatter

import tapeadmin

from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME

my_name = sys.argv[0].split('/')[-1]    # Get rid of "/usr/local/bin" and take only the final part

def load_args():
    """
    Loads the options for all possible script arguments
    """
    global config, logger

    usage = "tape-import [-C <config-file>] [--config-file <config-file>] " \
        "[--verbose] [-h | --help]"

    parser = argparse.ArgumentParser(
        description = "A wrapper script to import tapes",
        usage = f"{usage}", formatter_class=RawTextHelpFormatter
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Print more details on operations."
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        default = DEFAULT_CONFIG_PATH,
        help = "The path where the config file is located"
    )
    args = parser.parse_args()
    
    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return parser

def main():
    parser = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")
    # Control that we call "(...)/tape-import" without any arguments
    if len(sys.argv) > 1:
        parser.print_help()
        sys.exit(1)
    # Run the command
    cmd = tapeadmin.cta_smc_import
    msg = f'Running command: {cmd}'
    logger.info(msg)
    cmd_call = cmd_utils.run_cmd(
        cmd,
        number_of_attempts = 3,
        logger = logger
    )
    # Even though it's kinda broken, we control the return code of the call (apparently returns 0 if OK)
    if cmd_call.returncode != 0:
        msg = f'Execution of command "{cmd}" returned non-zero value ({cmd_call.returncode}). ' \
                f'{log_utils.format_stderr(cmd_call.stderr)}"'
        logger.error(msg)
    msg = "Tape import operation completed"
    logger.info(msg)

if __name__ == '__main__':
    main()
