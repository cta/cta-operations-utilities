#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Tape-label is a wrapper around the CTA tape labelling process which allows for
safe bulk-labelling of tape media.
"""

import sys
import json
import atexit
import argparse

from pathlib import Path
from datetime import datetime
from argparse import RawTextHelpFormatter

import tapeadmin

from ctautils import log_utils, cmd_utils
from tapeadmin import tape_drive, tape_pool, tape_medium
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH

from ctaopsadmin import TOOL_NAME


# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]

config, logger = None, None


def lock(drive_name:str):
    """
    Creates a lock file with the name of the host that created it,
    to prevent other hosts to run this script while another instance of it
    is running.
    :param drive_name: Name of the drive to acquire a lock for
    :return: True if operation was succesful, otherwise False
    (file already existed or operation failed)
    """
    logger.debug(f"Attempting to acquire lock for drive: {drive_name}")
    path = Path(config.get(my_name, 'lock_file_path'))
    lock_file = path / f'{drive_name}.lock'
    if lock_file.is_file():  # If lock file already exists
        logger.debug(f"Lock file {lock_file} already exists.")
        return False
    cmd_call = cmd_utils.run_cmd(f'whoami > {lock_file}', logger=logger)
    if cmd_call.returncode != 0:
        logger.error(
            f"Failed to create lock file {lock_file}."
        )

    return not cmd_call.returncode


def unlock(drive_name:str):
    """
    Deletes the lock file
    :param drive_name: Name of the drive to unlock
    :return: True if operation was succesful, otherwise False
    """
    logger.debug(f"Releasing lock for drive: {drive_name}")
    path = Path(config.get(my_name, 'lock_file_path'))
    lock_file = path / f'{drive_name}.lock'
    if lock_file.is_file():
        cmd_call = cmd_utils.run_cmd(f'rm {lock_file}', logger=logger)
        return not cmd_call.returncode

    logger.warning(f"Drive {drive_name} has no lock file.")
    return False


def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "A script to label tapes in CTA",
        formatter_class = RawTextHelpFormatter
    )

    # Required arguments
    required_named = parser.add_mutually_exclusive_group(required=True)
    required_named.add_argument(
        '-v',
        '--vid',
        type = str,
        help = 'The volume ID to label.'
    )
    required_named.add_argument(
        '-f',
        '--file',
        type = str, help = 'A file containing a list of volume IDs to be '
        'labeled. The format of the file is one VID per line.'
    )
    required_named.add_argument(
        '-s',
        '--srcpool',
        type = str,
        help = 'The source pool from which tapes are taken for labeling. '
    )

    # Optional arguments
    parser.add_argument(
        '-l',
        '--limit',
        type = int,
        help = "The maximum number of tapes to label."
    )
    parser.add_argument(
        '-d',
        '--dstpool',
        type = str,
        help = "The destination pool where tapes are put after labeling."
    )
    parser.add_argument(
        '-t',
        '--timeout',
        type = int,
        help = "Defines the timeout when waiting for a drive to become "
            "free. The value is given in seconds. This option can be "
            "helpful if many tapes are labeled in one go. The default "
            "value is 60 seconds."
    )
    parser.add_argument(
        "-o",
        "--oldLabel",
        type=str,
        help="The vid of the current tape label on the tape if it is not the same as VID",
    )
    parser.add_argument(
        '-D',
        '--drive',
        type = str,
        required = True,
        help = "The drive to be used."
    )
    parser.add_argument(
        '--skip-simple-errors',
        action = 'store_true',
        help = "Do not abort the script if a non-critical failure occurs in "
            "labeling one of the tapes. Instead, continue and label all the "
            "specified tapes remaining."
    )

    # Settings
    settings_group = parser.add_argument_group(
        'settings',
        "Configure how this script runs."
    )
    settings_group.add_argument(
        '-V',
        '--verbose',
        action = 'store_true',
        help = "Increase log verbosity."
    )
    settings_group.add_argument(
        '-C',
        '--config-file',
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )

    # Assign the variables to the options
    args = parser.parse_args()

    # Set up config holder and logger
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    logger = log_utils.init_logger(my_name, config)

    if args.limit:
        if args.limit < 1:
            log_utils.log_and_exit(
                logger,
                f'Wrong limit value ({str(args.limit)}). Must be greater than 0.'
            )
    else:
        args.limit = 0

    local_drives = tape_drive.get_local_drives(config, logger)
    if args.drive not in [drive['DriveName'] for drive in local_drives]:
        log_utils.log_and_exit(
            logger,
            f'Drive {args.drive} not found in set of locally configured drives.'
        )

    return args


def assemble_tapes_to_label(vid:str, opt_file:str, opt_srcp:str):
    """
    Gets the specified set of tapes to label, either by name, file or source pool
    :param vid: Tape VID string
    :param opt_file: File path string to file containing one vid per line
    :param opt_srcp: Tape pool name to pick tapes from
    :return: A list of tape VID strings to label, or None
    """
    logger.debug("Creating list of tapes to label.")
    tapes_to_label = []

    if vid:
        tapes_to_label.append(vid)

    if opt_file:
        try:
            with open(opt_file, mode='r', encoding='utf-8') as the_file:
                for line in the_file:
                    if not line.startswith('#'):  # If it is not a comment
                        vid = line.rstrip()
                        tapes_to_label.append(vid)
            the_file.close()
        except FileNotFoundError:
            logger.error(f'File not found: {opt_file}')
            return None

    if opt_srcp:
        if not tape_pool.pool_exists(opt_srcp):
            logger.error(f'Source pool not found: {opt_srcp}')
            return None
        cmd = f'{tapeadmin.cta_admin_tape_json} ls --tapepool {opt_srcp}'
        cta_admin_call = cmd_utils.run_cmd(cmd, logger=logger)

        if cta_admin_call.returncode != 0:
            logger.error(
                f'Execution of command [{cmd}] failed with result: ' \
                f'{cta_admin_call.stderr}'
            )
            return None
        # For tape_info (dict) in the list of tapes
        for tape_info in json.loads(cta_admin_call.stdout):
            a_tape_vid = tape_info['vid']
            tapes_to_label.append(a_tape_vid)

    if not tapes_to_label:
        logger.error('No tapes to label found.')

    return tapes_to_label


def show_label_summary(tapes_to_label:list, tapes_labeled:list,
                       tapes_unlabeled:dict):
    """
    Prints and logs the summary of the execution of this tape-label script.
    Shows the labeled and unlabeled tapes.
    :param tapes_to_label: List of tape VIDs submitted for labeling
    :param tapes_labeled: List of tape VIDs successfully labeled
    :param tapes_unlabeled: Dict of VIDs and errors for failed label ops
    """
    text = ''

    if tapes_labeled:
        text += f"\n{'-' * 37} (TAPES LABELED) {'-' * 37}\n"
        for i, vid in enumerate(tapes_labeled):
            text += f"{i + 1}) {vid}"
            if i != len(tapes_labeled) - 1:
                text += '\n'
            if vid in tapes_to_label:
                # Remove the labeled tapes from tapes_to_label for the summary
                tapes_to_label.remove(vid)

    if tapes_unlabeled:
        text += f"\n{'-' * 37} (TAPES NOT LABELED) {'-' * 37}\n"
        for i, vid in enumerate(tapes_unlabeled):
            text += f"{i + 1}) {vid} ({tapes_unlabeled[vid]})"
            if i != len(tapes_unlabeled)-1:
                text += '\n'
            # Remove the unlabeled tapes from tapes_to_label for the summary
            if vid in tapes_to_label:
                tapes_to_label.remove(vid)

    if tapes_to_label:
        text += f"\n{'-' * 37} (TAPES TO LABEL) {'-' * 37}\n"
        for i, vid in enumerate(tapes_to_label):
            text += f"{i + 1}) {vid}"
            if i != len(tapes_to_label)-1:
                text += '\n'

    if tapes_labeled or tapes_unlabeled or tapes_to_label:
        logger.info(f'\n{"#" * 35} (TAPE LABEL SUMMARY) {"#" * 35}\n{text}\n')


def do_preflight_checks(drive_name:str, srcpool:str, dstpool:str, vid:str):
    """
    Perform initial safety checks before doing anything dangerous.
    :param drive_name: Name of the drive to be used for labeling
    :param scrpool: The name of the pool from which the tapes to label are taken
    :param dstpool: The name of the pool into which the labeled tapes are put
    :param vid: The VID of the tape to label
    :return: True if ready to label, False otherwise
    """
    logger.debug("Performing pre-flight checks.")
    good_to_go = True

    if cmd_utils.is_root():
        logger.error("This command must not be run as root.")
        good_to_go = False

    if not lock(drive_name):
        logger.error(
            f"Could not obtain lock for drive {drive_name} " \
            "during preflight check."
        )
        good_to_go = False

    if srcpool is not None:
        pool_ok_for_label = tape_pool.is_pool_name_ok_to_label(
            pool_name = srcpool,
            config = config,
            logger = logger
        )
        if not pool_ok_for_label:
            logger.error(f'Source pool name ({srcpool}) not accepted.')
        good_to_go = False

    if dstpool is not None and not tape_pool.pool_exists(dstpool):
        logger.error(f'Destination pool ({dstpool}) does not exist.')
        good_to_go = False

    drive_status_and_reason = tape_drive.get_drive_status_and_reason(drive_name)

    # We obtain the name of the user executing the script (we know it from klist) 
    # maybe change this method why putting a dependency on kerberos 
    cmd_call_klist = cmd_utils.run_cmd("klist", logger=logger)
    klist_line_two= cmd_call_klist.stdout.split("\n")[1]
    whoami_klist = klist_line_two.split("Default principal: ")[1].split("@")[0]

    # We obtain the date of today ("YearMonthDay")
    date = datetime.now().strftime('%Y%m%d')
    msg = f"/usr/bin/cta-admin drive down <DRIVE_NAME>" \
        """--reason "username (date): script_name + description"\n""" \
        "SUGGESTION (the operator may just copy and execute this exact command): " \
        f"""/usr/bin/cta-admin drive down {drive_name} --reason """ \
        f'"{whoami_klist} ({date}): tape-label with tape {vid}"'
    if drive_status_and_reason['drive_status'] != 'DOWN':
        logger.error(
            f"Drive {drive_name} is not DOWN for labeling. "
            "Please, use the following command:\n"
            f"{msg}"
        )
        good_to_go = False
    if not drive_status_and_reason['reason'].rstrip():
        logger.error(
            f"Drive {drive_name} is DOWN without reason. " \
            "Please, use the following command:\n"
            f"{msg}"
        )
        good_to_go = False

    if not good_to_go:
        unlock(drive_name)

    return good_to_go


def main():
    """
    Run bulk tape label with lock file.
    """
    args = load_args()

    # Whatever happens, summary will be printed at the end (if tapes gathered)
    tapes_to_label, tapes_labeled, tapes_unlabeled = [], [], {}
    atexit.register(
        show_label_summary,
        tapes_to_label,
        tapes_labeled,
        tapes_unlabeled
    )
    # Whatever happens, the lock file will be erased at the end of the script
    # (if exists)
    atexit.register(unlock, args.drive)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.debug(f"Executing {my_name}")

    logger.info(
        f'Log file can be found in: {log_utils.get_logger_file_path(logger)}'
    )

    ready = do_preflight_checks(
        drive_name = args.drive,
        srcpool = args.srcpool,
        dstpool = args.dstpool,
        vid = args.vid
    )
    if not ready:
        log_utils.log_and_exit(
            logger,
            "Tape label checks failed, aborting!"
        )

    tapes_to_label = assemble_tapes_to_label(args.vid, args.file, args.srcpool)

    # Label the tapes
    tape_counter = 0
    for volume_id in tapes_to_label:
        # Print which tape are we processing and how many more tapes remain
        # (apart from the current one)
        tapes_remaining = len(tapes_to_label) - (tape_counter+1)
        if args.limit and args.limit < len(tapes_to_label):
            tapes_remaining = args.limit - (tape_counter+1)
        logger.info(
            f"Processing tape {volume_id}. Tapes remaining after this one: "\
            f"{str(tapes_remaining)}"
        )

        labeled, failed_reason = tape_medium.label_tape(
            vid=volume_id,
            drive_name=args.drive,
            config=config,
            logger=logger,
            old_label=args.oldLabel
        )
        tape_counter += 1

        # If labeling failed, log it and clean up
        if not labeled:
            tapes_unlabeled[volume_id] = failed_reason
            # Increase tape counter and check break condition
            if args.limit and tape_counter >= args.limit:
                break
            if args.skip_simple_errors:
                continue
            # Else, we abort the script and remove the lock file
            unlock(args.drive)
            log_utils.log_and_exit(logger, failed_reason)

        # Move labeled tape to destination pool (if given)
        if args.dstpool:
            logger.info(f'Changing tape {volume_id} to pool {args.dstpool}.')
            cmd = f'{tapeadmin.cta_admin_json_tape_ch} --tapepool ' \
                f'{args.dstpool} --vid {volume_id}'
            if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
                logger.error(f'Failed to execute command: {cmd}')

        # Labeling succeeded
        tapes_labeled.append(volume_id)
        if args.limit and tape_counter >= args.limit:
            break


if __name__ == '__main__':
    main()
