# CTA Ops Admin

The `ctaopsadmin` package provides a command line wrapper for CTA's `cta-admin` command, as well as additional operator tools for day-to-day tape-related tasks.

The goal of the `cta-ops-admin` command is to make life easier for operators by providing:

* Additional tools for hardware life-cycle management, such as the `drive test`, `tape label`, and `tape mediacheck` commands.
* Customization options for information output, such as tabulation options and unit formatting.
* More operator friendly error messages than what `cta-admin` provides by default.
* Mechanisms to separate operator actions from system activity, though options such as the choice of CTA frontend.

## Installation

### Configuration

The display options of `cta-ops-admin <item> ls`, as well as the behavior of the operations-specific tools can be extensively configured.

The following is a reference of available config options:

```yaml
tools:
  # -------------------------------
  # CTA OPS ADMIN
  # -------------------------------
  # Defaults for the operations-specific handlers
  cta-ops-admin:
    tape-drivetest:
      default_file_size: '10G'    # 10 GiB
      default_number_of_files: 100
      # default_block_size: 262144 # 256 * 1024
      default_file_path: '/tape-drivetest/'    # So it will be: /dev/shm/tape-drivetest/
    tape-export:
      number_of_export_attempts: 3
      sleep_time_between_export_attempts: 5  # in seconds
    tape-label:
      lock_file_path: '/tmp/'
    tape-mediacheck:
      random_file_size: 5368709120  # 5 * (1024 ** 3)  # Means 5 * 1024^3
    tape-summary:
      default_file_path: '/tape-summary/'
      cta_statistics_update_cmd: '/usr/bin/cta-statistics-update /etc/cta/cta-catalogue.conf'
    # Define commands and subcommands available to cta-ops-admin.
    # Any command without an explicit handler will be out-sourced to cta-admin instead.
    cta-ops-admin:
      # Substitute empty results in tables with this character
      empty_table_item_char: '-'
      # Date format for tables
      date_format: "%Y-%m-%d %H:%M"
      # Define all supported commands
      commands:
        showqueues:
          alias: sq
          help: "Show CTA queue information"
          subcommands:
            retrieve:
              alias: r
              handler: showqueues-retrieve
            archive:
              alias: a
              handler: showqueues-archive
          table_headers:
            mountType: "Type"
            tapepool: "Tape Pool"
            vo: "VO"
            logicalLibrary: "Library"
            vid: "VID"
            queuedFiles: "Files Queued"
            queuedBytes: "Data Queued"
            oldestAge: "Oldest"
            minAge: "Youngest"
            priority: "Priority"
            readMaxDrives: "Max Read Drives"
            writeMaxDrives: "Max Write Drives"
            curBytes: "Current Bytes"
            curMounts: "Mounts CUR"
            curFiles: "Files CUR"
            data: "Data"
            bytesPerSecond: "MB/s"
            tapes: "Tapes"
            tapesCapacity: "Capacity"
            tapesFiles: "Files on Tapes"
            tapesBytes: "Data on Tapes"
            fullTapes: "Full Tapes"
            writableTapes: "Writable Tapes"
        tape:
          alias: ta
          help: "Manage tape media"
          subcommands:
            summary:
              handler: tape-summary
            location:
              handler: tape-location
            export:
              handler: tape-export
            import:
              handler: tape-import
            label:
              handler: tape-label
            mediacheck:
              handler: tape-mediacheck
            mount:
              handler: tape-mount
            unmount:
              handler: tape-unmount
            add:
            ch:
            rm:
            reclaim:
            ls:
          header_alignment: ['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'right', 'right', 'right', 'left', 'left', 'left', 'left', 'left']
          table_headers:
            vid: "VID"
            mediaType: "Media Type"
            vendor: "Vendor"
            logicalLibrary: "Library"
            purchaseOrder: "Order"
            tapepool: "Tapepool"
            vo: "VO"
            encryptionKeyName: "Encryption Key"
            capacity: "Capacity"
            occupancy: "Occupancy"
            lastFseq: "Last fseq"
            full: "Full"
            #fromCastor: "From Castor"
            state: "State"
            stateReason: "Reason"
            #labelLog:
            #  drive: "Label Drive"
            #  time: "Label Time"
            lastWrittenLog:
              drive: "Last WRITE Drive"
              #time: "Last W.Time"
            writeMountCount: "W.Mounts"
            lastReadLog:
              drive: "Last READ Drive"
              time: "Last R.Time"
            readMountCount: "R.Mounts"
            #creationLog:
            #  username: "C.User"
            #  host: "C.Host"
            #  time: "C.Time"
            #lastModificationLog:
            #  username: "M.User"
            #  host: "M.Host"
            #  time: "M.Time"
            comment: "Comment"
        drive:
          alias: dr
          help: "Manage tape drives"
          subcommands:
            test:
              handler: tape-drivetest
            up:
              handler: 'custom:drive'
            down:
              handler: 'custom:drive'
            ls:
              handler: 'custom:drive'
            ch:
            rm:
          header_alignment: ['left', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'right', 'left', 'left', 'right', 'right', 'right', 'left', 'right', 'right', 'left']
          table_headers:
            logicalLibrary: "Library"
            driveName: "Drive"
            host: "Host"
            devFileName: "Device"
            rawLibraryslot: "SMC"
            desiredDriveState: "Desired"
            mountType: "Request"
            driveStatus: "Status"
            driveStatusSince: "Since"
            vid: "VID"
            tapepool: "Tapepool"
            filesTransferredInSession: "#Files"
            bytesTransferredInSession: "#Bytes"
            sessionId: "Session"
            # currentPriority: "Current Priority"
            currentActivity: "Activity"
            ctaVersion: "CTA Version"
            timeSinceLastUpdate: "Age"
            reason: "Reason"
        activitymountrule:
          help: "Manage mount policy to username mappings"
          alias: amr
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            diskInstance: "Instance"
            activityMountRule: "Rule Name"
            activityRegex: "Activity"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        admin:
          alias: ad
          help: "Manage CTA administrator accounts"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            user: "User"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        archiveroute:
          alias: ar
          help: "Manage Archive Routes"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            storageClass: "Storage Class"
            copyNumber: "#Copy"
            tapepool: "Tapepool"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        diskinstance:
          alias: di
          help: "Manage Disk (EOS) Instances"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            name: "Name"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        diskinstancespace:
          alias: dis
          help: "Manage Disk Instance Spaces"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            name: "Name"
            diskInstance: "Instance"
            freeSpaceQueryUrl: "Free Space Query URL"
            refreshInterval: "Refresh Interval"
            freeSpace: "Free Space"
            lastRefreshTime: "Last Refresh Time"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        disksystem:
          alias: ds
          help: "Manage Disk Systems"
          subcommands:
            add:
            ch:
            rm:
            ls:
          header_alignment: ['left', 'left', 'left', 'left', 'right', 'right', 'left', 'left', 'left', 'left', 'left', 'left', 'left', 'left']
          table_headers:
            name: "Name"
            diskInstance: "Instance"
            diskInstanceSpace: "Disk Space"
            fileRegexp: "Reg Exp"
            targetedFreeSpace: "Targeted Free Space"
            sleepTime: "Sleep"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        failedrequest:
          alias: fr
          help: "Manage Failed Requests"
          subcommands:
            rm:
            ls:
          table_headers:
            objectId: "Object ID"
            requestType: "Type"
            copyNb: "#Copy"
            tapepool: "Tapepool"
            requester:
              username: "User"
              groupname: "group"
        groupmountrule:
          alias: gmr
          help: "Manage Mount Policy to user group mappings"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            diskInstance: "Instance"
            groupMountRule: "Group"
            mountPolicy: "Policy"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        logicallibrary:
          alias: ll
          help: "Manage Logical Libraries"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            name: "Library"
            isDisabled: "Disabled"
            disabledReason: "Reason"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        mediatype:
          alias: mt
          help: "Manage Media Types"
          subcommands:
            add:
            ch:
            rm:
            ls:
          header_alignment: ['left', 'left', 'right', 'right', 'right', 'right', 'right', 'right', 'left', 'left', 'left', 'left', 'left', 'left']
          table_headers:
            name: "Media Type"
            cartridge: "Cartridge"
            capacity: "Capacity"
            #primaryDensityCode: "Primary Density Code"
            #secondaryDensityCode: "Secondary Density Code"
            numberOfWraps: "#Wraps"
            minLpos: "Min LPos"
            maxLpos: "Max LPos"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
        mountpolicy:
          alias: mp
          help: "Manage Mount Policies"
          subcommands:
            add:
            ch:
            rm:
            ls:
          header_alignment: ['left', 'right', 'right', 'right', 'right', 'left', 'left', 'left', 'left', 'left', 'left', 'left']
          table_headers:
            name: "Mount Policy"
            archivePriority: "Archive Priority"
            archiveMinRequestAge: "Min Archive Age"
            retrievePriority: "Retrieve Priority"
            retrieveMinRequestAge: "Min Retrieve Age"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        physicallibrary:
          alias: pl
          help: "Manage Physical Libraries"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            name: "Name"
            manufacturer: "Manufacturer"
            model: "Model"
            type: "Type"
            guiUrl: "GUI URL"
            webcamUrl: "WebCam URL"
            location: "Location"
            nbPhysicalCartridgeSlots: "Physical Slots"
            nbAvailableCartridgeSlots: "Available Slots"
            nbPhysicalDriveSlots: "Physical Slots"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        recycletf:
          alias: rtf
          help: "Inspect files in the recycle bin"
          subcommands:
            ls:
          table_headers:
            archiveId: "Archive ID"
            copyNumber: "#Copy"
            vid: "VID"
            fsec: "fseq"
            blockId: "Block ID"
            instance: "Instance"
            diskFxid: "Disk fxid"
            size: "Size"
            checksumType: "Checksum Type"
            checksumValue: "Checksum Value"
            storageClass: "Storage Class"
            owner: "Owner"
            group: "Group"
            deletionTime: "Deletion Time"
            pathWhenDeleted: "Path When Deleted"
            reason: "Reason"
        repack:
          alias: re
          help: "Manage Repacks"
          subcommands:
            add:
            rm:
            ls:
            err:
          table_headers:
            creationLog.time: "C.Time"
            repackTime: "Repack Time"
            creationLog.username: "C.User"
            vid: "VID"
            tapepool: "Tape Pool"
            userProvidedFiles: "Provided Files"
            totalFilesOnTapeAtStart: "Files At Start"
            totalBytesOnTapeAtStart: "Bytes At Start"
            totalFilesToRetrieve: "Total Files"
            totalBytesToRetrieve: "Total Bytes"
            filesLeftToRetrieve: "Files to Retrieve"
            filesLeftToArchive: "Files to Archive"
            failed: "Failed"
            status: "Status"
        requestermountrule:
          alias: rmr
          help: "Manage Mount Policy to username mappings"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            diskInstance: "Instance"
            requesterMountRule: "Requester Mount Rule"
            mountPolicy: "Policy"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        storageclass:
          alias: sc
          help: "Manage Storage Classes"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            name: "Storage Class"
            nbCopies: "# of Copies"
            vo: "VO"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        tapefile:
          alias: tf
          help: "Manage Tape Files"
          subcommands:
            ls:
            rm:
          table_headers:
            af:
              archiveId: "ID"
              size: "Size"
              # TODO: List not yet supported
              #checksum:
              #  type: "Cksum Type"
              #  value: "Cksum Value"
              storageClass: "Storage Class"
              creationTime: "C.Time"
            tf:
              copyNb: "#Copy"
              vid: "VID"
              fSeq: "fSeq"
              blockId: "Block ID"
            df:
              diskInstance: "Disk Instance"
              diskId: "Disk fxid"  # TODO: Convert to hex?
              ownerId:
                uid: "Owner"
                gid: "Group"
        tapepool:
          alias: tp
          help: "Manage Tape Pools"
          subcommands:
            add:
            ch:
            rm:
            ls:
          header_alignment: ['left', 'left', 'right', 'right', 'right', 'right', 'right', 'left', 'left', 'left', 'left', 'left']
          table_headers:
            name: "Name"
            vo: "VO"
            numTapes: "#Tapes"
            numPartialTapes: "#Partial"
            numPhysicalFiles: "#Physical Files"
            capacityBytes: "Capacity"
            dataBytes: "Data"
            encrypt: "Encrypt"
            supply: "Supply"
            #created:
            #  username: "C.User"
            #  host: "C.Host"
            #  time: "C.Time"
            modified:
              username: "M.User"
            #  host: "M.Host"
              time: "M.Time"
            comment: "Comment"
        version:
          alias: v
          help: "Show CTA version information"
        virtualorganization:
          alias: vo
          help: "Manage Virtual Organisations"
          subcommands:
            add:
            ch:
            rm:
            ls:
          table_headers:
            name: "Name"
            readMaxDrives: "Max Read Drives"
            writeMaxDrives: "Max Write Drives"
            maxFileSize: "Max File Size"
            diskinstance: "Disk Instace"
            creationLog:
              username: "C.User"
              host: "C.Host"
              time: "C.Time"
            lastModificationLog:
              username: "M.User"
              host: "M.Host"
              time: "M.Time"
            comment: "Comment"
```

## Usage

Use `cta-ops-admin` as you would use `cta-admin`:

``` bash
usage: cta-ops-admin [-h] [--verbose] [-C CONFIG_FILE] <cmd> [<subcmd>]
```

For example:

``` bash
usage: cta-ops-admin tape [-h]
                          {summary,location,export,import,label,mediacheck,mount,unmount,add,ch,rm,reclaim,ls}
                          ...

positional arguments:
  {summary,location,export,import,label,mediacheck,mount,unmount,add,ch,rm,reclaim,ls}
    summary
    location
    export
    import
    label
    mediacheck
    mount
    unmount
    add
    ch
    rm
    reclaim
    ls

optional arguments:
  -h, --help            show this help message and exit
```

``` bash
usage: cta-ops-admin drive [-h] {test,up,down,ls,ch,rm} ...

positional arguments:
  {test,up,down,ls,ch,rm}
    test
    up
    down
    ls
    ch
    rm

optional arguments:
  -h, --help            show this help message and exit
```

## Usage

Use it as you would use `cta-admin`, and try the additional operator tools:

### Drive tools

- `cta-ops-admin drive test`
    - Performs a read/write test on a given drive to check whether or not it functions as expected.

### Tape Tools

- `cta-ops-admin tape import|export`
    - A tools for bulk importing/exporting tapes through the library's I/O slot.
- `cta-ops-admin tape label`
    - Bulk label a selection of tapes, with additional safety checks to prevent data loss.
- `cta-ops-admin tape location`
    - Queries the SMC for the position of the given tape cartridge inside of its library.
- `cta-ops-admin tape mediacheck`
    - Similar to the `drive test` command, this tool performs read/write tests to test if a given cartridge is working as expected.
- `cta-ops-admin tape mount|unmount`
    - A tool for manually mounting/unmounting a tape in a drive in a user-friendly manner.
- `cta-ops-admin tape summary`
    - Provides a simple summary of a given tape for an operator.
