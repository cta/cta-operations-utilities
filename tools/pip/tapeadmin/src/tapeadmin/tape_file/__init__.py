# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN
"""
tape_file:
Helpers for interacting with tape files.
"""


import tapeadmin
import json

from ctautils import log_utils, cmd_utils
from logging import Logger

def get_tape_file_metadata(
    logger: Logger, archive_file_id: str =None, disk_file_fxid: str=None, disk_file_instance: str = None
):
    """
    Fetches the archive file metadata
    :param archive_id: The archive file id
    :param disk_file_id: The disk file id
    :param disk_file_instance: The disk file instance
    :return: The archive file metadata in map format
    """

    try:
        if archive_file_id:
            af_ls_cmd = (
                f"{tapeadmin.cta_admin_json_tape_files} ls --id {archive_file_id}"
            )
        elif disk_file_fxid and disk_file_instance:
            af_ls_cmd = (
                f"{tapeadmin.cta_admin_json_tape_files} ls --instance "
                f"{disk_file_instance} --fxid {disk_file_fxid}"
            )
        else:
            raise ValueError(
                "At least one argument must be provided " "for get_tape_file_metadata."
            )
        af_ls_call = cmd_utils.run_cmd(af_ls_cmd)
        cta_admin_tapefile_ls_object = json.loads(af_ls_call.stdout)[0]
        return cta_admin_tapefile_ls_object
    except json.decoder.JSONDecodeError:
        log_utils.log_and_exit(
            logger,
            f'[{af_ls_cmd}] - " \
        f"{log_utils.format_stderr(af_ls_call.stderr)}',
        )
    except Exception as e:
        log_utils.log_and_exit(
            logger,
            f'[{af_ls_cmd}] - " \
        f"{log_utils.format_stderr(af_ls_call.stderr)}',
        )
