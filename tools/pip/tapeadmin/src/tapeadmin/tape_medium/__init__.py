# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
tape_medium:
Helpers for interacting with various tape media, such as cartridges.
"""

import json

import tapeadmin

from tapeadmin import tape_drive, tape_library, tape_pool
from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig

from logging import Logger

def get_tape_vid_inside_drive(drive: dict, logger: Logger):
    """
    Gets the volume ID of the tape inside a specific drive
    (either mounted or ejected) if there exists one. None otherwise
    :param drive: The drive configuration dictionary
    :return: The volume ID of the tape inside the drive;
        None if there is no tape inside
    """
    drive_name = drive['DriveName']
    cmd = f'{tapeadmin.cta_admin_drive_json} ls {drive_name}'
    cmd_result = cmd_utils.run_cmd(cmd)
    if cmd_result.returncode != 0 and 'not found' in cmd_result.stderr:
        log_utils.log_and_exit(logger, f"Drive {drive_name} does not exist")

    drive_ordinal = tape_drive.get_ordinal(drive)
    cmd = f'{tapeadmin.cta_smc_query_json} D -D {drive_ordinal}'
    cmd_call = cmd_utils.run_cmd(cmd, number_of_attempts=3)

    json_result = None
    try:
        json_result = json.loads(cmd_call.stdout)[0]
    except json.JSONDecodeError:
        log_utils.log_and_exit(
            logger,
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    if json_result['vid'] == '':
        return None
    return json_result['vid']


def get_tape_files(vid:str):
    """
    Returns the list of files in a tape
    :param vid: The volumeID of the desired tape
    :return: The list of files present on the tape
    """
    cmd = f'{tapeadmin.cta_admin_json_tape_files} ls -v {vid}'
    cmd_call = cmd_utils.run_cmd(cmd)
    if cmd_call.returncode != 0:
        return None
    return json.loads(cmd_call.stdout)


def safe_get_tape_files(vid:str, buffer_file, logger: Logger=None):
    """
    Fetches the tape file info from CTA in json form and writes it to a file.
    This function is safer with regards to memory usage, as `tapefile ls`
    can generate large outputs (400MB).
    :param vid: The VID of the tape
    :param buffer_file: Store output in buffer, instead of RAM
    :param logger: (Optional) Initialized logging.logger object
    :return: Returncode of the executed cta-admin command
    """
    if logger is not None:
        logger.debug(f"Retrieving tape file data for tape {vid}")

    # Fetch file list from CTA
    tape_ls_call = cmd_utils.write_cmd_output(
        cmd = f'{tapeadmin.cta_admin_json_tape_files} ls -v {vid}',
        write_to = buffer_file,
        logger = logger
    )

    return tape_ls_call.returncode


def get_tape_info(vid:str, logger: Logger):
    """
    Gets information about a tape from "cta-admin tape"
    :param vid: The tape's volume ID as a string
    :param logger: Initialized logging.logger object
    :return: A dict with entries for all info returned by "cta-admin tape"
    """
    if not vid:
        log_utils.log_and_exit(
            logger,
            "No volume ID specified as parameter for get_tape_info."
        )

    cmd = f'{tapeadmin.cta_admin_json_tape_ls_vid} {vid}'
    cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
    try:
        tape_info_array = json.loads(cmd_call.stdout)
    except json.decoder.JSONDecodeError:
        log_utils.abort(
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    if not tape_info_array:
        return None

    return tape_info_array[0]


def get_tape_occupancy(vid: str, logger: Logger):
    """
    Returns the number of files and amount of data on a tape.
    :param vid: The volumeID of the desired tape
    :param logger: Initialized logging.logger object
    :return: The number of files and amount of data on a tape in bytes,
    None if call failed
    """
    if logger is not None:
        logger.debug(f"Retrieving occupancy of the tape {vid}")

    out = get_tape_info(vid, logger)
    return int(out['nbMasterFiles']), int(out['occupancy'])


def check_tape_is_mount_ready(vid: str, logger: Logger):
    """
    Checks if the tape is in the proper state for mount
    :param vid: Tape VID string
    :param logger: Initialized logging.logger object
    :returns: True if ready for mount, False otherwise
    """
    location = tape_queryvolume(vid=vid, logger=logger)
    if location != 'HOME':
        logger.warning(f"Tape {vid} is NOT in its HOME slot. Its location is {location}")
        return False
    return True


def do_VOL1_check(
    input_file: str, output_file: str, volume_id: str, logger: Logger = None
):
    """
    Performs a VOL1 check for the tape specified.
    :param input_file: The input file for the 'if' of dd command
    :param output_file: The output file for the 'of' of dd command
    :param volume_id:  The tape volume ID string
    :param logger: (Optional) Initialized logging.logger object
    :return: 'ok' if VOL1 check was ok; 'not_ok' otherwise
    """
    cmd = f'/usr/bin/dd if={input_file} of={output_file} bs=80 count=1'
    if cmd_utils.run_cmd(f'{cmd} > /dev/null 2>&1', logger=logger).returncode != 0:
        log_utils.error(f"Reading VOL1 failed for {volume_id}")
        return False
    else:
        cmd = f'{tapeadmin.grep} ^VOL1{volume_id} {output_file} >/dev/null 2>&1'
        if cmd_utils.run_cmd(cmd, logger=logger).returncode != 0:
            log_utils.abort(f"VOL1 does not contain {volume_id}, wrong tape?")
            return False
    return True


def disable_tape(volume_id: str, reason: str, logger: Logger):
    """
    Disables a tape in CTA
    :param volume_id: The volume ID of the tape to disable
    :param reason: The reason for disabling the tape, added in form of a
    comment with a specific format.
    :param logger: (Optional) Initialized logging.logger object
    :return: The corresponding error or success message after the disable
        operation
    """
    cta_admin_query_tape_json = f'{tapeadmin.cta_admin_json_tape_ls_vid} ' \
        f'{volume_id}'
    cmd_call = cmd_utils.run_cmd(cta_admin_query_tape_json)
    current_state = json.loads(cmd_call.stdout)[0]['state']
    if current_state.upper() == 'REPACKING':
        new_state = 'REPACKING_DISABLED'
    else:
        new_state = 'DISABLED'

    cmd = f'{tapeadmin.cta_admin_json_tape_ch} --vid {volume_id} ' \
        f'--state {new_state} --reason "{reason}"'
    cmd_call = cmd_utils.run_cmd(cmd, logger=logger)

    # Check the return code of the disable operation
    if cmd_call.returncode != 0:
        msg = f'Failed to disable tape {volume_id} with command: {cmd} ' \
              f'{log_utils.format_stderr(cmd_call.stderr)}'
        logger.error(msg)
        return msg

    # Store the status of the tape after disable operation
    cmd_call = cmd_utils.run_cmd(cta_admin_query_tape_json, logger=logger)

    # Check if the query command failed
    if cmd_call.returncode != 0:
        msg = f"Status of tape {volume_id} unknown after attempting to " \
            "disable it, due to failure of command: " \
            f"{cta_admin_query_tape_json} " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        logger.error(msg)
        return msg

    # Check the status of the tape
    current_state = json.loads(cmd_call.stdout)[0]['state']
    if (
            (current_state.upper() == 'DISABLED') or
            (current_state.upper() == 'REPACKING_DISABLED')
    ):
        msg = f'Tape {volume_id} has been disabled (Reason = {reason})'
        logger.info(msg)
    else:
        msg = f'Tried to disable tape {volume_id} but failed. ' \
            'Tape is still enabled.'
        logger.error(msg)

    return msg


def do_mount(vid: str, drive_name: str, config: CtaOpsConfig, logger: Logger = None):
    """
    Mount a tape in a drive.
    :param vid: VID string of the tape to mount
    :param drive_name: (Optional) Name string of the drive to use for the mount
    :param config: CtaOpsConfig object
    :param logger: Initialized logging.logger object
    :returns: True upon success, otherwise False
    """
    def mount_sys_checks():
        """
        Check if the system is ready to perform a drive unmount
        :returns: True if system in ready state, otherwise False
        """
        # Check that everything is ready to perform the mount
        daemons_ready = tape_drive.check_drive_mount_daemons(logger)
        if not daemons_ready:
            logger.error(
                "Can't mount without mount/unmount daemon, aborting"
            )
            return False
        if not drive_name:
            logger.error("No drive name specified, cannot mount!")
            return False

        if not drive:
            logger.error(
                f"Drive {drive_name} not found in set of locally configured " \
                "drives."
            )
            return False

        return True

    def mount_tape():
        """
        Mount a specific tape in a given drive.
        :returns: True if mount successful, otherwise False
        """
        cmd = f'{tapeadmin.cta_smc} -m -V {vid} -D {tape_drive.get_ordinal(drive)}'
        cmd_utils.run_cmd(cmd, number_of_attempts=3)
        if tape_drive.is_drive_free(drive):
            logger.error(
                f"Drive {drive_name} is still free after mount operation: ERROR"
            )
            return False
        else:
            tape_mounted = get_tape_vid_inside_drive(drive, logger)
            if tape_mounted != vid:
                logger.error(
                    f"Wrong tape mounted inside drive {drive_name}" \
                    f" (expected: {vid}; mounted instead: {tape_mounted})"
                )
                return False
            logger.info(f"Mount of tape {vid} inside drive {drive_name}: OK")
            return True

    drive = tape_drive.get_local_drive(drive_name, config, logger)

    if not mount_sys_checks():
        return False

    # Ensure we have set the  tape
    if not get_tape_info(vid, logger):
        logger.error(
            f"No info found for tape {vid} in cta-admin."
        )
        return False

    if not tape_drive.is_drive_free(drive):
        if has_ejected_tape(drive, logger):
            logger.error(
                f"Tape drive {drive_name} is not free, tape " \
                f"{get_tape_vid_inside_drive(drive, logger)} " \
                "is EJECTED in it."
            )
        else:
            logger.error(
                f"Tape drive {drive_name} is not free, tape " \
                f"{get_tape_vid_inside_drive(drive, logger)} " \
                "is MOUNTED inside."
            )
        return False

    same_lib, lib_tape, lib_drive = tape_library.are_tape_and_drive_in_the_same_lib(
        vid,
        drive_name,
        logger
    )
    if not same_lib:
        logger.error(
            f"Different libraries (drive={lib_drive} | tape={lib_tape})"
        )
        return False
    logger.debug(f"Same library ({lib_tape}): OK")

    tape_ready = check_tape_is_mount_ready(vid, logger)
    if not tape_ready:
        logger.error(
            f"Tape {vid} is not in the correct state for a mount, aborting mount."
        )
        return False

    return mount_tape()


def do_unmount(vid: str, drive_name: str, config: CtaOpsConfig, logger: Logger = None):
    """
    Unmount the tape presently inside the drive.
    If no tape VID is specified, then any tape inside the selected drive is unmounted.
    :param vid: String, the VID of the tape to unmount, None to detect present
    :param drive_name: String, the name of the drive to check, None to select first
    :param config: CtaOpsConfig object
    :param logger: A Python logging logger
    :returns: True if unmounted state achieved (unmount performed or no action),
    otherwise False
    """
    def unmount_sys_checks():
        """
        Check if the system is ready to perform a drive unmount.
        :returns: True if system in ready state, otherwise False
        """
        daemons_ready = tape_drive.check_drive_mount_daemons(logger)
        if not daemons_ready:
            logger.error(
                "Can't unmount without mount/unmount daemon"
            )
            return False
        if not drive_name:
            logger.error("No drive name specified, cannot unmount!")
            return False
        if not drive:
            logger.error(
                f"Drive {drive_name} not found in set of locally configured " \
                "drives."
            )
            return False
        return True

    def eject_tape():
        """
        Ensure there is no unmounted, but un-ejected, tape present.
        :returns: True if there is no ejected tape in the drive, otherwise False
        """
        if not has_ejected_tape(drive, logger):
            device = tape_drive.get_local_drive(
                drive_name = drive_name,
                config = config,
                logger = logger
            )['DriveDevice']
            cmd = f"{tapeadmin.mt} -f {device} eject"
            logger.debug(
                f"Tape drive {drive_name} has the tape " \
                f"{get_tape_vid_inside_drive(drive, logger)} "
                "still inside. Ejecting..."
            )
            eject = cmd_utils.run_cmd(cmd, logger=logger)
            if eject.returncode != 0:
                logger.error(f"Ejection failed with return code {eject.returncode}")
                return False
            return True
        else:
            logger.debug(
                f"Tape drive {drive_name} has the tape " \
                f"{get_tape_vid_inside_drive(drive, logger)} " \
                "in an EJECTED state."
            )
            return True

    def unmount_tape():
        """
        Physically move the tape away from the drive.
        May only be performed on supported libraries.
        """
        cmd = f'{tapeadmin.cta_smc} -d -D {tape_drive.get_ordinal(drive)} ' \
            '2>/dev/null'
        cmd_utils.run_cmd(cmd, number_of_attempts=3, logger=logger)
        if tape_drive.is_drive_free(drive):
            logger.info(
                f'Unmount of tape {vid} from drive {drive_name}: OK'
            )
            return True
        else:
            logger.info(
                f"Unmount of tape {vid} inside drive {drive_name}: ERROR"
            )
            return False

    drive = tape_drive.get_local_drive(drive_name, config, logger)

    if not unmount_sys_checks():
        return False

    # Unmounts on an empty drive take 2 minutes to result in an error.
    # This test is much faster
    if tape_drive.is_drive_free(drive):
        logger.debug(
            f"There is no tape inside {drive_name}: Drive is FREE"
        )
        return True

    # Ensure that we have the right tape VID
    if vid is None:
        vid = get_tape_vid_inside_drive(drive, logger)
        logger.debug(
            f"No vid specified, taking the first one found in the drive: {vid}"
        )

    else:
        if get_tape_vid_inside_drive(drive, logger) != vid:
            logger.warning(
                f"The specified tape {vid} is not in the specified drive {drive_name}"
            )
            return False

    # We are now ready to perform the eject of the tape
    if not eject_tape():
        return False

    return unmount_tape()


def is_tape_empty(volume_id:str):
    """
    Checks if a tape volume is empty
    (OF FILES THAT WE DON'T CARE ABOUT, not physically empty)
    and has been properly reclaimed
    :param volume_id: The tape volume ID
    :return: True if "logically" empty, not full and occupancy and lastFseq
    counters at 0. False otherwise
    """
    cmd = f'{tapeadmin.cta_admin_tape_json} ls --vid {volume_id}'
    cmd_call = cmd_utils.run_cmd(cmd)
    try:
        array_json = json.loads(cmd_call.stdout)
    except json.decoder.JSONDecodeError:
        log_utils.abort(
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    not_full = not array_json[0]['full']
    not_occupied = int(array_json[0]['occupancy']) == 0
    no_fseq = int(array_json[0]['lastFseq']) == 0
    no_files = int(array_json[0]['nbMasterFiles']) == 0

    return not_full and not_occupied and no_fseq and no_files


def is_tape_write_protected(device_path: str, logger: Logger):
    """
    Checks if the WR_PROT flag is set for a locally mounted tape.
    For the status (RDONLY, ...) see get_tape_info().
    :param device_path: Tape drive device (/dev/nstX) where the tape is mounted.
    :param config: CtaOpsConfig object
    :param logger: A Python logging logger
    :return: True if tape is write protected; False otherwise
    """
    logger.debug(
        f"Checking if the tape mounted at {device_path} is write protected."
    )
    cmd = f'{tapeadmin.mt} -f {device_path} stat'
    mt_call = cmd_utils.run_cmd(cmd, logger=logger)
    return 'WR_PROT' in mt_call.stdout


def tape_queryvolume(vid: str, logger: Logger, skip_simple_errors: bool = False):
    """
    Determines the location of a tape (previously,
    this function was the module called tape-queryvolume).
    :param vid: The volume ID of the tape we want to locate
    :param logger: A Python logging logger
    :param skip_simple_errors: Used by some scripts that don´t want to
        abort when an error occurs, but skip it
    :return: Location of the tape
        ('DRIVE ([element address])' || 'HOME' || 'UNKNOWN ([message])'
    """
    lib = None
    tape_info = get_tape_info(vid, logger)

    if not tape_info:
        logger.warning(f'No info found for tape {vid} using cta-admin.')
        return None

    if 'logicalLibrary' in tape_info.keys():
        lib = tape_info['logicalLibrary']
    else:
        log_utils.log_and_exit(
            logger,
            f'Library not found for tape "{vid}" in cta-admin tape.'
        )

    location = None
    cmd = f'{tapeadmin.cta_smc_query_json} V -V {vid}'
    cmd_call = cmd_utils.run_cmd(cmd, number_of_attempts=3, logger=logger)

    try:
        json_result_list = json.loads(cmd_call.stdout)
    except json.decoder.JSONDecodeError:
        log_utils.log_and_exit(
            logger,
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    if not json_result_list or json_result_list[0]['vid'] != vid:
        msg = f'No info found for tape "{vid}" in cta-smc.'
        if not skip_simple_errors:
            log_utils.log_and_exit(logger, msg)
        else:
            logger.error(msg)

    json_result = json_result_list[0]
    drive_element_address = json_result['elementAddress']
    tape_location = json_result['elementType']

    # Analyze the output
    if tape_location == 'drive':
        location = f'DRIVE (element address = {str(drive_element_address)})'
    elif tape_location == 'slot':
        location = 'HOME'
    else:
        location = f'UNKNOWN ({tape_location})'

    logger.debug(
        f"VID={vid} LIBRARY={lib} LOCATION={location} " \
        f"SLOT={str(drive_element_address)}"
    )

    return location


def has_ejected_tape(drive: dict, logger: Logger):
    """
    Checks if the drive has an ejected tape inside.
    :param drive: The drive dict, as given by cta-ops-drive-config-generate
    :param logger: Initialized logging.logger object
    :return: True if tape is present and ejected; False otherwise
    """
    device = drive['DriveDevice']
    cmd = f'{tapeadmin.mt} -f {device} stat'
    cmd_output = cmd_utils.run_cmd(
        cmd = cmd,
        logger = logger
    ).stdout
    return 'DR_OPEN' not in cmd_output and 'ONLINE' not in cmd_output


def label_tape(vid: str, drive_name: str, config: CtaOpsConfig, logger: Logger, old_label: str= None):
    """
    Write a label to a given tape, overwriting any existing ones.
    :param vid: The Volume ID of the tape
    :param drive_name: Name string of the drive to use for the mount
    :param logger: Initialized logging.logger object
    :param force: (Optional) Run the CTA label command with the --force option
    :returns: True if success, otherwise False, as well as a reason for failure.
    """
    # Check if tape exists
    tape_info_dict = get_tape_info(vid, logger)
    if not tape_info_dict:
        msg = f"No tape info available for {vid}, wrong volume ID?"
        logger.warning(msg)
        return False, msg

    # Tape must be ACTIVE (i.e. not DISABLED nor BROKEN)
    if (
            (tape_info_dict['state'].upper() != 'ACTIVE') or
            (tape_info_dict['stateReason'])
    ):
        msg = f"Tape {vid} state is: {tape_info_dict['state']} " \
                f"and the state reason is: {tape_info_dict['stateReason']} " \
                f"- it can't be labeled - the state has to be ACTIVE " \
                f"without any reason."
        logger.error(msg)
        return False, "Tape is not ACTIVE and/or there is a state reason set."

    # Is pool name OK?
    pool_name = tape_pool.get_tape_pool(vid)
    if not tape_pool.is_pool_name_ok_to_label(pool_name, config, logger):
        logger.error(
            f"Pool name '{pool_name}' is not accepted. " \
            "Valid prefixes are: " \
            f"{str(config.get('tape', 'pool_prefixes_ok_for_labeling'))}"
        )
        msg = f"""Pool name "{pool_name}" for tape [{vid}] not accepted."""
        return False, msg

    # Are tape and drive in the same lib?
    same_lib, lib_tape, lib_drive = tape_library.are_tape_and_drive_in_the_same_lib(
        vid,
        drive_name,
        logger
    )
    if same_lib:
        lib = tape_drive.get_library(drive_name, config, logger)
        logger.info(
            f"Same library ({lib}): OK"
        )
    else:
        msg = f"Different libraries (drive={lib_drive} | " \
            f"tape={lib_tape}): skipping ..."
        logger.error(msg)
        return False, msg

    # Is tape in HOME slot?
    location = tape_queryvolume(
        vid = vid,
        logger = logger,
        skip_simple_errors = False
    )
    if location != 'HOME':
        msg = f"Tape {vid} is not in the HOME slot. Its location is: {location}"
        logger.error(msg)
        return False, msg

    # Is tape empty and properly reclaimed?
    if not is_tape_empty(vid):
        msg = f"Tape {vid} is not empty(nbMasterFiles not at 0) " \
                f"or not reclaimed " \
                "(occupancy or lastFseq is not 0 or the tape is full)"
        logger.error(msg)
        return False, "Tape not empty or not reclaimed"

    # All checks passed, so label the tape
    logger.info(f"Proceeding to label tape {vid}...")

    cmd = f'{tapeadmin.cta_tape_label} --vid {vid} --drive {drive_name} --debug'
    if old_label:
        cmd += f' --oldlabel {old_label}'
    logger.info(f"Running command: {cmd}")

    cta_tape_label_returncode = cmd_utils.run_cmd_real_time_stdout(cmd,
        lambda line: logger.info(line)
    )

    if cta_tape_label_returncode != 0:
        msg = f"tape-label of tape {vid} failed when executing " \
            f"{tapeadmin.cta_tape_label} command."
        return False, msg
    logger.info(f"Labeled tape {vid}.")

    # Remove the tape encryption key ID
    if tape_info_dict['encryptionKeyName'] != "-":
        logger.info(f"Removing encryption key id from tape {vid}.")
        cmd = f'{tapeadmin.cta_admin_json_tape_ch} --vid {vid} -k \'\''
        if cmd_utils.run_cmd(cmd).returncode != 0:
            logger.error('Failed to execute command: {cmd}')

    return True, None
