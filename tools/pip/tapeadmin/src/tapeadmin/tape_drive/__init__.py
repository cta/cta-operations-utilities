# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
tape_drive:
Helpers for interacting with magnetic tape drives.
"""

import re
import json

from pathlib import Path

import tapeadmin

from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig
from logging import Logger

IBM_DRIVE_REGEX = "^Log page code=0xd,0x1, DS=0, SPF=1, page_len=0x"
ENV_VAL_MATCH_REGEX = r"^\s+00\s+(\w\w\s+){8}\s+\w\w\s+(\w\w)\s+"


def get_local_drives(config: CtaOpsConfig, logger: Logger):
    """
    Identify locally configured tape drives.
    :param config: CtaOpsConfig object
    :param logger: A Python logging logger
    :return: List of drive dictionaires
    """
    logger.debug("Looking up tape drives configured on this host.")
    drives_info = []

    drive_data_path = Path(config.get('tape', 'drive_facts'))
    if not cmd_utils.effectively_readable(drive_data_path):
        log_utils.log_and_exit(
            logger,
            f"Could not read drive information from {drive_data_path} !" \
            "Ensure that the file exists and is readable for this operation."
        )
    with open(drive_data_path, 'r', encoding='utf-8') as drive_file:
        drive_data = json.load(drive_file)
        if 'tape_drives' not in drive_data:
            log_utils.log_and_exit(
                logger,
                "Could not find 'tape_drives' key in the drive info file at" \
                f"{drive_data_path} . Is it formatted correctly?"
            )
        else:
            drives_info = drive_data['tape_drives']

    return drives_info


def get_local_drive(drive_name: str, config: CtaOpsConfig, logger: Logger):
    """
    Fetch drive configuration for a particular drive.
    :param drive_name: Drive name str matching config name
    :param config: CtaOpsConfig object
    :param logger: A Python logging logger
    :return: Dict containing selected drive config if found, otherwise None
    """
    local_drives = get_local_drives(config, logger)

    drive = None
    for drive_dict in local_drives:
        if drive_dict['DriveName'] == drive_name:
            drive = drive_dict

    return drive


def get_ordinal(drive:dict):
    """
    Gets the ordinal number of an IBM drive (the number in e.g. smc31)
    :param drive: The drive dict, as given by cta-ops-drive-config-generate
    :return: The ordinal device number: last 2 digits of the name
    """
    drive_ordinal_number = None
    pattern = re.compile('^smc(\d+)')
    control_path = drive['DriveControlPath']

    if re.match(pattern, control_path):
        drive_ordinal_number = pattern.match(control_path).group(1)

    return drive_ordinal_number

def get_drive_read_speed():
    """
    Returns the read speed of the cta drives in MBytes/s
    :return: The drivers read speed
    """
    return 300  # TODO we have no way of knowing the drive read speed


def get_drive_info(logger: Logger):
    """
    Fetch the name of a drive's library, itself, and its host.
    :param logger: A Python logging logger
    :return: Tuple of strings (library_name, drive_name, host_name)
    """
    cmd_call = cmd_utils.run_cmd(
        tapeadmin.cta_admin_json_tapedrive_info_command,
        logger = logger

    )
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            "Unable to get drive info from command " \
            f"{tapeadmin.cta_admin_json_tapedrive_info_command}"
        )
    drive_json = json.loads(cmd_call.stdout)[0]
    return (
        drive_json['logicalLibrary'],
        drive_json['driveName'],
        drive_json['host']
    )


def get_drive_model(drive_device: str, logger: Logger, timeout: int = None):
    """
    Returns the drive model, as written by lsscsi.
    :param drive_device: The path to the drive device (/dev/nstX)
    :param logger: A Python logging logger
    :param timeout: External command timeout in seconds
    :return: The drive model as a str
    """
    get_drive_model_command = f'''{tapeadmin.lsscsi} -g | ''' \
        f'''{tapeadmin.grep} tape | ''' \
        f'''{tapeadmin.grep} {drive_device} | ''' \
        ''' awk '{print $4}' '''

    model_call = cmd_utils.run_cmd(
        cmd = get_drive_model_command,
        timeout = timeout,
        logger = logger
    )

    if model_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            f"Could not get model for drive device {drive_device} " \
            f"using {get_drive_model_command}"
        )

    return model_call.stdout.rstrip()


def get_drive_firmware(drive_device: str, logger: Logger, timeout: int = None):
    """
    Returns the drive firmware, as written by lsscsi.
    :param drive_device: The path to the drive device (/dev/nstX)
    :param logger: A Python logging logger
    :param timeout: External command timeout in seconds
    :return: The drive model as a str
    """
    get_drive_firmware_command = f'''{tapeadmin.lsscsi} -g | ''' \
        f'''{tapeadmin.grep} tape | ''' \
        f'''{tapeadmin.grep} {drive_device} | ''' \
        ''' awk '{print $5}' '''

    model_call = cmd_utils.run_cmd(
        cmd = get_drive_firmware_command,
        timeout = timeout,
        logger = logger
    )

    if model_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            f"Could not get firmware version for drive device {drive_device} " \
            f"using {get_drive_firmware_command}"
        )

    return model_call.stdout.rstrip()


def map_changer_device_to_nst(
    tape_drive: str, serial: str, timeout: int, logger: Logger
):
    """
    Map the tape drive /dev/sgX device to /dev/nstX device.
    :param tape_drive: Drive device string, such as '/dev/sg2'
    :param serial: The tape drive's serial
    :param timeout: External command timeout in seconds
    :param logger: Initialized logging.logger object
    :returns: The drive's corresponding /dev/nstX device, as string
    """
    command = f'{tapeadmin.sg_map} | {tapeadmin.grep} {tape_drive} | ' \
            '''/usr/bin/awk '{print $2}' '''
    try:
        logger.debug(
            f"Executing command {command} with timeout of {timeout} seconds"
        )
        output = cmd_utils.run_cmd(command, timeout=timeout).stdout
    except Exception as error:
        log_utils.log_and_exit(logger, format(error))
    if output and output.startswith('/dev/nst'):
        tape_drive_nst = output.rstrip()
    else:
        # If the device is busy we want puppet to ignore this error and
        # try again later. Thus we exit with 0, as we assume that
        # there is likely still valid output in the destination
        # file from a previous run.
        log_utils.log_and_exit(
            logger,
            "Unable to identify /dev/nstX device for the tape drive " \
            f"{tape_drive} with serial number {serial} with " \
            f"command: {command} {output.rstrip()} Please check why " \
            "the command does not work (drive probably currently busy?) " \
            "and/or retry later ...",
            return_code = 3
        )

    logger.info(
        f"Tape drive {tape_drive} with serial number {serial} " \
        F"is mapped to {tape_drive_nst}"
    )

    return tape_drive_nst


def get_drive_serial(drive_device: str, timeout: int, logger: Logger):
    """
    Extract the serial number for each tape drive.
    :param tape_drive: Drive device string, such as '/dev/sg2'
    :param timeout: External command timeout in seconds
    :param logger: Initialized logging.logger object
    :returns: Tape drive serial string, such as "00078D29F3"
    """

    command = f'{tapeadmin.sg_inq} {drive_device} | ' \
        f'''{tapeadmin.grep} '^ Unit serial number: ' | ''' \
        '''/usr/bin/awk '{print $4}' '''
    try:
        cmd_call = cmd_utils.run_cmd(command, timeout=timeout, logger=logger)
        output = cmd_call.stdout
    except Exception as error:
        log_utils.log_and_exit(logger, format(error))
    if output:
        tape_drive_serial = output.rstrip()
    else:
        log_utils.log_and_exit(
            logger,
            f"Unexpected tape drive serial number: {output}"
        )

    return tape_drive_serial


def is_drive_down(drive: str, logger: Logger):
    """
    Checks if the specified drive is DOWN in cta-admin (the "actual"
    Drive Status column, not the Desired one).
    :param drive: The drive name
    :param logger: Initialized logging.logger object
    :return: True if the drive is DOWN; False otherwise
    """
    if not drive:
        log_utils.log_and_exit(
            logger,
            "Parameter 'drive' not specified for function is_drive_down(drive)."
        )
    cmd = f'{tapeadmin.cta_admin_drive_json} ls {drive}'
    cmd_call = cmd_utils.run_cmd(cmd)
    try:
        drive_status = json.loads(cmd_call.stdout)[0]['driveStatus'].lower()
    except json.decoder.JSONDecodeError:
        log_utils.log_and_exit(
            logger,
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    return drive_status == 'down'


def get_library(drive_name: str, config: CtaOpsConfig, logger: Logger):
    """
    Gets the logical library corresponding to the local drive.
    This functions assumes drive naming convention where the
    drive name is split by a separator, and that the two first parts of the name
    uniquely identify the library.
    Example drive name: IBMLIB3-TS1160-F5C3R1
    Explanation:          lib  - model -  location
    :param drive_name: String, the name of the drive to check
    :param config: CtaOpsConfig object
    :param logger: A Python logging logger
    :return: The drive's logical library name
    """
    drive_to_lib_map = config.get('tape', 'drive_to_lib_map')
    if not drive_name:
        log_utils.log_and_exit(
            logger,
            "No drive specified, cannot fetch corresponding library!"
        )

    separator = drive_to_lib_map['separator']
    # Extract first two components of drive name
    drive_name_root = separator.join(drive_name.split(separator)[0:2])
    try:
        return drive_to_lib_map[drive_name_root]
    except KeyError:
        logger.error(
            f"Could not get logical library corresponding to drive {drive_name}. " \
            f"Please check the configuration file {config.config_path}"
        )
        return None


def check_drive_down_with_reason(drive_name: str, logger: Logger):
    """
    Checks if the corresponding drive has status DOWN, and has a reason specified for being DOWN
    :param drive_name: String, the name of the drive to check
    :param logger: A Python logging logger
    :returns: True if drive correctly put down, else False
    """
    # We get the current drive status and reason
    drive_status_and_reason = get_drive_status_and_reason(drive_name)
    if not drive_status_and_reason:                 # Drive wasn't found
        msg = f"No info found for drive [{drive_name}]"
        log_utils.log_and_exit(logger, msg)
    drive_status = drive_status_and_reason['drive_status']
    msg = f'{tapeadmin.cta_admin_cmd} drive down <DRIVE_NAME> --reason ' \
        '"username (date): script_name + description"\n'
    if drive_status != 'DOWN':  # Drive must be DOWN to perform tape-drivetest
        logger.error(
            f"Drive {drive_name} must have status DOWN, but it's: " \
            f"{drive_status}. Please, use the command:\n{msg}"
        )
        return False
    elif not drive_status_and_reason['reason'].rstrip():
        # Apart from DOWN, the drive must have a "reason"
        logger.error(
            f"Drive {drive_name} is DOWN without reason. " \
            f"Please, specify a reason with the following command:\n{msg}"
        )
        return False
    logger.info(
        f"Drive {drive_name} is DOWN and has a reason " \
        f"({drive_status_and_reason['reason']}) - OK"
    )
    return True


def put_drive_down(drive_name: str, reason: str, logger: Logger):
    """
    Put a drive down in CTA.
    :param drive_name: The name of the drive to disable
    :param reason: The reason why the drive is being put down
    :param logger: A Python logging logger
    :return: The corresponding error or success message after the operation
    """
    # Try to put drive Down
    cmd = f'{tapeadmin.cta_admin_drive_json} down {drive_name} ' \
        f'--reason "{reason}"'
    cta_admin_drive_call = cmd_utils.run_cmd(cmd, logger=logger)

    # Check return code of the command execution
    if cta_admin_drive_call.returncode != 0:
        msg = f"Failed to put DOWN drive {drive_name} with command: {cmd} " \
              f"{log_utils.format_stderr(cta_admin_drive_call.stderr)}"
        logger.error(msg)
        return msg

    # Check "desiredDriveState" field to be DOWN, so drive will be DOWN
    # eventually
    cmd_json_ls_drive = f'{tapeadmin.cta_admin_drive_json} ls {drive_name}'
    desired_drive_status = json.loads(
        cmd_utils.run_cmd(cmd_json_ls_drive).stdout
    )[0]['desiredDriveState']
    if desired_drive_status == 'DOWN':
        msg = f"Drive {drive_name} is being put DOWN (Reason = {reason})"
        logger.info(msg)
    else:
        msg = f"Attempted to put DOWN drive {drive_name}, but desired drive " \
            f"status is still {desired_drive_status}"
        logger.error(msg)

    return msg


def is_drive_free(drive:dict):
    """
    Checks if the specified drive is free using "cta-smc" command
    :param drive: The drive dictionary, as given by cta-ops-drive-config-genrate
    :return: True if the drive is free; False otherwise
    """
    if not drive:
        log_utils.abort(
            'Parameter "drive" not specified for function is_drive_free(drive)'
        )
    cmd = f'{tapeadmin.cta_smc_query_json} D -D {get_ordinal(drive)}'
    cmd_call = cmd_utils.run_cmd(cmd, number_of_attempts=3)
    try:
        drive_info = json.loads(cmd_call.stdout)[0]
    except json.decoder.JSONDecodeError:
        log_utils.abort(
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    return drive_info['status'] == 'free'


def get_drive_status_and_reason(drive:str):
    """
    Gets the status of the specified drive in cta-admin
    (the "actual" Drive Status column, not the Desired one),
    as well as the reason assigned for being DOWN, if it exists
    :param drive: The drive name
    :return: {drive_status: "DOWN"|"UP"|etc. , reason: "some reason"}
    """
    if not drive:
        log_utils.abort(
            'Parameter "drive" not specified for function is_drive_down(drive)'
        )
    cmd = f'{tapeadmin.cta_admin_drive_json} ls {drive}'
    cmd_call = cmd_utils.run_cmd(cmd)
    try:
        drive_info = json.loads(cmd_call.stdout)[0]
        if not drive_info:
            return None
        drive_status = drive_info['driveStatus'].upper()
        reason = drive_info['reason']
    except json.decoder.JSONDecodeError:
        log_utils.abort(
            f'Error executing command: [{cmd}]. ' \
            f'{log_utils.format_stderr(cmd_call.stderr)}'
        )

    return {
        'drive_status': drive_status,
        'reason': reason
    }


def check_drive_mount_daemons(logger: Logger):
    """
    Checks if the necessary daemons for tape-(un)mount are present and running
    locally.
    :param logger: Initialized logging.logger object
    :returns: True if present, otherwise False
    """
    cmd_call = cmd_utils.run_cmd(
        f'ps axwu | {tapeadmin.grep} {tapeadmin.cta_rmcd}'
    )
    # We check in the list of processes if we have the desired one running
    # (we exclude the "grep" itself)
    if any(line for line in cmd_call.stdout.splitlines() if 'grep' not in line):
        logger.debug("Daemon 'cta-rmcd' running - OK")
        return True

    logger.warning(f"Daemon 'cta-rmcd' ({tapeadmin.cta_rmcd}) is not running")
    return False


def get_drive_temperature(device: str, logger: Logger):
    """
    Fetch the drive's sensed temperature, in degrees Celsius.
    :param device: Drive device path string, like "/dev/nst0"
    :param logger: Initialized logging.logger object
    :returns: An integer of the temperature, Celsius
    """
    logger.debug(f"Fetching drive temperature using device {device} .")
    temperature_cmd = f'{tapeadmin.sg_logs} ' \
        f'--page=0x0D,0x01 --control=1 --hex --paramp=0x0000 {device}'
    cmd_call = cmd_utils.run_cmd(temperature_cmd, logger=logger)
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            f"Unable to get drive temperature using command {temperature_cmd} ."
        )
    sg_logs = cmd_call.stdout
    header_line = sg_logs.split("\n")[0]
    if not re.search(IBM_DRIVE_REGEX, header_line):
        log_utils.log_and_exit(
            logger,
            "Unable to get drive temperature: Drive is not IBM."
        )
    sg_lines = sg_logs.split("\n")[1:]
    temp_match_regex = re.compile(ENV_VAL_MATCH_REGEX)
    for line in sg_lines:
        temp_match = re.match(temp_match_regex, line)
        if temp_match:
            return int(temp_match.group(2), 16)
    log_utils.log_and_exit(
        logger,
        "Unable to get drive temperature: " \
        f"No temperature found in output of {temperature_cmd} ."
    )
    return None


def get_drive_humidity(device: str, logger: Logger):
    """
    Fetch the drive's sensed humidity, as a percentage.
    :param device: Drive device path string, like "/dev/nst0"
    :param logger: Initialized logging.logger object
    :returns: An integer between 0 and 100
    """
    logger.debug(f"Fetching drive humidity using device {device} .")
    humidity_cmd = f'{tapeadmin.sg_logs} ' \
        f'--page=0x0D,0x01 --control=1 --hex --paramp=0x0100 {device}'
    cmd_call = cmd_utils.run_cmd(humidity_cmd, logger=logger)
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            f"Unable to get drive humidity using command {humidity_cmd} ."
        )
    sg_logs = cmd_call.stdout
    header_line = sg_logs.split("\n")[0]
    if not re.search(IBM_DRIVE_REGEX, header_line):
        log_utils.log_and_exit(
            logger,
            "Unable to get drive humidity: Drive is not IBM."
        )
    sg_lines = sg_logs.split("\n")[1:]
    humidity_match_regex = re.compile(ENV_VAL_MATCH_REGEX)
    for line in sg_lines:
        humidity_match = re.match(humidity_match_regex, line)
        if humidity_match:
            return int(humidity_match.group(2), 16)
    log_utils.log_and_exit(
        logger,
        f"Unable to get drive humidity: No humidity found in output of {humidity_cmd} ."
    )
    return None
