# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
tapeadmin:
A collection of commands and helper functions for interacting with CTA and
the surrounding infrastructure.
"""

import os

from ctautils import cmd_utils
from ctautils.config_holder import CtaOpsConfig

from logging import Logger


###############
# Package setup
###############
# https://docs.python.org/3/tutorial/modules.html#importing-from-a-package
# Allows for "from tapeadmin import *", which you shouldn't use in prod code,
# but is convenient for testing and debugging.
__all__ = ['tape_medium', 'tape_drive', 'tape_library', 'tape_pool','tape_file']

#####################
# Command definitions
#####################

# Prepare variables for the shell execution
UseSSSIfAvailable=''
keytab_file = '/etc/cta/tape-local.sss.keytab'

# Allow scripts to override which keytab to use by setting the env variable.
# Must be set BEFORE importing TapeAdmin
if 'CTAXrdSecSSSKT' in os.environ:
    keytab_file = os.environ['CTAXrdSecSSSKT']

if os.access(keytab_file, os.R_OK):
    UseSSSIfAvailable = f'XrdSecPROTOCOL=sss XrdSecSSSKT={keytab_file} '

# Allow overwriting the cta-cli config for cta-admin executions
CTA_CLI_CONFIG = '/etc/cta-ops/cta-cli.conf'

base_cta_cmd = '/usr/bin/cta-admin'
base_smc_cmd = '/usr/bin/cta-smc'
base_mt_cmd = '/bin/mt'
base_adler_cmd = '/usr/bin/xrdadler32'
base_rmcd_cmd = '/usr/bin/cta-rmcd'
base_lsscsi_cmd = '/usr/bin/lsscsi'
base_sg_inq_cmd = '/usr/bin/sg_inq'
base_sg_map_cmd = '/usr/bin/sg_map'
base_sg_logs_cmd = '/usr/bin/sg_logs'
base_grep_cmd = '/usr/bin/grep'
base_cta_tape_label_cmd = '/usr/bin/cta-tape-label'

cta_admin_cmd = f'{UseSSSIfAvailable} {base_cta_cmd} --json --config {CTA_CLI_CONFIG}'

cta_admin_tapepool_json         = f'{cta_admin_cmd} tapepool'
cta_admin_archivalroute_json    = f'{cta_admin_cmd} archiveroute'
cta_admin_tape_json             = f'{cta_admin_cmd} tape'
cta_admin_drive_json            = f'{cta_admin_cmd} drive'
cta_admin_json_drive_ls         = f'{cta_admin_cmd} drive ls'
cta_admin_json_pool_ls          = f'{cta_admin_cmd} tapepool ls'
cta_admin_json_tape_ls_all      = f'{cta_admin_cmd} tape ls --all'
cta_admin_json_tape_ls_vid      = f'{cta_admin_cmd} tape ls --vid'
cta_admin_json_tape_ls_tapepool = f'{cta_admin_cmd} tape ls --tapepool'
cta_admin_json_tape_ls_disabled = f'{cta_admin_cmd} tape ls --state DISABLED'
cta_admin_json_tape_ls_broken   = f'{cta_admin_cmd} tape ls --state BROKEN'
cta_admin_json_tape_ch          = f'{cta_admin_cmd} tape ch'
cta_admin_json_library_ls       = f'{cta_admin_cmd} logicallibrary ls'
cta_admin_json_showqueues       = f'{cta_admin_cmd} showqueues'
cta_admin_json_storageclass_ls  = f'{cta_admin_cmd} storageclass ls'
cta_admin_json_archivalroute_ls = f'{cta_admin_cmd} archiveroute ls'
cta_admin_json_tapepool_ls      = f'{cta_admin_cmd} tapepool ls'
cta_admin_json_repack_ls        = f'{cta_admin_cmd} repack ls'
cta_admin_json_repack_ls_vid    = f'{cta_admin_cmd} repack ls --vid'
cta_admin_json_repack_rm_vid    = f'{cta_admin_cmd} repack rm --vid'
cta_admin_json_tapefile_ls_vid  = f'{cta_admin_cmd} tapefile ls --vid'
cta_admin_json_tapedrive_info_command = f'{cta_admin_cmd} dr ls first'
cta_admin_json_tape_files       = f'{cta_admin_cmd} tf'


# Use .format(vid=vid) with these
cta_admin_json_last_read_log       = f'{cta_admin_cmd} tape ls --vid' + ' {vid} | jq -r \'.[] | .lastReadLog.time\''
cta_admin_json_last_write_log      = f'{cta_admin_cmd} tape ls --vid' + ' {vid} | jq -r \'.[] | .lastWrittenLog.time\''

cta_admin_tape_ch_tapepool_quarantine_vid  = f'{cta_admin_cmd} tape ch --tapepool quarantine --vid'
cta_admin_repack_add_tape               = f'{cta_admin_cmd} repack add --vid'
cta_admin_tape_reclaim_vid              = f'{cta_admin_cmd} tape reclaim --vid'
cta_admin_tape_ch                       = f'{cta_admin_cmd} tape ch'

adler32        = f'{base_adler_cmd}'
mt             = f'{base_mt_cmd}'
lsscsi         = f'{base_lsscsi_cmd}'
sg_inq         = f'{base_sg_inq_cmd}'
sg_map         = f'{base_sg_map_cmd}'
sg_logs        = f'{base_sg_logs_cmd}'
grep           = f'{base_grep_cmd}'
cta_tape_label = f'{base_cta_tape_label_cmd}'

# TODO: Specify cta-cli config file once these tools support it. In the mean
# time we specify the instance manually
cta_verify_file_cmd = '/usr/bin/cta-verify-file --instance eosctabogus --id'


cta_smc                         = f'{UseSSSIfAvailable} {base_smc_cmd}'
cta_smc_query_json              = f'{UseSSSIfAvailable} {base_smc_cmd} -j -q'
cta_smc_ioslot_query_json       = f'{UseSSSIfAvailable} {base_smc_cmd} --json -q L'
cta_smc_export_tape             = f'{UseSSSIfAvailable} {base_smc_cmd} -e V -V'
cta_smc_import                  = f'{UseSSSIfAvailable} {base_smc_cmd} --import'

cta_rmcd = f'{base_rmcd_cmd}'  # To check for its daemon

# Identify the SCSI media changer device of the library (if more devices
# detected, only the 1st one is taken for consideration, which assumes that each
# tape server is connected to exactly one tape library).
get_scsi_media_changer_device_cmd = f'''{lsscsi} -g | ''' \
    f'''{grep} '  mediumx ' | ''' \
    '''/usr/bin/awk '{print $7}' | ''' \
    '''/usr/bin/sort --version-sort | /usr/bin/head -1'''

# Identify all tape drive devices connected to the tape server
get_attached_devices = f'''{lsscsi} -g | {grep} '  tape    ' | ''' \
    '''/usr/bin/awk '{print $6, $7}' | /usr/bin/sort --version-sort'''


class CtaAdminCmd:
    """
    Provides a mechanism to configure TapeAdmin commands in a manner that:
    * still allows one to write a legible default here in the library
    * keeps the default accessible to legacy script which don't use the central config file
    If at some point there is no legacy code this could probably be done in a better manner.
    """
    def __init__(self, cmd_string:str, config:CtaOpsConfig):
        # Update SSS key path to use
        configured_keytab = config.get('default_user', 'sss_keytab_file')
        cmd_string.replace(keytab_file, configured_keytab)

        # Update binary/cmd path to use
        configured_cta_cmd = config.get('binaries', 'cta-admin')
        cmd_string = cmd_string.replace(base_cta_cmd, configured_cta_cmd)
        configured_smc_cmd = config.get('binaries', 'cta_smc')
        cmd_string = cmd_string.replace(base_smc_cmd, configured_smc_cmd)
        configured_lsscsi_cmd = config.get('binaries', 'lsscsi')
        cmd_string = cmd_string.replace(base_lsscsi_cmd, configured_lsscsi_cmd)
        configured_sg_inq_cmd = config.get('binaries', 'sg_inq')
        cmd_string = cmd_string.replace(base_sg_inq_cmd, configured_sg_inq_cmd)
        configured_sg_map_cmd = config.get('binaries', 'sg_map')
        cmd_string = cmd_string.replace(base_sg_map_cmd, configured_sg_map_cmd)
        configured_sg_logs_cmd = config.get('binaries', 'sg_logs')
        cmd_string = cmd_string.replace(base_sg_logs_cmd, configured_sg_logs_cmd)
        configured_grep_cmd = config.get('binaries', 'grep')
        cmd_string = cmd_string.replace(base_grep_cmd, configured_grep_cmd)

        # Update cta-admin config file path
        cli_conf_file = config.get('tape', 'cta-admin_config')
        if (
                os.path.isfile(cli_conf_file) and
                cmd_utils.effectively_readable(cli_conf_file)
        ):
            cmd_string = cmd_string.replace(CTA_CLI_CONFIG, cli_conf_file)
        else:
            # Default to proceeding without conf file
            cmd_string = cmd_string.replace('--config', '')
            cmd_string = cmd_string.replace(CTA_CLI_CONFIG, '')

        self.cmd_string = cmd_string

    def __str__(self):
        return self.cmd_string


def configure(config: CtaOpsConfig, logger: Logger = None):
    """
    Configure this library to respect the settings specified in the config file.
    This function must be called by the tool after parsing the config
    :param config: CtaOpsConfig
    :param logger: (Optional) Log to this logger
    """
    if logger is not None:
        logger.debug('Configuring TapeAdmin commands and settings')
    # Configure cta-admin commands
    global cta_admin_cmd, cta_admin_tapepool_json, cta_admin_tape_json,        \
        cta_admin_drive_json, cta_admin_json_drive_ls, cta_admin_json_pool_ls, \
        cta_admin_json_tape_ls_all, cta_admin_json_tape_ls_vid,                \
        cta_admin_json_tape_ls_tapepool, cta_admin_json_tape_ls_disabled,      \
        cta_admin_json_tape_ls_broken, cta_admin_json_tape_ch,                 \
        cta_admin_json_library_ls, cta_admin_json_showqueues,                  \
        cta_admin_json_storageclass_ls, cta_admin_json_archivalroute_ls,       \
        cta_admin_json_tapepool_ls, cta_admin_json_repack_ls,                  \
        cta_admin_json_repack_ls_vid, cta_admin_json_repack_rm_vid,            \
        cta_admin_json_tapefile_ls_vid,       \
        cta_admin_json_tapedrive_info_command, cta_admin_json_tape_files,      \
        cta_admin_json_last_read_log, cta_admin_json_last_write_log,           \
        cta_admin_tape_ch_tapepool_quarantine_vid, cta_admin_repack_add_tape,  \
        cta_admin_tape_reclaim_vid, cta_admin_tape_ch, \
        cta_admin_archivalroute_json

    cta_admin_cmd = str(CtaAdminCmd(cta_admin_cmd, config))
    cta_admin_tapepool_json = str(CtaAdminCmd(cta_admin_tapepool_json, config))
    cta_admin_archivalroute_json = str(CtaAdminCmd(cta_admin_archivalroute_json,config))
    cta_admin_tape_json = str(CtaAdminCmd(cta_admin_tape_json, config))
    cta_admin_drive_json = str(CtaAdminCmd(cta_admin_drive_json, config))
    cta_admin_json_drive_ls = str(CtaAdminCmd(cta_admin_json_drive_ls, config))
    cta_admin_json_pool_ls = str(CtaAdminCmd(cta_admin_json_pool_ls, config))
    cta_admin_json_tape_ls_all = str(CtaAdminCmd(cta_admin_json_tape_ls_all, config))
    cta_admin_json_tape_ls_vid = str(CtaAdminCmd(cta_admin_json_tape_ls_vid, config))
    cta_admin_json_tape_ls_tapepool = str(CtaAdminCmd(cta_admin_json_tape_ls_tapepool, config))
    cta_admin_json_tape_ls_disabled = str(CtaAdminCmd(cta_admin_json_tape_ls_disabled, config))
    cta_admin_json_tape_ls_broken = str(CtaAdminCmd(cta_admin_json_tape_ls_broken, config))
    cta_admin_json_tape_ch = str(CtaAdminCmd(cta_admin_json_tape_ch, config))
    cta_admin_json_library_ls = str(CtaAdminCmd(cta_admin_json_library_ls, config))
    cta_admin_json_showqueues = str(CtaAdminCmd(cta_admin_json_showqueues, config))
    cta_admin_json_storageclass_ls = str(CtaAdminCmd(cta_admin_json_storageclass_ls, config))
    cta_admin_json_archivalroute_ls = str(CtaAdminCmd(cta_admin_json_archivalroute_ls, config))
    cta_admin_json_tapepool_ls = str(CtaAdminCmd(cta_admin_json_tapepool_ls, config))
    cta_admin_json_repack_ls = str(CtaAdminCmd(cta_admin_json_repack_ls, config))
    cta_admin_json_repack_ls_vid = str(CtaAdminCmd(cta_admin_json_repack_ls_vid, config))
    cta_admin_json_repack_rm_vid = str(CtaAdminCmd(cta_admin_json_repack_rm_vid, config))
    cta_admin_json_tapefile_ls_vid = str(CtaAdminCmd(cta_admin_json_tapefile_ls_vid, config))
    cta_admin_json_tapedrive_info_command = str(
        CtaAdminCmd(cta_admin_json_tapedrive_info_command, config)
    )
    cta_admin_json_tape_files = str(CtaAdminCmd(cta_admin_json_tape_files, config))

    cta_admin_json_last_read_log  = str(CtaAdminCmd(cta_admin_json_last_read_log, config))
    cta_admin_json_last_write_log = str(CtaAdminCmd(cta_admin_json_last_write_log, config))

    cta_admin_tape_ch_tapepool_quarantine_vid = str(CtaAdminCmd(cta_admin_tape_ch_tapepool_quarantine_vid, config))
    cta_admin_repack_add_tape = str(CtaAdminCmd(cta_admin_repack_add_tape, config))
    cta_admin_tape_reclaim_vid  = str(CtaAdminCmd(cta_admin_tape_reclaim_vid, config))
    cta_admin_tape_ch = str(CtaAdminCmd(cta_admin_tape_ch, config))

    # Configure external commands
    global adler32, mt, cta_smc, cta_smc_query_json, cta_smc_ioslot_query_json, \
    cta_smc_export_tape, cta_smc_import, cta_rmcd, \
    cta_tape_label, get_scsi_media_changer_device_cmd, \
    get_attached_devices, lsscsi, sg_inq, sg_map, sg_logs, grep

    adler32 = config.get('binaries', 'adler32', default=base_adler_cmd)
    mt = config.get('binaries', 'mt', default=base_mt_cmd)
    lsscsi = config.get('binaries', 'lsscsi', default=base_lsscsi_cmd)
    sg_inq = config.get('binaries', 'sg_inq', default=base_sg_inq_cmd)
    sg_map = config.get('binaries', 'sg_map', default=base_sg_map_cmd)
    sg_logs = config.get('binaries', 'sg_logs', default=base_sg_logs_cmd)
    grep = config.get('binaries', 'grep', default=base_grep_cmd)
    cta_tape_label = config.get('binaries', 'cta_label', default=cta_tape_label)

    cta_smc                   = str(CtaAdminCmd(cta_smc, config))
    cta_smc_query_json        = str(CtaAdminCmd(cta_smc_query_json, config))
    cta_smc_ioslot_query_json = str(CtaAdminCmd(cta_smc_ioslot_query_json, config))
    cta_smc_export_tape       = str(CtaAdminCmd(cta_smc_export_tape, config))
    cta_smc_import            = str(CtaAdminCmd(cta_smc_import, config))

    cta_rmcd = config.get('binaries', 'cta_rmcd', default=base_rmcd_cmd)

    get_scsi_media_changer_device_cmd = str(CtaAdminCmd(
        get_scsi_media_changer_device_cmd, config))
    get_attached_devices = str(CtaAdminCmd(get_attached_devices, config))
