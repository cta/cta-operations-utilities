# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
tape_library:
Helpers for working with magnetic tape libraries, both physical and logical.
"""

import json

import tapeadmin

from tapeadmin import tape_medium
from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig
from logging import Logger


def safe_lib_to_pool_map(
    logical_library: str,
    mediatype: str,
    config: CtaOpsConfig,
    logger: Logger,
    vid: str = None,
):
    """
    Access the library to tapepool mapping dict in a manner which
    gives clearer errors.
    :param logical_library: Name of the logical library to look up
    :param mediatype: Name of the tape media model
    :param config: CtaOpsConfig with the nested dict priority mapping
    :param logger: A Python logging logger
    :param vid: (Optional) Add a to the log line for easier troubleshooting
    :returns: Pool postfix string if found, else None
    """
    ltp_map = config.get('tape', 'lib_to_pool_map')
    vid_str = ''
    if vid is not None:
        vid_str = f"VID '{vid}'"
    try:
        mediatypes = list(ltp_map[logical_library].keys())
    except KeyError:
        logger.warning(
            f"Could not find logical library mapping  for {vid_str}, "
            f"library '{logical_library}'. "
            f"Available options are: {list(ltp_map.keys)}"
        )
        return None

    try:
        postfix = ltp_map[logical_library][mediatype]
    except KeyError:
        logger.warning(
            f"Could not find tape pool mapping for {vid_str}, "
            f"media type '{mediatype}', library '{logical_library}', "
            f"tape pool ''. Available options are {mediatypes}. "
            f"Please check the configuration file {config.config_path}"
        )
        return None

    return postfix


def are_tape_and_drive_in_the_same_lib(volume_id: str, drive: str, logger: Logger):
    """
    Checks if a given tape and drive are in the same logical library
    :param volume_id: The volume ID
    :param drive: The drive name
    :param logger: A Python logging logger
    :return: True/False if same/different library, the tape library,
        the drive library
    """
    logger.debug(
        f"Checking if tape {volume_id} and local drive are in the same lib..."
    )

    tape_info = tape_medium.get_tape_info(volume_id, logger)

    if not tape_info:
        log_utils.abort(f'Not info found for tape "{volume_id}" in cta-admin')

    cmd = f'{tapeadmin.cta_admin_drive_json} ls {drive}'
    cmd_call = cmd_utils.run_cmd(cmd)
    try:
        drive_info_array = json.loads(cmd_call.stdout)
    except json.decoder.JSONDecodeError:
        log_utils.log_and_exit(
            logger,
            f"Error executing command: [{cmd}]. " \
            f"{log_utils.format_stderr(cmd_call.stderr)}"
        )

    if not drive_info_array:
        log_utils.log_and_exit(
            logger,
            f"Not info found for drive {drive} in cta-admin"
        )

    drive_info = drive_info_array[0]

    lib_drive = drive_info['logicalLibrary']
    lib_tape = tape_info['logicalLibrary']

    return lib_tape == lib_drive, lib_tape, lib_drive


def get_library_type(scsi_media_changer: str, timeout: int, logger: Logger):
    """
    Identify the tape library type from the serial number of the SCSI media
    changer device.
    :param scsi_media_changer: Media changer name string such as "/dev/sg3"
    :param timeout: External command timeout in seconds
    :param logger: (Optional) Initialized logging.logger object
    :returns: Library name string, such as "IBM-03584L32-0000078BB2840401"
    """
    command = f'{tapeadmin.sg_inq} {scsi_media_changer} | ' \
        '/usr/bin/egrep ' \
        "'^ (Unit serial number:|Vendor identification:|Product identification:) ' " \
        "| /usr/bin/cut -f2 -d : | /usr/bin/xargs | /usr/bin/tr ' ' '-'"
    try:
        cmd_call = cmd_utils.run_cmd(command, timeout=timeout, logger=logger)
        output = cmd_call.stdout
    except Exception as error:
        log_utils.log_and_exit(logger, format(error))
    if output and (output.startswith('SPECTRA-PYTHON-') or output.startswith('IBM-03584L')):
        tape_library = output.rstrip()
    else:
        log_utils.log_and_exit(logger, f"Unexpected tape library type: {output}")

    logger.info(
        f"Detected tape library type and the serial number: {tape_library}"
    )

    return tape_library
