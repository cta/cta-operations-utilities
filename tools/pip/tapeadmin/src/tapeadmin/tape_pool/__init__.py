# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
tape_pool:
Helpers for working with tape pools, which are logical groupings of tape.
"""

import json

import tapeadmin

from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig
from logging import Logger

def pool_exists(pool:str):
    """
    Check if a pool with the provided name exists
    :param pool: A pool name string
    :return: True if it exists, False otherwise
    """
    cmd = f"{tapeadmin.cta_admin_json_tapepool_ls} --name {pool}"
    cmd_call = cmd_utils.run_cmd(cmd)
    if cmd_call.returncode == 0:
        return True
    return False


def get_tape_pool(volume_id: str):
    """
    Gets the pool of the specified tape
    :param volume_id: The volume ID of the tape
    :return: The pool name
    """
    cmd = f'{tapeadmin.cta_admin_tape_json} ls --vid {volume_id}'
    cmd_call = cmd_utils.run_cmd(cmd)
    try:
        result_json_array = json.loads(cmd_call.stdout)
    except json.decoder.JSONDecodeError:
        log_utils.abort(f"Error executing command: [{cmd}]. {log_utils.format_stderr(cmd_call.stderr)}")

    if not result_json_array:
        log_utils.abort(f"No info found for the specified tape [{volume_id}] in cta-admin")

    return result_json_array[0]['tapepool']


def is_pool_name_ok_to_label(pool_name: str, config: CtaOpsConfig, logger: Logger):
    """
    Checks if a pool name is valid for its tape to be labeled
    :param pool_name: The pool name as a string
    :param config: CtaOpsConfig object
    :param logger: Initialized logging.logger object
    :return: True if pool name is ok for labeling; False otherwise
    """
    if not pool_name:
        logger.warning(
            "No pool name supplied to ok-for-label check."
        )
        return False
    for prefix in config.get('tape', 'pool_prefixes_ok_for_labeling'):
        if pool_name.startswith(prefix):
            return True
    return False
