# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Repack automation utilities.
Contains classes and functions used by both the automation tools and the
operator tools.
"""

import sys
import json

from datetime import datetime
from time import sleep

import tapeadmin

from ctautils import log_utils, sql_utils, cmd_utils
from tapeadmin import tape_medium, tape_library

###############
# Configuration
###############

ALLOWED_MODES = ['manual', 'auto', 'error']

REPACK_STAGE_NAMES = {
    1: 'Entered',
    2: 'Started',
    3: 'Repacked',
    4: 'Quarantined',
    5: 'Reclaimed',
    6: 'Finished'
}
N_REPACK_STAGES = len(REPACK_STAGE_NAMES)

DATE_FORMAT='%Y-%m-%d %H:%M'

# Describe the table we'll use, with the key being the name of the fields
# in the code, and the rest being expected table metadata.
# This is used to check the DB validity, and to allow easy future DB changes.
REF_TABLE = {  # TODO: Make config option?
    'tape': {
        'name': 'tape', 'type': 'varchar', 'col_num':0
    },
    'entered': {
        'name': 'entered', 'type': 'timestamp', 'col_num':1
    },
    'started': {
        'name': 'started', 'type': 'timestamp', 'col_num':2
    },
    'repacked': {
        'name': 'repacked', 'type': 'timestamp', 'col_num':3
    },
    'quarantined': {
        'name': 'quarantined', 'type': 'timestamp', 'col_num':4
    },
    'reclaimed': {
        'name': 'reclaimed', 'type': 'timestamp', 'col_num':5
    },
    'finished': {
        'name': 'finished', 'type': 'timestamp', 'col_num':6
    },
    'files': {
        'name': 'files', 'type': 'int8', 'col_num':7
    },
    'bytes': {
        'name': 'bytes', 'type': 'int8', 'col_num':8
    },
    'usage': {
        'name': '"usage"', 'type': 'int2', 'col_num':9
    },
    'last_write_date': {
        'name': 'last_write_date', 'type': 'timestamp', 'col_num':10
    },
    'mode': {
        'name': '"mode"', 'type': 'automation_modes', 'col_num':11
    },
    'priority': {
        'name': 'priority', 'type': 'int2', 'col_num':12
    },
    'mediatype': {
        'name': 'mediatype', 'type': 'varchar', 'col_num':13
    },
    'tapepool': {
        'name': 'tapepool', 'type': 'varchar', 'col_num':14
    },
    'responsible': {
        'name': 'responsible', 'type': 'varchar', 'col_num':15
    }
}
# TODO: Sort?
TABLE_FIELDS_STR = ', '.join([col['name'] for col in REF_TABLE.values()])


#########
# Classes
#########

class Repack:
    """
    A single repack item, represented by a row in the REPACK_STATUS table.
    Remember to run save() after modifying a property directly in order to write back
    to the DB.
    """
    def __init__(self, tape_tuple=None, tape:str=None, mode:str=None,
                 priority:int=None, db_connection=None, config=None,
                 logger=None, short=False):
        """
        A representation of a repack in the system.
        Initialize from a db tuple, or a tape VID.
        :param tape_tuple: A tuple of values, as returned from the DB
        :param tape: The tape volume ID
        :papam mode: (optional) 'manual' or 'auto' mode for repack management
        :param priority: (Optional) The repack's priority for CTA queueing.
            Int with higher number => higher priority
        :param db_connection: Database connection object from sql_utils.connect
        :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
        :param logger: (Optional) A Python logging logger
        :param short: (Optional) Display with reduced number of fields
        """
        self.config = config
        self.logger = logger

        # Declare vars for overridable values
        self.last_write_date = None
        self.n_files = None
        self.n_bytes = None
        self.usage = None
        self.mediatype = None
        self.tapepool = None
        self.status = None
        self.mode = 'auto'
        self.priority = int(self.config.get('default_priority'))

        # Allow overrides using supplied values,
        # priority loaded_db_values < tuple_values < arg_values
        if tape_tuple:
            self.vid = tape_tuple[REF_TABLE['tape']['col_num']]
            self.status = AutomationStatus(
                repack = self,
                date_entered = tape_tuple[REF_TABLE['entered']['col_num']],
                date_started = tape_tuple[REF_TABLE['started']['col_num']],
                date_repacked = tape_tuple[REF_TABLE['repacked']['col_num']],
                date_quarantined = tape_tuple[REF_TABLE['quarantined']['col_num']],
                date_reclaimed = tape_tuple[REF_TABLE['reclaimed']['col_num']],
                date_finished = tape_tuple[REF_TABLE['finished']['col_num']]
            )
            self.n_files = tape_tuple[REF_TABLE['files']['col_num']]
            self.n_bytes = tape_tuple[REF_TABLE['bytes']['col_num']]
            self.usage = tape_tuple[REF_TABLE['usage']['col_num']]
            self.last_write_date = tape_tuple[REF_TABLE['last_write_date']['col_num']]
            self.mode = tape_tuple[REF_TABLE['mode']['col_num']]
            self.priority = tape_tuple[REF_TABLE['priority']['col_num']]
            self.mediatype = tape_tuple[REF_TABLE['mediatype']['col_num']]
            self.tapepool = tape_tuple[REF_TABLE['tapepool']['col_num']]
            self.responsible = tape_tuple[REF_TABLE['responsible']['col_num']]
            self.in_db = True

        elif tape:
            self.vid = tape
            # Attempt to load existing values in DB
            if db_connection:
                self.in_db = self.load(db_connection)
            else:
                self.in_db = False
        else:
            log_utils.log_and_exit(
                logger,
                "Either pass a tape VID, or a tuple corresponding to a DB row"
            )

        # Set for the first time
        if not self.status:
            self.status = AutomationStatus(repack=self)
        if not self.in_db:
            tape_info = tape_medium.get_tape_info(self.vid, logger=logger)
            if tape_info is None:
                log_utils.log_and_exit(
                    logger,
                    f"Could not find tape with VID {self.vid} in CTA!"
                )
            self.n_files = int(tape_info['nbMasterFiles'])
            self.n_bytes = int(tape_info['masterDataInBytes'])
            capacity = int(tape_info['capacity'])
            if 'lastWrittenLog' in tape_info:
                self.last_write_date = datetime.fromtimestamp(
                    int(tape_info['lastWrittenLog']['time'])
                )
            self.mediatype = tape_info['mediaType']
            self.tapepool = tape_info['tapepool']
            if capacity is not None:
                self.usage = 100 * round(self.n_bytes / capacity, 2)
            else:
                self.usage = 0
            self.responsible = cmd_utils.get_username()

        self.mode = mode if mode else self.mode
        self.priority = priority if priority else self.priority

        # Automation user may be the responsible of the repack if it initiated
        # it, but otherwise it is always the last human operator who modified it.
        automation_user = config.get('default_user', 'name')
        present_user = cmd_utils.get_username()
        if present_user != automation_user:
            self.responsible = present_user

        # Set meta fields
        self.short = short

    def check_in_system(self, db_connection):
        """
        Checks if a given tape is known to the repack automation system.
        In other words, is it in the DB?
        :param db_connection: Database connection object from sql_utils.connect
        :param tape: The tape volume ID
        :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
        :param logger: (Optional) A Python logging logger
        :return: A boolean indicating whether or not there is an entry
        """
        query =f'''
            SELECT {REF_TABLE['tape']['name']}
            FROM {self.config.get('database', 'repack_table_name')}
            WHERE {REF_TABLE['tape']['name']} = %(tape)s
        '''
        result = sql_utils.execute_query(
            db_connection = db_connection,
            query = query,
            data = {
                'tape': self.vid
            },
            config = self.config,
            logger = self.logger
        )
        if len(result) >= 1:
            return True

        return False

    def set_mode(self, mode):
        """
        Set the mode of the repack.
        :param db_connection: Database connection object from sql_utils.connect
        :param mode: Specify if the repack should be automated, or operator controlled
        """
        if mode not in ALLOWED_MODES:
            log_utils.log_and_exit(
                self.logger,
                f"Mode needs to be one of {ALLOWED_MODES}"
            )
        self.logger.info(
            f"Changing repack mode of tape {self.vid} to {mode}"
        )
        self.mode = mode

    def set_file_count(self, file_count:int):
        """
        Update the repacks file counter with a new value.
        """
        self.n_files = file_count

    def set_byte_count(self, byte_count:int):
        """
        Update the repacks byte counter with a new value.
        """
        self.n_bytes = byte_count

    def submit_cta_repack(self, db_connection, mountpolicy:str=None):
        """
        Submit a repack of a tape to CTA.
        :param db_connection: Database connection object from sql_utils.connect
        :param mountpolicy: (Optional) Specify MP, otherwise determine from pri
        :returns: True on success, otherwise False
        """
        if not mountpolicy:
            mountpolicy = self.config.get(
                'priorities',
                self.priority,
                'mount_policy'
            )
        cmd = f'{tapeadmin.cta_admin_repack_add_tape} {self.vid} ' \
            f'--mountpolicy {mountpolicy}'
        cmd_call = cmd_utils.run_cmd(cmd, config=self.config, logger=self.logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                self.logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            self.declare_problematic(
                db_connection,
                self.config,
                self.logger,
                "Failed to create CTA repack"
            )
            return False

        self.logger.info(
            f"Repack request for tape {self.vid} successfully submitted."
        )

        # Register the repack start of the tape in the DB
        if self.status.get_date_started() is None:
            self.status.set_date_started()
        self.save(db_connection)

        return True

    def remove_cta_repack(self):
        """
        Remove the repack object in CTA corresponding to this repack.
        :returns: True on success, otherwise False
        """
        self.logger.debug(f"Removing CTA repack for tape {self.vid}")
        rm_cmd = f'{tapeadmin.cta_admin_json_repack_rm_vid} {self.vid}'
        rm_call = cmd_utils.run_cmd(
            rm_cmd,
            config = self.config,
            logger = self.logger
        )
        if rm_call.returncode == 0:
            self.logger.info(
                f"Successfully removed CTA repack for tape {self.vid}"
            )
            return True
        else:
            self.logger.warning(
                f"Failed to remove repack for tape {self.vid} from CTA"
            )
            return False

    def get_cta_repack(self, db_connection, expected=True):
        """
        Fetch the corresponding repack object from CTA.
        :param db_connection: Database connection object from sql_utils.connect
        :param expected: Do we expect to find a repack, or is none fine?
        :returns: Repack object dict, or None if none was found.
        """
        cmd = f'{tapeadmin.cta_admin_json_repack_ls_vid} {self.vid}'
        cmd_call = cmd_utils.run_cmd(cmd, logger=self.logger)
        if cmd_call.returncode != 0:
            if cmd_call.returncode == 1:
                self.logger.error(
                    f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                    "error (returncode 1)"
                )
            elif cmd_call.returncode == 2 and expected:
                self.declare_problematic(
                    db_connection,
                    self.config,
                    self.logger,
                    "Failed to find active repack object"
                )

            return None

        tape_repack_info = json.loads(cmd_call.stdout)[0]
        return tape_repack_info


    def to_dict(self, use_timestamps=False, short=False):
        """
        Return the repack as a dictionary of key-value pairs
        :param use_timestamps: Use epoch timestamps instead of date strings
        :param short: Return a reduced json, similar to the short repack ls mode
        :returns: A dict representing the repack in the system
        """

        priority_name = self.config.get('priorities', self.priority, 'name')

        if use_timestamps:
            if self.last_write_date is not None:
                last_write = int(self.last_write_date.timestamp())
            else:
                last_write = None
        else:
            last_write = str(self.last_write_date)

        if short:
            properties = {
                'tape': self.vid,
                'mediatype': self.mediatype,
                'tapepool': self.tapepool,
                'number_of_files': self.n_files,
                'number_of_bytes': self.n_bytes,
                'usage': self.usage,
                'last_write_date': last_write,
                'mode': self.mode,
                'priority': priority_name,
                'responsible': self.responsible
            }
            status_dict = self.status.to_dict(
                use_timestamps = use_timestamps,
                short = True
            )

            repack_dict = {**properties, **status_dict}
        else:
            properties = {
                'tape': self.vid,
                'mediatype': self.mediatype,
                'tapepool': self.tapepool,
                'number_of_files': self.n_files,
                'number_of_bytes': self.n_bytes,
                'usage': self.usage,
                'last_write_date': last_write,
                'mode': self.mode,
                'priority': priority_name,
                'responsible': self.responsible
            }
            status_dict = self.status.to_dict(
                use_timestamps = use_timestamps,
                short = False
            )

            repack_dict = {**properties, **status_dict}

        return repack_dict

    def to_json(self, short=False):
        """
        Turn this repack object into a json string
        :return: string with object json
        """
        return json.dumps(self.to_dict(short=short, use_timestamps=True))

    def determine_state(self, config, logger):
        """
        Estimate the state of this repack, based on info from cta-admin.
        Used for adding existing repacks to the automation.
        Updates the values of this repack object, remember to save manually.
        :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
        :param logger: (Optional) A Python logging logger
        :return: Boolean for valid state determined or invalid state encountered.
        """
        # Fetch tape info and deduce present state
        tape_cmd = f'{tapeadmin.cta_admin_json_tape_ls_vid} {self.vid}'
        tape_call = cmd_utils.run_cmd(tape_cmd)
        if tape_call.returncode != 0:
            logger.error(
                f"Could not find an existing tape with VID {self.vid}"
            )
            return False
        tape_json = json.loads(tape_call.stdout)[0]

        repack_cmd = f'{tapeadmin.cta_admin_json_repack_ls_vid} {self.vid}'
        repack_call = cmd_utils.run_cmd(repack_cmd)
        if repack_call.returncode != 0:
            logger.error(
                f"Could not find an existing repack for VID {self.vid}"
            )
            return False
        repack_json = json.loads(repack_call.stdout)[0]

        if repack_json['status'] == 'Failed':
            logger.errror(
                "Failed repacks cannot be added to the system."
            )
            return False

        self.n_files = int(repack_json['filesLeftToRetrieve'])
        self.n_bytes = int(repack_json['totalBytesToRetrieve'])
        self.mode = 'auto'
        self.mediatype = tape_json['mediaType']
        self.tapepool= tape_json['tapepool']
        self.short = True  # Placeholder, just in case
        self.priority = int(self.config.get('default_priority'))

        if 'capacity' in repack_json.keys():
            capacity = int(repack_json['capacity'])
            self.usage = 100 * round(self.n_bytes / capacity, 2)
        else:
            self.usage = 0

        if 'lastWrittenLog' in tape_json:
            last_written_timestamp = int(tape_json['lastWrittenLog']['time'])
            self.last_write_date = datetime.fromtimestamp(last_written_timestamp)
        else:
            self.last_write_date = None

        # Here entered time is the time of insertion into ATRESYS, and
        # start time is approximated using the repack object creation time
        entered, started, repacked = datetime.now(), None, None
        quarantined, reclaimed, finished = None, None, None
        if repack_json['status'] == 'Running' or repack_json['status'] =='Complete':
            start_timestamp = int(repack_json['creationLog']['time'])
            started = datetime.fromtimestamp(start_timestamp)

        if repack_json['status'] == 'Complete':
            end_timestamp = int(repack_json['repackFinishedTime'])
            repacked = datetime.fromtimestamp(end_timestamp)

            # Approximate times, since we don't have exact timestamps
            modification_timestamp = int(tape_json['lastModificationLog']['time'])
            log_lib = tape_json['logicalLibrary']
            pool_postfix = tape_library.safe_lib_to_pool_map(
                logical_library = log_lib,
                mediatype = self.mediatype,
                config = config,
                logger = logger,
                vid = self.vid
            )
            pool_prefix = config.get('priorities', self.priority, 'default_dst_pool_prefix')
            to_label_pool = f'{pool_prefix}{pool_postfix}'

            if tape_json['tapepool'] == 'quarantine' or tape_json['tapepool'] == to_label_pool:
                quarantined = datetime.fromtimestamp(modification_timestamp)
            if not tape_json['full']:
                reclaimed = datetime.fromtimestamp(modification_timestamp)
            if tape_json['tapepool'] == to_label_pool:
                finished = datetime.fromtimestamp(modification_timestamp)

        self.status = AutomationStatus(
            repack = self,
            date_entered = entered,
            date_started = started,
            date_repacked = repacked,
            date_quarantined = quarantined,
            date_reclaimed = reclaimed,
            date_finished = finished
        )

        return True

    def declare_problematic(self, db_connection, config=None,
                            logger=None, reason=""):
        """
        Set the repack into an error mode, such that it is no longer processed
        by the automated system while it waits for an operator to intervene.
        :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
        :param logger: (Optional) A Python logging logger
        :param reason: (Optional) A custom reason to include in the log line
        """
        if config is None:
            config = self.config
        if logger is None:
            logger = self.logger

        if reason != "":
            reason = f"Reason: {reason}"
        logger.warning(
            f"Pausing automation for tape {self.vid}, "
            f"it requires operator intervention! {reason}"
        )
        self.set_mode('error')
        # Check if a CTA repack exists, remove it if it is pending/starting
        if self.status.stage_num <= 2:
            cmd = f'{tapeadmin.cta_admin_json_repack_ls_vid} {self.vid}'
            cmd_call = cmd_utils.run_cmd(cmd, config=config, logger=logger)
            if cmd_call.returncode == 0:
                tape_repack_info = json.loads(cmd_call.stdout)[0]
                if tape_repack_info['status'] in ['Pending', 'Starting']:
                    self.remove_cta_repack()

        self.save(db_connection)

    def set_tape_repacking(self, db_connection, config=None, logger=None,
                           reason:str=None):
        """
        Supervised ACTIVE->REPACKING_PENDING->REPACKING state stransition
        :param db_connection: database connection object from sql_utils.connect
        :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
        :param logger: (Optional) A Python logging logger
        :returns: True if successful, False if it failed
        """
        if config is None:
            config = self.config
        if logger is None:
            logger = self.logger

        if reason is None:
            reason = "ATRESYS: Undergoing automated repack"

        set_state_cmd = f'{tapeadmin.cta_admin_json_tape_ch} --vid ' \
            f'{self.vid} --state REPACKING --reason "{reason}" --full true'
        cmd_call = cmd_utils.run_cmd(set_state_cmd, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {set_state_cmd} resulted in an XRootD, "
                "protobuf or frontend error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            self.declare_problematic(
                db_connection,
                config,
                logger,
                "Failed to change state to REPACKING"
            )
            return False

        logger.info(
            f"Submitted request to set state of {self.vid} to: REPACKING, "
            f"reason set to: '{reason}'"
        )
        repack_pending = True
        cmd = f'{tapeadmin.cta_admin_json_tape_ls_vid} {self.vid}'
        attempt_count = 0
        logger.info(f"Waiting to confirm state change (timeout {20*5}s).")
        while repack_pending:
            cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
            if cmd_call.returncode == 0:
                output_json = json.loads(cmd_call.stdout)[0]
                if output_json['state'] == 'REPACKING':
                    repack_pending = False
                    break
            # Quick work-around for race condition in CTA queue handling.
            # The queue might have been deleted by a tpsrv, so here we
            # re-create it so that the MaintenanceHandler can find it agaun.
            if attempt_count == 18:
                logger.info("Re-trying state change command due to possible CTA race condition.")
                retry_state_call = cmd_utils.run_cmd(
                    set_state_cmd,
                    logger = logger
                )
                if retry_state_call.returncode == 1:
                    log_utils.log_and_exit(
                        logger,
                        f"Command {set_state_cmd} resulted in an XRootD, "
                        "protobuf or frontend error (returncode 1)"
                    )
                elif retry_state_call.returncode == 2:
                    self.declare_problematic(
                        db_connection,
                        config,
                        logger,
                        "Failed to change state to REPACKING"
                    )
                    return False

            if attempt_count >= 20:
                logger.error(
                    f"Timed out while waiting for tape {self.vid} to "
                    "enter REPACKING state."
                )
                self.declare_problematic(
                    db_connection,
                    config,
                    logger,
                    "Timed out during REPACKING_PENDING->REPACK state change"
                )
                break
            attempt_count += 1
            sleep(5)
        if repack_pending:
            return False
        return True

    def retry(self, db_connection, config=None, logger=None):
        """
        If the repack has failed for some reason, reset the state machine
        such that it will be retried one more time by the system.
        :param db_connection: database connection object from sql_utils.connect
        :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
        :param logger: (Optional) A Python logging logger
        :returns: True if restart succeded, else False
        """
        if config is None:
            config = self.config
        if logger is None:
            logger = self.logger

        def set_repacking_and_start(self, logger=logger):
            """
            Put into repacking state and submit repack to CTA.
            """
            repacking = self.set_tape_repacking(db_connection)
            if not repacking:
                logger.error("Failed put tape in REPACKING state")
                return False
            submit = self.submit_cta_repack(db_connection)
            if not submit:
                logger.error("Failed to submit new repack")
                return False
            # Return repack into the automation's care
            self.set_mode('auto')
            self.save(db_connection)

            return True

        logger.debug(f"Retrying repack for tape {self.vid}")

        # If the repack failed before starting, don't bother the frontend
        if self.status.stage_num == 1 and self.mode == 'error':
            return set_repacking_and_start(self)

        # Get CTA repack info
        cmd = f'{tapeadmin.cta_admin_json_repack_ls_vid} {self.vid}'
        cmd_call = cmd_utils.run_cmd(cmd, config=config, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        if cmd_call.returncode == 0:
            cta_repack_info = json.loads(cmd_call.stdout)[0]
            # Repack mode might need to be updated
            if cta_repack_info['status'] == 'Failed':
                self.declare_problematic(db_connection)

            if self.mode == 'error':
                # Delete existing repack
                clean = clean_up_cta_repack(self.vid, logger, force=True)
                if not clean:
                    logger.error("Failed to delete existing CTA repack")
                    return False
                # Reset status timestamps
                self.status.set_date_repacked(None)
                self.status.set_date_quarantined(None)
                self.status.set_date_reclaimed(None)
                self.status.set_date_finished(None)
                self.status.calculate_stage_num()

                return set_repacking_and_start(self)
            else:
                logger.warning(
                    f"Repack for {self.vid} not in failed state, skipping"
                )
        return False

    def load(self, db_connection):
        """
        Load the values of a Repack entry from the repack database.
        :param db_connection: Database connection object from sql_utils.connect
        :return: True if entry found and values set, false otherwise
        """
        query = f'''
            SELECT {TABLE_FIELDS_STR}
            FROM {self.config.get('database', 'repack_table_name')}
            WHERE {REF_TABLE['tape']['name']} = %(tape)s
        '''
        result = sql_utils.execute_query(
            db_connection = db_connection,
            query = query,
            data = {
                'tape': self.vid
            },
            config = self.config,
            logger = self.logger
        )
        if len(result) >= 1:
            first = result[0]

            self.status = AutomationStatus(
                repack = self,
                date_entered = first[REF_TABLE['entered']['col_num']],
                date_started = first[REF_TABLE['started']['col_num']],
                date_repacked = first[REF_TABLE['repacked']['col_num']],
                date_quarantined = first[REF_TABLE['quarantined']['col_num']],
                date_reclaimed = first[REF_TABLE['reclaimed']['col_num']],
                date_finished = first[REF_TABLE['finished']['col_num']]
            )
            self.n_files = first[REF_TABLE['files']['col_num']]
            self.n_bytes = first[REF_TABLE['bytes']['col_num']]
            self.usage = first[REF_TABLE['usage']['col_num']]
            self.last_write_date = first[REF_TABLE['last_write_date']['col_num']]
            self.mode = first[REF_TABLE['mode']['col_num']]
            self.priority = first[REF_TABLE['priority']['col_num']]
            self.mediatype = first[REF_TABLE['mediatype']['col_num']]
            self.tapepool = first[REF_TABLE['tapepool']['col_num']]
            self.responsible = first[REF_TABLE['responsible']['col_num']]

            return True

        return False

    def save(self, db_connection):
        """
        Save a Repack as a row in the repack database.
        :param db_connection: Database connection object from sql_utils.connect
        """
        if self.in_db:
            self.__update(db_connection)
        else:
            self.__insert(db_connection)

    def __update(self, db_connection):
        """
        Update an existing row in the repack database.
        :param db_connection: Database connection object from sql_utils.connect
        """
        query = f'''
            UPDATE {self.config.get('database', 'repack_table_name')} SET
            {REF_TABLE['started']['name']} = %(start)s,
            {REF_TABLE['repacked']['name']} = %(end)s,
            {REF_TABLE['quarantined']['name']} = %(quarantined)s,
            {REF_TABLE['reclaimed']['name']} = %(reclaimed)s,
            {REF_TABLE['finished']['name']} = %(finished)s,
            {REF_TABLE['files']['name']} = %(files)s,
            {REF_TABLE['bytes']['name']} = %(bytes)s,
            {REF_TABLE['usage']['name']} = %(usage)s,
            {REF_TABLE['mode']['name']} = %(mode)s,
            {REF_TABLE['priority']['name']} = %(priority)s,
            {REF_TABLE['mediatype']['name']} = %(mediatype)s,
            {REF_TABLE['tapepool']['name']} = %(tapepool)s,
            {REF_TABLE['responsible']['name']} = %(responsible)s
            WHERE {REF_TABLE['tape']['name']} = %(tape)s
        '''
        data = {
            'start': self.status.get_date_started(),
            'end': self.status.get_date_repacked(),
            'quarantined': self.status.get_date_quarantined(),
            'reclaimed': self.status.get_date_reclaimed(),
            'finished': self.status.get_date_finished(),
            'files': self.n_files,
            'bytes': self.n_bytes,
            'usage': self.usage,
            'mode': self.mode,
            'priority': self.priority,
            'mediatype': self.mediatype,
            'tapepool': self.tapepool,
            'responsible': self.responsible,
            'tape': self.vid
        }

        sql_utils.execute_query(
            db_connection = db_connection,
            query = query,
            data = data,
            commit = True,
            config = self.config,
            logger = self.logger
        )

    def __insert(self, db_connection):
        """
        Inserts a new row into the repack database.
        The NOT NULL fields are mandatory, the rest are optional (default=NULL)
        :param db_connection: Database connection object from sql_utils.connect
        """
        query = f'''
            INSERT INTO {self.config.get('database', 'repack_table_name')}
            (  {TABLE_FIELDS_STR} )
            VALUES( %(tape)s, %(entered)s, %(start)s, %(end)s, %(quarantined)s,
            %(reclaimed)s, %(finished)s, %(files)s, %(bytes)s,
            %(usage)s, %(last_write_date)s, %(mode)s, %(priority)s, %(mediatype)s,
            %(tapepool)s, %(responsible)s )
        '''
        data = {
            'tape': self.vid,
            'entered': self.status.get_date_entered(),
            'start': self.status.get_date_started(),
            'end': self.status.get_date_repacked(),
            'quarantined': self.status.get_date_quarantined(),
            'reclaimed': self.status.get_date_reclaimed(),
            'finished': self.status.get_date_finished(),
            'files': self.n_files,
            'bytes': self.n_bytes,
            'usage': self.usage,
            'mode': self.mode,
            'priority': self.priority,
            'mediatype': self.mediatype,
            'tapepool': self.tapepool,
            'responsible': self.responsible,
            'last_write_date': self.last_write_date
        }

        sql_utils.execute_query(
            db_connection = db_connection,
            query = query,
            data = data,
            commit = True,
            config = self.config,
            logger = self.logger
        )
        pri_text = self.config.get('priorities', self.priority, 'name')
        self.logger.info(
            f"Repack of tape {self.vid} from pool {self.tapepool} has "
            f"been inserted into the database with priority {pri_text}."
        )
        self.in_db = True

    def __str__(self):
        return f"{self.to_dict()}"

    def __get_iter_fields(self):
        """
        Determine which fields we want __iter__, and thereby tabulation,
        to use.
        """
        priority_name = self.config.get('priorities', self.priority, 'name')
        if self.short:
            # We don't need seconds in output of short form
            iter_attrs = [
                self.vid,
                self.status,
                self.mediatype,
                self.tapepool,
                cmd_utils.convert_byte_unit(self.n_bytes),
                f"{int(self.usage)}%",
                self.__get_date_val(self.last_write_date),
                self.mode,
                priority_name,
                self.responsible
            ]
        else:
            iter_attrs = [
                self.vid,
                self.mediatype,
                self.tapepool,
                self.status.get_date_entered(),
                self.status.get_date_started(),
                self.status.get_date_repacked(),
                self.status.get_date_quarantined(),
                self.status.get_date_reclaimed(),
                self.status.get_date_finished(),
                self.n_files,
                cmd_utils.convert_byte_unit(self.n_bytes),
                f'{int(self.usage)}%',
                self.__get_date_val(self.last_write_date),
                self.mode,
                priority_name,
                self.responsible
            ]
        return iter_attrs

    def __get_date_val(self, date_field, as_timestamp=False):
        """
        Wrapper for fetching date times which may or may not be None/NULL.
        :param date_field: Repack datetime object
        :param as_timestamp: Return a timestamp int
        :returns: None, or epoch int or date string
        """
        if date_field is None:
                return None
        if as_timestamp:
            return int(date_field.timestamp())
        return date_field.strftime(DATE_FORMAT)

    def __iter__(self):  # Must be iterable for repack-manager tabulation
        for item in self.__get_iter_fields():
            yield item


# TODO: class TapeStatus, if tape field complexity grows more

class AutomationStatus():
    def __init__(self, repack:Repack, date_entered:datetime=None,
                 date_started:datetime=None, date_repacked:datetime=None,
                 date_quarantined:datetime=None,
                 date_reclaimed:datetime=None,
                 date_finished:datetime=None):
        """
        The progress/state of a repack in the automation system.
        :param repack: Parent Repack reference
        :param date_entered: (optional) Date of the starting of the repack
        :param date_started: (optional) Date of the starting of the repack
        :param date_repacked: (optional) Date of the ending of the repack
        :param date_quarantined: (optional) Date in which the tape was moved
            to a "quarantine" pool
        :param date_reclaimed: (optional) Date in which tape was effectively
            reclaimed
        :param date_finished: (optional) Date the repack process was concluded
        """
        self.repack = repack
        self.stage_dates = {
            1: date_entered,
            2: date_started,
            3: date_repacked,
            4: date_quarantined,
            5: date_reclaimed,
            6: date_finished,
        }
        if date_entered is None:
            self.stage_dates[1] = datetime.now()

        self.stage_num = self.calculate_stage_num()
        self.stage_total = N_REPACK_STAGES
        self.stage_name = REPACK_STAGE_NAMES[self.stage_num]

    def set_date_entered(self, time:datetime=datetime.now):
        """
        :param time: datetime object, defaults to now()
        """
        self.__set_date(1, time)

    def set_date_started(self, time:datetime=datetime.now):
        """
        :param time: datetime object, defaults to now()
        """
        if self.stage_num == 1:
            self.stage_num = 2
        self.stage_dates[2] = time
        self.__set_date(2, time)

    def set_date_repacked(self, time:datetime=datetime.now):
        """
        :param time: datetime object, defaults to now()
        """
        if self.stage_num == 2:
            self.stage_num = 3
        self.stage_dates[3] = time
        self.__set_date(3, time)

    def set_date_quarantined(self, time:datetime=datetime.now):
        """
        :param time: datetime object, defaults to now()
        """
        if self.stage_num == 3:
            self.stage_num = 4
        self.__set_date(4, time)

    def set_date_reclaimed(self, time:datetime=datetime.now):
        """
        :param time: datetime object, defaults to now()
        """
        if self.stage_num == 4:
            self.stage_num = 5
        self.__set_date(5, time)

    def set_date_finished(self, time:datetime=datetime.now):
        """
        :param time: datetime object, defaults to now()
        """
        if self.stage_num == 5:
            self.stage_num = 6
        self.__set_date(6, time)

    def __set_date(self, stage_num=int, time:datetime=datetime.now):
        """
        General date setting method to be used by all the stages.
        :param stage_num: The number of the stage to set
        :param time: The time to set, defaults to now()
        """
        if time == datetime.now:  # Evaluate at call, not at module load
            time = time()

        self.stage_dates[stage_num] = time
        self.repack.logger.info(
            f"Setting tape {self.repack.vid} {REPACK_STAGE_NAMES[stage_num]} "
            f"date to {time}"
        )

    def get_date_entered(self, as_timestamp=False):
        """
        :param as_timestamp: return a timestamp int
        :returns: datetime object or epoch int
        """
        return self.__get_date(1, as_timestamp)

    def get_date_started(self, as_timestamp=False):
        """
        :param as_timestamp: return a timestamp int
        :returns: datetime object or epoch int
        """
        return self.__get_date(2, as_timestamp)

    def get_date_repacked(self, as_timestamp=False):
        """
        :param as_timestamp: return a timestamp int
        :returns: datetime object or epoch int
        """
        return self.__get_date(3, as_timestamp)

    def get_date_quarantined(self, as_timestamp=False):
        """
        :param as_timestamp: return a timestamp int
        :returns: datetime object or epoch int
        """
        return self.__get_date(4, as_timestamp)

    def get_date_reclaimed(self, as_timestamp=False):
        """
        :param as_timestamp: Return a timestamp int
        :returns: Datetime object or epoch int
        """
        return self.__get_date(5, as_timestamp)

    def get_date_finished(self, as_timestamp=False):
        """
        :param as_timestamp: Return a timestamp int
        :returns: Datetime object or epoch int
        """
        return self.__get_date(6, as_timestamp)

    def __get_date(self, stage_num:int, as_timestamp=False):
        """
        General date fetching method to be used by all the stages.
        :param as_timestamp: Return a timestamp int
        :returns: Datetime object or epoch int
        """
        if as_timestamp:
            time = self.stage_dates[stage_num]
            if time is None:
                return None
            return int(time.timestamp())
        return self.stage_dates[stage_num]

    def calculate_stage_num(self):
        """
        :returns: The present stage number (1-indexed)
        """
        stage_num = 1
        if self.stage_dates[2] is not None:
            stage_num = 2
        if self.stage_dates[3] is not None:
            stage_num = 3
        if self.stage_dates[4] is not None:
            stage_num = 4
        if self.stage_dates[5] is not None:
            stage_num = 5
        if self.stage_dates[6] is not None:
            stage_num = 6
        return stage_num

    def to_dict(self, use_timestamps=False, short=False):
        """
        Return the repack status as a dictionary of key-value pairs
        :param use_timestamps: Use epoch timestamps instead of dates
        :param short: Return a reduced json, similar to the short repack ls mode
        :returns: A dict representing the repack status in the system
        """
        if short:
            if use_timestamps:
                stage_time = int(self.stage_dates[self.stage_num].timestamp())
            else:
                stage_time = self.stage_dates[self.stage_num].strftime(DATE_FORMAT)

            status = {
                'stage_number': self.stage_num,
                'stage_total': self.stage_total,
                'stage_name': self.stage_name,
                'stage_time': stage_time
            }
        else:
            status = {
                'date_entered': self.get_date_entered(as_timestamp=use_timestamps),
                'date_started': self.get_date_started(as_timestamp=use_timestamps),
                'date_repacked': self.get_date_repacked(as_timestamp=use_timestamps),
                'date_quarantined': self.get_date_quarantined(as_timestamp=use_timestamps),
                'date_reclaimed': self.get_date_reclaimed(as_timestamp=use_timestamps),
                'date_finished': self.get_date_finished(as_timestamp=use_timestamps),
            }
        return status

    def to_json(self):
        """
        Turn this repack status object into a json string
        :return: string with object json
        """
        return json.dumps(self.to_dict())

    def __str__(self):
        """
        :returns: A formatted string representation of this repack status
        """
        status = f"{self.stage_num}/{self.stage_total} {self.stage_name} " \
                f"{self.stage_dates[self.stage_num].strftime(DATE_FORMAT)}"
        return status


###########
# Functions
###########

def list_in_db(db_connection, vids=None, config=None, logger=None,
               long_fmt=None, order_by="progress", mode=None):
    """
    List repacks in the system
    :param db_connection: Database connection object from sql_utils.connect
    :param vids: (optional) Return only results with VIDs in the given list
    :param config: (Optional) A CtaOpsConfig object, which specifies where to
        write logs
    :param logger: (Optional) A Python logging logger
    :param long_fmt: (Optional) Increase level of output detail
    :param order_by: (Optional) Override ordering of results
    :param mode: (Optional) Return only repacks in selected mode
    :returns: List of found Repack objects
    """
    # Order repacks by user selected col, order by progress in the system after this.
    # This ordering ensures NULL values end up last,
    order_progress = f'''
        {REF_TABLE['finished']['name']} DESC NULLS LAST,
        {REF_TABLE['reclaimed']['name']} DESC NULLS LAST,
        {REF_TABLE['quarantined']['name']} DESC NULLS LAST,
        {REF_TABLE['repacked']['name']} DESC NULLS LAST,
        {REF_TABLE['started']['name']} DESC NULLS LAST,
        {REF_TABLE['entered']['name']} DESC NULLS LAST
    '''
    order_options = {
        'progress': '',
        'entered': f'{REF_TABLE["entered"]["name"]},',
        'last-written': f'{REF_TABLE["last_write_date"]["name"]},',
        'usage': f'{REF_TABLE["usage"]["name"]},'
    }
    if order_by not in order_options.keys():
        log_utils.log_and_exit(logger, f"Unknown order '{order_by}'")

    query = f'''
        SELECT {TABLE_FIELDS_STR} 
        FROM {config.get('database', 'repack_table_name')}
    '''

    if vids:
        query += 'WHERE tape = ANY(%(vids)s) '
    elif mode is not None:
        query += 'WHERE mode = %(mode)s'

    query += f'ORDER BY {order_options[order_by]} {order_progress}'
    data = {
        'vids': vids,
        'mode': mode,
    }
    tape_tuples = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        data = data,
        config = config,
        logger = logger
    )

    repacks = []
    for tape in tape_tuples:
        repacks.append(
            Repack(
                tape_tuple = tape,
                db_connection = db_connection,
                config = config,
                logger = logger,
                short = not long_fmt
            )
        )

    # Fix the responsible to the correct in the db (because Repack __init__ modify the user to the current one)
    for tape_tuple,repack in zip(tape_tuples,repacks):
        repack.responsible = tape_tuple[REF_TABLE['responsible']['col_num']]

    return repacks


def count_in_db(db_connection, config=None, logger=None):
    """
    Count the number of tapes/repacks present in the automation system.
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object
    :param logger: (Optional) A Python logging logger
    :returns: Number of tapes as an integer
    """
    query = f'''
        SELECT COUNT({REF_TABLE['tape']['name']})
        FROM {config.get('database', 'repack_table_name')}
    '''
    tape_count = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger = logger
    )

    return int(tape_count[0][0])


def remove_from_db(db_connection, vids, config, logger=None):
    """
    Remove one or multiple repacks from the DB.
    Warn about repacks which were not found.
    :param db_connection: Database connection object from sql_utils.connect
    :param vids: Remove entries with VIDs in this iterable
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    """
    for vid in vids:
        # Warn user if repack in remove list was not found, then skip
        test_present_repack = Repack(
            tape = vid,
            db_connection = db_connection,
            config = config,
            logger = logger
        )
        if not test_present_repack.in_db:
            logger.warning(
               f"Tape with VID {vid} not present in the the repack automation "
               "database, skipping."
            )
            continue
        # Check for active repacks
        cmd = f'{tapeadmin.cta_admin_json_repack_ls_vid} {vid}'
        cmd_call = cmd_utils.run_cmd(cmd, config=config)  # Don't log fails
        if cmd_call.returncode == 0:
            logger.warning(f"Repack object for tape {vid} exists in CTA")
            tape_repack_info = json.loads(cmd_call.stdout)[0]
            if tape_repack_info['status'] == 'Running':
                logger.warning(f"Repack of {vid} is running, leaving as-is!")
            else:
                logger.info(f"Repack of {vid} is not running, cleaning it up.")
                test_present_repack.remove_cta_repack()

        # Remove entry from Repack DB
        logger.info(f"Removing tape with VID {vid} from the automation database.")
        query = f'''
            DELETE FROM {config.get('database', 'repack_table_name')}
            WHERE tape = %(vid_to_remove)s
        '''
        data = {
            'vid_to_remove': vid
        }
        sql_utils.execute_query(
            db_connection = db_connection,
            query = query,
            data = data,
            config = config,
            logger = logger,
            commit = True
        )


def remove_all_from_db(db_connection, config, logger=None):
    """
    Remove all repacks from the DB
    :param db_connection: Database connection object from sql_utils.connect
    :param vids: Remove entries with VIDs in this iterable
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    """
    query = f'''
        DELETE 
        FROM {config.get('database', 'repack_table_name')}
    '''
    sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger = logger,
        commit = True
    )


def select_repacks_to_start(db_connection, config, logger=None, order_by='entered'):
    """
    Select the repack entries which have not been started
    (whose DATE_REPACK_START is NULL) from the database.
    Repacks are always selected by priority, then by specified order_by param.
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :param order_by: Override the second ordering criteria of repacks selected
    :return: The list of VID(s) to repack
    """
    order_options = {
        'entered': f'{REF_TABLE["entered"]["name"]}',
        'last-written': f'{REF_TABLE["last_write_date"]["name"]}',
        'usage': f'{REF_TABLE["usage"]["name"]}',
        'random': 'random()'
    }

    if order_by not in order_options.keys():
        log_utils.log_and_exit(logger, f"Unknown order '{order_by}'")

    query = f'''
        SELECT {TABLE_FIELDS_STR}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['started']['name']} is NULL
        AND {REF_TABLE['mode']['name']} = 'auto'
        ORDER BY {REF_TABLE['priority']['name']} DESC, {order_options[order_by]}
    '''
    unstarted = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger = logger
    )
    return [
        Repack(
            tape_tuple = a_tuple,
            db_connection = db_connection,
            config = config,
            logger = logger
        )
        for a_tuple in unstarted
    ]


def select_active_repacks(db_connection, config, logger=None):
    """
    Select repacks which are currently being being processed, according to the DB
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: The list of Repack objects being repacked
    """
    query = f'''
        SELECT {TABLE_FIELDS_STR}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['started']['name']} IS NOT NULL
        AND {REF_TABLE['repacked']['name']} IS NULL
        AND {REF_TABLE['mode']['name']} = 'auto'
        ORDER BY {REF_TABLE['started']['name']}
    '''
    unfinished = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger = logger
    )
    return [
        Repack(
            tape_tuple = a_tuple,
            db_connection = db_connection,
            config = config,
            logger = logger
        )
        for a_tuple in unfinished
    ]


def select_repacks_to_quarantine(db_connection, config, logger=None):
    """
    Select the repacks where the tapes are ready to be moved to the quarantine pool
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: The list of VID to move to the quarantine pool
    """
    query = f'''
        SELECT {TABLE_FIELDS_STR}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['repacked']['name']} IS NOT NULL
        AND {REF_TABLE['quarantined']['name']} IS NULL
        AND {REF_TABLE['mode']['name']} = 'auto'
        ORDER BY {REF_TABLE['repacked']['name']}
    '''
    unmoved = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger = logger
    )
    return [
        Repack(
            tape_tuple = a_tuple,
            db_connection = db_connection,
            config = config,
            logger = logger
        )
        for a_tuple in unmoved
    ]


def select_repacks_to_reclaim(db_connection, config, logger=None):
    """
    Select the tapes that are ready to be reclaimed
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: The list of VID to reclaim
    """
    query = f'''
        SELECT {TABLE_FIELDS_STR}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['quarantined']['name']} IS NOT NULL
        AND {REF_TABLE['reclaimed']['name']} IS NULL
        AND now() - {REF_TABLE['quarantined']['name']} > %(grace_period)s * interval '1 DAY'
        AND {REF_TABLE['mode']['name']} = 'auto'
        ORDER BY {REF_TABLE['quarantined']['name']}
    '''
    data = {
        'grace_period': config.get('quarantine_days'),
    }
    unreclaimed = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        data = data,
        config = config,
        logger = logger
    )
    return [
        Repack(
            tape_tuple = a_tuple,
            db_connection = db_connection,
            config = config,
            logger = logger
        )
        for a_tuple in unreclaimed
    ]


def select_repacks_to_finish(db_connection, config, logger=None):
    """
    Select the tapes that are ready to be moved to a tolabel/erase pool
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: The list of Repack objects which should have their tape moved
        to a tolabel/erase pool.
    """
    query = f'''
        SELECT {TABLE_FIELDS_STR}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['reclaimed']['name']} IS NOT NULL
        AND {REF_TABLE['finished']['name']} IS NULL
        AND {REF_TABLE['mode']['name']} = 'auto'
        ORDER BY {REF_TABLE['reclaimed']['name']}
    '''
    unmoved = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger=logger
    )
    return [
        Repack(
            tape_tuple = a_tuple,
            db_connection = db_connection,
            config = config,
            logger = logger
        )
        for a_tuple in unmoved
    ]


def select_completed_repacks(db_connection, config, logger=None):
    """
    Select the tapes which have completed their automated repack.
    :param db_connection: Database connection object from sql_utils.connect
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: The list of Repack objects which have finished
    """
    query = f'''
        SELECT {TABLE_FIELDS_STR}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['finished']['name']} IS NOT NULL
        AND {REF_TABLE['mode']['name']} = 'auto'
        ORDER BY {REF_TABLE['finished']['name']}
    '''
    finished = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        config = config,
        logger = logger
    )
    return [
        Repack(
            tape_tuple = a_tuple,
            db_connection = db_connection,
            config = config,
            logger=logger
        )
        for a_tuple in finished
    ]


def check_tape_in_system(db_connection, tape, config, logger):
    """
    Checks if a given tape is known to the repack automation system.
    In other words, is it in the DB?
    :param db_connection: Database connection object from sql_utils.connect
    :param tape: The tape volume ID
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: A boolean indicating whether or not there is an entry
    """
    query = f'''
        SELECT {REF_TABLE['tape']['name']}
        FROM {config.get('database', 'repack_table_name')}
        WHERE {REF_TABLE['tape']['name']} = %(tape)s
    '''
    data = {
        'tape': tape
    }
    result = sql_utils.execute_query(
        db_connection = db_connection,
        query = query,
        data = data,
        config = config,
        logger = logger
    )
    if len(result) >= 1:
        return True

    return False


def clean_up_cta_repack(vid:str, logger, force=False):
    """
    Remove a finished repack from CTA
    :param vid: The tape volume ID
    :param logger: (Optional) A Python logging logger
    :param force: (Optional) Remove repack even if not complete
    :return: Boolean based on success
    """

    # Check that the repack request is present
    cmd = f'{tapeadmin.cta_admin_json_repack_ls_vid} {vid}'
    cmd_call = cmd_utils.run_cmd(cmd)
    if cmd_call.returncode != 0:
        logger.error(
            f"Command ({cmd}) failed. {log_utils.format_stderr(cmd_call.stderr)}"
        )
        return False
    logger.debug(f"Repack request for {vid} found")
    repack_info = json.loads(cmd_call.stdout)[0]

    # Remove the repack request if complete
    # Valid states: Starting, Running, Complete, Failed
    if not force:
        if repack_info['status'] != 'Complete':
            logger.warning(
                f"Repack for tape {vid} is not Complete, " \
                f"was {repack_info['status']}, skipping"
            )
            return False

    cmd = f'{tapeadmin.cta_admin_json_repack_rm_vid} {vid}'
    cmd_call = cmd_utils.run_cmd(cmd)
    if cmd_call.returncode != 0:
        logger.error(
            f"Command ({cmd}) failed. {log_utils.format_stderr(cmd_call.stderr)}"
        )
        return False
    logger.info(
        f"Repack request for {vid} removed"
    )
    return True


def print_script_banner(config, logger, script_name:str,
                        print_config_vals:list=[]):
    """
    Print the info banner whown at the beginning of every script execution.
    Script-specific config is fetched by default, additional values may be
    supplied.
    :param config: (Optional) A CtaOpsConfig object, which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :param script_name: Name of the calling script
    :param print_config_vals:  (Optional) list of relevant config values to print
    """

    logger.info(f"{'-'*80}")
    logger.info(f"Executed command: {' '.join(sys.argv)}")
    options = ""
    for key, val in config.get(script_name, default={}).items():
        options += f" {key}: {val}"
    for opt in print_config_vals:
        options += f" {opt}: {config.get(opt)}"

    logger.info(f'Found default config options from config file:{options}')


def repack_check_env(db_connection, args=None, config=None, logger=None):
    """
    Check that the CTA environment is compatible with the repack automation.
    :param db_connection: Database connection object from sql_utils.connect
    :param args: (Optional) Provide in order to switch on status print
    :param args: (Optional) Provide CtaOpsConfig object with config options
    :param logger: (Optional) A Python logging logger
    :returns: Boolean true if config is valid, else false
    """
    logger.debug("Checking correctness of ATRESYS and CTA setup")

    ANSI_SUCCESS = config.get('colors', 'ansi_color_success')
    ANSI_FAIL = config.get('colors', 'ansi_color_fail')
    ANSI_END = config.get('colors', 'ansi_end')

    env_valid = True

    # Check that the table is correct
    # Good metadata fetching blog post:
    # https://www.alberton.info/postgresql_meta_info.html
    logger.debug("Checking integrity of ATRESYS database")
    target_table = config.get('database', 'repack_table_name')
    schema_query = '''
        SELECT a.attnum AS col_num, a.attname AS column_name,
            t.typname AS data_type
        FROM pg_class c, pg_attribute a, pg_type t
        WHERE c.relname = %s AND a.attnum > 0
            AND a.attrelid = c.oid AND a.atttypid = t.oid
        ORDER BY a.attnum
    '''
    layout = sql_utils.execute_query(
        db_connection = db_connection,
        query = schema_query,
        data = (target_table, ),
        config = config,
        logger = logger
    )
    col_dict = {
        k-1: {'name': col_name, 'type': col_type, 'col_num': k-1}
        for k, col_name, col_type in layout
    }
    # Check that the columns found are specified by the reference
    simple_REF_TABLE = [
        {
            'name': a['name'].replace('"', ''),
            'type': a['type'],
            'col_num':a['col_num']
        }
        for a in REF_TABLE.values()
    ]
    for col in col_dict.values():
        if col in simple_REF_TABLE:
            if args is not None:
                print(
                    f"Table {target_table}, column {col['name']}: " \
                    f"{ANSI_SUCCESS}valid{ANSI_END}"
                )
        else:
            if args is not None:
                print(
                    f"Table {target_table}, column {col['name']}: " \
                    f"{ANSI_FAIL}invalid{ANSI_END}"
                )
            env_valid = False
    # Check that the reference columns exist in the real table
    for col in REF_TABLE.values():
        simple_col = {
            'name': col['name'].replace('"', ''),
            'type': col['type'],
            'col_num': col['col_num']
        }
        if simple_col not in col_dict.values():
            if args is not None:
                print(
                    f"Table {target_table}, column {simple_col['name']}: " \
                    f"{ANSI_FAIL}not found{ANSI_END}"
                )
            env_valid = False

    # Check that the quarantine tape pool exists
    cmd = f"{tapeadmin.cta_admin_json_tapepool_ls} --name quarantine"
    cmd_call = cmd_utils.run_cmd(cmd)
    if not cmd_call.returncode:
        if args is not None:
            print(f"Tape pool quarantine: {ANSI_SUCCESS}found{ANSI_END}")
    else:
        if args is not None:
            print(f"Tape pool quarantine: {ANSI_FAIL}not found{ANSI_END}")
        env_valid = False

    # Check that we have all of the tolabel and erase pools
    logger.debug("Checking integrity of destination tape pools")
    for _, mediamap in config.get('tape', 'lib_to_pool_map').items():
        for postfix in mediamap.values():
            pri_items = config.get('priorities').items()
            prefixes = {
                priority['default_dst_pool_prefix'] for _, priority in pri_items
            }
            for unique_prefix in prefixes:
                pool_name = f"{unique_prefix}{postfix}"
                cmd = f"{tapeadmin.cta_admin_json_tapepool_ls} --name {pool_name}"
                cmd_call = cmd_utils.run_cmd(cmd)
                if not cmd_call.returncode:
                    if args is not None:
                        print(
                            f"Tape pool {pool_name}: {ANSI_SUCCESS}found{ANSI_END}"
                        )
                else:
                    if args is not None:
                        print(
                            f"Tape pool {pool_name}: {ANSI_FAIL}not found{ANSI_END}"
                        )
                    env_valid = False

    logger.debug(f"ATRESYS and CTA environment valid: {env_valid}")
    return env_valid

def priority_name_to_num(priority_name:str, config, logger, dst_override=None):
    """
    Get the priority value from a priority name.
    :param priority_name: String for common priority name 'low', 'high' ...
    :param config: CtaOpsConfig with the nested dict priority mapping
    :param logger: A Python logging logger
    :param dst_override: Override default destinations, which yield alternate
        priority names
    :returns: Integer representation of priority
    """
    # This is inelegant, but works better the other way round in the
    # Repack init
    priority = -1
    for key, values in config.get('priorities').items():
        if values['name'].split('-')[0] == priority_name:
            if dst_override is not None:
                if values['default_dst_pool_prefix'] != dst_override:
                    continue
            priority = key
            break
    if priority == -1:
        log_utils.log_and_exit(
            logger,
            "Could not determine correct priority for the repack"
        )
    return priority
