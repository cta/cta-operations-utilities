# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Common showqueues module for both archive and retrieve usage.
"""

import sys
import json

from collections import OrderedDict

import tapeadmin

from ctautils import cmd_utils, dict_utils, table_utils, log_utils

def do_showqueues(args, mount_type, config, logger):

    cmd = tapeadmin.cta_admin_json_showqueues
    summary = {}

    cmd_call = cmd_utils.run_cmd_popen(cmd)
    if cmd_call.returncode == 124:
        logger.error(f"Timeout reached when executing command: {cmd}")
        sys.exit(124)
    if cmd_call.returncode != 0:
        logger.error(f"ERROR executing command: {cmd} {log_utils.format_stderr(cmd_call.stderr)}")
        sys.exit(1)

    showqueues_list = json.loads(cmd_call.stdout)
    if not showqueues_list:
        if args.json:
            print('[]')
        logger.info("No queued requests found in the output of 'cta-admin showqueues'.")
        sys.exit(0)

    # Translate field name to cta-admin nomenclature, if applicable
    group_by = args.group_by
    if group_by == 'library':
        group_by = 'logicalLibrary'

    # Get the names of all the distinct tape pools that have queued requests
    groupby_list = set()
    for request in showqueues_list:
        groupby_list.add(request[group_by])

    # Order alphabetically
    groupby_list = sorted(groupby_list)

    for groupby_item in groupby_list:
        # Initialize variables
        summary[groupby_item] = OrderedDict()
        tapes = set()
        summary[groupby_item] = {
            'minAge': 1000000000, # Some very large value
            'oldestAge': 0,
            'queuedFiles': 0,
            'queuedBytes': 0,
            'curMounts': 0,
            'curFiles': 0,
            'curBytes': 0,
            'tapesFiles': 0,
            'tapesBytes': 0,
            'tapesCapacity': 0,
            'highestPriority': 0
        }
        # "RETRIEVE" requests have tapesCount, but "ARCHIVE" ones don't
        if mount_type == 'RETRIEVE':
            summary[groupby_item]['tapesCount'] = 0
        for request in showqueues_list:
            if request[group_by] == groupby_item and mount_type in request['mountType']:
                request = dict_utils.dict_fields_to_int(request)
                summary[groupby_item] = {
                    'minAge': summary[groupby_item]['minAge'],
                    'oldestAge': summary[groupby_item]['oldestAge'],
                    'highestPriority': summary[groupby_item]['highestPriority'],
                    'queuedFiles': summary[groupby_item]['queuedFiles'] + request['queuedFiles'],
                    'queuedBytes': summary[groupby_item]['queuedBytes'] + request['queuedBytes'],
                    'curMounts': summary[groupby_item]['curMounts'] + request['curMounts'],
                    'curFiles': summary[groupby_item]['curFiles'] + request['curFiles'],
                    'curBytes': summary[groupby_item]['curBytes'] + request['curBytes'],
                    'tapesFiles': summary[groupby_item]['tapesFiles'] + request['tapesFiles'],
                    'tapesBytes': summary[groupby_item]['tapesBytes'] + request['tapesBytes'],
                    'tapesCapacity': summary[groupby_item]['tapesCapacity'] + request['tapesCapacity']
                }
                # Calculate the minAge (only consider the queued requests above 0)
                if (request['minAge'] > 0):
                    summary[groupby_item]['minAge'] = min(summary[groupby_item]['minAge'], request['minAge'])
                # Calculate the oldestAge (only consider the queued requests above 0)
                if (request['oldestAge'] > 0):
                    summary[groupby_item]['oldestAge'] = max(summary[groupby_item]['oldestAge'], request['oldestAge'])
                # Calculate the highest priority
                summary[groupby_item]['highestPriority'] = max(summary[groupby_item]['highestPriority'], request['priority'])
                # Add tape to the set of tapes
                tapes.add(request['vid'])
        # Determine the number of distinct tapes for the set of requests, and the highest priority
        if mount_type == 'RETRIEVE':
            summary[groupby_item]['tapesCount'] = len(tapes)

    # Print only JSON if option is specified
    if args.json:
        print(json.dumps(summary, indent=4, sort_keys=True))
        sys.exit(0)

    # Print to console in table format, if JSON option is not specified
    group_by_header = ''
    if group_by.upper() == 'LOGICALLIBRARY':
        group_by_header = 'LOGICAL_LIBRARY'
    elif group_by.upper() == 'TAPEPOOL':
        group_by_header = 'TAPE_POOL'
    elif group_by.upper() == 'VO':
        group_by_header = 'VO'
    else:
        logger.error(f'Wrong item to group by: "{group_by}"')
        sys.exit(1)
    headers = [f'{group_by_header}', 'PRIORITY', 'Qd_BYTES', 'Qd_FILES',
               'MOUNTS', 'CURRENT_FILES', 'CURRENT_BYTES', 'MIN_AGE', 'OLDEST', 'TAPES_BYTES',
               'TAPES_CAPACITY', 'TAPES_FILES']
    if mount_type == 'RETRIEVE':
        headers.insert(1, 'TAPES')
    rows = []
    #for item in summary: # sort alphabetically
    for item in sorted(summary, key=lambda x: (summary[x]['queuedBytes']), reverse=True): # sort by queued bytes
        if summary[item]['queuedBytes'] == 0:
            continue
        row = [
            item,
            summary[item]["highestPriority"],
            '{:>8}'.format(cmd_utils.convert_byte_unit(summary[item]["queuedBytes"])),
            summary[item]["queuedFiles"],
            summary[item]["curMounts"],
            summary[item]["curFiles"],
            '{:>13}'.format(cmd_utils.convert_byte_unit(summary[item]["curBytes"])),
            summary[item]["minAge"],
            summary[item]["oldestAge"],
            '{:>11}'.format(cmd_utils.convert_byte_unit(summary[item]["tapesBytes"])),
            '{:>14}'.format(cmd_utils.convert_byte_unit(summary[item]["tapesCapacity"])),
            summary[item]["tapesFiles"]
        ]
        if mount_type == 'RETRIEVE':
            row.insert(1, summary[item]["tapesCount"])
        rows.append(row)
    if not rows:
        if args.json:
            print('[]')
        logger.info(f'No {mount_type} queued requests found in the output of "cta-admin showqueues"')
        sys.exit(0)
    print(table_utils.get_table(headers, rows, config, group_by=group_by))
