# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Mail notification helpers
"""

import re
import logging
import socket

from string import Template
from datetime import datetime

from ctautils import log_utils, cmd_utils, config_holder, dict_utils


###############
# Configuration
###############

logger = logging.getLogger('cta-mail-sender')

tmp_file_prefix = '/tmp'

###########
# Functions
###########

def send_mail(notification_name: str, mail_template_fields: dict, template_file=None, config=None):
    """
    Uses "sendmail" command to parse the corresponding alert fields in for of an email, and send it
    :param notification_name: The name mentioned in the logs if email sending should fail
    :param mail_template_fields: The values to fill the fields in the mail template
    :param template_file: The template file to use
    :param config: A ConfigHolder containing a dict with the parsed config YAML
    """

    # Temporarily support both config formats
    if isinstance(config, config_holder.ConfigHolder):
        binaries = config.config['binaries']
        sendmail = binaries['sendmail']
        if config is not None and config.config is not None:
            if 'email_template' in config.config:
                template = config.config['email_template']
    else:
        sendmail = config.get('binaries', 'sendmail')
        # If we have an explicitly given template, use that
        if template_file is None:
            # Else, if we have template specified in the config, use that
            template_file = config.get('email', 'template')

    # Recover the Rundeck job ID from the file crated upon execution
    try:
        with open(f'{tmp_file_prefix}/rundeck_job_id.txt', 'r') as f:
            mail_template_fields['RUNDECK_JOB_ID'] = f.read()
    except Exception:
        mail_template_fields['RUNDECK_JOB_ID'] = '(unknown)'

    mail_template_fields['HOSTNAME'] = socket.gethostname()
    mail_template_fields['TIMESTAMP_SUBJECT'] = datetime.now().strftime('%d %b %Y %H:%M')
    mail_template_fields['TIMESTAMP_FOOTER'] = datetime.now().strftime('%a %b %d %H:%M:%S %Y')

    template = None
    if config is not None:
        template = dict_utils.get_or_default(
            dictionary = config.config,
            field_name = 'email_template',
            default = None
        )
    if template_file is not None:
        template = template_file

    if template is None:
        log_utils.log_and_abort(config, logger, "No template specified")

    mail_content = substitute_template(template, mail_template_fields)
    # TODO: Give mail_sender complete control of SENDER field, once cta/operations tools no longer use it

    subject = re.search('Subject:\s*(.+)\n', mail_content).group(1)
    emails_to = re.search('To:\s*(.+)\n', mail_content).group(1)

    log_utils.write_to_log(
        config,
        logger,
        logging.INFO,
        subject
    )

    # We create the temp file that holds the email body and header
    temp_file = datetime.now().strftime("%d-%m-%Y_%H.%M.%S.%f.sendmail")
    try:
        with open(f'{tmp_file_prefix}/{temp_file}', 'w', encoding='utf-8') as f:
            f.write(mail_content)

    except Exception:
        log_utils.write_to_log(
            config,
            logger,
            logging.ERROR,
            f'Error creating temp file {tmp_file_prefix}/{temp_file} for sendmail. Mail not sent',
        )
        return False

    if template is None:
        template = "Undefined"

    # We pass the created temp file to the sendmail command
    cmd = f'{sendmail} -t < {tmp_file_prefix}/{temp_file}'

    if cmd_utils.run_cmd(cmd, print_to_stderr=True).returncode != 0:
        log_utils.write_to_log(
            config,
            logger,
            logging.ERROR,
            f'Error sending email "{notification_name}" ' \
            f" {template.split('/')[-1]} to recipient(s) {emails_to}"
        )
    else:
        log_utils.write_to_log(
            config,
            logger,
            logging.INFO,
            f"Email \"{notification_name}\" {template.split('/')[-1]} " \
            f'successfully sent to recipient(s) {emails_to}'
            )
    return True

def substitute_template(template_file: str, template_fields: dict):
    """
    Substitutes the fields inside a template file with the given values for each field
    :param template_file: The template file
    :param template_fields: The values for each field of the template to substitute
    :return: The text resulting from filling the template
    """
    with open(template_file, 'r') as file:
        body = Template(file.read())
    return body.safe_substitute(template_fields)

