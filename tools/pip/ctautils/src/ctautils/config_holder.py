# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
A simple wrapper for holding the configuration options of various cta-ops tools.
These options are usually read from the tool's config yaml.
"""

import logging

from ctautils import yaml_loader, log_utils, dict_utils

###############
# Configuration
###############

DEFAULT_CONFIG_PATH = '/etc/cta-ops/cta-ops-config.yaml'

DEFAULT_LOGGER = logging.getLogger('config_holder')

class ConfigHolder:
    """
    Utility class to store diverse configuration parameters and use them in all the rest of the scripts
    """
    config = None         # Dict, config options
    log_file_path = None  # String, explicit log file path

    def __init__(self, config=None, log_file_path=None):
        '''
        Initialize from parsed config dict
        :param config: A dictionary of config options
        :param log_file_path: Easy access to log file path
        '''
        if config is not None:
            self.config = config

            if 'log_file_path' in self.config:
                self.log_file_path =  self.config['log_file_path']
            else:
                self.log_file_path = log_file_path
        else:
            self.config = {}
            self.log_file_path = log_file_path


class CtaOpsConfig:
    """
    Parse the CTA Operations config file and load the correct settings.
    'global' options are shared by all scripts, tool-specific options are
    only loaded by the concerned tool.
    """
    config = {}
    tool = ""
    config_path = ""

    def __init__(self, tool_name:str=None, config_path:str=DEFAULT_CONFIG_PATH):
        """
        Load config upon init.
        :param tool_name: Name of the specific tool to load the config for
        :param config_path: File path in system to load the config from
        """
        self.tool = tool_name
        self.config_path = config_path
        self.logger = DEFAULT_LOGGER
        self.set_config(self.from_yaml(config_path, tool_name))

    def set_config(self, conf_dict:dict):
        """
        Set the stored config to the given dictionary
        :param conf_dict: Dictionary of CTA operations config options
        """
        self.config = conf_dict

    def set_logger(self, logger:logging.Logger):
        """
        Set a specific logger for the methods to use.
        :param logger: Configured logger to use
        """
        self.logger = logger

    def set(self, key, value):
        """
        Allow scripts to modify the config they pass on to library functions.
        Useful for modifying fields such as verbosity
        :param key: Key to set in config
        :param value: Value to set the key to
        """
        self.config[key] = value

    def get(self, *keys, default=None):
        """
        Get the config value corresponding to the key, if it exists.
        :param key: The key (generally str or int) to look up in the config
        :param logger: (Optional) Specify a cusom logger for error logging
        :param default: (Optional) Return this default value if keys not found
        :returns: A specified value (may be a dict if a subtree is specified)
        """
        val = self.config
        for key in keys:
            try:
                val = val[key]
            except (KeyError, TypeError):
                if default is not None:
                    val = default
                else:
                    log_utils.log_and_exit(
                        self.logger,
                        f"Key '{key}' not defined in loaded config for "
                        f"tool {self.tool}"
                    )

        # Handle case of setting defined in conf file, but left empty
        if val is None:
            val = default

        return val

    def from_yaml(self, yaml_path:str, tool_name:str=None):
        """
        Load the configuration from a yaml file.
        :param yaml_path: Path to the yaml file to parse
        :param tool_name: Name of the specific tool to load the config for
        :returns: A dictionary of config options
        """
        full_config = yaml_loader.load_yaml(yaml_path)

        # If a tool is specified, the tool's config is loaded at the same
        # level as global options. This means the tool setting may
        # override an identically named global one!
        if tool_name is not None:
            try:
                conf_dict = dict_utils.recursive_dict_merge(
                    [full_config['global'], full_config['tools'][tool_name]]
                )
            except TypeError:
                log_utils.log_and_exit(
                    self.logger,
                    f"Could not find config for "
                    f"tool {self.tool} in {self.config_path}"
                )
        else:
            conf_dict = {
                **full_config['global']
            }
        return conf_dict


class ErrorTranslator:
    """
    Translate error messages using a file listing preferred substitutions.
    """
    translations_dict = None

    def __init__(self, config):
        "Load the translation config file"
        trans_path_str = config.get('logging', 'error_translation_file', default=None)
        self.translations_dict = self.from_yaml(trans_path_str, config.tool)


    def from_yaml(self, file_path, tool_name=None):
        """
        Load the error matching and replacement rules from a yaml file.
        Supplying the config translation file is optional. If none is supplied,
        an empty translator will be created
        :param file_path: Path to the yaml file to parse
        :param tool_name: Name of the specific tool to load translations for
        :returns: A dictionary of message translations
        """
        if file_path is None or file_path == "":
            return {}

        all_translations = yaml_loader.load_yaml(file_path)

        # If a tool is specified, the tool's config is loaded at the same
        # level as global options. This means the tool setting may
        # override an identically named global one!
        conf_dict = {}
        if file_path is not None:
            if tool_name is not None and tool_name in all_translations['tools']:
                try:
                    conf_dict = dict_utils.recursive_dict_merge(
                        [all_translations['global'], all_translations['tools'][tool_name]]
                    )
                except TypeError:
                    log_utils.log_and_exit(
                        DEFAULT_LOGGER,
                        f"Could not find config for "
                        f"tool {tool_name} in {file_path}"
                    )
            else:
                conf_dict = {
                    **all_translations['global']
                }
        return conf_dict

    def translate(self, err_msg):
        """
        Check if there is a valid translation for this error and return that.
        :param err_msg: The generated error message
        :returns: Error msg translation, or None if none found.
        """
        if "Command failed:" in err_msg:
            # TODO: Find a more solid way of getting the executable
            abs_executable = err_msg.rsplit("Command failed:")[1].rsplit()[0]
            executable = abs_executable.rsplit('/', maxsplit=1)[-1]
            for cmd, cmd_items in self.translations_dict.items():
                if cmd == executable:
                    for item in cmd_items:
                        if item['tool_string'] in err_msg:
                            return item['translation']
        return None
