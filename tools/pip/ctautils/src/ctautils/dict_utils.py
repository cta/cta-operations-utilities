# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Various dictionary tricks
"""

from collections import OrderedDict


def dict_fields_to_int(d):
    """
    Casts to int all the possible fields of a 1-level dictionary (no nested dicts)
    :param d: The dictionary
    :return: The modified dictionary
    """
    for key in d.keys():
        try:
            d[key] = int(d[key])
        except Exception:
            continue
    return d


def get_or_default(dictionary: dict, field_name: str, default, operation=None):
    """
    Gets the corresponding value of a dictionary if the key exists, returns the default otherwise.
    In addition, allows to apply an optional operation to the data if the key existed.
    :param dictionary: The dictionary
    :param field_name: The key whose value we want to retrieve
    :param default: The default value if the key didn't exist
    :param operation: (optional) The operation to apply to the data if the key was found
    :return: The requested data with the optional operation performed, or the default value
    """
    value = dictionary.get(field_name, default)     # Value = default => when the KEY was NOT found
    if value is None:                               # Value = default => when the KEY was found but value was None
        value = default
    if value != default and operation is not None:  # If correct value was found for the given key, apply the operation
        return operation(value)
    return value


def unit_list_dict_to_string(unit_list_dict: dict):
    """
    Returns a string representation of a dictionary containing the list of units
    where the observed target unit failed
    :param unit_list_dict: The dictionary of the units and the times where the
        observed unit failed
    :return: The string representation of the dictionary
    """
    string_units_where_observed_target_failed = ''
    ordered_dict = dict(
        OrderedDict(
            sorted(unit_list_dict.items(), key=lambda t: t[1], reverse=True)
        )
    )
    for unit in ordered_dict.keys():
        string_units_where_observed_target_failed += f'{unit}: {ordered_dict[unit]} time(s)\n'
    return string_units_where_observed_target_failed


def recursive_dict_merge(ordered_dict_list:list):
    """
    Recursively merge an ordered list of dictionaries, such that values of
    early dicts are overwritten by later ones.
    Allows for overwriting nested values, without clearing the whole parent dict
    as one would with {**a,**b}.
    In other words, {'k': {'a1': 'val1', 'a2':'val2'}} + {'k': {'a1': 'val3'}}
    produces {'k': {'a1': 'val3', 'a2':'val2'}} .
    See also https://stackoverflow.com/questions/70310388/how-to-merge-nested-dictionaries/70310511#70310511
    :param dict_list: Ordered list of dictionaries
    :returns: Merged dictionary
    """
    def dict_merge(src_dict, dst_dict):
        for key, val in src_dict.items():
            if isinstance(val, dict):
                dict_merge(val, dst_dict.setdefault(key, {}))
            else:
                dst_dict[key] = val

    out_dict = {}
    for src_dict in ordered_dict_list:
        dict_merge(src_dict, out_dict)

    return out_dict


class AggFunctions:
    _functions = ["SUM","MAX","LAST"] 
    @staticmethod
    def SUM(dics: list[dict], fields: list[str]):
        common_keys = set(dics[0].keys()) - set(fields)
        d = {k: v for k, v in dics[0].items() if k in common_keys}
        d.update({f: sum(d[f] for d in dics) for f in fields})
        return d

    @staticmethod
    def MAX(dics: list[dict], fields: list[str]):
        common_keys = set(dics[0].keys()) - set(fields)
        d = {k: v for k, v in dics[0].items() if k in common_keys}
        d.update({f: max(d[f] for d in dics) for f in fields})
        return d

    @staticmethod
    def LAST(dics: list[dict], fields: list[str]):
        return dics[-1]
