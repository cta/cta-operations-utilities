# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Utility functions for command execution
"""

import os
import re
import pwd
import sys
import stat
import math
import shutil
import logging
import subprocess

from pathlib import Path

from ctautils import log_utils

def is_root():
    """
    Checks if the user executing the script is root
    :return: True if root, False otherwise
    """
    return os.getuid() == 0

def get_username():
    """
    Wrapper for getting the active user's username
    :returns: Username as string
    """
    username = pwd.getpwuid(os.getuid()).pw_name
    return username

def abort_if_root(config=None, logger=None):
    """
    Aborts if the script is run as root
    :param config: (optional) CtaOpsConfig object
    :param logger: (optional) Logging logger object
    """
    if is_root():
        msg = 'Must not be ROOT to run this script, aborting'
        if config is not None and logger is not None:
            log_utils.log_and_abort(config, logger, msg)
        log_utils.abort(msg)


def create_ops_owned_file(path:str, config, mode=0o664, content:str=None):
    """
    Create a file owned by the operations default group with r+w permissions.
    Missing directories are created as `mkdirs -p` would do.
    Existing files at that location will be accepted (+ overwritten by content)
    :param path: A path-like object
    :param config: CtaOpsConfig object
    :param mode: Mode value for file and parent dir, default 664
    :param content: (Optional) Write content to the file
    """
    path = Path(path)

    # Set umask to allow group write permission changes for directores.
    # The default umask is 0o022, turning any `chown 0o664` into `chown 0o644`.
    # Log files need to be group-writable, as the original file creator is
    # preserved and cannot be changed without root or special capabilities.
    old_umask = os.umask(0o0002)

    if not path.parent.exists():
        # Log directories need to be fully group owned for modifications
        # and for listing contents.
        path.parent.mkdir(mode=0o775, parents=True)
        shutil.chown(
            path = path.parent,
            group = config.get('default_user', 'group')
        )
    if not path.exists():
        # Log files should also be group owned, but don't need exec permissions.
        # Group r/w is important as operator actions can spawn files owned by
        # them personally.
        path.touch(mode=mode)
        shutil.chown(
            path = path,
            group = config.get('default_user', 'group')
        )
    if content is not None:
        with open(path, 'w', encoding='utf-8') as f:
            f.write(content)

    os.umask(old_umask)

def run_cmd(cmd, number_of_attempts=1, log_function=None, print_to_stderr=False,
            timeout=None, config=None, logger=None, shell=True):
    """
    Runs a command in console using subprocess.run(...).
    This command will wait for the process to finish, so is not recommended for
    long-running interactive commands where continuous feedback is needed.
    :param cmd: The command to run
    :param number_of_attempts: (optional) Number of times to attempt the execution of the given command
    :param log_function: (optional) The logging function to use in the event of failure in the execution of the command
    :param print_to_stderr: (optional) If True, uses log_utils.error() to print the failed executions to STDERR
    :param timeout: Maximum execution time in seconds for each command attempt. None = no timeout
    :param config: (optional) CtaOpsConfig object
    :param logger: (optional) Logging logger object
    :param shell: (optional) Run the cmd through the shell. Default:True
    :return: The result of the call (contains stdout, stderr, returncode, etc.)
    """
    if logger is not None:
        logger.debug(f"Executing shell command: {cmd}")
    for attempt in range(number_of_attempts):
        try:
            cmd_call = subprocess.run(
                cmd,
                shell = shell,
                encoding = 'utf-8',
                stdout = subprocess.PIPE,
                stderr = subprocess.PIPE,
                executable = '/bin/bash',  # Needed for the option to set shell vars
                timeout = timeout
            )
        except subprocess.TimeoutExpired as e:
            retrying = " , retrying" if attempt + 1 < number_of_attempts else ""
            msg = f"Timeout reached after {attempt + 1} attempts{retrying}"
            if log_function:
                log_function(msg)
            if print_to_stderr:
                log_utils.error(msg)
            if config is not None and logger is not None:
                # TODO: Safe to replace with logger.error?
                log_utils.write_to_log(config, logger, logging.ERROR, msg)
            elif logger is not None:  # TODO: This is what we should converge to
                logger.debug(msg)     # Deprecate the others

        # We retry until cmd_call is successful or we run out of attempts
        remaining_attempts = number_of_attempts - (attempt + 1)
        if cmd_call.returncode != 0:
            msg = f'Command failed: {cmd} {log_utils.format_stderr(cmd_call.stderr)}.'
            if number_of_attempts > 1:
                msg += f' Remaining attempts: {remaining_attempts}'
            if log_function:
                log_function(msg)
            if print_to_stderr:
                log_utils.error(msg)
            if config is not None and logger is not None:
                log_utils.write_to_log(config, logger, logging.ERROR, msg)
            elif logger is not None:  # TODO: This is what we should converge to
                logger.debug(msg)     # Deprecate the others
        else:
            break
    return cmd_call


def write_cmd_output(cmd, write_to, number_of_attempts=1, timeout=None,
                     logger=None, shell=True):
    """
    Runs a command in console using subprocess.run(...).
    Similar to run_cmd(), except that the cmd's stdout is written elsewhere.
    :param cmd: The command to run
    :param write_to: Destination file-like object to write stdout to
    :param number_of_attempts: (optional) Number of times to attempt the execution of the given command
    :param timeout: Maximum execution time in seconds for each command attempt. None = no timeout
    :param config: (optional) CtaOpsConfig object
    :param logger: (optional) Logging logger object
    :param shell: (optional) Run the cmd through the shell. Default:True
    :return: The result of the call (contains stdout, stderr, returncode, etc.)
    """
    if logger is not None:
        logger.debug(f"Executing shell command: {cmd}")
    for attempt in range(number_of_attempts):
        try:
            cmd_call = subprocess.run(
                cmd,
                shell = shell,
                encoding = 'utf-8',
                stdout = write_to,
                stderr = subprocess.PIPE,
                executable = '/bin/bash',  # Needed for the option to set shell vars
                timeout = timeout
            )
        except subprocess.TimeoutExpired as e:
            retrying = " , retrying" if attempt + 1 < number_of_attempts else ""
            msg = f"Timeout reached after {attempt + 1} attempts{retrying}"

        # We retry until cmd_call is successful or we run out of attempts
        remaining_attempts = number_of_attempts - (attempt + 1)
        if cmd_call.returncode != 0 and logger is not None:
            msg = f'Command failed: {cmd} {log_utils.format_stderr(cmd_call.stderr)}. ' \
                  f'Remaining attempts: {remaining_attempts}'
            logger.debug(msg)
        else:
            break
    # We expect that the next action will be to read the content back
    write_to.seek(0)

    return cmd_call


def run_cmd_interactive(cmd, logger, shell=True):
    """
    Run a command such that the stdout/stderr is printed continuously,
    instead of printing everything at the end.
    Useful for long but interactive commands.
    :param cmd: The command to run
    :param logger: (optional) Logging logger object
    :param shell: (optional) Run the cmd through the shell. Default:True
    :returns: The popen process object
    """
    logger.debug(f"Executing interactive command: {cmd}")

    proc = subprocess.Popen(
        cmd,
        shell = shell,
        encoding = 'utf-8',
        executable = '/bin/bash'
    )

    # Wait for the execution to complete, and print any stdout/stderr in the
    # mean time.
    proc.communicate()

    return proc

def run_cmd_popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, use_popen=False, n_attempts=1):
    """
    Runs a command in console using Popen
    :param cmd: The command to run
    :param stdout: (optional) The desired stdout parameter for the Popen() function
    :param stderr: (optional) The desired stderr parameter for the Popen() function
    :param use_popen: (optional) If True, uses Popen (to handle the stdout/stderr in real time)
    :param n_attempts: (optional) Number of attempts to execute the script in case it fails
    :return: The result of the call (contains stdout, stderr, returncode, etc.)
    """
    # Use Popen for real-time output
    if use_popen:
        # Not using "timeout" because these executions take a lot of time (mediacheck, drivetest...)
        return subprocess.Popen(cmd, shell=True, stderr=stderr, stdout=stdout, encoding='utf-8')

    # Not using Popen
    def call_cmd():
        return subprocess.run(cmd, shell=True, stdout=stdout, stderr=stderr, encoding='utf-8')
    cmd_call = call_cmd()
    while n_attempts > 1:
        if cmd_call.returncode == 0:
            break
        n_attempts -= 1
        print(f'Command "{cmd}" failed {log_utils.format_stderr(cmd_call.stderr)}. Remaining attempts: {n_attempts}',
              file=sys.stderr)
        cmd_call = call_cmd()
    return cmd_call


# TODO: Doesn't work as supposed... it returns the output, but NOT in real time (Python bug apparently)
def run_cmd_real_time_stdout(cmd, the_function):
    """
    Runs a command in console using Popen and allows the treatment of the stdout line by line
    :param cmd: The command to run
    :param the_function: The function to apply to every line of the stdout
    :return: The return code of the command execution
    """
    with subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, encoding='utf-8', bufsize=1) as process:
        while True:
            line = process.stdout.readline()
            the_function(line.rstrip())
            if not line:
                break
    return process.returncode


def remove_directory_recursively(path):
    """
    Removes recursively the specified directory and everything inside it
    :param path: The path of the directory to remove
    :return: The return code of the command run
    """
    cmd = f'rm -r {path}'
    return run_cmd(cmd).returncode


def do_cleanup(tmp_dir, config, logger):
    """
    Cleans up temporary files.
    :param tmp_dir: The temp directory used to store temp files
    :param config: (optional) CtaOpsConfig object
    :param logger: (optional) Logging logger object
    """
    for single_file in os.listdir(tmp_dir):
        log_utils.write_to_log(
            config,
            logger,
            logging.DEBUG,
            f"Deleting file {single_file} from {tmp_dir}"
        )
        os.remove(f"{tmp_dir}/{single_file}")

    log_utils.write_to_log(
        config,
        logger,
        logging.DEBUG,
        f"Deleting tmp directory {tmp_dir}"
    )
    os.rmdir(f"{tmp_dir}")


def convert_byte_unit(n_bytes:int, si=True, digits=1):
    """
    Convert a number of bytes into the closest sensible unit (KB, MB, GB).
    :param n_bytes: Integer byte count
    :param si: Toggle SI use of mega/mebi (KB/KiB) units
    :returns: String with the rounded number and unit
    """
    # Special case to avoid log(0)
    if n_bytes == 0:
        return '0B'

    if si:
        units = ('B', 'K', 'M', 'G', 'T', 'P', 'E')
        base = 1000
    else:
        units = ('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB')
        base = 1024

    x = int(math.floor(math.log(n_bytes, base)))
    to_pow_of = math.pow(base, x)
    out_num = round(n_bytes / to_pow_of, digits)

    return f"{out_num}{units[x]}"

def is_real_number(string):
    '''
    Checks if a string represents a real number
    :param string: The string to be checked
    :return: True if the string represents a real number, otherwise False
    '''
    try:
        float(string)
        return True
    except ValueError:
        return False

def split_byte_string(byte_string):
    '''
    Splits a byte string formatted as [number][unit] 
    :param byte_string: The formatted byte string
    :return: Tuple containing the number and unit parts of the byte string
    '''
    match = re.match(r"([0-9.]+)([A-Za-z]+)$", byte_string)
    if match:
        number = match.group(1)
        unit = match.group(2)
        return number, unit
    else:
        return None, None


def get_size_from_format(format, si=True):
    r"""
    Converts a string from format [0-9]+(\.[0-9]+)?([BKMGTP](i[BKMGTP])?)?
    into the corresponding number
    :param format: The formatted string
    :param si: Toggle SI use of mega/mebi (KB/KiB) units
    :return: The corresponding number as an integer
    """

    format_dict = {
        'B': 1,
        'K': 1E3,
        'M': 1E6,
        'G': 1E9,
        'T': 1E12,
        'P': 1E15,
        'E': 1E18,
    }

    if format.isdigit():
        return int(format)

    if si:
        num, format_suffix = format[:-1], format[-1]
    else:
        num, format_suffix = split_byte_string(format)
        # get only the first letter of the suffix so it has a match in format_dict
        format_suffix = format_suffix[0]

    if format_suffix in format_dict:
        base = format_dict[format_suffix]

        if (si is False) and (base != 1):
            base = 1024**(math.log(base, 1000))

        if num.isdigit():
            return int(num) * base
        elif is_real_number(num):
            return int(float(num) * base)

    return None


def display_hint(command_hint:str):
    """
    Print command hint for operators, such as an example command.
    :param command_hint: Example command to print.
    """
    hint = f'SUGGESTION (the operator may just copy and execute this exact command): ' \
        f'{command_hint}'

    log_utils.eprint(hint)


def effectively_readable(path):
    """
    Checks if a file is readable or not
    :param path: The path to the file we want to check
    :return: True if readable, False otherwise
    """
    uid = os.getuid()
    euid = os.geteuid()
    gid = os.getgid()
    egid = os.getegid()

    # This is probably true most of the time, so just let os.access()
    # handle it.  Avoids potential bugs in the rest of this function.
    if uid == euid and gid == egid:
        return os.access(path, os.R_OK)

    st = os.stat(path)

    # This may be wrong depending on the semantics of the OS.
    # i.e. if the file is -------r--, does the owner have access or not?
    if st.st_uid == euid:
        return st.st_mode & stat.S_IRUSR != 0

    # See comment for UID check above.
    groups = os.getgroups()
    if st.st_gid == egid or st.st_gid in groups:
        return st.st_mode & stat.S_IRGRP != 0

    return st.st_mode & stat.S_IROTH != 0


def effectively_executable(path):
    """
    Checks if a file is executable or not
    :param path: The path to the file we want to check
    :return: True if executable, False otherwise
    """
    uid = os.getuid()
    euid = os.geteuid()
    gid = os.getgid()
    egid = os.getegid()

    if uid == euid and gid == egid:
        return os.access(path, os.X_OK)

    st = os.stat(path)

    if st.st_uid == euid:
        return st.st_mode & stat.S_IXUSR != 0

    groups = os.getgroups()
    if st.st_gid == egid or st.st_gid in groups:
        return st.st_mode & stat.S_IXGRP != 0

    return st.st_mode & stat.S_IXOTH != 0
