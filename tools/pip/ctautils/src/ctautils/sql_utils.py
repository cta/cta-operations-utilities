# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Utilities for interacting with databases
"""

import logging

from contextlib import closing

import psycopg

from ctautils import log_utils, config_holder, dict_utils


def connect(config, db_name:str=None):
    """
    Establishes a connection to the corresponding MySQL database and returns
    the DB object
    :param config: A ConfigHolder object, which contains the DB credentials
    :param db_name: Name of a specific database
    :return: The resulting DB connection object
    """
    # Temporarily support both config objects
    if isinstance(config, config_holder.ConfigHolder):
        if db_name is None:
            db_name = dict_utils.get_or_default(
                config.config,
                'db_name',
                None
            )
        db = psycopg.connect(
            host = config.config['db_host'],
            port = config.config['db_port'],
            user = config.config['db_username'],
            password = config.config['db_password'],
            dbname = db_name,
            cursor_factory=psycopg.ClientCursor  # Client-side binding cursor
        )
    else:
        if db_name is None:
            db_name = config.get('database', 'name', default=None)
        db = psycopg.connect(
            host = config.get('database', 'host'),
            port = config.get('database', 'port'),
            user = config.get('database', 'username'),
            password = config.get('database', 'password'),
            dbname = db_name,
            cursor_factory=psycopg.ClientCursor  # Client-side binding cursor
        )
    return db


def execute_query(db_connection, query, data=None, db_name=None, commit=False, 
                  config=None, logger=None):
    """
    Executes a given query, differentiating between a query that needs a commit 
        (INSERT, DELETE...) vs one that doesn't (SELECT)
    :param db_connection: A DB connection object, as returned by connect()
    :param query: The query string to execute
    :param data: (Optional) Iterable of query variables to insert
    :param db_name: (Optional) The name of the db to query, if the connection 
        object does not explicitly reference it
    :param commit: (optional) Specify if the query needs a commit. If not, 
        it means we're doing a SELECT
    :param config: (Optional) A ConfigHolder object, 
        which specifies where to write logs
    :param logger: (Optional) A Python logging logger
    :return: The query result, if we didn't perform any commit 
        (meaning we executed a SELECT)
    """

    # Fallback config, in case none is given
    if config is None:
        config = config_holder.ConfigHolder(
            log_file_path = '/tmp/cta_sql_utils_fallback.log'
        )

    # Fallback logger, in case none is given
    if logger is None:
        logger = logging.getLogger('cta_sql_utils_logger')

    # Allow override of db
    if db_name is not None:
        db_connection.database = db_name

    # Execute the DB query
    try:
        query_result = None

        with closing(db_connection.cursor()) as cursor:
            pretty_query = f"{cursor.mogrify(' '.join(query.split()), data)}"
            # Temporarily support both config objects
            if isinstance(config, config_holder.ConfigHolder):
                log_utils.write_to_log(
                    config,
                    logger,
                    logging.DEBUG,
                    f'Executing query: "{pretty_query}"'
                )
            else:
                logger.debug(f'Executing query: "{pretty_query}"')
            cursor.execute(query, data)
            if commit:
                db_connection.commit()
            else:
                query_result = cursor.fetchall()  # List of tuples

        return query_result

    except Exception as e:
        log_utils.write_to_log(
            config,
            logger,
            logging.ERROR,
            f"""Error executing query: "{cursor.mogrify(' '.join(query.split()), data)}" -- ({str(e)})"""
        )
        log_utils.log_and_abort(
            config,
            logger,
            'Execution will terminate due to previous error'
        )
