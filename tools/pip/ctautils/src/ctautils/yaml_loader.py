# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Helpers for dealing with yaml files
"""

import sys
import yaml


def load_yaml(path_to_yaml_file):
    """
    Loads a YAML file and retrieves its content
    :param path_to_yaml_file: The path to the desired YAML file (path/filename)
    :return: A dict containing the values of the parsed YAML file
    """
    try:
        with open(path_to_yaml_file, 'r') as stream:
            return yaml.safe_load(stream)
    except Exception as e:
        print(f'Could not load YAML file: {path_to_yaml_file} ({str(e)})')
        sys.exit(1)
