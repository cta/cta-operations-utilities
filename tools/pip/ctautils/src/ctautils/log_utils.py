# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Log writing utilities
https://docs.python.org/3/howto/logging-cookbook.html
"""

import os
import re
import sys
import gzip
import json
import stat
import time
import shutil
import inspect
import logging
import logging.handlers

from pwd import getpwuid
from pathlib import Path
from datetime import datetime

from ctautils import dict_utils, cmd_utils, config_holder


###############
# Configuration
###############

# The equivalent of octal 664
DEFAULT_LOG_FILE_PERM = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | \
    stat.S_IWGRP | stat.S_IROTH

LOG_STDERR_PATTERN = re.compile(r'\(STDERR: .*\)')

###########
# Utilities
###########

def is_log_file_owner(user, path):
    """
    Checks if the user executing the script is the same as the owner of the log file
    :param user: The name of the user executing the script
    :return: True if same user; False otherwise
    """
    def find_owner(filename):
        return getpwuid(os.stat(filename).st_uid).pw_name

    owner = find_owner(path)
    return {
            'is_owner': user == owner,
            'owner_username': owner
    }


def now():
    """
    Return date and time in "YYYY/MM/DD HH:MM:SS"
    :return: Current date and time
    """
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def datetime_str_to_unix_ts(log_ts_str, format_str):
    '''
    Converts a string representing a datetime in the given format into a unix timestamp
    :param log_ts_str: The timestamp string to be converted
    :param format_str: The format description of log_ts_str
    :returns: the timestamp of the string in unix format
    '''
    if log_ts_str is None:
        return 0
    return int(time.mktime(datetime.strptime(log_ts_str, format_str).timetuple()))


def get_caller_name():
    """
    Gets the name of the called function.
    Assumes it is called by a logging helper function, such that the
    function we want to get the name from is two steps up in the stack.
    :return: The name of the called function
    """
    current_frame = inspect.currentframe()
    caller_frame = inspect.getouterframes(current_frame, 2)
    caller_name = caller_frame[2][3]

    return caller_name


##############################
# Python logging-based logging
##############################
class RotatingCompressingGroupFileHandler(logging.handlers.RotatingFileHandler):
    """
    Filesize-based rotating log file handler which also sets the file permissions
    such that the newly created file is tape-group writable.
    Rotated logs are .gz compressed.
    :param filename: Path-like or string pointing to the log file to rotate
    :param date_fmt: Format if date in rotated file names ('%Y-%m-%d')
    :param group: Name of the UNIX group owning the log file
    """
    filename = None
    date_fmt = None
    file_owner_group = None

    def __init__(self, filename, group:str, date_fmt:str, mode='a', maxBytes=0,
                 backupCount=0, encoding='utf-8', delay=False):
        """
        Delegate init to parent class, and save intended owner UNIX group.
        """
        self.filename = Path(filename)
        self.date_fmt = date_fmt
        self.file_owner_group = group
        super().__init__(
            filename,
            mode = mode,
            maxBytes = maxBytes,
            backupCount = backupCount,
            encoding = encoding,
            delay = delay
        )

    def my_namer(self):
        """
        Overwrite how we want to name rotated log files.
        Give them a timestamp and gz compression extension.
        This method should be called 'namer', but the python 3.6 logging
        module is too old for that.
        """
        timestamp = datetime.now().strftime(self.date_fmt)
        return f'{self.filename}-{timestamp}.gz'

    def my_rotator(self, src, dst):
        """
        Specify how we rotate the log file.
        :param src: Source file path
        :param dst: Destination File path
        """
        with open(src, 'rb') as source_file:
            with gzip.open(dst, 'wb') as dst_file:
                shutil.copyfileobj(source_file, dst_file)
        shutil.chown(
            path = dst,
            group = self.file_owner_group
        )
        os.remove(src)

    def doRollover(self):  # Name decided by logging lib
        """
        Do standard timed rollover, but also give the group permission to write.
        """
        if self.stream:
            self.stream.close()
            self.stream = None

        log_archive_list = sorted(Path('.').glob('*.gz'))
        if self.backupCount > 0:
            # Delete undesired log files
            n_logs_to_delete = len(log_archive_list) - self.backupCount
            for i in range(n_logs_to_delete):
                if os.path.exists(log_archive_list[i]):
                    os.remove(log_archive_list[i])

            dst_file_name = self.my_namer()
            self.my_rotator(self.filename, dst_file_name)

        self.filename.touch()  # TODO: with exists_ok = True, when supported
        shutil.chown(
            path = self.filename,
            group = self.file_owner_group
        )
        os.chmod(self.filename, DEFAULT_LOG_FILE_PERM)


class TranslatingFormatter(logging.Formatter):
    """
    Logging record formatter capable of replacing confusing error messages
    emitted by external tools.
    """
    record_translator = None

    def __init__(self, fmt, datefmt, style, config):
        """
        Set up for error message replacement.
        """
        super().__init__(
            fmt = fmt,
            datefmt = datefmt,
            style = style
        )
        self.record_translator = config_holder.ErrorTranslator(
            config = config
        )

    def formatMessage(self, record):
        """
        Format the message text of the record to emit.
        If a replacement is found, then the replacement is formatted the same way
        an stderr output from the command would be formatted
        :param record: Generated log record to format
        :returns: Formatted log message
        """
        translation = self.record_translator.translate(record.msg)
        if translation is not None:
            record.msg = LOG_STDERR_PATTERN.sub(
                format_stderr(translation),
                record.msg
            )
        return self._style.format(record)

def format_stderr(stderr:str):
    """
    Format a command's stderr output in a standard way.
    :param stderr: The error message returned by the executed command
    :returns: Formatted error message
    """
    return f"(STDERR: {stderr.rstrip()})"

def init_logger(name:str, config,
                json_file:bool=False, set_conf_logger=True):
    """
    Set up a CTA operations logger explicitly, for fine-grained control.
    :param name: Name of the logger, usually the calling script's name
    :param config: CtaOpsConfig object
    :param json_file: (Optional) Log to file with structured json instead.
    :param set_conf_logger: (Optional) Make conf object log with this logger
    :returns: logging logger object
    """
    # Hierarchical tool_name-script_name logger
    name_hierarchy = (config.tool, name)
    logger_name = '.'.join(name_hierarchy)
    log_file_path= f'''{config.get('logging', 'log_dir')}/{logger_name}.log'''

    logger = logging.getLogger(logger_name)

    # Create dir structure and file with correct permissions
    cmd_utils.create_ops_owned_file(
        path = log_file_path,
        config = config,
        mode = DEFAULT_LOG_FILE_PERM,
    )

    # Set output level
    logger.setLevel(logging.INFO)
    if config.get('debug'):
        logger.setLevel(logging.DEBUG)

    # Message formatting
    # See https://docs.python.org/3/library/stdtypes.html#str.format for options
    fmt = TranslatingFormatter(
        fmt = "{asctime} [{levelname}] [{funcName}] {message}",
        datefmt = config.get('logging', 'date_format'),
        style='{',
        config = config
    )

    max_log_size = config.get('logging', 'rotation_max_size', default='1G')
    # Log to file with size-based log rotation
    #
    rot_file_handler = RotatingCompressingGroupFileHandler(
        filename = log_file_path,
        group = config.get('default_user', 'group'),
        date_fmt = config.get('logging', 'date_format').replace(' ', '_'),
        maxBytes = cmd_utils.get_size_from_format(max_log_size),
        backupCount = config.get('logging', 'rotation_keep_files'),
    )
    if json_file:
        rot_file_handler.setFormatter(
            logging.Formatter(
                fmt = '{{ "timestamp":{asctime} "level":"{levelname}"' \
                '"function":"{funcName}" "msg":"{message}" }}',
                datefmt = '%s',
                style='{'
            )
        )
    else:
        rot_file_handler.setFormatter(fmt)
    logger.addHandler(rot_file_handler)
    # stderr logging
    stderr_handler = logging.StreamHandler()
    stderr_handler.setFormatter(fmt)
    logger.addHandler(stderr_handler)

    # Set the configuration object used to config this logger,
    # which is generally used by the remainder of the calling script
    # to log with this logger as well.
    if set_conf_logger:
        config.set_logger(logger)

    return logger


def get_logger_file_path(logger):
    """
    Return the path of the file the logger writes to.
    Assumes the logger was created with init_logger().
    :param logger: The logging logger object
    :returns: Absolute path string
    """
    return logger.handlers[0].baseFilename

def log_and_exit(logger:logging.Logger, msg:str, return_code:int=1):
    """
    Wrapper for logging a last message, before exiting the program.
    :param logger: A named logger from the standard logger module
    :param msg: The message to log
    :param return_code: The return code to use
    """
    if return_code > 0:
        logger.critical(msg)
    else:
        logger.info(msg)
    sys.exit(return_code)


def write_to_log(config, logger, level, msg: str, log_file_path=None, show_level=True, show_timestamp=True,
                 write_to_stdout=False, write_to_stderr=True):
    """
    DEPRECATED - use init_logger() and normal logger.info()... calls instead.
    Writes to the specified log file.
    Also writes the same message to stderr by default.
    :param config: A CtaOpsConfig
    :param logger: A named logger from the standard logger module
    :param level: The log level (logging.INFO, logging.DEBUG, ...)
    :param msg: The message to log
    :param log_file_path: (optional) The path for the log file
    :param show_level: (optional) Logs also the level (INFO, DEBUG, ...)
    :param show_timestamp: (optional) Logs also the timestamp (%Y-%m-%d %H:%M:%S)
    :param write_to_stdout: (optional) Writes also to STDOUT using StreamHandler
    :param write_to_sterr: (optional) Writes also to stderr using StreamHandler
    """

    # Temporarily support both config objects
    if isinstance(config, config_holder.ConfigHolder):
        # If we don't specify a log file path, we take the one stored in ConfigHolder,
        # else default to logger name, which is a fallback.
        if log_file_path is None and config.log_file_path is not None:
            log_file_path = config.log_file_path
        else:
            log_file_path = dict_utils.get_or_default(
                config.config,
                'log_file_path',
                f'/var/log/cta/{logger.name}'
            )
        min_log_level = logging.DEBUG if dict_utils.get_or_default(config.config, 'debug', False) else logging.INFO
    else:
        # arg-based log_file_path no longer supported!
        log_file_path = f"{config.get('logging', 'log_dir')}/{logger.name}.log"
        min_log_level = logging.DEBUG if config.get('debug') else logging.INFO

    if not os.path.exists(os.path.dirname(log_file_path)):      # If directories don't exist, create them
        os.makedirs(os.path.dirname(log_file_path))
    if not os.path.exists(log_file_path):                       # Also create the file itself
        open(log_file_path, 'a').close()
        os.chmod(log_file_path, DEFAULT_LOG_FILE_PERM)

    handlers = [logging.FileHandler(log_file_path)]             # Writes to the specified file
    if write_to_stdout:
        handlers.append(
            logging.StreamHandler(stream=sys.stdout)            # Writes to stdout if specified
        )
    if isinstance(config, config_holder.ConfigHolder) and write_to_stderr:
        handlers.append(
            logging.StreamHandler()                             # Writes to sterr if specified
        )

    datefmt = None
    the_format = "%(message)s"                                  # Different format configurations
    if show_level:
        the_format = f"[%(levelname)-s] {the_format}"
    if show_timestamp:
        the_format = f"%(asctime)s {the_format}"
        datefmt = "%Y-%m-%d %H:%M:%S"

    for handler in logging.root.handlers[:]:                    # Remove previous existing logging configuration
        logging.root.removeHandler(handler)

    logging.basicConfig(                                        # Apply new logging configuration for current message
        level = min_log_level,
        format = the_format,
        datefmt = datefmt,
        handlers = handlers
    )
    logger.log(level, f'[{get_caller_name()}] {msg}')           # Log the message with the specified level


def log_and_abort(config, logger, msg: str, log_file_path=None, show_level=True, show_timestamp=True,
                 write_to_stdout=False, write_to_stderr=True, return_code=1):
    """
    DEPRECATED - Use log_and_exit() instead.
    A variation of write_to_log which exits the program
    due to an encountered problem.
    Note that we do not call write_to_log, in order to maintain a stable
    frame depth for get_caller_name().
    :param config: A CtaOpsConfig (with log_file_path set)
    :param logger: A named logger from the standard logger module
    :param msg: The message to log
    :param log_file_path: (optional) The path for the log file
    :param show_level: (optional) Logs also the level (INFO, DEBUG, ...)
    :param show_timestamp: (optional) Logs also the timestamp (%Y-%m-%d %H:%M:%S)
    :param write_to_stdout: (optional) Writes also to STDOUT using StreamHandler
    :param write_to_sterr: (optional) Writes also to stderr using StreamHandler
    :param return_code: The return code to use
    """

    level = logging.CRITICAL

    # Temporarily support both config objects
    if isinstance(config, config_holder.ConfigHolder):
        # If we don't specify a log file path, we take the one stored in ConfigHolder,
        # else default to logger name, which is a fallback.
        if log_file_path is None and config.log_file_path is not None:
            log_file_path = config.log_file_path
        else:
            log_file_path = dict_utils.get_or_default(
                config.config,
                'log_file_path',
                f'/var/log/cta/{logger.name}'
            )
        min_log_level = logging.DEBUG if dict_utils.get_or_default(
            config.config, 'debug', False) else logging.INFO
    else:
        # arg-based log_file_path no longer supported!
        log_file_path = f"{config.get('logging', 'log_dir')}/{logger.name}.log"
        min_log_level = logging.DEBUG if config.get('debug') else logging.INFO

    if not os.path.exists(os.path.dirname(log_file_path)):      # If directories don't exist, create them
        os.makedirs(os.path.dirname(log_file_path))
    if not os.path.exists(log_file_path):                       # Also create the file itself
        open(log_file_path, 'a').close()
        os.chmod(log_file_path, DEFAULT_LOG_FILE_PERM)

    handlers = [logging.FileHandler(log_file_path)]             # Writes to the specified file
    if write_to_stdout:
        handlers.append(
            logging.StreamHandler(stream=sys.stdout)            # Writes to stdout if specified
        )
    if write_to_stderr:
        handlers.append(
            logging.StreamHandler()                             # Writes to sterr if specified
        )

    datefmt = None
    the_format = "%(message)s"                                  # Different format configurations
    if show_level:
        the_format = f"[%(levelname)-s] {the_format}"
    if show_timestamp:
        the_format = f"%(asctime)s {the_format}"
        datefmt = "%Y-%m-%d %H:%M:%S"

    for handler in logging.root.handlers[:]:                    # Remove previous existing logging configuration
        logging.root.removeHandler(handler)

    logging.basicConfig(                                        # Apply new logging configuration for current message
        level = min_log_level,
        format = the_format,
        datefmt = datefmt,
        handlers = handlers
    )
    logger.log(level, f'[{get_caller_name()}] {msg}')           # Log the message with the specified level

    sys.exit(return_code)


#################################################
# Basic logging without the Python logging module
#################################################

def eprint(msg):
    """
    Error printing
    :param msg: The message
    """
    sys.stderr.write(f"{msg}\n")


def info(msg):
    """
    Print a message to STDOUT
    :param msg: Message to be printed
    """
    print(f"{now()} [INFO] [{get_caller_name()}] {msg}")


def debug(msg):
    """
    Print a message to STDOUT
    :param msg: Message to be printed
    """
    print(f"{now()} [DEBUG] [{get_caller_name()}] {msg}")


def warning(msg):
    """
    Prints a warning message
    :param msg: Message to be printed
    """
    eprint(f"{now()} [WARN] [{get_caller_name()}] {msg}")


def error(msg):
    """
    Prints an error message
    :param message: Message to be printed
    """
    eprint(f"{now()} [ERROR] [{get_caller_name()}] {msg}")


def abort(msg, add_timestamp=True):
    """
    Print a message to STDERR and abort the script
    :param msg: Message to be printed
    :param add_timestamp: (optional) Specifies whether adding timestamp at the beginning of the message or not
    """
    if add_timestamp:
        eprint(f"{now()} [ABORT] [{get_caller_name()}] {msg}")
    else:
        eprint(f"[ABORT] {get_caller_name()}  {msg}")

    sys.exit(1)


def exit(msg, add_timestamp=True):
    """
    Print a message to STDERR and exit the script. Differs from abort()
    by the fact that this is not considered to be an error/problem.
    :param msg: Message to be printed
    :param add_timestamp: (optional) Specifies whether adding timestamp at the beginning of the message or not
    """
    if add_timestamp:
        eprint(f"{now()} [INFO] [{get_caller_name()}] {msg}")
    else:
        eprint(f"[INFO] {get_caller_name()}  {msg}")

    sys.exit(0)


def exit_with_message(return_code, key_id='', encryption_key='', message=''):
    """
    Print relevant information to stdout and exit with the return code.
    :param return_code: Embedded in JSON output and exited with.
    :param key_id: The encryption key_id found.
    :param encryption_key: The encryption key found.
    :param message: The message payload of the JSON output.
    """
    code_error_hash = {
        0: 'SUCCESS',
        1: 'ERROR',
        2: 'ERROR_INVALID_ARGS',
    }
    output = {
        'response': {
            'code': return_code,
            'description': code_error_hash[return_code],
        },
        'message': message,
        'key_id': key_id,
        'encryption_key': encryption_key
    }
    print(json.dumps(output))
    sys.exit(return_code)
