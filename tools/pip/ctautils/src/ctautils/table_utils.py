# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Table formatting helpers.
"""

from collections import namedtuple

import tabulate

# Adjust tabulate's settings, and specify a custom table format for the purpose
# of extra dense tables. Nicer in the terminal.
tabulate.PRESERVE_WHITESPACE = True
tabulate.MIN_PADDING = 0

TableFormat = namedtuple(
    "TableFormat",
    [
        "lineabove",
        "linebelowheader",
        "linebetweenrows",
        "linebelow",
        "headerrow",
        "datarow",
        "padding",
        "with_header_hide",
    ],
)

tabulate._table_formats['plain_compact'] = TableFormat(
    lineabove = tabulate.Line("", "-", " ", ""),
    linebelowheader = None,
    linebetweenrows = None,
    linebelow = tabulate.Line("", "-", " ", ""),
    headerrow = tabulate.DataRow("", " ", ""),
    datarow = tabulate.DataRow("", " ", ""),
    padding = 0,
    with_header_hide=["lineabove", "linebelow"],
)


def color_headers(headers, config):
    """
    Takes a list of headers and colors them.
    :param headers: The list of headers
    :param config: CtaOpsConfig
    :return: The colored list of headers for Unix interpretation
    """
    color = config.get('colors', 'ansi_color_table')
    end_color = config.get('colors', 'ansi_end')
    out_headers = []
    for header in headers:
        multi_line_header = f'{color}{header}{end_color}'
        out_headers.append(multi_line_header)
    return out_headers

def get_table(headers:list, rows:list, config, col_align=None, group_by=None, color=True):
    """
    Produce a table and format it according to the provided configuration.
    :param headers: List of header strings
    :param rows: List of table rows with data
    :param config: CtaOpsConfig
    :param col_align: Tabulate column alignment
    :param group_by: The item to group by (to control format corner cases)
    :param color: Boolean to toggle use of ANSI color codes
    :returns: A tabulate table
    """
    if color:
        headers = color_headers(headers, config)
    table_format = config.get('table_format')
    table = tabulate.tabulate(
        rows,
        headers,
        tablefmt = table_format,
        colalign = col_align,
        #maxcolwidths = config.get('table_max_col_width', default=30)
        # TODO: Use global header and row formatting once we upgrade to 0.9.0+
    )

    return table
