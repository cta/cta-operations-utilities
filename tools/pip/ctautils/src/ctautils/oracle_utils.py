# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Oracle DB interaction helpers
"""

import cx_Oracle


def get_oracle_connection(oracle_config_file_path):
    """
    Connects to the Oracle "tapelog" database
    :param oracle_config_file_path: The path to the config file with the Oracle database credentials
    :return: The connection object to the database
    """
    # We parse the corresponding config file for the DB credentials
    credentials = {'DBNAME': None, 'DBUSER': None, 'DBPASSWD': None}
    for credential in credentials.keys():
        with open(oracle_config_file_path, 'r') as config_file:
            for line in config_file:
                if f'{credential}=' in line:
                    credentials[credential] = line.split(f'{credential}=')[1].rstrip()
                    break
    # We connect to the database using the acquired credentials
    return cx_Oracle.connect(f'{credentials["DBUSER"]}/{credentials["DBPASSWD"]}@{credentials["DBNAME"]}')

