# Think to use case-converter library to better cover the use cases.

def to_pascal_case(text: str) -> str:
    words = text.replace("-", " ").replace("_", " ").split()
    if not words:
        return ""
    if len(words) == 1:
        return text.capitalize() if text.isupper() or text.islower() else text[0].upper() + text[1:] 
    return "".join(word.capitalize() for word in words)


def to_all_caps(text: str) -> str:
    words = text.replace("-", " ").replace("_", " ").split()
    return "_".join(word.upper() for word in words)
