# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Utility functions for interacting with InfluxDB

Relies on https://github.com/influxdata/influxdb-python/
"""
from influxdb import InfluxDBClient

from ctautils import dict_utils, config_holder


def connect(config, database=None):
    """
    Establishes a connection to the corresponding InfluxDB database and returns the DB object
    :param config: A ConfigHolder object, which contains the DB credentials
    :param database: Name of a specific database
    :return: The resulting InfluxDBClient object
    """
    # Temporarily support both config objects
    if isinstance(config, config_holder.ConfigHolder):
        if database is None:
            database = dict_utils.get_or_default(
                config.config,
                'influxdb_database',
                None
            )

        influxdb_client = InfluxDBClient(
            host = config.config['influxdb_host'],
            port = config.config['influxdb_port'],
            username = config.config['influxdb_username'],
            password = config.config['influxdb_password'],
            ssl=True,
            verify_ssl = False,
            database = database,
            timeout = dict_utils.get_or_default(
                config.config,
                'influxdb_client_timeout',
                300
            )
        )
    else:
        if database is None:
            database = config.get('influxdb_database', default=None)

        influxdb_client = InfluxDBClient(
            host = config.get('influxdb_host'),
            port = config.get('influxdb_port'),
            username = config.get('influxdb_username'),
            password = config.get('influxdb_password'),
            ssl = True,
            verify_ssl = False,
            database = database,
            timeout = config.get('influxdb_client_timeout', default=300)
        )

    return influxdb_client


def write_point_list(influxdb_client, point_list, retention_policy):
    """
    Simple wrapper for writing point lists of dicts to influxdb.
    Points must have timestamps in second-resolution.
    This way one saves time on debugging problems such as forgetting to set
    the time precision.

    Point format:
    {
        "measurement": <measurement_name>,
        "tags": {
            "tag_key_one": 'tag_value_one',
            "tag_key_two": 'tag_value_two'
        },
        "time": <unix_timestamp_in_seconds>,
        "fields": {
            "field_key_one": field_val_one,
            "field_key_two": field_val_two
        }
    }
    """
    influxdb_client.write_points(
        point_list,
        batch_size = 40000,
        protocol = 'json',
        retention_policy = retention_policy,
        time_precision='s'
    )


