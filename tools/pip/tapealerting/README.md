# Tape Alerting System

This is the basic documentation on usage, maintenance and functioning on the **Tape Alerting System** for CTA.

## Tape Monitoring Infrastructure

To run this tool one will need:

1. A periodic trigger for execution, such as `cron` or Rundeck
2. An InfluxDB time series database for storing events
3. A log gathering and parsing software such as Fluentd
4. The package `cta-cli`, in order to connect 
5. Preferably a python virtual environment to install the pip package and its dependencies into

## Execution flow

The Tape Alerting System can be executed via CLI with a single command, `cta-ops-tape-alerting-system`. 
It must run on a host with CTA cli-tools installed such that it can connect to the desired frontend.
For each execution of the script, the following steps are performed:

1. The last **24 hours** (default) rows from the corresponding InfluxDB measurement of finished tape sessions are fetched for analysis.
2. These are compared to a local cache of already processed sessions, in order to avoid duplicating alerts.
3. Each enabled and applicable **alerting job** is executed on each of the new entries gathered from InfluxDB.
4. If the occurrence and unique device **thresholds** of a given alert is reached, such alert is **raised** for a given **item** (tape/drive):
    1. If we had already raised **any** alert for such item in the past 24 hours, the alert is **masked** ("ignored") and no further actions are taken.
    2. Otherwise, a notification **email** is sent to the configured recipient, the event is logged in InfluxDB for use in further monitoring, the corresponding tape/drive is **disabled/put down**, and the **time**, **alert name** and **item ID** are logged to allow alert **masking** for the next 24 hours.

## Project Structure
Let's briefly explain the structure and basic purpose of each file and folder in this project:

### tape\_alerting\_system.py
Entry point of the program. This is the script that shall be called from CLI for the system to work.

