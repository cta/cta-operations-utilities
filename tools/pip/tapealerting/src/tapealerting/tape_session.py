# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN


import json

from datetime import datetime

from ctautils import cmd_utils, dict_utils
from ctautils.config_holder import CtaOpsConfig

class TapeSession:
    """
    Representation of a logged CTA Tape Session.
    """
    def __init__(
        self, json_str: str = None, db_entry: dict = None, config: CtaOpsConfig = None
    ):
        """
        Init either from a json string (from file) or a DB query result.
        :param json_str: Load session obj from this json representation
        :param db_entry: Load session obj from database row
        :param config: CtaOpsConfig object
        """
        self.errors = {}

        if json_str is not None:
            self._from_json(json_str)
        elif db_entry is not None:
            self._from_db_entry(db_entry, config)

    def get_id(self):
        """
        Return the ID corresponding to the type of session
        :return: The session's volume request ID as a string
        """
        return self.vol_req_id

    def _from_json(self, json_str:str):
        """
        Create the TapeSession object from a stored json representation.
        :param json_str: The json string from the local cache
        """
        json_dict = json.loads(json_str)

        # Special care for typing, as the json log format is still being
        # refined at this time.
        vol_req_id = json_dict['vol_req_id']
        if vol_req_id is not None and not isinstance(vol_req_id, int):
            vol_req_id = int(vol_req_id)

        self.time = datetime.fromtimestamp(
            int(json_dict['timestamp_ns']) / 10**9
        )

        self.library = json_dict['library']
        self.drive = json_dict['drive']
        self.hostname = json_dict['hostname']
        self.vid = json_dict['vid']
        self.vol_req_id = vol_req_id
        self.mount_type = json_dict['mount_type']
        # These were put in after the fact, so we have to account for
        # the cache not containing these fields. TODO: Clean up later.
        self.files_transferred = dict_utils.get_or_default(
            json_dict,
            'files_transferred',
            0
        )
        self.bytes_transferred = dict_utils.get_or_default(
            json_dict,
            'bytes_transferred',
            0
        )
        self.transfer_rate = dict_utils.get_or_default(
            json_dict,
            'transfer_rate',
            0.0
        )
        self.vo = json_dict['vo']
        self.status = json_dict['status']

        self.mountAttempted = int(
            dict_utils.get_or_default(
                json_dict,
                'mountAttempted',
                1
            )
        )
       
        self.tape_pool =  json_dict['tape_pool']

    def _from_db_entry(self, db_entry:dict, config):
        """
        Take the TapeSessionFinished info from InfluxDB (which stems from the
        cta-taped logs), and create a TapeSession object from one row.
        :param db_entry: Session data point/row from the DB in dict form
        :param config: CtaOpsConfig object
        """

        # Special care for typing, as the json log format is still being
        # refined at this time.
        vol_req_id = db_entry['volReqId']
        if vol_req_id is not None and not isinstance(vol_req_id, int):
            vol_req_id = int(vol_req_id)

        self.time = datetime.fromtimestamp(
            int(db_entry["epoch_time"])
        )

        self.library = db_entry["logicalLibrary"]
        self.drive = db_entry["tapeDrive"]
        self.hostname = db_entry['hostname']
        self.vid = db_entry["tapeVid"]
        self.vol_req_id = vol_req_id
        self.mount_type = db_entry["mountType"]
        self.files_transferred = int(
            dict_utils.get_or_default(
                db_entry,
                'filesCount',
                default = 0,
            )
        )
        self.bytes_transferred = int(
            dict_utils.get_or_default(
                db_entry,
                'dataVolume',
                default = 0,
            )
        )
        self.transfer_rate = float(
            dict_utils.get_or_default(
                db_entry,
                'driveTransferSpeedMBps',
                default = 0.0,
            )
        )

        self.vo = db_entry["vo"]
        self.status = db_entry["status"]
        # We cannot back populate old tape sessions; if the field is not set
        # assume we attempted to mount
        self.mountAttempted = int(
            dict_utils.get_or_default(
                db_entry,
                'mountAttempted',
                1
            )
        )

        self.tape_pool = db_entry["tapePool"]

        # ERRORS!
        # TODO: Move config!
        error_types = config.get(
            'jobs',
            'check_too_many_tape_alerts_tape',
            'counter_names'
        )
        for configured_error in error_types:
            if (configured_error in db_entry and
                db_entry[configured_error] is not None):
                self.errors[configured_error] = int(db_entry[configured_error])

    def _to_dict(self):
        """
        Produce a dict representation of the session object.
        :return: Dict
        """
        # InfluxDB is already using ns here, so we will too
        properties = {
            'timestamp_ns': int(self.time.strftime('%s%f'))*10**3,
            'library': self.library,
            'drive': self.drive,
            'hostname': self.hostname,
            'vid': self.vid,
            'vol_req_id': self.vol_req_id,
            'mount_type': self.mount_type,
            'files_transferred': self.files_transferred,
            'bytes_transferred': self.bytes_transferred,
            'transfer_rate': self.transfer_rate,
            'vo': self.vo,
            'status': self.status,
            'tape_pool': self.tape_pool,
            'tape_mounted': False,
            'mount_id': 'NA',
            #'total_time': self.total_time,
        }
        return properties

    def to_json(self):
        """
        Return a json representation of the TapeSession object.
        :return: json string
        """
        return json.dumps(self._to_dict())

    def to_list(self, config=None):
        """
        Return a list representation of the session's properties,
        mainly for tabulation.
        :param config: (Optional) CtaOpsConfig object
        """
        properties = self._to_dict()

        # Replace timestamp with human-friendly time
        if config is not None:
            datetime_fmt = config.get('logging', 'date_format')
        else:
            datetime_fmt = "%Y-%m-%d %H:%M:%S"
        properties['timestamp_ns'] = self.time.strftime(datetime_fmt)
        properties['bytes_transferred'] = cmd_utils.convert_byte_unit(
            n_bytes = self.bytes_transferred
        )
        properties['transfer_rate'] = round(self.transfer_rate, 1)

        # We don't want to include mount and volume IDs in the output,
        # but they are used by the script internally.
        del properties['vol_req_id']
        del properties['mount_id']

        property_list = list(properties.values())

        error_string = ''
        for key, val in self.errors.items():
            error_string += f'{key}({val}), '

        property_list.append(error_string)
        return property_list

    def __str__(self):
        """
        String formatting, largely intended for logging of individual sessions.
        """
        str_vals = [str(val) for val in self._to_dict().values()]
        out_string = ' | '.join(str_vals)
        # TODO: Add errors
        return out_string

    def __eq__(self, other):
        """
        Compare TapeSessions. Needed for checking collection membership.
        :param other: The other TapeSession to compare to
        """
        if isinstance(other, type(self)):
            if (self._to_dict() == other._to_dict()):
                return True
        return False
