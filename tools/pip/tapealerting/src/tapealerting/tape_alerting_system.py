#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Tape Alerting System - Identifies and reacts to tape related incidents.
"""

import sys
import json
import inspect
import argparse

from string import Template
from pathlib import Path
from datetime import datetime, timedelta

import urllib3
from importlib.resources import path
import tapeadmin

from influxdb import exceptions
from ctautils import ( 
    log_utils, 
    influxdb_utils, 
    config_holder, 
    cmd_utils, 
    dict_utils, 
    table_utils, 
    mail_sender
)
from tapeadmin import tape_medium, tape_drive

from tapealerting.tas_alert import TasAlert
from tapealerting.tape_alert import TapeAlert
from tapealerting.drive_alert import DriveAlert
from tapealerting.tape_session import TapeSession
from tapealerting.mount_session import MountSession

###############
# Configuration
###############

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].rsplit('/', maxsplit=1)[-1]

config, logger = None, None

TOOL_NAME = 'cta-ops-tape-alerting'
DEFAULT_CONFIG_PATH = '/etc/cta-ops/cta-ops-config.yaml'

# Suppress warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

QUERY_HOURS_TAPE_SESSIONS = '''
    SELECT *
    FROM "$measurement"
    WHERE time > now() - $nano_seconds
'''

# Tables
ADDITIONAL_INFO_HEADERS = [
    'Time', 'Library', 'Drive', 'Host', 'VID', 'Mount Type', 'Files', 'Data', 'Rate (MB/s)', 'VO',
    'Status', 'Pool', 'Mounted?', 'Error(s)'
]

######
# Jobs
######

def check_consecutive_session_failed_tape(session:TapeSession, last_sessions:list[TapeSession]):
    """
    Checks if the consecutive failed tape sessions caused by the malfunctioning
    of a tape reached the threshold.
    :param session: The TapeSession to work on
    :param last_sessions: The past CTA TapeSessions within the time frame
    :return: TasAlert object if problem detected, otherwise None
    """
    # Get the function name, which must match a config file entry
    job_name = inspect.currentframe().f_code.co_name
    logger.debug(f"Running alert job {job_name}")
    # 1) If the status field is None or not even present, we can't execute the job
    if session.status is None:
        logger.warning(
            "Job check_consecutive_session_failed_tape could not be executed " \
            "for last row because status field was missing"
        )
    # 2) Same with the VID field
    elif session.vid is None:
        logger.warning(
            "Job check_consecutive_session_failed_tape could not be executed " \
            "for last row because VID field was missing"
        )
    # 3) Else, we check for more session failures due to this tape ONLY if this
    # last one was failure
    elif session.status == 'failure' and session.mountAttempted:

        # Construct the Alert Object ahead of time for record keeping,
        # even though we might not trigger it.
        alert = TapeAlert(
            name = job_name,
            vid =  session.vid,
            config = config
        )
        alert.add_cause(session)

        for other_session in last_sessions:
            # We filter the last sessions to find ones related to the same unit
            if (other_session.vid == session.vid and
                other_session != session):
                # Break when we reach a success
                if other_session.status == 'success':
                    break
                elif other_session.status == "failure" and other_session.mountAttempted:
                    alert.add_cause(other_session)

        if alert.evaluate_raise():
            return alert

    return None


def check_consecutive_session_failed_drive(session: TapeSession, last_sessions:list[TapeSession]):
    """
    Checks if the consecutive failed tape sessions caused by the malfunctioning
    of a drive reach the threshold.
    :param session: The TapeSession to work on
    :param last_sessions: The past CTA TapeSessions within the time frame
    """
    # Get the function name, which must match a config file entry
    job_name = inspect.currentframe().f_code.co_name
    logger.debug(f"Running alert job {job_name}")
    # 1) If the status field is None or not even present, we can't execute the job
    if session.status is None:
        logger.warning(
            "Job check_consecutive_session_failed_drive could not be executed " \
            "for last row because status field was missing"
        )
    # 2) Same with the driveUnit field
    elif session.drive is None:
        logger.warning(
            "Job check_consecutive_session_failed_drive could not be executed " \
            "for last row because drive field was missing"
        )
    # 3) Same with the tapeVid field
    elif session.vid is None:
        logger.warning(
            "Job check_consecutive_session_failed_drive could not be executed " \
            "for last row because tape field was missing (we are not able to " \
            "know if the drive failed in multiple different tapes)"
        )
    # 4) Else, we check for more session failures due to this drive ONLY if this
    # last one was failure.
    elif session.status == 'failure' and session.mountAttempted:
        # Construct the Alert Object ahead of time for record keeping,
        # even though we might not trigger it.
        alert = DriveAlert(
            name = job_name,
            drive =  session.drive,
            config = config
        )
        alert.add_cause(session)

        for other_session in last_sessions:
            # We filter the last sessions to find ones related to the same unit
            if (other_session.drive == session.drive
                and other_session != session):
                # Break when we reach a success
                if other_session.status == 'success':
                    break
                elif other_session.status == "failure" and other_session.mountAttempted:
                    alert.add_cause(other_session)

        if alert.evaluate_raise():
            return alert

    return None


def check_too_many_mounts_tape(session, last_sessions):
    """
    Checks if a tape was mounted too many times in the specified time window
    :param session: The TapeSession to work on
    :param last_sessions: The past CTA TapeSessions within the time frame
    """
    # Get the function name, which must match a config file entry
    job_name = inspect.currentframe().f_code.co_name
    logger.debug(f"Running alert job {job_name}")
    # If the TPVID field is None or not even present, we can't execute the job
    if session.vid is None:
        logger.warning(
            "Job check_too_many_mounts_tape could not be executed for last " \
            "entry because 'vid' field was missing."
        )
        return None
    # Construct the Alert Object ahead of time for record keeping,
    # even though we might not trigger it.
    alert = TapeAlert(
        name = job_name,
        vid = session.vid,
        config = config
    )
    alert.add_cause(session)

    # We filter the last sessions to find ones related to the same unit
    for other_session in last_sessions:
        if (other_session.vid == session.vid and
            other_session != session):
            # All mounts count, failure and success
            alert.add_cause(other_session)

    if alert.evaluate_raise():
        return alert

    return None


def check_too_many_tape_alerts_tape(session, last_sessions):
    """
    Checks if the number of tape alerts occurred in the specified time window
    reaches the threshold.
    :param session: The TapeSession to work on
    :param last_sessions: The latest CTA TapeSessions
    :return: Alert corresponding to job
    """
    # Get the function name, which must match a config file entry
    job_name = inspect.currentframe().f_code.co_name
    logger.debug(f"Running alert job {job_name}")
    return search_errors_with_particular_names(
        alert_name = job_name,
        target_error_names = config.get(
            'jobs',
            job_name,
            'counter_names'
        ),
        session = session,
        last_sessions = last_sessions
    )


def check_too_many_tape_position_errors_tape(session, last_sessions):
    """
    Checks if the number of Tape Position errors for a tape occurred in the
    specified time window reaches the threshold.
    :param session: The TapeSession to work on
    :param last_sessions: The past CTA TapeSessions within the time frame
    :return: Alert corresponding to job
    """
    # Get the function name, which must match a config file entry
    job_name = inspect.currentframe().f_code.co_name
    logger.debug(f"Running alert job {job_name}")
    return search_errors_with_particular_names(
        alert_name = job_name,
        target_error_names = ['Error_tapePosition'],
        session = session,
        last_sessions = last_sessions
    )


def check_too_many_tape_position_errors_drive(session, last_sessions):
    """
    Checks if the number of Tape Position errors for a drive occurred in the
    specified time window reaches the threshold.
    :param session: The TapeSession to work on
    :param last_sessions: The past CTA TapeSessions within the time frame
    :return: Alert corresponding to job
    """
    # Get the function name, which must match a config file entry
    job_name = inspect.currentframe().f_code.co_name
    logger.debug(f"Running alert job {job_name}")
    return search_errors_with_particular_names(
        alert_name = job_name,
        target_error_names = ['Error_tapePosition'],
        session = session,
        last_sessions = last_sessions
    )


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "Tape Alerting System - " \
            "Sends emails of diverse Tape and Drive alerts",
    )

    parser.add_argument(
        '-a',
        '--drive-down-alert-file',
        type = str,
        help = "Instant trigger for 'Putting the drive down' alert from " \
        "cta-taped. Expects path to a json file with drive-down events. " \
        "For automatic execution from logging layers such as fluentd."
    )

    parser.add_argument(
        '-t',
        '--rolling-window-hours',
        type = int,
        help = "How many hours back in time to examine. Overrides config value."
    )

    # Settings
    settings_group = parser.add_argument_group(
        'settings',
        "Configure how this script runs"
    )
    settings_group.add_argument(
        '-C',
        '--config-file',
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "Full path of the file containing the tool configuration"
    )
    settings_group.add_argument(
        '-d',
        '--dry-run',
        action = 'store_true',
        help = "Perform don't perform any action, just log the output."
    )
    settings_group.add_argument(
        '-V',
        '--verbose',
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    # Assign the variables to the options
    args = parser.parse_args()

    # Assign specified config file (YAML) to the config object
    global config, logger
    config = config_holder.CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    if args.rolling_window_hours is not None:
        config.set('rolling_window_hours', args.rolling_window_hours)

    logger = log_utils.init_logger(my_name, config)

    return args


def tabulate_tapesessions(sessions:list, max_rows:int=20):
    """
    Create a table of tape sessions for reporting.
    :param sessions: List of TapeSession objects
    :param max_rows: Limit tabulation to n rows, even if there are more sessions
    :return: Tabulate table object
    """

    return table_utils.get_table(
        headers = ADDITIONAL_INFO_HEADERS,
        rows = [ session.to_list(config) for session in sessions[0:max_rows] ],
        config = config,
        color = False
    )


def run_jobs(
        args:argparse.Namespace,
        tape_session:TapeSession, last_sessions:list,
        alerts_already_raised:list, influxdb_client):
    """
    Executes the jobs present in the configuration file (those starting with "check_")
    :param args: Argparse parser output
    :param tape_session: The tape session to process
    :param last_sessions: TapeSessions which occurred in the specified window
    :param alerts_raised: The list of already raised alerts
    :param influxdb_client: The InfluxDB client connection
    """
    logger.debug(f"Processing session: {tape_session}")

    configured_jobs = [
        job_name for job_name in config.get('jobs').keys()
        if job_name.startswith('check_')
    ]
    just_raised = []

    for alerting_job in configured_jobs:
        try:
            # Execute the method whose name matches
            candidate_alert = globals()[alerting_job](tape_session, last_sessions)
            if candidate_alert is not None:
                if candidate_alert not in alerts_already_raised:
                    # Raise the alert
                    raise_alert(
                        args = args,
                        session = tape_session,
                        alert = candidate_alert,
                        influxdb_client = influxdb_client
                    )
                    just_raised.append(candidate_alert)
                else:
                    logger.debug(
                        f"Alert {candidate_alert} has already been raised, skipping."
                    )
        except KeyError as key_error:
            if alerting_job in str(key_error):
                logger.error(
                    f"Alerting job {alerting_job} not found. Please revise the " \
                    f"configuration file, or implement it.",
                )
            else:
                raise
            sys.exit(1)
        except TypeError as type_error:
            if alerting_job in str(type_error):
                logger.error(
                    f"Wrong parameters for alerting job {alerting_job}. Either " \
                    f"it does not exist as an alerting job, or the syntax " \
                    f"in its definition is wrong.",
                )
            else:
                raise
            sys.exit(1)
    return just_raised


def update_alerts_raised_influxdb(alert:TasAlert, influxdb_client):
    """
    Queries InfluxDB through the client connection, to update the corresponding
    alerts raised measurement.
    :param alert: TasAlert object to update the measurement with
    :param influxdb_client: The InfluxDb client connection
    """
    try:
        logger.debug(f"Writing raised alert {alert} to InfluxDB.")
        json_body = [alert.to_point(config)]

        influxdb_client.write_points(json_body)
    except Exception as e:
        logger.error(f"Could not write {alert} to InfluxDB: {e}")


def get_tape_sessions(influxdb_client, hours_back:int):
    """
    Gets the info of all the CTA tape sessions from now to hours_back ago.
    :param influxdb_client: DB connection client
    :param hours_back: Number of hours into the past to look
    :return: List of TapeSession objects
    """
    logger.debug("Fetching latest tape sessions from monitoring database.")
    instance = config.get('instance', default='production')
    measurement = config.get('influxdb_measurement_sessions')
    last_sessions = []

    try:
        # Prepare the query with configured time span
        query_template = Template(QUERY_HOURS_TAPE_SESSIONS)
        query_tape_sessions_instance = query_template.safe_substitute(
            measurement = measurement,
            nano_seconds = str(hours_back*60*60*1000000000),
        )
        # Differentiate between "production", "preproduction", ... instances
        query_hour_tape_sessions_instance = f"{query_tape_sessions_instance} AND " \
            f"instance = '{instance}'"

        query_result = influxdb_client.query(
            query_hour_tape_sessions_instance
        )

        # Convert result generator to list and filter for relevant events
        for row in query_result.get_points():
            # If we don't have neither mountId nor volReqId, we don't store that row.
            # Technically already enforced by query, but doesn't hurt to check
            if 'mountId' in row or 'volReqId' in row:
                if row['wasTapeMounted']:
                    new_session = MountSession(db_entry=row, config=config)
                else:
                    new_session = TapeSession(db_entry=row, config=config)

                logger.debug(f"Found CTA session: {new_session}")
                last_sessions.append(new_session)
            else:
                continue

    except exceptions.InfluxDBClientError as error:
        log_utils.log_and_exit(
            logger,
            str(error)
        )

    return last_sessions


def clean_unnecessary_alerts_raised(alerts_raised:list):
    """
    Removes the occurrences from the beginning of the alerts_raised that we
    don't need anymore, to reduce memory usage.
    Helps getting rid of the info we don't need anymore
    (We only need data in the specified time frame; get rid of everything older)
    :param alerts_raised: List of previously raised TasAlert objects
    :return: List of raised alerts with old events removed
    """
    rolling_window = int(config.get('rolling_window_hours'))
    recent_alerts_raised = [
        alert for alert in alerts_raised
        if alert.time >= datetime.now() - timedelta(hours=rolling_window)
    ]
    return recent_alerts_raised


def clean_sessions_out_of_time_frame(sessions:list):
    """
    Filter sessions by their timestamp, discard all which are not inside
    the configured rolling window.
    """
    rolling_window = int(config.get('rolling_window_hours'))
    sessions_in_window = [
        session for session in sessions
        if session.time >= datetime.now() - timedelta(hours=rolling_window)
    ]
    return sessions_in_window

def save_alerts_raised_to_file(alerts_raised:list):
    """
    Makes the alerts_raised persistent into a file that will be overwritten
    after every execution.
    :param alerts_raised: List of TasAlert objects
    :param path: Path of the file to write to.
    """
    path = Path(config.get('alerts_raised_file'))
    logger.debug(f"Saving raised alerts to local cache file at: {path}")
    try:
        cmd_utils.create_ops_owned_file(
            path = path,
            config = config,
            content = '\n'.join([a.to_json() for a in alerts_raised])
        )
    except Exception as e:
        log_utils.log_and_exit(
            logger,
            f"Can't write to file {path} ({str(e)})"
        )


def get_alerts_raised_from_file(path:str):
    """
    Gets the last raised alerts from the specified file in the configuration.
    :param path: Path to the file containing raised alerts in json form
    :return: List of TasAlert objects created by parsing the file content
    """
    logger.debug(
        f"Fetching previously raised alerts from cache file at {path} ."
    )
    try:
        with open(path, 'r', encoding='utf-8') as json_file:
            lines = json_file.readlines()
            alerts = []

            for line in lines:
                # TODO: Nicer way to determine type without parsing json twice
                alert_dict = json.loads(line)
                if alert_dict['target'] == 'drive':
                    alerts.append(DriveAlert(json_str=line))
                elif alert_dict['target'] == 'tape':
                    alerts.append(TapeAlert(json_str=line))
                else:
                    logger.warning(
                        f"Unrecognized alert type in cache: {alert_dict}"
                    )
            return alerts
    except FileNotFoundError:  # There's no file
        logger.warning(f"File {path} not found. Can be OK if first execution.")
        return []
    # File is empty or doesn't contain a valid JSON structure.
    # Can be correct if no alerts.
    except ValueError:
        logger.warning(
            f"File {config.get('alerts_raised_file')} " \
            "empty or wrong format. Can be OK if no recent alerts."
        )
        return []
    except Exception as e:
        log_utils.log_and_exit(
            logger,
            f"Can't read from file " \
            f"{config.get('alerts_raised_file')} ({str(e)})."
        )
        return []


def save_processed_tape_sessions_to_file(tape_sessions:list):
    """
    Makes the last processed tape sessions persistent into a file that will be
    overwritten after every execution.
    :param tape_sessions: List of TapeSession objects
    """
    path = Path(config.get('last_processed_tape_sessions_file'))
    logger.debug(f"Saving last processed sessions to local cache file at: {path}")
    try:
        cmd_utils.create_ops_owned_file(
            path = path,
            config = config,
            content = '\n'.join([s.to_json() for s in tape_sessions])
        )

    except Exception as e:
        log_utils.log_and_exit(
            logger,
            f"Can't write to file {path} ({str(e)})."
        )


def get_already_processed_tape_sessions_from_file(path:str):
    """
    Gets the already processed tape sessions from the specified file in the
    configuration.
    :return: List of TapeSession objects read from the json file
    """
    try:
        with open(path, 'r', encoding='utf-8') as json_file:
            lines = json_file.readlines()
            sessions = []

            for line in lines:
                session = json.loads(line)
                if session['tape_mounted']:
                    sessions.append(MountSession(json_str=line, config=config))
                else:
                    sessions.append(TapeSession(json_str=line, config=config))

            return sessions
    except FileNotFoundError:   # There's no file
        logger.warning(
            f"File {path} " \
            f"not found. Can be OK if first execution.",
        )
        return []
    # File is empty or doesn't contain a valid JSON structure.
    # Can be correct if no sessions.
    except ValueError:
        logger.warning(
            f"File {path} " \
            f"empty or wrong format. Can be OK if no recent tape sessions.",
        )
        return []
    except Exception as e:
        log_utils.log_and_exit(
            logger,
            f"Can't read from file {path} ({str(e)}).",
        )
        return []


def search_errors_with_particular_names(
        alert_name:str, target_error_names:list, session:TapeSession,
        last_sessions):
    """
    Common code for alerting jobs consisting of raising alerts based on
    particular error names occurred.
    :param alert_name: The alert name
    :param target_error_names: The list of error names to search for
    :param session: The TapeSession to work on
    :param last_sessions: The latest CTA TapeSessions
    :param influxdb_client: The InfluxDB client connection
    :return: TasAlert to raise if found, otherwise None
    """
    target = config.get('jobs', alert_name, 'target')

    relevant_last_sessions = []

    if target == 'tape':
        # If the VID field is None or not even present, we can't execute the job
        if session.vid is None:
            logger.warning(
                f"Job {alert_name} could not be executed for last row " \
                "because VID field was missing"
            )
            return None
        alert = TapeAlert(
            name = alert_name,
            vid = session.vid,
            config = config
        )

        # We filter the result set to find only the rows with the VID we want
        relevant_last_sessions = [
            other_session for other_session in last_sessions
                if (other_session.vid == session.vid and
                    other_session != session)
        ]

    elif target == 'drive':
        # If the Drive Unit field is None or not even present, we can't execute the job
        if session.drive is None:
            logger.warning(
                f"Job {alert_name} could not be executed for last row " \
                "because drive field was missing"
            )
            return None

        alert = DriveAlert(
            name = alert_name,
            drive = session.drive,
            config = config
        )

        # We filter the result set to find only the rows with the drive we want
        relevant_last_sessions = [
            other_session for other_session in last_sessions
                if (other_session.drive == session.drive and
                    other_session != session)
        ]

    else:
        logger.error(
            f"No target specified for '{alert_name}'. Alerting job not executed."
        )
        return None

    #  We check for more session failures ONLY if this last one contains at
    #  least 1 TAPE ALERT.
    any_desired_error_found_session = False
    for error_name in target_error_names:
        if error_name in session.errors.keys():
            any_desired_error_found_session = True
            alert.add_cause(session)
            break
    if not any_desired_error_found_session:
        return None

    # Check the last sessions for occurrences of the specified error(s)
    for other_session in relevant_last_sessions:
        for error_name in target_error_names:
            if error_name in other_session.errors:
                alert.add_cause(other_session)
                break

    if alert.evaluate_raise():
        return alert

    return None


def raise_alert(
        args:argparse.Namespace, session:TapeSession, alert:TasAlert,
        influxdb_client):
    """
    Alert operator of the detection of the issue by various means.
    :param args: Argparse parser output
    :param session: The TapeSession to work on
    :param alert: The triggered TasAlert
    :param influxdb_client: The InfluxDB client connection
    """
    logger.info(
        f"Raising alert: {alert} for {alert.get_target()} {alert.get_item()}"
    )

    # Prepare the alert table rendering
    table = tabulate_tapesessions(
        sessions = alert.get_cause(),
        max_rows = alert.max_displayed_sessions
    )

    # Send an alert email to the operators.
    # This email is converted into a SNOW ticket at CERN.
    send_tas_alert_email(
        session = session,
        alert = alert,
        table = table,
        dry_run = args.dry_run
    )

    if not args.dry_run:
        # Update the corresponding InfluxDB alerts raised measurement.
        # Used in dashboards.
        try:
            update_alerts_raised_influxdb(alert, influxdb_client)
        except Exception as e:
            logger.error(
                f"Couldn't update InfluxDB measurement of alerts raised ({str(e)})"
            )


def get_supplemental_drive_info(drive:str):
    """
    Fetch supplemental tape drive information from the host where it is
    connected. This relies on the output of the tape drive config generation
    tool being present in a file on the host.
    :param drive: The name of the drive to look up
    :return: Dictionary of local drive information
    """
    drive_info = {}
    # Map drive name back to the its sg device
    drive_facts_file_path = config.get('tape', 'drive_facts_file_path')
    with open(drive_facts_file_path, 'r', encoding='utf-8') as facts:
        try:
            drive_facts = json.load(facts)
        except json.decoder.JSONDecodeError:
            log_utils.log_and_exit(f"Invalid JSON at {drive_facts_file_path} !")

    nst_device = None
    for drive_info in drive_facts['tape_drives']:
        if drive_info['DriveName'] == drive:
            nst_device = drive_info['DriveDevice']
            break
    if nst_device is None:
        log_utils.log_and_exit(
            logger,
            f"Could not find information about drive {drive} in " \
            f"drive config file {drive_facts_file_path} ."
        )
    st_device = nst_device.replace('nst', 'st')

    get_device_cmd = f'''{tapeadmin.base_lsscsi_cmd} -g | grep tape | ''' \
        f'''grep {st_device} ''' \
        '''| awk '{print $7}' '''

    device_call = cmd_utils.run_cmd(get_device_cmd)
    drive_device = device_call.stdout.strip()

    # Identify the drive vendor
    # TODO: Do this properly if we get some non-IBM drives
    vendor = "IBM"
    drive_info['vendor'] = vendor

    # Get the drive serial and firmware
    cmd_timeout = config.get('timeout', default=10)
    serial = tape_drive.get_drive_serial(drive_device, cmd_timeout, logger)
    drive_info['serial'] = serial

    firmware_version = tape_drive.get_drive_firmware(
        drive_device = drive_device,
        timeout = cmd_timeout,
        logger = logger
    )
    drive_info['firmware'] = firmware_version

    # Find location in library
    # Assume this is contained within the name for now
    location = drive.split('-')[-1]
    drive_info['location'] = location

    drive_info['name'] = drive

    return drive_info


def send_drive_down_email(
        cta_log:dict, tape_info:dict, drive_info:dict, disable_info:str,
        down_reason:str):
    """
    Sends immediate notification for a drive going down.
    Fills the template fields with the desired values and uses mail_sender to
    send the mail.
    :param cta_log: The triggering cta log line, json parsed to dict
    :param tape_info: CTA-admin tape-ls output, as dict
    :param drive_info: Additional drive information dict, found locally
    :param disable_info: Output of the tape_medium.disable_tape() call
    :param down_reason: The reason TAS put on the drive
    """
    logger.debug(
        "Sending drive down event alert (opening ticket)."
    )

    log_path = f"/var/log/cta/cta-taped-{drive_info['name']}.log"

    # Fill in template fields
    mail_template_fields = {
        'SENDER': config.get('email', 'sender'),
        'RECIPIENTS': ', '.join(
            config.get('ticket_email', 'recipients', 'drive')
        ),
        'drive_name': dict_utils.get_or_default(
            cta_log,
            'drive_name',
            default = "Unknown"
        ),
        'host_name': dict_utils.get_or_default(
            cta_log,
            'hostname',
            default = "Unknown"
        ),
        'disable_info': disable_info,
        'down_reason': down_reason,
        'tape_state': tape_info['state'],
        'probable_error': dict_utils.get_or_default(
            cta_log,
            'message',
            default = "Unknown"
        ),
        'tape_drive_vendor': dict_utils.get_or_default(
            drive_info,
            'vendor',
            default = "Unknown"
        ),
        'tape_drive_location': dict_utils.get_or_default(
            drive_info,
            'location',
            default = "Unknown"
        ),
        'tape_drive_serial': dict_utils.get_or_default(
            drive_info,
            'serial',
            default = "Unknown"
        ),
        'tape_drive_firmware': dict_utils.get_or_default(
            drive_info,
            'firmware',
            default = "Unknown"
        ),
        'tape_name': tape_info['vid'],
        'tape_library': tape_info['logicalLibrary'],
        'tape_model': tape_info['mediaType'],
        'tape_manufacturer': tape_info['vendor'],
        'tape_capacity': tape_info['capacity'],
        'tape_occupancy': tape_info['occupancy'],
        'tape_masterbytes': tape_info['masterDataInBytes'],
        'tape_nbfiles': tape_info['nbMasterFiles'],
        'tape_statereason': tape_info['stateReason'],
        'tape_full': tape_info['full'],
        'tape_pool': tape_info['tapepool'],
        'log_file_name': log_path,
        'log_file_content': json.dumps(cta_log, indent=2)
    }

    # Send the mail
    if config.get('suppress_emails'):
        logger.warning(
            "SUPPRESS_EMAILS: Mail for Drive Put Down Alert not sent."
        )
    else:
        # Fetch email template
        email_template = config.get('drive_down_email_template', default=None)
        if email_template is None or email_template == "":
            with path(__package__,'TapeDriveDown.tmpl') as temp_path:
                email_template = str(temp_path)

        mail_sender.send_mail(
            notification_name = 'Drive Put Down Alert',
            mail_template_fields = mail_template_fields,
            template_file = email_template,
            config = config
        )


def send_tas_alert_email(
        session:TapeSession, alert:TasAlert, table, dry_run:bool=False):
    """
    Fills the template fields with the desired values and uses mail_sender to
    send the mail. This email is sent after a 'TAS scan'.
    :param session: The TapeSession to work on
    :param alert: The triggered TasAlert
    :param table: The tabulate table representing the sessions to report
    :param dry_run: Don't send the email, write it to a local file instead
    :return: True if email sent, otherwise false
    """
    cause = alert.get_cause()
    distinct_units = alert.get_distinct_units()
    # Gather the different fields for the mail
    mail_template_fields = {
        'SENDER': config.get('email', 'sender'),
        'RECIPIENTS': ', '.join(
            config.get('ticket_email', 'recipients', alert.get_target())
        ),
        'n_hours': str(config.get('rolling_window_hours')),
        'n_event': str(len(cause)),
        'threshold_failure': str(alert.threshold_failure),
        'threshold_units': str(alert.threshold_units),
        'n_units': str(len(distinct_units)),
        'unit_list': ', '.join(distinct_units),
        'additional_info': str(table),
        'max_sessions': config.get(
            'jobs',
            alert.name,
            'max_displayed_sessions'
        ),
    }

    if isinstance(alert, TapeAlert):
        mail_template_fields['tape_name'] = alert.get_item()
    elif isinstance(alert, DriveAlert):
        mail_template_fields['drive_name'] = alert.get_item()
        mail_template_fields['host_name'] = session.hostname.split('.')[0]

    # Check if we will Disable/Put Down the tape/drive
    if config.get('jobs', alert.name, 'disable'):
        explanation_template = Template(
            config.get(
                'jobs',
                alert.name,
                'disable_reason_explanation'
            )
        )
        datetime_fmt = config.get('logging', 'date_format')
        datetime_reason = datetime.now().strftime(datetime_fmt)
        explanation_reason = explanation_template.safe_substitute(
            times = str(len(cause)),
            units = str(len(distinct_units)),
            time_range = str(config.get('rolling_window_hours'))

        )
        if isinstance(alert, TapeAlert):
            # We need to specify a "reason" when disabling with cta-admin
            # (currently done via "--comment")
            reason_tape = f'[TAS {datetime_reason}]: ' \
                f'{explanation_reason}'
            if not dry_run:
                mail_template_fields['action'] = tape_medium.disable_tape(
                    alert.get_item(),
                    reason_tape,
                    logger
                )
            else:
                mail_template_fields['action'] = "Dry run, no action performed."
        elif isinstance(alert, DriveAlert):
            # We need to specify a "reason" when putting a drive down with cta-admin
            reason_drive = f'[TAS {datetime_reason}]: {explanation_reason}'
            if not dry_run:
                mail_template_fields['action'] = tape_drive.put_drive_down(
                    alert.get_item(),
                    reason_drive,
                    logger
                )
            else:
                mail_template_fields['action'] = "Dry run, no action performed."
    # Send the mail
    if config.get('suppress_emails'):
        logger.warning(
            f"SUPPRESS_EMAILS: Mail for '{alert.name}' not sent"
        )
    else:
        # Find the mail template file
        template_name_or_path = config.get(
            'jobs',
            alert.name,
            'template',
            default=None
        )
        if template_name_or_path is None or template_name_or_path == "":
            logger.error(
                f"No email template specified for alert {alert.name} in " \
                f"{config.config_path} ! No email sent."
            )
            return False
        else:
            template_path = Path(template_name_or_path)
            # Was the template specified by a valid path?
            if (template_path.is_file() and
                cmd_utils.is_effectively_readable(template_path)):
                email_template = template_path
            else:
                with path(__package__,template_name_or_path) as temp_path:
                    email_template = str(temp_path)

        # For debugging, write to file instead of sending the email.
        if dry_run:
            timestamp = datetime.now().timestamp()
            dryrun_mail_path = Path(f'/var/tmp/tape-alerting-system-dryrun-{timestamp}')
            logger.debug(
                f"Writing dryrun email to {dryrun_mail_path}, " \
                "instead of sending."
            )
            with open(email_template, 'r', encoding='utf-8') as t:
                template = Template(t.read())
                body = template.safe_substitute(mail_template_fields)
                cmd_utils.create_ops_owned_file(
                    path = dryrun_mail_path,
                    config = config,
                    content = body
                )
            return False

        # Otherwise, we just send the email
        return mail_sender.send_mail(
            notification_name = alert.name,
            mail_template_fields = mail_template_fields,
            template_file = email_template,
            config = config
        )

    return False


def handle_drive_down(log_line_buffer_path:str, dry_run:bool=False):
    """
    Immediate response to cta-taped putting its drive into the Down state due
    to a detected issue.
    :param log_line_buffer_path: Path to tmp file with cta-taped json log entry
    :param dry_run: Don't execute if true
    :return: True if success, False otherwise
    """
    logger.info("Received Drive Down event.")
    datetime_fmt = config.get('logging', 'date_format')
    with open (log_line_buffer_path, 'r', encoding='utf-8') as log_entries:
        for line in log_entries:
            try:
                event_dict = json.loads(line)
            except json.JSONDecodeError as e:
                logger.error(
                    f"Could not decode CTA JSON log message: {e}. Skipping!"
                )
                continue

            # Check that this event is within the configured time frame.
            # A safeguard against re-playing old events on accident.
            rolling_window = int(config.get('rolling_window_hours'))
            event_time = datetime.fromtimestamp(event_dict['epoch_time'])
            if event_time <= datetime.now() - timedelta(hours=rolling_window):
                datetime_str = event_time.strftime(datetime_fmt)
                logger.warning(
                    f"Received CTA event with timestamp {datetime_str}, " \
                    f"which is not within configured time range of {rolling_window}."
                )
                continue

            drive_name = None
            if 'drive_name' in event_dict:
                drive_name = event_dict['drive_name']
            else:
                # If really needed, we could make fluentd pass the file name,
                # which contains the drive name.
                log_utils.log_and_exit(
                    logger,
                    "No 'drive_name' given in log CTA-taped log message." \
                    "Unable to handle drive-Down event!"
                )

            # Look for affected tape
            vid = None
            if 'tapeVid' in event_dict:
                vid = event_dict['tapeVid']
            if vid is None:
                drive_dict = tape_drive.get_local_drive(drive_name, config, logger)
                vid = tape_medium.get_tape_vid_inside_drive(drive_dict, logger)

            logger.debug(
                f"Drive to put Down: {drive_name}. " \
                f"Tape to disable: {vid}."
            )
            # Notify the operators of the issue
            if not dry_run:
                # Disable the tape
                tape_disable_msg = "No tape found in drive, tape not disabled."
                datetime_reason = datetime.now().strftime(datetime_fmt)
                if vid is not None:
                    tape_reason = f"[TAS {datetime_reason}]: " \
                        "Tape potentially stuck in drive and disabled."
                    tape_disable_msg = tape_medium.disable_tape(
                        vid,
                        tape_reason,
                        logger
                    )

                # Use the drive's reason to indicate the situation
                drive_reason = f"[TAS {datetime_reason}] " \
                    "Drive down, cartridge may be stuck."
                tape_drive.put_drive_down(drive_name, drive_reason, logger)
                drive_info_dict = get_supplemental_drive_info(drive_name)

                # Gather tape info, after disable to get up-to-date info
                tape_info_dict = {
                    "vid": "NA",
                    "state": "NA",
                    "logicalLibrary": "NA",
                    "mediaType": "NA",
                    "vendor": "NA",
                    "capacity": "NA",
                    "occupancy": "NA",
                    "masterDataInBytes": "NA",
                    "nbMasterFiles": "NA",
                    "stateReason": "NA",
                    "full": "NA",
                    "tapepool": "NA",
                }
                if vid is not None:
                    tape_info_dict = tape_medium.get_tape_info(vid, logger)

                # Send notification / create ticket
                send_drive_down_email(
                    cta_log = event_dict,
                    tape_info = tape_info_dict,
                    drive_info = drive_info_dict,
                    disable_info = tape_disable_msg,
                    down_reason = drive_reason
                )


        return True


def do_alert_scan(args:argparse.Namespace):
    """
    Fetch the latest CTA tape sessions, load the last processed ones from
    the cache, then execute the alerting jobs on the diff of these.
    :param args: Argparse parser output
    """
    rolling_window = int(config.get("rolling_window_hours"))
    logger.debug(
        "Execution of tape-alerting-system with a lookup on InfluxDB of " \
        f"{rolling_window} hours..."
    )

    client = influxdb_utils.connect(config)

    # Inform if program is running as a test, suppressing emails
    if config.get('suppress_emails', default=False):
        logger.warning(
            'PROGRAM RUNNING AS SUPPRESS_EMAILS. ALERT EMAILS WILL NOT BE SENT',
        )

    # We recover the last raised alerts from the file, if any
    alerts_file_path = config.get('alerts_raised_file')
    alerts_raised = get_alerts_raised_from_file(alerts_file_path)
    recent_alerts_raised = clean_unnecessary_alerts_raised(alerts_raised)


    # We recover the last CTA processed tape sessions from the file, if any
    session_cache_path = config.get('last_processed_tape_sessions_file')
    cached_tape_sessions = get_already_processed_tape_sessions_from_file(
        session_cache_path
    )
    already_processed_tape_sessions = clean_sessions_out_of_time_frame(
        cached_tape_sessions
    )

    # Read all the latest sessions from CTA within the specified time window
    recent_db_sessions = get_tape_sessions(client, rolling_window)

    # We filter for only the sessions which appeared since the last execution.
    new_sessions = []
    if not already_processed_tape_sessions:
        new_sessions = recent_db_sessions
    else:
        for session in recent_db_sessions:
            if session not in already_processed_tape_sessions:
                new_sessions.append(session)

    logger.info(
        f"CTA: Found {len(new_sessions)} new unprocessed session(s) " \
        "since last lookup."
    )

    # Prepare ordered list, starting with the oldest session
    new_sessions_sorted = sorted(
        new_sessions,
        key=lambda new_session: new_session.time,
        reverse=True
    )
    # For each new session from CTA, we perform every check available on it
    for i_session in range(0, len(new_sessions_sorted)):
        raised = run_jobs(
            args = args,
            tape_session = new_sessions_sorted[i_session],
            last_sessions = recent_db_sessions,  # Compare to all earlier ones
            alerts_already_raised = recent_alerts_raised,
            influxdb_client = client,
        )
        recent_alerts_raised.extend(raised)

    # And we update the last processed tape sessions for CTA, which is
    # the whole result set we passed through.
    already_processed_tape_sessions.extend(new_sessions_sorted)

    client.close()

    if not args.dry_run:
        # We update the list of last processed tape sessions.
        # It should be sorted with the most resent sessions last.
        save_processed_tape_sessions_to_file(
            sorted(
                already_processed_tape_sessions,
                key=lambda new_session: new_session.time,
            )
        )
        # We update the list of alerts that we have raised
        save_alerts_raised_to_file(recent_alerts_raised)

    logger.info("Alert scan complete, all jobs performed.")


def main():
    """
    Decide if we are responding to a rapid-response drive-down hook,
    or are scanning through the last sessions for drive alerts.
    """
    args = load_args()

    logger.info("Starting Tape Alerting System run.")

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    if args.drive_down_alert_file is not None:
        # Trigger an immediate alert hook
        handle_drive_down(args.drive_down_alert_file, args.dry_run)
    else:
        # Do a regular alert scan
        do_alert_scan(args)


if __name__ == '__main__':
    main()
