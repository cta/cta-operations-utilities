# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

import json

from tapealerting.tape_session import TapeSession
from ctautils.config_holder import CtaOpsConfig


class MountSession(TapeSession):
    """
    A TapeSession where the tape was mounted.
    """
    errors = {}

    def __init__(
        self, json_str: str = None, db_entry: str = None, config: CtaOpsConfig = None
    ):
        """
        Init either from a json string (from file) or a DB query result.
        The main difference to a basic TapeSession is the presence of a Mount ID
        :param json_str: Load session obj from this json representation
        :param db_entry: Load session obj from database row
        :param config: CtaOpsConfig object
        """
        if json_str is not None:
            self._from_json(json_str)
        elif db_entry is not None:
            self._from_db_entry(db_entry, config)

        self.errors = {}

    def get_id(self):
        """
        Return the Session's ID
        :return: The session's mount ID as a string
        """
        return self.mount_id

    def _from_json(self, json_str:str):
        """
        Create the MountSession object from a stored json representation.
        :param json_str: The json string from the local cache
        """
        super()._from_json(json_str)

        # Special care for typing, as the json log format is still being
        # refined at this time.
        mount_id = json.loads(json_str)['mount_id']
        if mount_id is not None and not isinstance(mount_id, int):
            mount_id = int(mount_id)

        self.mount_id = mount_id

    def _from_db_entry(self, db_entry:dict, config):
        """
        Take the TapeSessionFinished info from InfluxDB (which stems from the
        cta-taped logs), and create a MountSession object from one row.
        :param db_entry: Session data point/row from the DB in dict form
        :param config: CtaOpsConfig object
        """
        super()._from_db_entry(
            db_entry = db_entry,
            config = config
        )

        # Special care for typing, as the json log format is still being
        # refined at this time.
        mount_id = db_entry['mountId']
        if mount_id is not None and not isinstance(mount_id, int):
            mount_id = int(mount_id)
        self.mount_id = mount_id

    def _to_dict(self):
        """
        Produce a dict representation of the session object.
        :return: Dict
        """
        properties = super()._to_dict()
        properties['mount_id'] = self.mount_id
        properties['tape_mounted'] = True

        return properties

    def __str__(self):
        tape_session_str = super().__str__()
        my_str = f"{tape_session_str} | {self.mount_id}"
        return my_str
