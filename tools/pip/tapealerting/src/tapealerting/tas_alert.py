# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
The TasAlert is the generic alert class on which unit-specific alerts are based.
"""

import json

from datetime import datetime

from ctautils.config_holder import CtaOpsConfig

from tapealerting.tape_session import TapeSession

class TasAlert:
    """
    Representation of an alert that was or might be raised.

    name: Name of the alert job and the corresponding config entry
    time: The string representation of the datetime when the alert was raised
    unit_list_dict: The dictionary of the units involved in the alert and the times they suffered it
    cause: List of TapeSessions causing the alert
    """
    def __init__(self, name:str=None, time:str=None,
                 json_str:str=None,
                 config:CtaOpsConfig = None):
        """
        Create an alert object, either from its parameters, or from a json string.
        """
        self._cause = []

        if json_str is None:
            self.name = name
            if time is not None:
                self.time = datetime.strptime(time, '%s')
            else:
                self.time = datetime.now()

            self.threshold_failure = config.get(
                'jobs',
                name,
                'threshold_failure'
            )
            self.threshold_units = config.get(
                'jobs',
                name,
                'threshold_units'
            )
            self.max_displayed_sessions = config.get(
                'jobs',
                name,
                'max_displayed_sessions'
            )

        else:
            json_dict = json.loads(json_str)
            self.name = json_dict['name']
            self.time = datetime.fromtimestamp(
                int(json_dict['timestamp_ns']) / 10**9
            )
            self.threshold_failure = json_dict['threshold_failure']
            self.threshold_units = json_dict['threshold_units']

    def _to_dict(self):
        """
        Produce a dict representation of the TasAlert object.
        :return: Dict
        """
        properties = {
            'name': self.name,
            'item': self.get_item(),
            'target': self.get_target(),
            'timestamp_ns': int(self.time.strftime('%s%f'))*10**3,
            'threshold_failure': self.threshold_failure,
            'threshold_units': self.threshold_units,
        }
        return properties

    def get_target(self):
        """
        Get the target (tape/drive) of this alert.
        Placeholder for child classes to implement.
        :return: String with type name of infra which this alert is related to
        """
        return None

    def get_item(self):
        """
        Get the specific tape/drive identifier.
        Placeholder for child classes to implement
        """
        return None

    def add_cause(self, session:TapeSession):
        """
        Add a single session to the list of sessions causing this alert.
        :param session: TapeSession object
        """
        if session not in self._cause:
            self._cause.append(session)

    def set_cause(self, session_list:list):
        """
        Associate the TapeSessions causing the alert to the object.
        :param session_list: A list of TapeSessions
        """
        self._cause = session_list

    def get_cause(self):
        """
        Return the sessions which triggered this alert.
        :return: List of TapeSessions
        """
        return self._cause

    def get_distinct_units(self):
        """
        Get the unique units (tapes/drives) involved in the event.
        TODO: This could be made more elegant by using a different
        data structure for _cause, but for now a list will have to do.
        :return: Integer count of unique drives/tapes
        """
        pass  # Unit specific, implement in drive_alert or tape_aler

    def evaluate_raise(self):
        """
        Check if conditions for raising this alert are met.
        """
        if len(self.get_distinct_units()) < self.threshold_units:
            return False
        if len(self.get_cause()) < self.threshold_failure:
            return False

        return True

    def to_json(self):
        """
        Return a json representation of the TasAlert object.
        :return: json string
        """
        return json.dumps(self._to_dict())

    def to_point(self, config):
        """
        Produce Alert representation to be written into InfluxDB.
        :param config: CtaOpsConfig object
        :return: Dict data point representation of the alert for InfluxDB
        """
        measurement = config.get('influxdb_measurement_raised')
        instance = config.get('instance')

        json_body = {
            'measurement': measurement,
            'tags':
                {
                    'instance': instance,
                    'name': self.name,
                    'target': self.get_target(),
                },
            'time': self.time,
            'fields':
                {
                    'item': self.get_item(),
                    'mount_ids': ','.join(
                        [str(session.get_id()) for session in self.get_cause()]
                    )
                }
        }
        return json_body

    def __eq__(self, other):
        """
        Compare TasAlerts. Needed for checking collection membership.
        :param other: The Other
        """
        if isinstance(other, type(self)):
            if (self.name == other.name and
                self.get_item() == other.get_item()):
                return True
        return False

    def __str__(self):
        """
        Pruduce an alert name string. This is for insertion into InfluxDB.
        """
        alert_parts = []
        for word in self.name.replace('check_', '').split('_'):
            alert_parts.append(word.capitalize())

        return ' '.join(alert_parts)
