# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Define Alert class for tape media
"""

import json

from ctautils.config_holder import CtaOpsConfig

from tapealerting.tas_alert import TasAlert


class TapeAlert(TasAlert):
    """
    An alert concerning a tape media (cartridge).
    """
    def __init__(self, name:str=None, time:str=None,
                 vid:str=None, json_str:str=None,
                 config:CtaOpsConfig = None):
        super().__init__(
            name = name,
            time = time,
            json_str = json_str,
            config = config
        )
        if json_str:
            self.vid = json.loads(json_str)['item']
        else:
            self.vid = vid

    def get_item(self):
        """
        Get Tape VID
        :return: Tape VID
        """
        return self.vid

    def get_target(self):
        """
        What sort of item (drive/tape) is this alert targeting?
        """
        return 'tape'

    def get_distinct_units(self):
        """
        Get the unique units (tapes/drives) involved in the event.
        :return: The set of unique drives involved in the alert
        """
        distinct_units = []
        for session in self.get_cause():
            # Some session log entries may not have a valid drive name
            if session.drive and session.drive not in distinct_units:
                distinct_units.append(session.drive)
        return distinct_units
