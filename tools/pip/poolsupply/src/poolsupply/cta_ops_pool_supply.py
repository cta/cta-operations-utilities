#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

# LOGIC:
#
# * CTA tape pools should have at least X partial tapes available for writing.
# * Eligible partial tapes are those that are ACTIVE (i.e. not DISABLED nor BROKEN),
#   not FULL, not in a DISABLED logical tape library
#   and not FROM CASTOR.
# * If the number of eligible partial tapes of a given tape pool falls below the
#   configured limit, the pool needs to be re-supplied with fresh tapes.
# * Fresh supply tapes are taken from tape pools defined in the "supply" column.
# * There can be multiple supply tape pools and they can be separated by
#   a separator (usually comma).
# * There is no distinction between what is a supply pool and what is not, if
#   a pool has a value in the "supply" column, tapes are taken from that pool.
#   Because of this, irregularities, cyclical loops and other misconfiguration
#   are possible - please be careful.
# * This script is intended to run every 15 minutes.
#
# Author: Vladimir Bahyl - 7/2019

# TODO: This script should use the cmd_utils.run_cmd() method for commands.
# That way you get built-in debug logging.

import sys
import json
import argparse

from random import shuffle
from datetime import datetime
from subprocess import STDOUT, check_output

import tapeadmin
from poolsupply import TOOL_NAME
from ctautils import log_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH


###############
#
# CONFIGURATION
#
###############

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]

config, logger = None, None

###########
#
# FUNCTIONS
#
###########

def load_args():
    """
    Set up argparse, which is the backbone of this script.
    """
    parser = argparse.ArgumentParser(
        description = "Tool to move tapes from the configured supply tape " \
        "pools into various user pools."
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)


def extract_eligible_tapes(disabledlibraries, tapepool, timeout=None):
    '''
    Extract eligible ACTIVE tapes (not DISABLED, not BROKEN, not FULL,
    not FROM CASTOR and not in a DISABLED library) from a given tape pool.
    :param disabledlibraries: List of disabled libraries.
    :param tapepool: Name of the tape pool.
    :return: List of eligible tapes.
    '''
    tapes = []
    command = ' '.join(
        (
            tapeadmin.cta_admin_json_tape_ls_tapepool,
            tapepool,
            "--state",
            "ACTIVE",
            "--full",
            "false"
        )
    )
    try:
        logger.debug(
            f"Executing command {command} with timeout of {timeout} seconds"
        )
        output = check_output(
            command,
            stderr = STDOUT,
            timeout = timeout,
            shell = True
        ).decode("UTF-8")
    except Exception as error:
        logger.error(format(error))
        sys.exit(-1)

    if output:
        tapes = [
            line["vid"] for line in json.loads(output)
            if (not line["logicalLibrary"] in disabledlibraries) and
            (not line["fromCastor"])
        ]
    else:
        logger.warning(
            f"No eligible tapes identified in the tape pool: {tapepool}"
        )

    return tapes

##########
#
# MAIN
#
##########
def main():
    load_args()

	# How many seconds to wait for the external command to finish
    timeout = config.get('timeout')
	# Characted which separates multiple tape pools in the CTA "supply" column
    separator = config.get('separator')

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.info(
        "--- Starting processing all tape pools at: " \
        f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"
    )

    # Extract the list of all tape pools from CTA (in the JSON format)
    command = tapeadmin.cta_admin_json_pool_ls
    try:
        logger.debug(
            f"Executing command {command} with timeout of {timeout} seconds"
        )
        output = check_output(
            command,
            stderr = STDOUT,
            timeout = timeout,
            shell = True
        ).decode("UTF-8")
    except Exception as error:
        log_utils.log_and_abort(config, logger, format(error))
    if output:
        tapepools = json.loads(output)
    else:
        logger.info("List of CTA tape pools is empty, nothing to do, exiting.")
        sys.exit(0)

    # Extract the list of DISABLED tape libraries from CTA (in the JSON format)
    command = tapeadmin.cta_admin_json_library_ls
    try:
        logger.debug(
            f"Executing command {command} with timeout of {timeout} seconds"
        )

        output = check_output(
            command,
            stderr = STDOUT,
            timeout = timeout,
            shell = True
        ).decode("UTF-8")
    except Exception as error:
        logger.error(format(error))
        sys.exit(-1)

    disabledlibraries = []
    if output:
        disabledlibraries = [
            line["name"] for line in json.loads(output) if line["isDisabled"]
        ]
        logger.warning(
            f"List of DISABLED logical tape libraries: {disabledlibraries}"
        )
    else:
        logger.info(
            "No DISABLED logical tape libraries detected, " \
            "considering tapes from all libraries."
        )

    # Prepare list of tape pool names (which will be needed later to check that
    # a supply pool actually exist)
    tapepoolnames = [tapepool["name"] for tapepool in tapepools]

    # Iterate over the extracted CTA tape pools and re-fill them with supply
    # tapes as needed.
    for tapepool in tapepools:
        logger.info(
            f"Tape pool: {tapepool['name']} which should have at least: " \
            f"{tapepool['numPartialTapes']} eligible partial tape(s) is " \
            f"supplied from: {tapepool['supply']}"
        )


        if tapepool["supply"]:

            # Check if re-filling is actually needed
            currentpartialtapes = len(
                extract_eligible_tapes(disabledlibraries, tapepool["name"],timeout)
            )
            if currentpartialtapes < int(tapepool["numPartialTapes"]):
                logger.info(
                    f"Tape pool: {tapepool['name']} only has: " \
                    f"{currentpartialtapes} eligible partial tape(s) " \
                    "available, re-filling."
                )

            else:
                logger.info(
                    f"Tape pool: {tapepool['name']} already has: " \
                    f"{currentpartialtapes} eligible partial tape(s) " \
                    "available, skipping."
                )

                continue

            # Prepare the eligible supply tapes from a given supply pool(s)
            supplytapes = []
            for supplypool in tapepool["supply"].split(separator):
                if supplypool in tapepoolnames:
                    supplytapes.extend(
                        extract_eligible_tapes(disabledlibraries, supplypool,timeout)
                    )
                else:
                    logger.warning(
                        f"Configured supply pool: {supplypool} is not a valid "
                        "tape pool name, skipping."
                    )
            # Randomize it
            # (so that tapes are picked at random from multiple pools)
            shuffle(supplytapes)

            # Move the required number of supply tapes to the given tape pool
            # (if any eligible tapes were identified)
            if len(supplytapes) > 0:
                logger.info(
                    f"Identified: {len(supplytapes)} supply tapes, " \
                    "moving random" \
                    f"{int(tapepool['numPartialTapes']) - currentpartialtapes} " \
                    f"to the pool: {tapepool['name']}"
                )

                for i in range(int(tapepool["numPartialTapes"]) - currentpartialtapes):
                    if i < len(supplytapes):
                        command = ' '.join(
                            (tapeadmin.cta_admin_json_tape_ch,
                             "--vid",
                             supplytapes[i],
                             "--tapepool",
                             tapepool["name"])
                        )
                        try:
                            logger.debug(
                                f"Executing command {command} with timeout of " \
                                f"{timeout} seconds."
                            )
                            output = check_output(
                                command,
                                stderr = STDOUT,
                                timeout = timeout,
                                shell = True
                            ).decode("UTF-8")
                        except Exception as error:
                            logger.error(format(error))
                            sys.exit(-1)

                        logger.info(
                            f"Tape: {supplytapes[i]} moved to the pool: {tapepool['name']}"
                        )

                    else:
                        logger.warning(
                            "Unable to completely re-fill the tape pool: " \
                            f"{tapepool['name']}, run out of supply tapes"
                        )
            else:
                logger.warning(
                    f"Unable to re-fill the tape pool: {tapepool['name']}, " \
                    "no eligible supply tapes identified in the supply pool(s):" \
                    f" {tapepool['supply']}")

        else:
            logger.warning(
                f"Unable to re-fill the tape pool: {tapepool['name']} because " \
                f"the supply pool: {tapepool['supply']} is not properly configured"
            )

    logger.info(
        "--- Finished processing all tape pools at: " \
        f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n"
    )


if __name__ == '__main__':
    main()
