# ATRESYS - Automated Tape REpacking SYStem

ATRESYS is a tool for automating the tape repack process taking place in a [CTA](https://eoscta.docs.cern.ch/) system. 
The automation tools manage the repack on a level one step higher than the repack concept in CTA, such that the whole workflow both before and after the repack itself can be automated.

ATRESYS contains:
* A command line tool `cta-ops-repack-manager`, which allows an operator to interact with the system.
* A set of scripts `cta-ops-repack-<1-6>...` and `cta-ops-repack-monitor`, which are intended to be run by a job scheduler such as Rundeck or cron.

## States and operation modes

Repack state information is saved in a dedicated table with the following layout:

```sql
                         Table "public.repack_status_prod"
     Column      |              Type              | Collation | Nullable | Default 
-----------------+--------------------------------+-----------+----------+---------
 tape            | character varying(20)          |           | not null | 
 entered         | timestamp(0) without time zone |           | not null | 
 started         | timestamp(0) without time zone |           |          | 
 repacked        | timestamp(0) without time zone |           |          | 
 quarantined     | timestamp(0) without time zone |           |          | 
 reclaimed       | timestamp(0) without time zone |           |          | 
 finished        | timestamp(0) without time zone |           |          | 
 files           | bigint                         |           | not null | 
 bytes           | bigint                         |           | not null | 
 usage           | smallint                       |           | not null | 
 last_write_date | timestamp(0) without time zone |           |          | 
 mode            | automation_modes               |           | not null | 
 priority        | smallint                       |           | not null | 
 mediatype       | character varying(20)          |           | not null | 
 tapepool        | character varying(20)          |           | not null | 
 responsible     | character varying(40)          |           | not null |
Indexes:
    "repack_status_prod_pkey" PRIMARY KEY, btree (tape)
```

As a repacking tape moves through the system it will go through the following *states*:
1. **Entered:** The tape has been entered into the automation system.
2. **Started:** A repack object for the tape has been created in CTA.
3. **Repacked:** The repack of the tape has completed in CTA.
4. **Quarantined:** The tape is being held in quarantine before being reclaimed.
5. **Reclaimed:** The tape has been reclaimed using `cta-admin tape reclaim`.
6. **Finished:** The tape has been moved to its destination tolabel/erase pool.

Additionally, an automated repack may run in three different *modes*:
* **auto:** The tape is being processed by the repack automation system.
* **manual:** An operator is working with the tape, the repack automation system will not interact with it.
* **error:** An error has been detected and the repack automation system will not process the tape further until an operator has intervened.

## Command line tool

The repack automation system comes with a command line tool for operators to manage the automated repacks: `cta-ops-repack-manager`

This tool can be used similarly to cta-admin-ops, in order to view, change, remove, etc. jobs from the system.
Run `cta-ops-repack-manager --help` for a complete list of options

## Scheduled scripts

- **cta-ops-repack-0-scan:**
    - Scans through all known tapes and identifies tapes which should be repacked
    - Right now the script supports scanning for full tapes with a low level of utilization
- **cta-ops-repack-1-prepare:**
    - Create repack archive routes and tapepools _(if enable_repack_archive_routes: true)_
    - Prepares tapes for repack (adds them to the database)
        - Performs validity checks
- **cta-ops-repack-2-start:**
    - Launches the repack of the tapes which have not been repacked yet 
    - Limits number of concurrent repacks in CTA
- **cta-ops-repack-3-end:**
    - Identifies tapes which have been successfully repacked
- **cta-ops-repack-4-quarantine:**
    - Moves repacked tapes to the quarantine pool and updates the DB accordingly
- **cta-ops-repack-5-reclaim:**
    - Reclaims tapes that have completed their quarantine
- **cta-ops-repack-6-finish:**
    - Move full tapes of `repack_` tapepools to user ones _(if enable_repack_archive_routes: true)_
    - Identifies tapes that have been reclaimed and moves them to the appropriate "tolabel_" or "erase" pool
- **cta-ops-repack-monitor:**
    - Exports the current contents of the DB table to a JSON log file, which can be consumed by a monitoring tool

## Requirements

- A DB with a schema as shown above. The tool is intended to be used with Postgresql.
- A working CTA environment. Use `cta-ops-repack-manager check-env` to verify that all needed pools have been created.
- An external scheduling tool, such as `cron` or `Rundeck`.
    
## Design decisions/philosophy

* Each automated repack is represented by a single line in the DB table. That way an operator has the option of easily querying the DB without specialized knowledge.
* `cta-ops-repack-manager` supports both short and extended output forms. The short output should be the default, the extended output can be used for troubleshooting.
* `cta-ops-repack-monitor` outputs JSON logs for easy parsing using Fluentd.
