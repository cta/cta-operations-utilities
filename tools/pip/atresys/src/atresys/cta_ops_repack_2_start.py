#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Launches the repack of the tapes present on the table.
Only tapes which that have not been repacked yet are launched (those whose
`DATE_REPACK_START` is NULL).
A maximum number of concurrent automated repacks is respected. So if
the number of tapes in the table exceed this value, they will have to wait.
"""

import os
import sys
import argparse

from string import Template
from datetime import datetime, timedelta
from pathlib import Path

import tapeadmin

from ctautils import log_utils, sql_utils, cmd_utils, repack_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME

###############
# Configuration
###############

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].rsplit('/', maxsplit=1)[-1]
logger, config = None, None


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "This script launches the repack of the tapes that "
            "have not been repacked yet in the REPACK_STATUS table "
            "of the cta_repack database"
    )

    parser.add_argument(
        "-m",
        "--mountpolicy",
        type = str,
        required = False,
        help = "Set a different mount policy than the default given in the "
               "config file"
    )
    parser.add_argument(
        "-o",
        "--order-by",
        choices = ["entered", "last-written", "usage", "random"],
        default = "entered",
        help = "Choose how to prioritize VIDs for repack start"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger, verbose
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    return args


def display_status(db_connection, args):
    """
    Give a system state summary, which makes it easier to reason about what the
    automation is working with.
    :param db_connection: Database connection object from sql_utils.connect
    :param args: Parsed parameters returned from argparse
    """
    n_repacks = repack_utils.count_in_db(db_connection, config, logger)
    n_eligible = len(
        repack_utils.select_repacks_to_start(db_connection, config, logger)
    )
    logger.info(
        f"There are {n_repacks} repacks in the automation system, " \
        f"out if which {n_eligible} are eligible to be started."
    )

    max_active = config.get('max_active_repacks')
    n_active = len(repack_utils.select_active_repacks(db_connection, config, logger))

    logger.info(
        f"{n_active} out of a maximum of {max_active} concurrent repacks are running."
    )

    # How many will be added
    n_to_add = max(max_active - n_active, 0)
    logger.info(
        f"This execution will attempt to start up to {n_to_add} new repacks."
    )

    # Selection criteria
    logger.info(
        f"Prioritising tapes for repack start by criteria: {args.order_by}"
    )


def clean_old_tf_ls_files():
    """
    Clean up old tape file inventory files created by this script.
    """
    file_path_template = config.get(my_name, 'existing_files_on_tape')
    file_root = Path(file_path_template).parent
    pattern = config.get(my_name, 'cleanup_pattern')
    keep_days = config.get(my_name, 'keep_tf_files_days')
    now = datetime.now()

    logger.info(f"Cleaning old tapefile-ls files in {file_root}:")

    # Check for tf-ls files, delete old ones.
    for txt_file in file_root.iterdir():
        if txt_file.match(pattern):
            ctime = datetime.fromtimestamp(os.path.getctime(txt_file))
            if now - ctime > timedelta(days=keep_days):
                logger.info(f"Deleting file {txt_file}")
                txt_file.unlink()


def main():
    """
    Start active repacks in CTA
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    repack_utils.print_script_banner(
        config,
        logger,
        my_name,
        ['default_priority', 'max_active_repacks']
    )

    # Establish connection to DB
    db_connection = sql_utils.connect(config)

    display_status(db_connection, args)

    # Get the list of tapes to repack
    repacks_to_start = repack_utils.select_repacks_to_start(
        db_connection,
        config = config,
        logger = logger,
        order_by = args.order_by
    )

    # Count the number of tapes which are already being repacked in an automated fashion
    active_repacks = repack_utils.select_active_repacks(
        db_connection,
        config = config,
        logger = logger
    )
    n_active_repacks = len(active_repacks)

    # Add tapes to repack
    repacks_started = []
    n_tapes_allowed = config.get('max_active_repacks')
    for repack in repacks_to_start:
        # Check that we don't exceed our quota of managed concurrent repacks
        if len(repacks_started) + n_active_repacks >= n_tapes_allowed:
            logger.info(
                "Reached maximum number of concurrent repacks after adding " \
                f"{len(repacks_started)} tape(s). " \
                f"{len(repacks_to_start) - len(repacks_started)} tape(s) skipped."
            )
            break

        # Substitute the tape name in the template path
        date = datetime.now().strftime('%Y_%m_%d-%H_%M_%S')
        template = Template(config.get(my_name, 'existing_files_on_tape'))
        tape_file_list_path = \
            f'{template.safe_substitute(DATE=date, TAPE=repack.vid)}'
        # Save the output of the tapefile command (list of files on this tape)
        # to the corresponding text file
        logger.info(
            f'Extracting list of files on the tape: {repack.vid}'
        )
        cmd_utils.create_ops_owned_file(
            path = tape_file_list_path,
            config = config
        )
        cmd = f'{tapeadmin.cta_admin_json_tapefile_ls_vid} {repack.vid}'
        with open(tape_file_list_path, 'w', encoding='utf-8') as write_to:
            cmd_call = cmd_utils.write_cmd_output(
                cmd = cmd,
                write_to = write_to,
                logger = logger
            )
            if cmd_call.returncode == 1:
                log_utils.log_and_exit(
                    logger,
                    f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                    "error (returncode 1)"
                )
            elif cmd_call.returncode == 2:
                repack.declare_problematic(
                    db_connection,
                    config,
                    logger,
                    "Failed to read files"
                )
                continue

        logger.info(
            f"List of files on the tape {repack.vid} extracted and saved (in JSON format) here: "
            f"{tape_file_list_path}"
        )

        # Put the tape into REPACKING state,
        # wait for the REPACK_PENDING->REPACKING transition to complete
        if not repack.set_tape_repacking(db_connection):
            continue

        if not repack.submit_cta_repack(db_connection,mountpolicy=args.mountpolicy):
            continue
        repacks_started.append(repack.vid)

    if len(repacks_started) == 0:
        log_utils.log_and_exit(
            logger,
            'No tapes have been submitted for repack',
            return_code = 0
        )

    logger.debug(
        f'{len(repacks_started)} tape(s) submitted for repack'
    )

    # Close DB connections
    db_connection.close()

    # Maintenance: Clean up old tapefile-ls putput files
    clean_old_tf_ls_files()


if __name__ == '__main__':
    main()
