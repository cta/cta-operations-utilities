#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Re-submit sub-requests until repack is deemed to be complete.
Identify tapes which have been repacked, update the DB,
and output file spread statistics.
"""

import os
import sys
import json
import argparse

from datetime import datetime, timedelta

import tapeadmin

from ctautils import log_utils, sql_utils, repack_utils
from tapeadmin import tape_medium
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME

###############
# Configuration
###############
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]
config, logger = None, None


REPACK_TIMEOUT_DAYS = 5

###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "This script identifies tapes which have been successfully"
        " and completely repacked, and updates the repack DB accordingly"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    return args


def write_repack_summary(repack, repack_info:dict):
    """
    Write a summary of the repack to logs, both in human-readable form
    and for our monitoring DB.
    :param repack: The repack object
    :param repack_info: Dict of cta-admin repack ls output
    """
    dst_tapes = [
        dst_info['vid'] for dst_info in repack_info['destinationInfos']
    ]
    n_repacked_files = int(repack_info['archivedFiles'])
    n_files_to_repack = int(repack_info['totalFilesToRetrieve'])

    missing_files = n_files_to_repack - n_repacked_files
    if missing_files:
        files_check = f'All {n_repacked_files} files have been repacked'
    else:
        files_check = f'Failed to repack {missing_files} file(s) ' \
            f'(repacked: {n_repacked_files}, ' \
            f'expected: {n_files_to_repack})'

    # Create a statistics log entry, simplified to avoid nested json
    spread_log_line = repack_info
    spread_log_line['timestamp'] = str(int(datetime.now().timestamp()))
    spread_log_line['command'] = 'cta-admin --json repack ls'
    spread_log_line['host'] = os.uname()[1]
    stat_file = config.get(my_name, 'repack-end-json-path')
    with open(stat_file, 'a', encoding='utf-8') as f:
        f.write(f'{json.dumps(spread_log_line)}\n')

    # Write human-readable log entry
    msg = f'Tape {repack.vid} repacked. SUMMARY: {files_check}, ' \
            f'now spread on {len(dst_tapes)} different ' \
            f'tape(s): {", ".join(dst_tapes)}. '
    logger.info(msg)


def timeout_check(repack, db_connection):
    """
    Check if the repack takes too long. Move it into the 'error' state if
    this is the case such that operators have a look.
    :param repack: The repack object
    :param db_connection: Database connection object from sql_utils.connect
    :returns: True if timed out, otherwise False
    """
    start = repack.status.get_date_started()

    if datetime.now() - start > timedelta(days=REPACK_TIMEOUT_DAYS):
        logger.error(
            f"Tape {repack.vid} has been repacking for longer than "
            f"{REPACK_TIMEOUT_DAYS} days, please investigate cause."
        )
        repack.declare_problematic(db_connection)
        return True

    return False


def submit_subrequest(repack, db_connection, tape_repack_info):
    """
    Clean up completed subrequest and queue another.
    :param repack: The repack object
    :param db_connection: Database connection object from sql_utils.connect
    :returns: True on success, otherwise False
    """
    logger.info(f"Submitting new repack sub-request for tape {repack.vid}")
    if tape_repack_info['status'] == 'Complete':
        old_removed = repack.remove_cta_repack()
        if not old_removed:
            logger.error(
                f"Failed to clean up old repack for tape {repack.vid}, aborting"
            )
            return False

    return repack.submit_cta_repack(db_connection)

def check_repack_complete(repack, db_connection, tape_repack_info):
    """
    Check whether or not the tape has been fully repacked.
    :param repack: The repack object
    :param db_connection: Database connection object from sql_utils.connect
    :returns: True if tape repack is complete, otherwise False
    """

    # If its status is NOT "Complete", we do not proceed further
    if tape_repack_info['status'] != 'Complete':
        if tape_repack_info['status'] == 'Failed':
            if repack.mode == 'auto':
                logger.warning(
                    f'CTA repack for tape {repack.vid} failed'
                )
                repack.declare_problematic(
                    db_connection = db_connection,
                    reason = "Repack in Failed state"
                )
        elif tape_repack_info['status'] in ['Running', 'Starting']:
            logger.info(
                f'Tape {repack.vid} is still being repacked. Skipping...'
            )
            timeout_check(repack, db_connection)

        return False

    # This single repack is complete, but is the tape fully repacked?
    if not tape_repack_info['allFilesSelectedAtStart']:
        return False

    return True


def main():
    """
    Go through the repacking tapes, submit new subrequests for completed ones,
    and advance completely repacked ones to the next stage.
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    repack_utils.print_script_banner(config, logger, my_name)

    # Establish connection to DB
    db_connection = sql_utils.connect(config)

    tapes_repacked = []
    active_repacks = repack_utils.select_active_repacks(
        db_connection,
        config=config,
        logger=logger
    )
    for repack in active_repacks:
        # Get Repack info from CTA
        tape_repack_info = repack.get_cta_repack(db_connection)
        if tape_repack_info is None:
            # Check that tape is empty and move to next stage,
            # otherwise re-submit repack request.
            logger.warning(
                f"Tape {repack.vid} has no repack request in CTA. " \
                "Attempting to recover."
            )
            tape_info = tape_medium.get_tape_info(repack.vid, logger)
            files_on_tape = int(tape_info['nbMasterFiles'])
            bytes_on_tape = int(tape_info['masterDataInBytes'])
            tape_dirty = tape_info['dirty']
            if files_on_tape != 0 or bytes_on_tape != 0 or tape_dirty:
                logger.info(
                    f"Repack for tape {repack.vid} deemed incomplete, " \
                    "submitting new repack request to CTA."
                )
                repack.submit_cta_repack(db_connection)
                continue
            else:
                logger.error(
                    "Could not recover from missing CTA-side repack object" \
                    f"for tape {repack.vid}."
                )
                repack.declare_problematic(db_connection)
                continue

        # Use this opportunity to update the file counter
        if tape_repack_info is not None:
            repack.set_file_count(
                int(tape_repack_info['filesLeftToRetrieve'])
            )

        # We check if the tape has finished being repacked,
        # and if needed requeue a new subrequest.
        complete = check_repack_complete(repack, db_connection, tape_repack_info)
        if not complete:
            if tape_repack_info['status'] == 'Complete':
                submit_subrequest(repack, db_connection, tape_repack_info)

            continue

        # The repack for this tape is complete, advance to next stage
        repack.status.set_date_repacked()
        repack.save(db_connection)
        tapes_repacked.append(repack.vid)

        write_repack_summary(repack, tape_repack_info)

    if len(tapes_repacked) == 0:
        log_utils.log_and_exit(
            logger,
            'No tapes have been marked as repacked',
            return_code=0
        )

    logger.info(
        f'{len(tapes_repacked)} available tape(s) marked as repacked'
    )

    # Close DB connections
    db_connection.close()


if __name__ == '__main__':
    main()
