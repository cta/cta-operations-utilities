#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Repack Manager - the command line tool for interacting with and managing
the CTA repack automation system.
"""

import sys
import json
import argparse

from datetime import datetime
from tabulate import tabulate

import tapeadmin

from ctautils import log_utils, sql_utils, table_utils, repack_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME


###############
# Configuration
###############

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None


ALLOWED_MODES = ['manual', 'auto', 'error']

REPACK_STAGE_NAMES = {
    1: 'Entered',
    2: 'Started',
    3: 'Repacked',
    4: 'Quarantined',
    5: 'Reclaimed',
    6: 'Finished'
}

ANSI_TABLE, ANSI_END = None, None
LS_EXTENDED_HEADERS, LS_SHORT_HEADERS = None, None

TABLE_ALIGN_EXTENDED = ["left", "left", "left", "left", "left", "left", "left",
                        "left", "left", "right", "right", "right", "left",
                        "left", "left", "left"]

TABLE_ALIGN_SHORT = ["left", "left", "left", "left", "right", "right", "left",
                     "right", "right", "left"]

###########
# Functions
###########

def load_args():
    """
    Set up argparse, which is the backbone of this script.
    """
    parser = argparse.ArgumentParser(
        description = "Repack automation command line utility"
    )

    # Settings
    settings_group = parser.add_argument_group(
        'settings',
        'Configure how this script runs'
    )
    settings_group.add_argument(
        '-C',
        '--config-file',
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "Full path of the file containing the repack configuration"
    )
    settings_group.add_argument(
        '-j',
        '--json',
        action = 'store_true',
        help = "Print json instead of pretty output"
    )
    settings_group.add_argument(
        '-V',
        '--verbose',
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    # Actions
    subparsers = parser.add_subparsers()

    parser_ls = subparsers.add_parser(
        'list',
        aliases = ['ls'],
        help = "List repacks in the system"
    )
    parser_ls.set_defaults(func=repack_ls)
    parser_ls.add_argument(
        '-e',
        '--extended',
        action = 'store_true',
        help = "Display extended output, useful for troubleshooting"
    )
    parser_ls.add_argument(
        '-m',
        '--mode',
        choices = ALLOWED_MODES,
        default = None,
        help = "List only repacks with given mode"
    )
    parser_ls.add_argument(
        '-o',
        '--order-by',
        choices = ['progress', 'entered', 'last-written', 'usage'],
        default = 'progress',
        help = "Override ls output order"
    )
    parser_attach = subparsers.add_parser(
        'attach',
        aliases = ['at'],
        help = "Create a repack automation entry for an existing CTA repack"
    )
    parser_ls.add_argument(
        '-v',
        '--vid',
        type = str,
        nargs = '+',
        default = [],
        help = "Tape cartridge VID(s)"
    )
    parser_attach.set_defaults(func=repack_attach)
    parser_attach.add_argument(
        '-v',
        '--vid',
        type = str,
        nargs = '+',
        default = [],
        help = "Tape cartridge VID(s)"
    )
    parser_add = subparsers.add_parser(
        'create',
        aliases = ['add', 'a'],
        help = "Create a new repack entry in the automation for a given VID"
    )
    parser_add.set_defaults(func=repack_add)
    parser_add.add_argument(
        '-d',
        '--destination',
        type = str,
        choices = ['tolabel', 'erase'],
        help = "Override priority-determined tolabel/erase destination. "
        "Use together with the --priority flag. Defaults to config file mapping"
    )
    parser_add.add_argument(
        '-p',
        '--priority',
        type = str,
        choices = ['high', 'low'],
        default = 'low',
        help = "Repack priority, which determines mountpolicy + tolabel/erase "
            "pool destinations. Defaults to 'low'"
    )
    parser_add.add_argument(
        '--reason',
        type = str,
        default = None,
        help = "Override tape reason string with a custom one"
    )
    parser_add.add_argument(
        '--start',
        action = 'store_true',
        help = "Bypass active repack rate limiting, queue in CTA immediately"
    )
    parser_add.add_argument(
        '-v',
        '--vid',
        type = str,
        nargs = '+',
        default = [],
        help = "Tape cartridge VID(s)"
    )

    parser_rm = subparsers.add_parser(
        'remove',
        aliases = ['rm'],
        help = "Remove repacks from the system"
    )
    parser_rm.set_defaults(func=repack_rm)
    parser_rm.add_argument(
        '-a',
        '--all',
        action = 'store_true',
        help = "Remove ALL repacks from the list"
    )
    parser_rm.add_argument(
        '-c',
        '--complete',
        action = 'store_true',
        help = "Remove repacks which have completed (state 6/6)"
    )
    parser_rm.add_argument(
        '-v',
        '--vid',
        type = str,
        nargs = '+',
        help = "Tape cartridge VID(s)"
    )

    parser_ch = subparsers.add_parser(
        'change',
        aliases = ['ch'],
        help = "Change a repack in the system"
    )
    parser_ch.set_defaults(func=repack_ch)
    parser_ch.add_argument(
        '-d',
        '--destination',
        type = str,
        choices = ['tolabel', 'erase'],
        help = "Override priority-determined tolabel/erase destination"
    )
    parser_ch.add_argument(
        '--mode',
        choices = ALLOWED_MODES,
        help = "Change whether or not the repack is operator controlled "
            "or automated by the system"
    )
    parser_ch.add_argument(
        '-p',
        '--priority',
        type = str,
        choices = ['high', 'low'],
        default = 'low',
        help = "Repack priority, which determines mountpolicy + tolabel/erase "
            "pool destinations. Defaults to value set in config file."
    )
    parser_ch.add_argument(
        '--started',
        type = str,
        #help = f"Set repack '{REPACK_STAGE_NAMES[2]}' time to 'now', "
        #    "'-' or any valid ISO 8601-formatted datetime"
        help = f"Set repack '{REPACK_STAGE_NAMES[2]}' time to 'now', "
            "'-' or any valid Y-m-d H:M:S-formatted datetime"
    )
    parser_ch.add_argument(
        '--repacked',
        type = str,
        #help = f"Set repack '{REPACK_STAGE_NAMES[3]}' time to 'now',"
        #    "'-' or any valid ISO 8601-formatted datetime"
        help = f"Set repack '{REPACK_STAGE_NAMES[3]}' time to 'now',"
            "'-' or any valid Y-m-d H:M:S-formatted datetime"
    )
    parser_ch.add_argument(
        '--quarantined',
        type = str,
        #help = f"Set repack '{REPACK_STAGE_NAMES[4]}' time to 'now', '-' or "
        #    "any valid ISO 8601-formatted datetime"
        help = f"Set repack '{REPACK_STAGE_NAMES[4]}' time to 'now', '-' or "
            "any valid Y-m-d H:M:S-formatted datetime"
    )
    parser_ch.add_argument(
        '--reclaimed',
        type = str,
        #help = f"Set repack '{REPACK_STAGE_NAMES[5]}' time to 'now', "
        #    "'-' or any valid ISO 8601-formatted datetime"
        help = f"Set repack '{REPACK_STAGE_NAMES[5]}' time to 'now', "
            "'-' or any valid Y-m-d H:M:S-formatted datetime"
    )
    parser_ch.add_argument(
        '--finished',
        type = str,
        #help = f"Set repack workflow's '{REPACK_STAGE_NAMES[6]}' "
        #    "time to 'now', '-' or any valid ISO 8601-formatted datetime"
        help = f"Set repack workflow's '{REPACK_STAGE_NAMES[6]}' "
            "time to 'now', '-' or any valid Y-m-d H:M:S-formatted datetime"
    )
    parser_ch.add_argument(
        '-v',
        '--vid',
        type = str,
        nargs = '+',
        help = "Tape cartridge VID(s)"
    )

    parser_check = subparsers.add_parser(
        'check-env',
        aliases = ['ck'],
        help = "Checks that the repack env is set up correctly"
    )
    parser_check.set_defaults(func=check_env)

    parser_retry = subparsers.add_parser(
        'retry',
        aliases = ['rt', 'restart'],
        help = "Re-submit a repack which has failed"
    )
    parser_retry.set_defaults(func=repack_retry)
    parser_retry.add_argument(
        '-v',
        '--vid',
        type = str,
        nargs = '+',
        default = [],
        help = "Tape cartridge VID(s)"
    )

    args = parser.parse_args()

    # Assign specified config file (YAML) to the config object
    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    # Colors must be loaded after config is parsed
    configure_tables()

    logger = log_utils.init_logger(my_name, config)
    return args, parser

def configure_tables():
    """
    Prepare table aesthetics based on config.
    """
    global ANSI_TABLE, ANSI_END
    ANSI_TABLE = config.get('colors', 'ansi_color_table')
    ANSI_END = config.get('colors', 'ansi_end')

    global LS_EXTENDED_HEADERS, LS_SHORT_HEADERS
    LS_EXTENDED_HEADERS = [
        f"{ANSI_TABLE}Tape{ANSI_END}",
        f"{ANSI_TABLE}Media{ANSI_END}",
        f"{ANSI_TABLE}Pool{ANSI_END}",
        f"{ANSI_TABLE}{REPACK_STAGE_NAMES[1]}{ANSI_END}",
        f"{ANSI_TABLE}{REPACK_STAGE_NAMES[2]}{ANSI_END}",
        f"{ANSI_TABLE}{REPACK_STAGE_NAMES[3]}{ANSI_END}",
        f"{ANSI_TABLE}{REPACK_STAGE_NAMES[4]}{ANSI_END}",
        f"{ANSI_TABLE}{REPACK_STAGE_NAMES[5]}{ANSI_END}",
        f"{ANSI_TABLE}{REPACK_STAGE_NAMES[6]}{ANSI_END}",
        f"{ANSI_TABLE}Files{ANSI_END}",
        f"{ANSI_TABLE}Bytes{ANSI_END}",
        f"{ANSI_TABLE}Usage{ANSI_END}",
        f"{ANSI_TABLE}Last Written{ANSI_END}",
        f"{ANSI_TABLE}Mode{ANSI_END}",
        f"{ANSI_TABLE}Priority{ANSI_END}",
        f"{ANSI_TABLE}Operator{ANSI_END}"
    ]

    LS_SHORT_HEADERS = [
        f"{ANSI_TABLE}Tape{ANSI_END}",
        f"{ANSI_TABLE}Status{ANSI_END}",
        f"{ANSI_TABLE}Media{ANSI_END}",
        f"{ANSI_TABLE}Pool{ANSI_END}",
        f"{ANSI_TABLE}Bytes{ANSI_END}",
        f"{ANSI_TABLE}Usage{ANSI_END}",
        f"{ANSI_TABLE}Last Written{ANSI_END}",
        f"{ANSI_TABLE}Mode{ANSI_END}",
        f"{ANSI_TABLE}Priority{ANSI_END}",
        f"{ANSI_TABLE}Operator{ANSI_END}"
    ]


def repack_ls(db_con, args, config):
    """
    List repacks in the system in tabulated form
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    vids = args.vid

    logger.debug(
        f"Listing repack items in database {db_con.info.dbname}, "
        f"ordered by {args.order_by}",
    )

    repacks = repack_utils.list_in_db(
        db_connection = db_con,
        vids = vids,
        config = config,
        logger = logger,
        long_fmt = args.extended,
        order_by = args.order_by,
        mode = args.mode
    )

    if args.json:
        repack_dicts = [
            repack.to_dict(use_timestamps=True, short=not args.extended)
            for repack in repacks
        ]
        print(json.dumps(repack_dicts))

    else:
        if args.extended:
            headers = LS_EXTENDED_HEADERS
            align = TABLE_ALIGN_EXTENDED
        else:
            headers = LS_SHORT_HEADERS
            align = TABLE_ALIGN_SHORT

        if len(repacks) == 0: # Special case for empty result set,
            align = ''        # tabulate tries to align cols which don't exist.

        print(
            table_utils.get_table(
                headers = headers,
                rows = repacks,
                config = config,
                col_align = align
            )
        )


def repack_rm(db_con, args, config):
    """
    Remove a repack in the system.
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    if args.vid:
        vids = args.vid
        logger.info(
            f"Removing repacks with the following VIDs from the system: {vids}"
        )
        repack_utils.remove_from_db(db_con, vids, config, logger)
    elif args.all:
        logger.info("Removing ALL repacks from the system")
        repack_utils.remove_all_from_db(db_con, config, logger)
    elif args.complete:
        logger.info("Removing completed repacks from the system")
        completed_repacks = repack_utils.select_completed_repacks(
            db_con,
            config,
            logger
        )
        vids = [repack.vid for repack in completed_repacks]
        repack_utils.remove_from_db(db_con, vids, config, logger)
    else:
        log_utils.log_and_exit(
            logger,
            "Command 'repack rm' requires at least one VID, "
            "or the --all/--complete flag to be set"
        )


def repack_ch(db_con, args, config):
    """
    Change a repack in the system.
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    if args.vid:
        vids = args.vid
        for vid in vids:
            repack = repack_utils.Repack(
                tape = vid,
                db_connection = db_con,
                config = config,
                logger=logger
            )
            # Warn if the VID does not exist
            if not repack.in_db:
                logger.warning(
                    f"Tape with VID {vid} not present in the the repack "
                    "automation database, skipping."
                )
                continue
            # Change the repack mode
            if args.mode:
                repack.set_mode(args.mode)
            # Change one or multiple the repack timestamps
            # TODO: Update this to Python3.7+ when the time is right
            fmt = config.get('logging' 'date_format', default='%Y-%m-%d %H:%M:%S')
            if args.started:
                if args.started == 'now':
                    new_time = datetime.now()
                elif args.started == '-':
                    new_time = None
                else:
                    new_time = datetime.strptime(args.started, fmt)
                    #new_time = datetime.fromisoformat(args.started)  # Python3.7+
                repack.status.set_date_started(new_time)
            if args.repacked:
                if args.repacked == 'now':
                    new_time = datetime.now()
                elif args.repacked == '-':
                    new_time = None
                else:
                    new_time = datetime.strptime(args.repacked, fmt)
                    #new_time = datetime.fromisoformat(args.repacked)  # Python3.7+
                repack.status.set_date_repacked(new_time)
            if args.quarantined:
                if args.quarantined == 'now':
                    new_time = datetime.now()
                elif args.quarantined == '-':
                    new_time = None
                else:
                    new_time = datetime.strptime(args.quarantined, fmt)
                    #new_time = datetime.fromisoformat(args.quarantined)  # Python3.7
                repack.status.set_date_quarantined(new_time)
            if args.reclaimed:
                if args.reclaimed == 'now':
                    new_time = datetime.now()
                elif args.reclaimed == '-':
                    new_time = None
                else:
                    new_time = datetime.strptime(args.reclaimed, fmt)
                    #new_time = datetime.fromisoformat(args.reclaimed)  # Python3.7+
                repack.status.set_date_reclaimed(new_time)
            if args.finished:
                if args.finished == 'now':
                    new_time = datetime.now()
                elif args.finished == '-':
                    new_time = None
                else:
                    new_time = datetime.strptime(args.finished, fmt)
                    #new_time = datetime.fromisoformat(args.finished)  # Python3.7+
                repack.status.set_date_finished(new_time)
            if args.priority:
                priority = repack_utils.priority_name_to_num(
                    args.priority,
                    config,
                    logger,
                    args.destination
                )
                repack.priority = priority

            repack.save(db_con)

    else:
        log_utils.log_and_exit(
            logger,
            "Command 'repack ch' requires at least one VID"
        )


def repack_add(db_con, args, config):
    """
    Create a new repack in the automation system.
    The repack will be queued in CTA by the automation.
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    if args.vid:
        if args.start and args.reason is None:
            log_utils.log_and_exit(
                logger,
                "A reason has to be given with the '--start' flag"
            )

        vids = args.vid
        logger.info(f"Adding the following tape VIDs to the automation: {vids}")
        for vid in vids:
            logger.info(f"Adding automated repack for tape {vid}")

            # Allow override of priority (and destination with a custom priority)
            priority = None
            if args.priority:  # User specifies a priority string
                priority = repack_utils.priority_name_to_num(
                    args.priority,
                    config,
                    logger,
                    args.destination
                )

            repack = repack_utils.Repack(
                tape = vid,
                db_connection = db_con,
                priority = priority,
                config = config,
                logger = logger
            )
            repack.save(db_con)

            if args.start:
                repack.set_tape_repacking(db_con)
                repack.submit_cta_repack(db_con)

    else:
        log_utils.log_and_exit(
            logger,
            "Command 'repack add' requires at least one VID"
        )


def repack_attach(db_con, args, config):
    """
    Take an existing CTA repack and add it to the repack automation system.
    An appropriate status for the repack in the automation system is estimated.
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    if args.vid:
        vids = args.vid
        logger.info(
            "Adding existing repacks with the following VIDs from the system: "
            f"{vids}"
        )

        for vid in vids:
            logger.info(f"Adding automated repack for tape {vid}")
            repack = repack_utils.Repack(
                tape = vid,
                db_connection = db_con,
                config = config,
                logger=logger
            )
            state_valid = repack.determine_state(config, logger)
            if state_valid:
                repack.save(db_con)
            else:
                logger.warning(f"Skipping tape {vid}")
    else:
        log_utils.log_and_exit(
            logger,
            "Command 'repack attach' requires at least one VID"
        )


def repack_retry(db_con, args, config):
    """
    Easy command for restarting a failed repack.
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    if args.vid:
        vids = args.vid
        logger.info(
            "Retrying existing repacks for the following VIDs: "
            f"{vids}"
        )

        repacks_retried = 0
        for vid in vids:
            repack = repack_utils.Repack(
                tape = vid,
                db_connection = db_con,
                config = config,
                logger=logger
            )
            if repack.retry(db_con):
                repacks_retried += 1
        logger.info(
            f"Retried {repacks_retried} out of {len(vids)} repacks"
        )

    else:
        log_utils.log_and_exit(
            logger,
            "Command 'repack retry' requires at least one VID"
        )


def check_env(db_con, args, config):
    """
    Call library env check, supplying the correct args such as logger.
    :param db_con: Database connection
    :param args: The command line args from argparse
    :param config: A CtaOpsConfig object
    """
    repack_utils.repack_check_env(
        db_connection = db_con,
        args = args,
        config = config,
        logger=logger
    )


def main():
    """
    Provide command line interface for the CTA repack automation system.
    """
    args, parser = load_args()

    # Configure TapeAdmin
    tapeadmin.configure(config, logger)

    # Establish DB connection
    db_con = sql_utils.connect(config)

    # Determine course of action, based on subparser default for 'func'
    if hasattr(args, 'func'):
        args.func(db_con, args, config)
    else:
        parser.print_help()

    # Close DB connections
    db_con.close()

if __name__ == '__main__':
    main()
