#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Verify that tapes are empty and move reclaimed tapes to the tolabel/erase pools.
Verify full tapes in repack_<> tapepools and move them to user tapepools
"""

import sys
import json
import argparse

import tapeadmin

from ctautils import log_utils, sql_utils, cmd_utils, repack_utils
from tapeadmin import tape_library, tape_pool
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME

from collections import namedtuple

TapeFileArchiveRoute = namedtuple(
    "TapeFileArchiveRoute", ["storageClass", "copyNumber"]
)


###############
# Configuration
###############

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None


###########
# Functions
###########


def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "This script identifies tapes which have been reclaimed, "
            "and moves them to the 'tolabel/erase' pools after having checked "
            "once more that they are empty. Then updates the repack DB "
            "accordingly."
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    return args


def get_repack_tapepools() -> list[str]:
    """Get a list of all repack tapepools from CTA catalogue"""
    cmd = f"{tapeadmin.cta_admin_json_tapepool_ls}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    return [
        tp["name"]
        for tp in json.loads(cta_adm_call.stdout)
        if tp["name"].startswith("repack") and tp["vo"] == "REPACK"
    ]

def get_full_tapes(tapepool:str):
    cmd = f"{tapeadmin.cta_admin_json_tape_ls_tapepool} {tapepool} --state ACTIVE --full true"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    return [ta["vid"] for ta in json.loads(cta_adm_call.stdout)]

def get_archive_routes(vid: str) -> set[str]:
    """
    Return the set of (storageClass,copyNb) couple found as metadata of all files on this tape
    :param vid: A str representing the tape to scan
    """
    cmd = f"{tapeadmin.cta_admin_json_tape_files} ls --vid {vid}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return set(
            TapeFileArchiveRoute(tf["af"]["storageClass"], tf["tf"]["copyNb"])
            for tf in json.loads(cta_adm_call.stdout)
        )
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )

def get_all_archive_routes() -> list:
    """Return a list of all archive routes found in CTA catalogue"""
    cmd = f"{tapeadmin.cta_admin_json_archivalroute_ls}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return json.loads(cta_adm_call.stdout)
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )


def get_storage_class(name: str):
    """
    Return storage class info (as dict) from CTA catalogue
    """
    cmd = f"{tapeadmin.cta_admin_json_storageclass_ls} --name {name}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return json.loads(cta_adm_call.stdout)[0]
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )


def get_dest_tapepools(vid:str, all_archive_routes:list) -> list[str]:
    ar_on_tape = get_archive_routes(vid)

    dest_tapepools = set()

    for tape_ar in ar_on_tape:
        get_storage_class(tape_ar.storageClass) # Just check if the storageClass still exists
        dest_tapepools |= { ar["tapepool"]
            for ar in all_archive_routes
            if ar["storageClass"] == tape_ar.storageClass
            and ar["copyNumber"] == tape_ar.copyNumber
            and ar["archiveRouteType"] == "DEFAULT"
        }

    return dest_tapepools


def move_tapes_from_repack_tapepools():
    cta_archive_routes = get_all_archive_routes()
    for repack_tp in get_repack_tapepools():
        vids = get_full_tapes(repack_tp)
        if not vids:
            logger.info(
                f"nothing to do for the tapepool {repack_tp} as none of the tapes are full"
            )
            continue

        for vid in vids:
            for dest_tp in get_dest_tapepools(vid, cta_archive_routes):
                cmd = f"{tapeadmin.cta_admin_json_tape_ch} -t {dest_tp} -v {vid} "
                cta_adm_call = cmd_utils.run_cmd(
                    cmd, number_of_attempts=3, timeout=90, logger=logger
                )
                if cta_adm_call.returncode == 0:
                    logger.info(f"Tape {vid} sucessfully moved to {dest_tp} tapepool")
                    break
            else:
                logger.error(f"Failed to move tape {vid} to {dest_tp} tapepool")

def main():
    """
    Move reclaimed tapes to their appropriate label pools.
    Move full tapes from repack tapepools to user ones.
    """
    args = load_args()

    repack_utils.print_script_banner(config, logger, my_name)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    # Establish connection to DB
    db_connection = sql_utils.connect(config)

    if config.get("enable_repack_archive_routes",default=False):
        move_tapes_from_repack_tapepools()

    # Verify that all known destination pools exist
    env_valid = repack_utils.repack_check_env(
        db_connection = db_connection,
        config = config,
        logger = logger
    )
    if not env_valid:
        log_utils.log_and_exit(
            logger,
            "The CTA setup is invalid for automated repack, please run "
            "'cta-ops-repack-manager check-env'."
        )

    repacks_to_finish = repack_utils.select_repacks_to_finish(
        db_connection,
        config = config,
        logger = logger
    )
    tapes_failed_to_be_moved = {}  # {volume_id: failure_description}

    for repack in repacks_to_finish:

        # After a tape is reclaimed, it will have to fulfill the following conditions:
        # 1) NOT full
        # 2) occupancy = 0
        # 3) last fseq = 0
        cmd = f'{tapeadmin.cta_admin_json_tape_ls_vid} {repack.vid}'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                "Failed to look up tape"
            )
            tapes_failed_to_be_moved[repack.vid] = 'Failed to look up tape'
            continue
        tape_info = json.loads(cmd_call.stdout)[0]
        problems = []
        # 1)
        if tape_info['full']:
            problems.append('tape marked as full')
        # 2)
        if tape_info['occupancy'] != '0':
            problems.append(f'occupancy = {tape_info["occupancy"]}')
        # 3)
        if tape_info['lastFseq'] != '0':
            problems.append(f'last fseq = {tape_info["lastFseq"]}')
        # We discard the tape if any of the previous conditions were not met
        if problems:
            tapes_failed_to_be_moved[repack.vid] = f'({", ".join(problems)})'
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                f'{tapes_failed_to_be_moved[repack.vid]}'
            )
            continue

        # We determine the correct destination pool based on a tape's
        # logical library and media type
        logical_library = tape_info['logicalLibrary']
        media_type = tape_info['mediaType']
        pool_postfix = tape_library.safe_lib_to_pool_map(
            logical_library = logical_library,
            mediatype = media_type,
            config = config,
            logger = logger,
            vid = repack.vid
        )
        if pool_postfix is None:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                'Failed to fetch destination pool postfix'
            )
            continue
        pool_prefix = config.get('priorities', repack.priority, 'default_dst_pool_prefix')
        to_label_pool = f'{pool_prefix}{pool_postfix}'

        # First, we check if the corresponding "tolabel/erase" pool exists
        if not tape_pool.pool_exists(to_label_pool):
            logger.error(
                f'Failed to move tape {repack.vid} to the pool "{to_label_pool}", '
                f'because the pool does not exist'
            )
            tapes_failed_to_be_moved[repack.vid] = 'Corresponding pool ' \
                f'"{to_label_pool}" does not exist'
            continue

        # Then, try to move the tape to the corresponding label pool
        cmd = f'{tapeadmin.cta_admin_tape_ch} --vid {repack.vid} --tapepool {to_label_pool}'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                f'Failed to move to the pool "{to_label_pool}"'
            )
            tapes_failed_to_be_moved[repack.vid] = cmd_call.stderr.rstrip()
            continue
        logger.info(
            f'Tape {repack.vid} has been moved to the pool "{to_label_pool}"'
        )

        # Update the repack automation DB entry
        repack.tapepool = to_label_pool
        repack.status.set_date_finished()
        repack.save(db_connection)

    if not repacks_to_finish:
        logger.info(
            'No tapes found to move to a "tolabel/erase" pool'
        )
        sys.exit(0)

    # Inform if any tapes have failed to be moved
    if tapes_failed_to_be_moved:
        tapes_and_failures = ', '.join(
            [f'{a_tape} ({reason})'
             for a_tape, reason in tapes_failed_to_be_moved]
        )
        logger.warning(
            f'Failed to move {len(tapes_failed_to_be_moved.keys())} '
            f'tape(s) to a "tolabel/erase" pool: {tapes_and_failures}'
        )

    # Inform if we managed to move any tape at all, or not
    tapes_moved = len(repacks_to_finish) - len(tapes_failed_to_be_moved.keys())
    if tapes_moved > 0:
        logger.info(
            f'Successfully moved {tapes_moved} tape(s) to "tolabel/erase" pools'
        )
    else:
        log_utils.log_and_exit(
            logger,
            'No tapes could be moved to any "tolabel/erase" pools',
            return_code = 0
        )

    # Close DB connections
    db_connection.close()


if __name__ == '__main__':
    main()
