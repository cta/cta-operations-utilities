#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Move repacked tapes to the quarantine pool, update the DB.
Tapes remain in quarantine for a period of time before being reclaimed.
Also removes the corresponding repack object in CTA.
"""

import sys
import json
import argparse

import tapeadmin

from ctautils import log_utils, sql_utils, cmd_utils, repack_utils
from tapeadmin import tape_pool
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME


###############
# Configuration
###############
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]
config, logger = None, None


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "This script moves tapes which have been repacked to the "
                    "quarantine pool and updates the repack DB accordingly"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    return args


def main():
    """
    Move repacked tapes to the quarantine pool
    """
    args = load_args()

    repack_utils.print_script_banner(config, logger, my_name)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    # Check that the quarantine pool exists
    if not tape_pool.pool_exists('quarantine'):
        log_utils.log_and_exit(
            logger,
            "Could not find destination pool 'quarantine', "
            "please ensure that the pool exists."
        )

    # Establish connection to DB
    db_connection = sql_utils.connect(config)

    repacks_to_quarantine = repack_utils.select_repacks_to_quarantine(
            db_connection,
            config = config,
            logger = logger
    )

    tapes_failed_to_be_moved = {}   # {volume_id: reason_why}

    for repack in repacks_to_quarantine:

        # We check if the tape has any files registered in CTA
        cmd = f'{tapeadmin.cta_admin_json_tapefile_ls_vid} {repack.vid}'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                "Failed to list tape files"
            )
            tapes_failed_to_be_moved[repack.vid] = 'Failed to list tape files'
            continue
        n_master_files = len(json.loads(cmd_call.stdout))

        if n_master_files > 0:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                f"Tape contains {n_master_files} master files. "
            )
            msg = f'{n_master_files} master files on tape'
            tapes_failed_to_be_moved[repack.vid] = msg
            continue

        # We move it to the quarantine pool
        cmd = f'{tapeadmin.cta_admin_tape_ch_tapepool_quarantine_vid} ' \
            f'{repack.vid}'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                'Failed to change tapepool'
            )
            tapes_failed_to_be_moved[repack.vid] = 'Failed to change tapepool'
            continue
        logger.info(
            f"Tape {repack.vid} has been moved to the quarantine pool"
        )

        # Remove the repack object in CTA
        clean = repack_utils.clean_up_cta_repack(repack.vid, logger)
        if not clean:
            logger.error(
                f"Failed to clean up existing repack for vid {repack.vid}, "
                "skipping"
            )
            continue

        # Set the tape state to ACTIVE. It remains 'full' until reclaimed
        cmd = f'{tapeadmin.cta_admin_json_tape_ch} --vid {repack.vid} ' \
            '--state ACTIVE'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode != 0:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                "Failed to set tape ACTIVE"
            )
            tapes_failed_to_be_moved[repack.vid] = 'Failed to set tape ACTIVE'
            continue
        logger.info(
            f"State of the tape {repack.vid} changed to: ACTIVE"
        )

        # Update the repack automation DB entry
        repack.tapepool = "quarantine"
        repack.status.set_date_quarantined()
        repack.save(db_connection)

    if len(repacks_to_quarantine) == 0:
        log_utils.log_and_exit(
            logger,
            "No tapes found to move to the quarantine pool",
            return_code = 0
        )

    # Inform if any tapes have failed to be moved
    if tapes_failed_to_be_moved:
        tapes_and_failures = ', '.join(
            [f'{a_tape} ({reason})'
             for a_tape, reason in tapes_failed_to_be_moved.items()]
        )
        logger.warning(
            f"Failed to move {len(tapes_failed_to_be_moved.keys())} "
            f"tape(s) to quarantine: {tapes_and_failures}"
        )

    # Inform if we managed to move any tape at all, or not
    failed_moved_keys = tapes_failed_to_be_moved.keys()
    tapes_moved = len(repacks_to_quarantine) - len(failed_moved_keys)
    if tapes_moved > 0:
        logger.info(
            f"Successfully moved {len(repacks_to_quarantine)} tape(s) to "
            "the quarantine pool"
        )
    else:
        log_utils.log_and_exit(
            logger,
            'No tapes could be moved to the quarantine pool'
        )

    # Close DB connection
    db_connection.close()


if __name__ == '__main__':
    main()
