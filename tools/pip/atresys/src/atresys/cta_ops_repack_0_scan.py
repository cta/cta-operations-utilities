#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Scans through the list of known tapes and identifies candidates for repacking.
The output of this script is meant for the cta-ops-repack-1-prepare script.
"""

import sys
import json
import argparse

import tapeadmin

from ctautils import log_utils,  cmd_utils, repack_utils

from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME
###############
# Configuration
###############
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]
config, logger = None, None


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "Scans for tapes in need of a repack, "
            "prints selected VIDs",
    )
    parser.add_argument(
        "-n",
        "--number-of-tapes",
        type = int,
        help = "Upper limit for number of tapes to select"
    )
    parser.add_argument(
        "-o",
        "--output-file",
        type = str,
        help = "Write output to a file instead of stdout"
    )
    parser.add_argument(
        "-p",
        "--pool-filter",
        type = str,
        nargs = '*',
        help = "If specified, consider only tapes in this "
                "white-space separated pool list"
    )
    parser.add_argument(
        "-t",
        "--usage-threshold",
        type = float,
        help = "A float [0.0, 1.0] that specifies the minimum "
            "masterDataInBytes/capacity ratio "
            "a tape must have before being considered for repack. "
            "0 = unused and 1 = at capacity"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )
    settings_group.add_argument(
        "-j",
        "--json",
        action = 'store_true',
        help = "Write output as JSON (default disabled)"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    # Read defaults from config file
    if args.number_of_tapes is None:
        args.number_of_tapes = config.get(my_name, 'number_of_tapes')
    if args.usage_threshold is None:
        args.usage_threshold = config.get(my_name, 'tape_usage_threshold')

    return args


def get_all_tape_info():
    """
    Get information about all tapes in the system.
    :return: A cta-admin ta ls output dictionary
    """
    cmd = f'{tapeadmin.cta_admin_json_tape_ls_all}'
    cta_adm_call = cmd_utils.run_cmd(
        cmd,
        number_of_attempts = 3,
        timeout = 90,
        logger = logger
    )

    if cta_adm_call.returncode == 0:
        return json.loads(cta_adm_call.stdout)
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )

def get_repack_tapepools() -> list[str]:
    cmd = f"{tapeadmin.cta_admin_json_tapepool_ls}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return [ tp["name"] for tp  in json.loads(cta_adm_call.stdout) if tp["name"].startswith("repack") and tp["vo"] == "REPACK"]  
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )

def select_by_usage(tape_info:dict, threshold:float, count:int,
                    pool_filter:list=None, ignore_list:list=None):
    """
    Select n tapes based on how much of their capacity is used by live files.
    :param tape_info: Tape information json
    :param threshold: The minimum masterDataInBytes/capacity threshold to qualify
    :param count: Max number of tapes to return
    :param pool_filter: List of strings. If given, select only tapes in these pools
    :return: A list of VIDs which should be repacked
    """
    # Key: VID, vals: capacity_in_use_fraction
    eligible_tapes = {}

    for tape in tape_info:
        # Ignore tapes in pools that are ineligible for repack
        if ignore_list is not None and tape['tapepool'] in ignore_list:
            continue
        # Discard 0-occupancy tapes before we get any further
        if int(tape['occupancy']) == 0:
            continue
        # Discard non-full tapes
        if not tape['full']:
            continue
        # Only tapes in the ACTIVE state are eligible
        if tape['state'] != 'ACTIVE':
            continue
        # Select only from specified pools
        if pool_filter is not None and tape['tapepool'] not in pool_filter:
            continue
        # Calculate how much of the tape is in use
        in_use_frac = int(tape['masterDataInBytes']) / int(tape['occupancy'])

        if in_use_frac < threshold:
            eligible_tapes[tape['vid']] = in_use_frac

    # Sort by usage, ascending order, use up to <count> VIDs
    selected_tapes = sorted(
        eligible_tapes,
        key=eligible_tapes.get,
    )[0:count]

    return selected_tapes

def main():
    """
    Search for candidate tapes for repack and output results
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    repack_utils.print_script_banner(
        config,
        logger,
        my_name
    )
    ignore_list = config.get(my_name, 'ignore_pools')
    if config.get("enable_repack_archive_routes", default=False):
        ignore_list.extend(get_repack_tapepools())

    logger.info(f"Scanning for {args.number_of_tapes} tapes")
    logger.info(f"Using tape usage threshold of {args.usage_threshold}")
    logger.info(f"Excluding tapes in the following pools: {ignore_list}")
    if args.output_file:
        logger.info(f"Writing output to file: {args.output_file}")

    # Identify the tapes we're interested in repacking
    tape_info = get_all_tape_info()

    selected_by_usage = select_by_usage(
        tape_info = tape_info,
        threshold = args.usage_threshold,
        count = args.number_of_tapes,
        pool_filter = args.pool_filter,
        ignore_list = ignore_list
    )

    logger.info(f"Found {len(selected_by_usage)} tape(s) matching criteria")

    selected_tapes_str = ' '.join(selected_by_usage)
    logger.debug(f"Selected the following tapes: {selected_tapes_str}")

    # Output formatting
    if args.json:
        output = json.dumps(selected_by_usage)
    else:
        if args.output_file:
            str_sep = '\n'  # Multi-line, 1 entry per line
        else:
            str_sep = ' '   # Single line, picked up by rundeck job
        output = str_sep.join(selected_by_usage)

    # Write output
    if args.output_file:
        with open(args.output_file, 'w', encoding='utf-8') as f:
            f.write(output)
    else:
        print(output)

if __name__ == '__main__':
    main()
