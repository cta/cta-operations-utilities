#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Generates statistics to feed into the monitoring infrastructure.
Runs commands to generate json, which is fed into fluentd
"""

import os
import sys
import json
import logging
import argparse

from datetime import datetime

import tapeadmin

from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME

###############
# Configuration
###############

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config = None, None


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = 'A script for periodic monitoring data generation '
    )

    parser.add_argument(
        '-f',
        '--config-file',
        type=str,
        default = DEFAULT_CONFIG_PATH,
        help='The path for the config file (file has to be in YAML format)'
    )
    parser.add_argument(
        '-o',
        '--output-file',
        type=str,
        default='',
        help='Override configured output file'
    )
    parser.add_argument(
        '-V',
        '--verbose',
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    return args


def init_repack_ls_logger(output_path:str):
    """
    Create a logger for the output file read by the log gatherer,
    such as fluentd. This logger handles the log rotation.
    :param output_path: File path to write to
    :returns: Logging logger object
    """

    ls_logger = logging.getLogger(f'repack_ls_logger')
    ls_logger.setLevel(logging.INFO)

    max_log_size = config.get('logging', 'rotation_max_size', default='1G')

    # Create dir structure and file with correct permissions
    cmd_utils.create_ops_owned_file(
        path = output_path,
        config = config,
        mode = log_utils.DEFAULT_LOG_FILE_PERM,
    )

    fmt = logging.Formatter(
        fmt = "{message}",
        datefmt = config.get('logging', 'date_format'),
        style = "{"
    )

    rot_file_handler = log_utils.RotatingCompressingGroupFileHandler(
        filename = output_path,
        group = config.get('default_user', 'group'),
        date_fmt = config.get('logging', 'date_format').replace(' ', '_'),
        maxBytes = cmd_utils.get_size_from_format(max_log_size),
        backupCount = config.get('logging', 'rotation_keep_files'),
    )
    rot_file_handler.setFormatter(fmt)
    ls_logger.addHandler(rot_file_handler)

    return ls_logger


def add_repack_manager_log_entry(args):
    """
    Run repack-manager --json ls, write output into fluentd-monitored log file.
    :param args: Parsed argparse arguments
    """

    output_file = config.get(my_name, 'ls_out')
    if args.output_file != '':
        output_file = args.output_file
    ls_logger = init_repack_ls_logger(output_file)

    timestamp = str(int(datetime.now().timestamp()))  # td-agent expects a string

    cmd = 'cta-ops-repack-manager --json ls'
    cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(logger, "Failed to list automated repacks")
    try:
        repacks_list = json.loads(cmd_call.stdout)
    except json.decoder.JSONDecodeError:
        log_utils.log_and_exit(
            logger,
            "Failed to parse json. Output of cta-ops-repack-manager is invalid."
        )

    logger.info(
        f"Writing new log entries to file {output_file} using config "
        f"in {args.config_file}."
    )
    # Add timestamp and command to each message line
    for repack in repacks_list:
        # TODO: Could be done nicer in the logger
        out_dict = repack
        out_dict['timestamp'] = timestamp
        out_dict['command'] = cmd
        out_dict['host'] = os.uname()[1]
        ls_logger.info(out_dict)


def main():
    """
    Create log entries for Fluentd to consume
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    add_repack_manager_log_entry(args)


if __name__ == '__main__':
    main()
