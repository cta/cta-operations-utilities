#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Prepares tapes for repack (adds them to the database)
"""

import sys
import json
import socket
import argparse

from collections import namedtuple
TapeFileArchiveRoute = namedtuple(
    "TapeFileArchiveRoute", ["storageClass", "copyNumber"]
)

from itertools import groupby
from datetime import datetime

import tapeadmin
from importlib.resources import path

from ctautils import log_utils, sql_utils, mail_sender, cmd_utils, repack_utils
from tapeadmin import tape_medium, tape_pool
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME


###############
# Configuration
###############

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "A script to prepare tapes for repack "
        "(add them to the database). "
        "Capable of ingesting the VID list by command line flag, "
        "file or by stdin."
    )
    parser.add_argument(
        "-v",
        "--vids",
        type = str,
        nargs = '+',
        help = "One or multiple volume IDs to repack."
    )
    parser.add_argument(
        "-F",
        "--file",
        type = str,
        help = "A file containing a list of volume IDs to be repacked. "
            "The format of the file is one VID per line."
    )
    parser.add_argument(
        "-s",
        "--srcpool",
        type = str,
        help = "The source pool from which tapes are taken for repack."
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    if not args.vids and not args.file and not args.srcpool:
        # Attempt to read tape file list from stdin
        vids = []
        if not sys.stdin.isatty():
            for line in sys.stdin:
                vids += line.rstrip().split()
        if len(vids) > 0:
            args.vids = vids
        else:
            parser.print_help()
            sys.exit(1)

    return args


def get_all_tapepools() -> list:
    """Return a list of all tapepool found in CTA catalogue"""
    cmd = f"{tapeadmin.cta_admin_json_tapepool_ls}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return json.loads(cta_adm_call.stdout)
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )


def get_archive_routes(vid: str) -> set[TapeFileArchiveRoute]:
    """
    Return the set of (storageClass,copyNb) couple found as metadata of all files on this tape   
    :param vid: A str representing the tape to scan
    """
    cmd = f"{tapeadmin.cta_admin_json_tape_files} ls --vid {vid}"    
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return set(
            TapeFileArchiveRoute(tf["af"]["storageClass"], tf["tf"]["copyNb"])
            for tf in json.loads(cta_adm_call.stdout)
        )
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )


def get_all_archive_routes() -> list:
    """Return a list of all archive routes found in CTA catalogue"""
    cmd = f"{tapeadmin.cta_admin_json_archivalroute_ls}"    
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return json.loads(cta_adm_call.stdout)
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )


def get_storage_class(name:str):
    """
    Return storage class info (as dict) from CTA catalogue
    """
    cmd = f"{tapeadmin.cta_admin_json_storageclass_ls} --name {name}"
    cta_adm_call = cmd_utils.run_cmd(
        cmd, number_of_attempts=3, timeout=90, logger=logger
    )
    if cta_adm_call.returncode == 0:
        return json.loads(cta_adm_call.stdout)[0]
    else:
        log_utils.log_and_exit(
            logger,
            f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
            cta_adm_call.returncode,
        )


def get_new_needed_archiveroutes(
    ar_on_tapes: set[TapeFileArchiveRoute], all_archive_routes: list
) -> dict[TapeFileArchiveRoute]:
    """
    Compute new needed archive routes we have to create for REPACK
    :param ar_on_tapes: A set of (storageClass,copyNb) couples found on all tape files we want to repack
    :param all_archive_routes: list of all archive routes in CTA catalogue
    :param all_tape_pools: list of all tapepools in CTA catalogue
    :return: dict of new needed REPACK archive routes
    """
    new_needed_re_ar = {}

    for storageClass, tfars in groupby(
        sorted(ar_on_tapes, key=lambda x: x.storageClass), key=lambda x: x.storageClass
    ):
        sc = get_storage_class(storageClass)

        default_ars = [
            ar
            for ar in all_archive_routes
            if ar["storageClass"] == storageClass
            and ar["archiveRouteType"] == "DEFAULT"
        ]

        if len(default_ars) != int(sc["nbCopies"]):
            log_utils.log_and_exit(
                logger,
                f"Number of DEFAULT Archive Route doesn't match nbCopies needed by {storageClass} storageClass",
            )

        for tape_ar in tfars:
            if not 1 <= tape_ar.copyNumber <= int(sc["nbCopies"]):
                log_utils.log_and_exit(
                    logger,
                    f"At least one fileCopyNumber ({tape_ar.copyNumber}) doesn't match its storageClass in tapes_to_repack",
                )

        for i in (set(range(1,int(sc["nbCopies"])+1)) - set(
            ar["copyNumber"]
            for ar in all_archive_routes
            if ar["storageClass"] == storageClass and ar["archiveRouteType"] == "REPACK"
        )):
            new_needed_re_ar[TapeFileArchiveRoute(storageClass,i)] = {
                "storageClass": tape_ar.storageClass,
                "copyNumber": i,
                "archiveRouteType": "REPACK",
            }

    return new_needed_re_ar


def add_new_needed_tapepools(new_needed_ar:dict[TapeFileArchiveRoute], all_archive_routes:list, all_tapepools: list)-> tuple[dict,dict]:
    """
    From new needed archive routes, compute the tapepool we have create.
    :param new_needed_ar: REPACK Archive routes we have to create for REPACK
    :param all_archive_routes: list of all archive routes in CTA catalogue
    :param all_tape_pools: list of all tapepools in CTA catalogue
    :return: (dict of needed archive routes,dict of needed tapepools)
    """
    new_needed_re_tp= {}
    for tfar in list(new_needed_ar.keys()):
        default_tp_name = next(
            (
                ar["tapepool"]
                for ar in all_archive_routes
                if ar["storageClass"] == tfar.storageClass
                and ar["archiveRouteType"] == "DEFAULT"
                and ar["copyNumber"] == tfar.copyNumber
            ),
            False,
        )
        if not default_tp_name:
            log_utils.log_and_exit(
                f"DEFAULT Archive route for (storageClass,CopyNumber)=({tfar.storageClass},{tfar.copyNumber}) not found in CTA catalogue: not able to get tapepool information"
            )

        repack_tp_name=f"repack_{default_tp_name}"

        if next((tp for tp in all_tapepools if tp["name"] == repack_tp_name), False):
            new_needed_ar[tfar]["tapepool"] = repack_tp_name
            new_needed_ar[tfar]["comment"] = f"{tfar.storageClass} -> {repack_tp_name} (Created by ATRESYS)"
            break

        default_tp = next((tp for tp in all_tapepools if tp["name"] == default_tp_name), False)
        if not default_tp:
            log_utils.log_and_exit(f"Tapepool {default_tp_name} not found in CTA catalogue")


        # For now: We don't create archive routes and tapepool where src tapepool is encrypted
        if default_tp["encrypt"]:
            logger.warning("Repack tapepools and archive routes with encrypted is not yet supported")
            del new_needed_ar[tfar]
            break

        new_needed_ar[tfar]["tapepool"] = repack_tp_name
        new_needed_ar[tfar]["comment"] = f"{tfar.storageClass} -> {repack_tp_name} (Created by ATRESYS)"

        new_needed_re_tp[repack_tp_name] = {
            "name": repack_tp_name,
            "vo": "REPACK",
            "numPartialTapes": 1,
            "encrypt": default_tp["encrypt"],
            "supply": default_tp["supply"],
            "comment": f"REPACK tapepool for {default_tp_name} (Created by ATRESYS)",
        }

    return new_needed_ar, new_needed_re_tp

def compute_actions_needed_for_repack(vids:list[str])-> dict:
    """
    Compute all needed adds of resources to the system 
    to be able to repack all tapes without issues.
    :param vids: list of vids to repack
    :return: A dict containing needs tapepools and archive routes creations.
    """
    archives_routes, tape_pools = get_all_archive_routes(), get_all_tapepools()
    ar_on_tapes = {ar for vid in vids for ar in get_archive_routes(vid)}
    new_needed_ar = get_new_needed_archiveroutes(ar_on_tapes,archives_routes)
    new_needed_ar,new_needed_tp = add_new_needed_tapepools(new_needed_ar,archives_routes, tape_pools)

    actions = {
        "add_tapepool": list(new_needed_tp.values()),
        "add_archiveroute": list(new_needed_ar.values()),
    }

    logger.info("List of actions which will be taken:")
    logger.info(json.dumps(actions))

    return actions


def do_actions(actions:dict):
    """
    Perform cta-admin command based on actions dict to create new
    tapepools and archive routes
    :param actions: A dict containing needs tapepools and archive routes creations.
    """
    for action in actions["add_tapepool"]:
        logger.info(f"Adding {action['name']} tapepool")
        cmd = ( 
        f"{tapeadmin.cta_admin_tapepool_json} add " 
        f"-n {action['name']} "
        f"--vo {action['vo']} "
        f"-p {action['numPartialTapes']} "
        f"-e {str(action['encrypt']).lower()} "
        f"-s '{action['supply']}' "
        f"-m '{action['comment']}' "
        )
        if not action["supply"]:
            logger.warning(f"The new tapepool {action['name']} has no supply tapepool set up!")
        cta_adm_call = cmd_utils.run_cmd(
            cmd, number_of_attempts=3, timeout=90, logger=logger
        )
        if cta_adm_call.returncode != 0:
            log_utils.log_and_exit(
                logger,
                f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
                cta_adm_call.returncode,
            )
    for action in actions["add_archiveroute"]:
        logger.info(f"Adding {action['comment']} archive route")
        cmd = (
        f"{tapeadmin.cta_admin_archivalroute_json} add "
        f"-s {action['storageClass']} "
        f"-c {action['copyNumber']} "
        f"--art {action['archiveRouteType']} "
        f"-t {action['tapepool']} "
        f"-m '{action['comment']}' "
        )
        cta_adm_call = cmd_utils.run_cmd(
            cmd, number_of_attempts=3, timeout=90, logger=logger
        )
        if cta_adm_call.returncode != 0:
            log_utils.log_and_exit(
                logger,
                f"Error in subprocess: {cmd}: (stderr: {cta_adm_call.stderr})",
                cta_adm_call.returncode,
            )

def select_tapes_to_repack(db_connection, vids) -> list[repack_utils.Repack]:
    """
    Checks if a tape is valid for repack, and if so,
    insert it into the database for the rest of the repack workflow
    :param db_connection: Database connection object from sql_utils.connect
    :param vids: A list of tape VIDs
    :return: The tapes to repack
    """
    tapes_to_repack = []

    for tape in vids:

        # For each tape, first check if it exists
        tape_info = tape_medium.get_tape_info(tape, logger)
        if tape_info is None:
            logger.error(
                f'Tape candidate "{tape}" does not exist in CTA. Skipping...'
            )
            continue

        # A tape is valid for repack when it:
        #   1. Is in the ACTIVE state
        #   2. Does not already exist in the repack system
        #   3. Is marked as full
        # If the tape doesn't fulfill all the previous conditions, we skip it
        failed_conditions = []
        if tape_info['state'] != 'ACTIVE':
            failed_conditions.append(f"Invalid tape state: {tape_info['state']}")

        tape_exists = repack_utils.check_tape_in_system(
            db_connection,
            tape,
            config,
            logger
        )
        if tape_exists:
            failed_conditions.append(f"Already present: {tape}")

        if not tape_info['full']:
            failed_conditions.append("Invalid state: Not full")

        # If any of the conditions failed, skip this tape and show in the log
        # file which condition(s) failed
        if failed_conditions:
            logger.error(
                f'Tape candidate {tape} is not eligible for repack due to the '
                f'following reason(s): {", ".join(failed_conditions)}. '
                f'Skipping...'
            )
            continue
        tapes_to_repack.append( repack_utils.Repack(
            tape = tape,
            db_connection = db_connection,
            config = config,
            logger = logger
        ))
    return tapes_to_repack

def send_recap_email(db_connection,tapes_to_repack: list[repack_utils.Repack]):

    # If we have collected either empty tapes or tapes to repack,
    # we send a mail as a report
    if tapes_to_repack:
        err_repacks = repack_utils.list_in_db(
            db_connection = db_connection,
            config = config,
            logger = logger,
            mode = 'error'
        )
        in_error_state = [
            f'- {repack.vid}: {repack.status.stage_name} files'
            for repack in err_repacks
        ]
        candidates = [
            f'- {repack.vid}: {repack.n_files} files, '
            f'{cmd_utils.convert_byte_unit(repack.n_bytes)} data, '
            f'{repack.tapepool} pool'
            for repack in tapes_to_repack
        ]
        email_template_fields = {
            'RECIPIENTS': ', '.join(config.get('email', 'recipients')),
            'SENDER': config.get('email', 'sender'),
            'TIMESTAMP_SUBJECT': datetime.now().strftime('%d %b %Y %H:%M'),
            'COUNT_TAPES': len(tapes_to_repack),
            'TAPES_TO_REPACK': '\n'.join(candidates),
            'TAPES_IN_ERROR': '\n'.join(in_error_state),
            'TIMESTAMP_FOOTER': datetime.now().strftime('%a %b %d %H:%M:%S %Y'),
            'HOSTNAME': socket.gethostname(),
            'CMD_CALL': ' '.join(sys.argv)
        }
        # Check if we have a config-provided template file, otherwise fall back
        # to module-provided template
        email_template = config.get(my_name, 'email', 'template', default="No")
        if email_template == "No":
            with path(__package__, "cta_ops_repack_prepare.tmpl") as temp_path:
                email_template = str(temp_path)

        mail_sender.send_mail(
            notification_name = my_name,
            mail_template_fields = email_template_fields,
            template_file = email_template,
            config = config
        )

    return tapes_to_repack


def gather_tapes_from_cmd(args):
    """
    Extracts the volume IDs of the tapes specified as the command line arguments
    (either single VID, file or srcpool).
    :param args: Parsed argparse arguments
    :return: A list of volume IDs to repack
    """
    tapes_to_prepare = []

    if args.vids:
        for vid in args.vids:
            tapes_to_prepare.append(vid)

    if args.file:
        try:
            with open(args.file, mode='r', encoding='utf-8') as the_file:
                for line in the_file:
                    if not line.startswith('#'):  # If it is not a comment
                        volume_id = line.rstrip()
                        tapes_to_prepare.append(volume_id)
            the_file.close()
        except FileNotFoundError:
            log_utils.log_and_exit(
                logger,
                f'File not found: {args.file}'
            )

    if args.srcpool:
        if not tape_pool.pool_exists(args.srcpool):
            msg = f'Source pool not found: {args.srcpool}'
            log_utils.log_and_exit(
                logger,
                msg
            )
        cmd = f'{tapeadmin.cta_admin_tape_json} ls --tapepool {args.srcpool}'
        cta_admin_call = cmd_utils.run_cmd(cmd, logger=logger)
        for the_tape_info in json.loads(cta_admin_call.stdout):
            a_tape_vid = the_tape_info['vid']
            tapes_to_prepare.append(a_tape_vid)

    if not tapes_to_prepare:  # Is empty
        log_utils.log_and_exit(
            logger,
            'No tapes found for repack',
            return_code=0
        )

    return tapes_to_prepare


def main():
    """
    Insert a list of repack VIDs into the system
    """
    args = load_args()
    repack_utils.print_script_banner(config, logger, my_name)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    # Establish connection to DB
    db_connection = sql_utils.connect(config)

    # Compute changes needed
    vid_list = gather_tapes_from_cmd(args)
    tapes_to_repack = select_tapes_to_repack(db_connection, vid_list)

    if config.get("enable_repack_archive_routes", default=False):
        actions = compute_actions_needed_for_repack(
            [
                tape.vid
                for tape in tapes_to_repack
                if (tape_info:=tape_medium.get_tape_info(tape.vid, logger))["encryptionKeyName"] == "-"
                or not tape_info["encryptionKeyName"]  # For now we skip tapes with encryption key name
            ]
        )
        # Really applying changes on the system
        do_actions(actions)

    for tape in tapes_to_repack:
        tape.save(db_connection)
    send_recap_email(db_connection,tapes_to_repack)

    number_of_tapes = len(tapes_to_repack) if tapes_to_repack else "No"
    logger.info(f"{number_of_tapes} tape(s) prepared for repack")

    # Close DB connections
    db_connection.close()


if __name__ == '__main__':
    main()
