#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Reclaim repacked tapes in the reclaim tape pool
"""

import sys
import argparse

import tapeadmin

from ctautils import log_utils, sql_utils, cmd_utils, repack_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from atresys import TOOL_NAME


###############
# Configuration
###############
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]
config, logger = None, None


###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "This script reclaims tapes which have been repacked and "
            "moved to the reclaim pool, and updates the repack DB accordingly"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    return args


def main():
    """
    Reclaim tapes which have finished their quarantine time
    """
    args = load_args()

    repack_utils.print_script_banner(config, logger, my_name)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    # Establish connection to DB
    db_connection = sql_utils.connect(config)

    repacks_to_reclaim = repack_utils.select_repacks_to_reclaim(
        db_connection,
        config = config,
        logger = logger
    )

    for repack in repacks_to_reclaim:

        # We try to reclaim it
        logger.info(
            f"Reclaiming tape {repack.vid} ..."
        )
        cmd = f'{tapeadmin.cta_admin_tape_reclaim_vid} {repack.vid}'
        cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
        if cmd_call.returncode == 1:
            log_utils.log_and_exit(
                logger,
                f"Command {cmd} resulted in an XRootD, protobuf or frontend "
                "error (returncode 1)"
            )
        elif cmd_call.returncode == 2:
            repack.declare_problematic(
                db_connection,
                config,
                logger,
                "Failed to reclaim tape"
            )
            continue
        logger.info(
            f"Tape {repack.vid} has been reclaimed"
        )

        # Update the DATE_RECLAIMED column in the DB
        repack.status.set_date_reclaimed()
        repack.save(db_connection)

    if not repacks_to_reclaim:
        logger.info(
            "No tapes found to reclaim"
        )
        sys.exit(0)

    logger.info(
        f"Successfully reclaimed {len(repacks_to_reclaim)} tape(s)"
    )

    # Close DB connections
    db_connection.close()


if __name__ == '__main__':
    main()
