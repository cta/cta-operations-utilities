#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

import os
import sys
import json
import argparse
import tempfile

import tapeadmin
from tapeadmin import tape_file
from ctautils import cmd_utils, log_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctaopseos import TOOL_NAME

my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None

# TODO: Make config options
DEFAULT_NB_FILES_PER_SPLIT = 1000
NB_RETRIES                 = 10

def parse_args():
    """
    Parse command-line arguments for changing the storage class for a given path in EOS.

    :return: argparse.Namespace object containing the parsed arguments
    """
    parser = argparse.ArgumentParser(description="Change storage class for a given path in EOS.")

    parser.add_argument(
        '-l',
        '--location-of-dump',
        type = str,
        help = 'The location of dump',
        required=True
    )
    parser.add_argument(
        '-i',
        '--instance',
        type = str,
        help = 'The instance where the files are located in EOS',
        required=True
    )
    parser.add_argument(
        '-n',
        '--nb-files-per-execution',
        type = str,
        help = 'The number of files per split for the input files, higher number will lead to more memory usage',
        required=False,
        default = DEFAULT_NB_FILES_PER_SPLIT
    )

    # Settings
    settings_group = parser.add_argument_group(
        'settings',
        'Configure how this script runs'
    )
    settings_group.add_argument(
        '-C',
        '--config-file',
        type = str,
        help = 'The path for the config file (file has to be in YAML format)',
        required = False,
        default = DEFAULT_CONFIG_PATH
    )
    settings_group.add_argument(
        '-V',
        '--verbose',
        type = str,
        help = 'Verbose logging',
        required = False,
        default = False
    )
    args = parser.parse_args()

    return args

class ChangeStorageClass:
    def __init__(self, args, files_per_split):
        self.eos_instance                  = args.instance
        self.number_of_files_per_execution = files_per_split
        self.location_of_dump              = args.location_of_dump

    def show_progress(self, i, total_amount_of_files, nb_not_updated_files_catalogue, tmp_change_storage_class_dir_root):
        """
        Display the progress of updating storage class for files in CTA.

        :param i: int : Iteration number
        :param total_amount_of_files: int : Total number of files to update from EOS
        :param nb_not_updated_files_catalogue: int : Total number of files with updated storage class in the catalogue
        :param tmp_change_storage_class_dir_root: str : Temporary directory root for storing log and input files
        """
        print("--------------------------------------------------------------------------------------------------------------------------------------------")
        print(f"| Iteration {i}")
        print(f"| Total number of files to update:                                         {total_amount_of_files}")
        print(f"| Total number of files with updated storage class in the CATALOGUE:       {total_amount_of_files - nb_not_updated_files_catalogue}")
        print("--------------------------------------------------------------------------------------------------------------------------------------------")

        # Exit if all files are consistent and updated
        if nb_not_updated_files_catalogue == 0:
            print()
            print("============================================================================================================================================")
            print("All files have been updated")
            print("============================================================================================================================================")
            sys.exit(0)

    def get_archive_id_from_dump_line(self, line):
        """
        Extract the archive ID from a given line of JSON-formatted data.

        :param line: str : A single line of JSON-formatted data
        :return: str : The extracted archive ID
        """
        line_data      = json.loads(line)
        archive_id_key = "xattr.sys.archive.file_id"
        archive_id     = line_data[archive_id_key]
        return archive_id

    def read_and_create_input_file(self, local_extracted_dump_location, tmp_change_storage_class_dir):
        """
        Read the extracted dump file and create an input file for changing the storage class.

        :param local_extracted_dump_location: str : The path to the extracted dump file
        :param tmp_change_storage_class_dir: str : The temporary directory for storing change storage class files
        :return: str : The path to the created input file
        """
        change_storage_class_input_path = f"{tmp_change_storage_class_dir}/cta-change-storage-class-input.txt"
        with open(local_extracted_dump_location, "r") as extracted_dump_file, open(change_storage_class_input_path, "w") as input_file:
            number_of_files_to_update = 0
            total_number_of_files = 0
            for line in extracted_dump_file:
                try:
                    if not line.strip():
                        continue
                    total_number_of_files = total_number_of_files + 1
                    archive_file_id         = self.get_archive_id_from_dump_line(line)
                    tapefile_ls_object = tape_file.get_tape_file_metadata(
                        logger=logger, archive_file_id=archive_file_id
                    )
                    if tapefile_ls_object == None:
                        continue
                    storage_class_catalogue = tapefile_ls_object["af"]["storageClass"]
                    line_data               = json.loads(line)
                    new_storage_class       = line_data["xattr.sys.archive.storage_class"]
                    fid                     = line_data["fid"]
                    archive_id              = line_data["xattr.sys.archive.file_id"]

                    if storage_class_catalogue != new_storage_class:
                        number_of_files_to_update = number_of_files_to_update + 1
                        input_file_line = {
                        "archiveFileId": archive_id,
                        "fid": fid,
                        "instance": self.eos_instance,
                        "storageclass": new_storage_class,
                        }
                        input_file.write(json.dumps(input_file_line) + "\n")
                except KeyError:
                    print('Error: {archive_id_key} not found in object')
                    continue

            return change_storage_class_input_path, number_of_files_to_update, total_number_of_files

    def split_input_file(self, change_storage_class_input_path, tmp_change_storage_class_dir):
        """
        Split the input file into smaller files based on the specified number of files per execution.

        :param change_storage_class_input_path: str : The path to the input file for changing the storage class
        :param tmp_change_storage_class_dir: str : The temporary directory for storing change storage class files
        :return: str : The path to the directory containing the split files
        """
        split_dir_path = f"{tmp_change_storage_class_dir}/split/"
        os.makedirs(split_dir_path, exist_ok=True)
        cmd = f"split -l {self.number_of_files_per_execution} {change_storage_class_input_path} {split_dir_path}"
        cmd_utils.run_cmd(cmd, logger=logger)

        return split_dir_path

    def run_cta_change_storage_class_on_split_files(self, split_dir_path):
        """
        Execute the CTA change storage class command on each split file in the specified directory.

        :param split_dir_path: str : The path to the directory containing the split files
        """
        split_files = os.listdir(split_dir_path)
        for split_file_name in split_files:
            split_file_path = f"{split_dir_path}{split_file_name}"
            cmd = f"cta-change-storage-class-in-catalogue --json {split_file_path}"
            status = cmd_utils.run_cmd(cmd, logger=logger)
            if status.stderr:
                sys.exit(1)

    def change_storage_class(self, nb_retries):
        """
        Change the storage class for a given path in EOS by iterating through retries if needed.

        This method performs the following steps:
        1. Creates a temporary folder for storing intermediate files.
        2. For each retry iteration (up to NB_RETRIES):
            a. Shows the progress of the storage class update operation.
            b. Reads and creates an input file for changing the storage class based on the extracted dump.
            c. Splits the input file into smaller files.
            d. Executes the CTA change storage class command on each split file.
        3. If all files are not updated after NB_RETRIES iterations, displays a message and exits the program.
        """
        with tempfile.TemporaryDirectory() as temp_folder:
            for i in range(nb_retries):
                extracted_dump_location = self.location_of_dump

                change_storage_class_input_path, number_of_files_not_updated, total_number_of_files = self.read_and_create_input_file(extracted_dump_location, temp_folder)
                self.show_progress(i, total_number_of_files, number_of_files_not_updated, temp_folder)
                split_dir_path = self.split_input_file(change_storage_class_input_path, temp_folder)
                self.run_cta_change_storage_class_on_split_files(split_dir_path)

            print(f"The script has run {NB_RETRIES} iterations and not all files have been updated yet.")
            print("Exiting now.")

def main():
    """
    Entry point for the script to change the storage class for a given path in EOS.
    """
    args = parse_args()

    global config, logger
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    nb_retries = None
    if config.get(my_name, 'nb_retries'):
        nb_retries = config.get(my_name, 'nb_retries')
    else:
        nb_retries = NB_RETRIES

    files_per_split = None
    if config.get(my_name, 'files_per_split'):
        files_per_split = config.get(my_name, 'files_per_split')
    else:
        files_per_split = args.files_per_split

    changer = ChangeStorageClass(args=args, files_per_split=files_per_split)
    changer.change_storage_class(nb_retries)

if __name__ == "__main__":
    main()
