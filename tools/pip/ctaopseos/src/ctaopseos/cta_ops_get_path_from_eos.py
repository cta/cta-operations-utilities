#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Fetches the path for the specified file from EOS
"""

import sys
import json
import argparse
import requests

import tapeadmin
from tapeadmin import tape_file

from ctautils import log_utils, yaml_loader
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctaopseos import TOOL_NAME


my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
logger, config = None, None


def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "This script fetches the path in EOS for the provided files",
        usage = f"\n{sys.argv[0]} [--instance/-i <instance>] \n"
                f"{' '*len(sys.argv[0])} [--fxid/-f <eos-file-id>] \n"
                f"{' '*len(sys.argv[0])} [--archive-id/-I <cta-archive-id>] \n"
    )
    parser.add_argument(
        "-i", 
        "--instance", 
        type=str,
        help="The instance where the file is located."
    )
    parser.add_argument(
        "-f", 
        "--fxid", 
        type=str,
        help="The fxid of the file."
    )
    parser.add_argument(
        "-I", 
        "--archive-id", 
        type=str,
        help="The archive id of the file."
    )

    # Settings
    settings_group = parser.add_argument_group(
        'settings',
        'Configure how this script runs'
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help="The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V", 
        "--verbose", 
        action = "store_true",
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    global config, logger
    if args.config_file is not None:
        config_path = args.config_file
    else:
        config_path = DEFAULT_CONFIG_PATH
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = config_path
    )

    if args.verbose:
        config.set('debug', True)

    logger = log_utils.init_logger(my_name, config)

    if not (args.instance and args.fxid) and not args.archive_id:
        parser.print_help()
        sys.exit(1)

    return args


def get_eos_rest_key_tab_path():
    """
    Find the path to the eos rest keytab yaml file. The script will choose the
    config path in the following order:
    ctaops-config -> default value (/etc/cta/eos.rest.keytab.yaml)
    :param args: The cmd line arguments
    :returns: The path to eos.rest.keytab.yaml
    """
    if config.get(my_name, 'rest_key_tab'):
        rest_key_tab_path = config.get(my_name, 'rest_key_tab')
    else:
        rest_key_tab_path = '/etc/cta/eos.rest.keytab.yaml'
    return rest_key_tab_path


def read_key_tab(yaml_path, instance):
    """
    Read the yaml keytab file to find the host, port
    and token of the instance
    :param yaml_path: Path to the keytab file
    :param instance: The instance where the path is located
    :returns: host, port and token
    """
    keytab = yaml_loader.load_yaml(yaml_path)
    key = keytab[instance]
    return key["port"], key["token"]


def get_fxid(archive_id):
    """
    Get the fxid from the Catalogue
    :param archive_id: The archive id
    :returns: The disk file id in hexadecimal format
    """
    tape_file_object = tape_file.get_tape_file_metadata(archive_file_id=archive_id, logger=logger)

    decimal_number = int(tape_file_object['df']['diskId'])
    fxid = str(hex(decimal_number)[2:])

    return fxid, tape_file_object['df']['diskInstance']


def get_path(fxid, host, port, token):
    """
    Get the path from EOS
    :param fxid: The disk file id in hexadecimal format
    :param host: The EOS host
    :param port: The EOS port
    :param token: The EOS token
    :returns: The disk file id in hexadecimal format
    """
    try:
        headers =  {"Authorization": f"Bearer {token}"}
        url = f'http://{host}:{port}/proc/user/?mgm.cmd=fileinfo&mgm.path=fxid:{fxid}&mgm.format=json'
        timeout_s = 10
        response = requests.get(url, headers=headers, timeout=timeout_s)
        path = json.loads(response.text)['path']
        return path
    except requests.exceptions.ConnectionError as e:
        print(e)
        return None


def main():
    """
    Fetches the path for the specified file from EOS
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    if args.archive_id is not None:
        fxid, instance = get_fxid(archive_id=args.archive_id)
    else:
        fxid = args.fxid
        instance = args.instance
    key_tab_path = get_eos_rest_key_tab_path()
    port, token = read_key_tab(key_tab_path, instance)
    path = get_path(fxid, instance, port, token)
    print(path)


if __name__ == '__main__':
    main()
