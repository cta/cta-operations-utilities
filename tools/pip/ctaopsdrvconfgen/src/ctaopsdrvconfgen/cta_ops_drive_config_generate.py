#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
# This script will generate tape drive configuration in JSON
# format for the CTA tape daemon.
# Tape libraries supported: IBM TS4500 (configuration in JSON
# format) and Spectra Logic TFinity (configuration in XML format).
# For all connected tape drives, it will use the tape drive
# serial number to extract additional information from the
# library configuration files.
#
# Author: Vladimir Bahyl - 2/2024
"""


import re
import os
import sys
import json
import argparse

from xml.dom import minidom

import tapeadmin
from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH    

from tapeadmin import tape_library,tape_drive
from ctaopsdrvconfgen import TOOL_NAME

##########
#
# CONFIGURATION
#
##########

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].rsplit('/', maxsplit=1)[-1]
config, logger = None, None

# Key for the list of drive configs in the output json.
# If using Puppet external facts, then this key is where the data will be
# accessible.
OUT_DATA_KEY = 'tape_drives'

##########
#
# FUNCTIONS
#
##########

def load_args():
    """
    Set up argparse, which is the backbone of this script.
    """
    parser = argparse.ArgumentParser(
        description = "Tool to move tapes from the configured supply tape " \
        "pools into various user pools."
    )

    parser.add_argument(
        '-C',
        '--config-file',
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "Full path to the configuration file"
    )
    parser.add_argument(
        '-V',
        '--verbose',
        action = 'store_true',
        help = "Generate more detailed logs"
    )
    parser.add_argument(
        '-o',
        '--output-file',
        type = str,
        help = "Write output to a json file instead, with the given path+name"
    )
    parser.add_argument(
        '-l',
        '--use-legacy-mapping',
        action = 'store_true',
        help = "Use the legacy_library_mapping field in the config file to " \
        "map generated logical library names of the new format to existing " \
        "logical libraries in CTA. " \
        "Useful for migrations and transition periods."
    )

    args = parser.parse_args()

    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return args


def get_scsi_media_changer(timeout:int):
    """
    Identify the SCSI media changer device of the library (if more devices
    detected, only the 1st one is taken for consideration, which assumes
    that each tape server is connected to exactly one tape library).
    :param timeout: External command timeout in seconds
    :returns: Media changer name string, such as "/dev/sg3"
    """
    command = tapeadmin.get_scsi_media_changer_device_cmd
    try:
        cmd_call = cmd_utils.run_cmd(command, timeout=timeout, logger=logger)
        output = cmd_call.stdout
    except Exception as error:
        log_utils.log_and_exit(logger, format(error))
    if output and output.startswith('/dev/sg'):
        scsi_media_changer = output.rstrip()
    else:
        log_utils.log_and_exit(
            logger,
            f"Unexpected SCSI media changer device: {output}"
        )
    logger.info(
        f"Detected first SCSI media changer device: {scsi_media_changer}"
    )

    return scsi_media_changer


def get_connected_drive_devices(timeout:int):
    """
    Identify all tape drive devices connected to the tape server.
    :param timeout: External command timeout in seconds
    :returns: List of drive device strings, such as "['/dev/sg2']"
    """
    command = tapeadmin.get_attached_devices
    try:
        cmd_call = cmd_utils.run_cmd(command, timeout=timeout, logger=logger)
        output = cmd_call.stdout
    except Exception as error:
        log_utils.log_and_exit(logger, format(error))
    if output and output.startswith('/dev/st'):
        tape_drives = output.rstrip().splitlines()
    else:
        log_utils.log_and_exit(logger, f"Unexpected tape drive devices: {output}")
    logger.info(f"Detected tape drive devices: {tape_drives}")

    return [ tuple(td.split()) for td in tape_drives ]


def process_library_config_xml(tape_lib:str, drive_serial:str, drive_nst:str):
    """
    Read library config from XML files, as supplied by SpectraLogic libraries.
    :param tape_lib: Library name string: "IBM-03584L32-0000078BB2840401"
    :param drive_serial: The tape drive's serial, as string
    :param: The drive's corresponding /dev/nstX device, as string
    :returns: Tape drive configuration dict
    """
    def get_drive_model():
        tape_drive_types = config.get('tape', 'tape_drive_types')
        drive_model = None
        for drive_type in tape_drive_types:
            xml_drive_type = XML_drive_details.getElementsByTagName("driveType")[0].firstChild.data
            if drive_type['name'] == xml_drive_type:
                drive_model = drive_type['model']

        if drive_model is None:
            log_utils.log_and_exit(
                logger,
                f"Unexpected tape drive type: {xml_drive_type}"
            )
        return drive_model

    tape_lib_conf_file = f'{config.get("library_config_dir")}/{tape_lib}.xml'
    if (os.path.isfile(tape_lib_conf_file) and
        cmd_utils.effectively_readable(tape_lib_conf_file)):
        with open(tape_lib_conf_file, 'r', encoding='utf-8') as lib_conf:
            # Extract other details about the tape drive from the library
            # configuration
            XML_tape_lib_conf_file = minidom.parse(lib_conf)
            XML_drives = XML_tape_lib_conf_file.getElementsByTagName("drive")

            if XML_drives:
                # Find the first drive that matches the detected drive serial number
                XML_drive_details = [
                    drive for drive in XML_drives
                    if (drive.getElementsByTagName("serialNumber")[0].firstChild.data
                        == drive_serial.lstrip("0"))
                ][0]
            else:
                log_utils.log_and_exit(
                    logger,
                    "Unexpected or empty library XML configuration file "
                    f"{tape_lib_conf_file}:\n" \
                    f"{XML_tape_lib_conf_file.toprettyxml(indent='', newl='')}"
                )

            logger.debug(
                "Details of the tape drive "
                f"{drive_serial} from the library configuration file " \
                f"{tape_lib_conf_file}\n" \
                "{XML_drive_details.toprettyxml(indent='', newl='')}"
            )
    else:
        log_utils.log_and_exit(
            logger,
            f"Library XML configuration file {tape_lib_conf_file} not found " \
            "or incorrect permissions."
        )

    if XML_drive_details is None:
        # Output empty json in case of nothing found, puppet/facter is unhappy with empty files
        logger.warning(
            f"Tape drive with serial number {drive_serial} not found in the library " \
            f"configuration file: {tape_lib_conf_file}"
        )
        return {}

    drive_type = get_drive_model()

    partition = XML_drive_details.getElementsByTagName("partition")[0].firstChild.data
    logical_lib = f'{partition}-{drive_type}'

    tape_drive_name_match = re.match(
        r"FR(\d+)\/DBA(\d+)\/fLTO-DRV(\d+)",
        XML_drive_details.getElementsByTagName("ID")[0].firstChild.data
    )
    frame_num = tape_drive_name_match.group(1).zfill(2)
    dba_num = tape_drive_name_match.group(2)
    slot_num = tape_drive_name_match.group(3)
    # F = Frame, B = Drive Bay Assembly (DBA), S = Slot in DBA
    location = f'F{frame_num}B{dba_num}S{slot_num}'

    tape_drive_configuration = {}
    tape_drive_configuration['DriveLogicalLibrary'] = logical_lib
    tape_drive_configuration['DriveName'] = f"{logical_lib}-{location}"
    tape_drive_configuration["DriveDevice"] = drive_nst
    partition_drive_num = str(
        int(
            XML_drive_details.getElementsByTagName(
                "partitionDriveNumber"
            )[0].firstChild.data
        ) - 1
    )
    tape_drive_configuration['DriveControlPath'] = f'smc{partition_drive_num}'

    return tape_drive_configuration


def process_library_config_json(tape_lib:str, drive_serial:str, drive_nst:str):
    """
    Read library config for drive serial from json files,
    as supplied by IBM libraries.
    :param tape_lib: Library name string: "IBM-03584L32-0000078BB2840401"
    :param drive_serial: The tape drive's serial, as string
    :param: The drive's corresponding /dev/nstX device, as string
    :returns: Tape drive configuration dict
    """
    def get_drive_model(tape_drive_json):
        tape_drive_types = config.get('tape', 'tape_drive_types')
        drive_model = None
        for drive_type in tape_drive_types:
            if drive_type['name'] == tape_drive_json['mtm']:
                drive_model = drive_type['model']

        if drive_model is None:
            log_utils.log_and_exit(
                logger,
                f"Unexpected tape drive type: {tape_drive_json['mtm']}"
            )
        return drive_model

    tape_lib_conf_file = f'{config.get("library_config_dir")}/{tape_lib}.json'
    if (os.path.isfile(tape_lib_conf_file) and
        cmd_utils.effectively_readable(tape_lib_conf_file)):
        with open(tape_lib_conf_file, 'r', encoding='utf-8') as lib_conf:
            # Extract other details about the tape drive from the library configuration
            try:
                lib_config = json.load(lib_conf)
            except json.JSONDecodeError as error:
                log_utils.log_and_exit(logger, format(error))
            if lib_config is not None:
                tape_drive_json = None
                for drive in lib_config:
                    if drive['sn'] ==  drive_serial.lstrip("0"):
                        tape_drive_json = drive
            else:
                log_utils.log_and_exit(
                    logger,
                    f"Unexpected JSON output for tape drive {drive_serial}: " \
                    f"{tape_lib_conf_file}"
                )
            logger.debug(
                f"Details of the tape drive {drive_serial} from the library " \
                f"configuration file {tape_lib_conf_file}:" \
                f"\n{json.dumps(tape_drive_json, indent=2)}"
            )
    else:
        log_utils.log_and_exit(
            logger,
            f"Library JSON configuration file {tape_lib_conf_file} not found " \
            "or incorrect permissions."
        )

    if tape_drive_json is None:
        # Output empty json in case of nothing found, puppet/facter is unhappy with empty files
        logger.warning(
            f"Tape drive with serial number {drive_serial} not found in the library " \
            f"configuration file: {tape_lib_conf_file}"
        )
        return {}

    drive_type = get_drive_model(tape_drive_json)
    logical_lib = f"{tape_drive_json['logicalLibrary']}-{drive_type}"
    location = tape_drive_json['location'].lstrip("drive_")

    location_match = re.match(
        r"F(\d+)C(\d+)R(\d+)", location
    )
    frame_num = location_match.group(1).zfill(2)
    column_num = location_match.group(2)
    row_num = location_match.group(3)
    # F = Frame, C = Column, R = Row
    location = f"F{frame_num}C{column_num}R{row_num}"

    tape_drive_configuration = {}
    tape_drive_configuration['DriveLogicalLibrary'] = logical_lib
    tape_drive_configuration['DriveName'] = f"{logical_lib}-{location}"
    tape_drive_configuration["DriveDevice"] = drive_nst

    # As observed on IBM tape libraries at CERN, the drive element
    # addresses are sequential and start from 257 so we will use that
    # value for substraction to calculate the drive ordinal number
    # (this assumption saves a call to the SCSI media changer
    # (implemented above))
    # [root@tpsrvXXX ~]# cta-smc -q D
    # Drive Ordinal   Element Addr.     Status     Vid
    #             0             257       free
    #             1             258       free
    #             2             259       free
    #             3             260       free
    lib_ctrl_path = f'smc{str(int(tape_drive_json["elementAddress"]) - 257)}'
    tape_drive_configuration['DriveControlPath'] = lib_ctrl_path

    return tape_drive_configuration


##########
#
# MAIN
#
##########

def main():
    """
    Fetch local device information, then find the corresponding entry
    in the config drive config file produced by the tape library.
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)
    timeout = config.get('timeout')

    scsi_media_changer = get_scsi_media_changer(timeout=timeout)
    tape_lib = tape_library.get_library_type(scsi_media_changer, timeout, logger)

    process_library_conf= None 
    if tape_lib.startswith("SPECTRA-PYTHON-"):
        process_library_conf = process_library_config_xml
    elif tape_lib.startswith('IBM-03584L'):
        process_library_conf = process_library_config_json
    else:
        log_utils.log_and_exit(
            logger,
            f"Unexpected tape library type: {tape_lib}, " \
            "this should not have happened, aborting."
        )

    tape_drives = get_connected_drive_devices(timeout)

    tape_drives_configuration = {OUT_DATA_KEY: []}
    for tape_drv in tape_drives:
        # tape_drv (/dev/stX,/dev/sgX)
        tape_drive_serial = tape_drive.get_drive_serial(
            drive_device = tape_drv[1],
            timeout = timeout,
            logger = logger
        )

        # Use the tape drive serial number to extract other details about the
        # tape drive from the library configuration.
        drive_nst = tape_drv[0].replace("/st","/nst")
        tape_drive_configuration = process_library_conf(
            tape_lib=tape_lib, drive_serial=tape_drive_serial, drive_nst=drive_nst
        )
        # Map new library names to old format if desired
        if args.use_legacy_mapping and tape_drive_configuration:
            legacy_mapping = config.get('legacy_library_mapping')
            legacy_lib_name = legacy_mapping[tape_drive_configuration['DriveLogicalLibrary']]
            tape_drive_configuration['DriveLogicalLibrary'] = legacy_lib_name

        tape_drives_configuration[OUT_DATA_KEY].append(
            tape_drive_configuration
        )

    json_output = json.dumps(tape_drives_configuration, indent=2)
    if args.output_file:
        logger.info(
            "Writing configuration of the tape drives in JSON format to file: " \
            f"{args.output_file}"
        )
        with open(args.output_file, 'w', encoding='utf-8') as out_file:
            out_file.write(json_output)
    else:
        print(json_output)

    # Give exit empty config exit code if applicable
    if not tape_drives_configuration:
        sys.exit(2)


if __name__ == '__main__':
    main()
