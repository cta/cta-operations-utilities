#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2021-2022 CERN
# @license      This program is free software, distributed under the terms of the GNU General Public
#               Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING". You can
#               redistribute it and/or modify it under the terms of the GPL Version 3, or (at your
#               option) any later version.
#
#               This program is distributed in the hope that it will be useful, but WITHOUT ANY
#               WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#               PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and immunities
#               granted to it by virtue of its status as an Intergovernmental Organization or
#               submit itself to any jurisdiction.

"""
Verification feeder selects tapes for verification and then gradually starts
a process for each.
"""

import sys
import json
import argparse

from time import sleep
from datetime import datetime, timedelta
from random import randrange

import tapeadmin
from ctautils import log_utils, cmd_utils
from tapeverify import TOOL_NAME
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH


my_name = sys.argv[0].split('/')[-1]  # Get rid of "/usr/local/bin" and take only the final part
config, logger = None, None

all_tapes                     = {}

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = 'A script to submit background verification jobs via cta-verify-tape',
        usage = f'{my_name} [--filter <status_filter>]'
            ' [--verify_path <file path>] [-C|--config-file <file path>]'
            ' [--verify_policy <policy>] [--tapepool <tapepool>]'
            ' [--min_data_on_tape <data in bytes>]'
            ' [--min_relative_capacity <capacity percentage>]'
            ' [--full_tapes] [--noaction] [--minage <number>] [--maxverify <number>]'
            ' [--verify_options <str>] [--verbose] [-h | --help]'
            ' [--verification_mount_policy <str>] [--ts_format <str>] [--sleep_time <int>]'
    )

    parser.add_argument(
        '--filter',
        type = str,
        default = '',
        help = 'Filter out tapes matching the states passed. '
            'Multiple states should be separated with ",".'
    )
    parser.add_argument(
        '--tapepool',
        type=str,
        default = '',
        help = 'Only verify tapes in the tapepool selected. '
            'Multiple tapepools should be separated with ","'
    )
    parser.add_argument(
        '--full_tapes',
        action = 'store_true',
        help = 'verify only tapes that are full'
    )
    parser.add_argument(
        '--minage',
        type = int,
        help = f'verify tapes which have not been verified for at least N days.'
    )
    parser.add_argument(
        '--min_data_on_tape',
        type = int,
        help = 'verify tapes which have at least N bytes of data.'
    )
    parser.add_argument(
        '--min_relative_capacity',
        type = int,
        help = 'verify tapes which have have been at least N%% filled.'
    )
    parser.add_argument(
        '--maxverify',
        type = int,
        help = 'maximum number verify processes to run concurrently.'
    )
    parser.add_argument(
        '--verify_path',
        type = str,
        help = 'location of tape-verify executable.'
    )
    parser.add_argument(
        '--verify_policy',
        type = str,
        help = 'Select the policy for tape verification: random, last_read, last_write, last_verified.'
    )
    parser.add_argument(
        '--verify_options',
        type = str,
        help = 'options to pass into tape_verify'
    )
    parser.add_argument(
        '--noaction',
        action = 'store_true',
        help='Perform a dry run only.'
    )
    parser.add_argument(
        '--list_current_verifications',
        action = 'store_true',
        help='Just list current ongoing verifications and exit.'
    )
    parser.add_argument(
        '--verification_mount_policy',
        type = str,
        help = 'The verification mount policy to use.'
    )
    parser.add_argument(
        '--ts_format',
        type = str,
        help = 'The format for the timestamps used in the script.'
    )
    parser.add_argument(
        '--sleep_time',
        type = int,
        help = 'The time to sleep between verification jobs in seconds.'
    )
    #
    # Settings
    settings_group = parser.add_argument_group(
        'settings',
        'Configure how this script runs'
    )
    settings_group.add_argument(
        '-V',
        '--verbose',
        action = 'store_true',
        help = 'print more details on operations.'
    )
    settings_group.add_argument(
        '-C',
        '--config-file',
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = 'The path for the config file (file has to be in YAML format)'
    )

    args = parser.parse_args()

    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)
    
    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    args.minage = args.minage or config.get(my_name, 'default_min_age')
    args.maxverify = args.maxverify or config.get(my_name, 'default_max_verify')
    args.min_data_on_tape = args.min_data_on_tape or config.get(my_name, 'default_min_data_on_tape')
    args.min_relative_capacity = args.min_relative_capacity or config.get(my_name, 'default_min_relative_capacity')
    args.verify_options = args.verify_options or config.get(my_name, 'default_verify_options')
    args.verify_policy = args.verify_policy or config.get(my_name, 'default_verify_policy')
    args.verify_path = args.verify_path or config.get(my_name, 'default_tape_verify_path')
    args.verification_mount_policy = args.verification_mount_policy or config.get(my_name, 'verification_mount_policy')
    args.ts_format = args.ts_format or config.get(my_name, 'ts_format')
    args.sleep_time = args.sleep_time or config.get(my_name, 'sleep_time')

   
    global opt_tapepools
    opt_tapepools = set(args.tapepool.split(',')) if args.tapepool != '' else {}


    if args.verify_policy not in verify_policies:
        msg = f"Verify policy must be one of: {', '.join(verify_policies)}"
        log_utils.log_and_exit(logger, msg)

    if int(args.minage) < 0:
        msg = 'Minimum age of tape verification must be at least 0'
        log_utils.log_and_exit(logger, msg)

    if int(args.maxverify) <= 0:
        msg = 'Maximum verification jobs must be at least 1'
        log_utils.log_and_exit(logger, msg)

    return args

def get_random_order(tape):
    """
    Returns random integer between 0 and the unix timestamp of the last modification
    or random integer between 0 and 1000000000 if the modification record is missing
    :param tape: (dict) The desired tape
    :return: The random number
    """
    try:
        lastModifiedTime = tape['lastModificationLog']['time']
    except KeyError:
        lastModifiedTime = 1000000000 # tape is missing the modification time
    return randrange(int(lastModifiedTime))


def get_last_verification_ts_for_tape(tape):
    """
    Returns the unix timestamp of the last verification of the tape passed
    or 0 if the tape was never verified
    :param tape: (dict) The desired tape
    :return: The unix timestamp of the last verification of the tape
    """
    verificationStatus = tape['verificationStatus']
    if len(verificationStatus) == 0: # tape has never been verified
        return 0
    verification_status_map = json.loads(verificationStatus)
    return int(verification_status_map["date"])


def get_last_read_log_time(tape):
    """
    Returns the unix timestamp of the last read performed on the tape passed
    or 0 if the tape was never read
    :param tape: (dict) The desired tape
    :return: The unix timestamp of the last read performed on the tape
    """
    try:
        lastReadTime = tape['lastReadLog']['time']
    except KeyError:
        lastReadTime = 0 # tape has never been read
    return int(lastReadTime)


def get_last_write_log_time(tape):
    """
    Returns the unix timestamp of the last write performed on the tape passed
    or 0 if the tape was never written
    :param tape: (dict) The desired tape
    :return: The unix timestamp of the last write performed on the tape
    """
    try:
        lastWrittenTime = tape['lastWrittenLog']['time']
    except KeyError:
        lastWrittenTime = 0 # tape has never been written
    return int(lastWrittenTime)


verify_policies = {'random': get_random_order,
                   'last_read': get_last_read_log_time,
                   'last_write': get_last_write_log_time,
                   'last_verified': get_last_verification_ts_for_tape}


def get_ts_from_n_days_ago(n, ts_format):
    """
    Returns the unix timestamp of exactly n days ago
    :param n: the number of days passed
    returns: timestamp from n days ago
    """
    return log_utils.datetime_str_to_unix_ts(
        (datetime.now() - timedelta(days=n)).strftime("%Y-%m-%d %H:%M:%S"),
        ts_format
    )


def is_tape_ongoing_verification(tape):
    """
    Checks if the tape is currently undergoing verification.
    :param tape: A dictionary representing the tape.
    :return: True if the tape is undergoing verification, False otherwise.
    """
    if len(tape["verificationStatus"]) == 0: # tape has never been verified
        return False
    verification_status_map = json.loads(tape["verificationStatus"])
    return verification_status_map["status"] == "ongoing"


def is_tape_empty(tape):
    """
    Checks if the tape is empty (has no data).
    :param tape: A dictionary representing the tape.
    :return: True if the tape is empty, False otherwise.
    """
    return int(tape["masterDataInBytes"]) == 0


def mark_verified_tape_as_done(tape):
    """
    Marks a verified tape as done in the verification process.
    :param tape: A dictionary representing the tape.
    """
    vid = tape["vid"]
    verification_status = json.loads(tape['verificationStatus'])
    verification_status['status'] = 'finished'
    change_command = f'{tapeadmin.cta_admin_tape_ch} --vid {vid} --verificationstatus {json.dumps(json.dumps(verification_status))}'
    cmd_call = cmd_utils.run_cmd(change_command, logger=logger)
    if cmd_call.returncode != 0:
        msg = f'Unable to change verification status of tape {vid} to finished'
        log_utils.log_and_exit(logger, msg)
    logger.info(f"Verification for tape {vid} has finished since the last {my_name} run")


def mark_verified_tapes_as_done(tapes, queues):
    """
    Marks multiple verified tapes as done in the verification process.
    :param tapes: A list of dictionaries representing the tapes.
    :param queues: A list of dictionaries representing the tape queues.
    :return: A filtered list of tapes that still need verification.
    """
    vid_set = {tape["vid"] for tape in tapes}
    queues = list(filter(lambda queue: queue["mountType"] == "RETRIEVE" and queue["vid"] in vid_set, queues)) 
    vid_queue_map = {queue["vid"]: queue for queue in queues}
    filtered_tapes = []
    for tape in tapes:
        if tape["vid"] not in vid_queue_map:
            # no retrieve queue for the tape, verification is done
            mark_verified_tape_as_done(tape)
            continue
        filtered_tapes.append(tape)
    return filtered_tapes


def get_tape_list():
    """
    Obtains the list of tapes available to the current tape server
    :return: the list of tapes
    """
    cmd_call = cmd_utils.run_cmd(
        tapeadmin.cta_admin_json_tape_ls_all,
        logger = logger
    )
    if cmd_call.returncode != 0:
        return None
    dict = json.loads(cmd_call.stdout)
    return dict


def main():
    """
    Select a set of tapes to verify, then launch the verification for each.
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    logger.info(f'Executed as: {" ".join(sys.argv)}')

    all_tapes = get_tape_list()
    if all_tapes is None:
        log_utils.log_and_exit(
            logger,
            "Could not get list of all tapes"
        )

    # Get list of queued tapes
    cmd_call = cmd_utils.run_cmd(
        tapeadmin.cta_admin_json_showqueues,
        logger = logger
    )
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            "Could not get list of queued tapes"
        )
    queues = json.loads(cmd_call.stdout)

    # Change verification_status of tapes that have finished verification to "done"
    tapes_being_verified = list(filter(lambda tape: is_tape_ongoing_verification(tape), all_tapes))
    if tapes_being_verified is None:
        log_utils.log_and_exit(
            logger,
            "Could not get tapes currently undergoing verification"
        )
    if not (args.noaction or args.list_current_verifications):
        tapes_being_verified = mark_verified_tapes_as_done(tapes_being_verified, queues)
        if tapes_being_verified is None:
            log_utils.log_and_exit(
                logger,
                "Could not set tapes that have finished verification as done"
            )

    logger.info(f'Currently running verification on tapes: {", ".join((tape["vid"] for tape in tapes_being_verified))}')
    if args.list_current_verifications:
        sys.exit(0)

    # Exit if the number of tapes to verify concurrently is already satisfied
    num_verification_jobs = len(tapes_being_verified)
    logger.info(f'{num_verification_jobs} tapes are currently being verified, target is {args.maxverify}')

    if num_verification_jobs >= int(args.maxverify):
        logger.info("Nothing to do, exiting...")
        sys.exit(0)

    jobs_to_start = int(args.maxverify) - num_verification_jobs

    # filter out the tapes ongoing verification and empty tapes
    tapes = list(filter(lambda tape: not is_tape_ongoing_verification(tape), all_tapes))
    tapes = list(filter(lambda tape: not is_tape_empty(tape), tapes))

    logger.info(f'{len(tapes)} tapes are eligible for verification')

    # filter non full tapes if --full_tapes is specified
    if args.full_tapes:
        logger.info('--full_tapes specified, selecting only tapes that are marked as full')
        tapes = list(filter(lambda tape: tape['full'] is True, tapes))
        logger.info(f'After selecting only full tapes, {len(tapes)} tapes are eligible for verification')

    # filter tapes whose state match the states passed in --filter
    for state_filter in (set(args.filter.split(',')) if args.filter != '' else {}):
        logger.info(f'Filtering out tapes with status={state_filter}')
        tapes = list(filter(lambda tape: tape['state'] != state_filter, tapes))
        logger.info(f'After filtering out tapes with status={state_filter}, ')

    # filter tapes whose tapepool is the tapepool passed in --tapepool
    if opt_tapepools:
        logger.info(f'--tapepool specified, selecting only tapes in tapepools {",".join(opt_tapepools)}')
        tapes = list(filter(lambda tape: tape['tapepool'] in opt_tapepools, tapes))
        logger.info(
            f'After selecting only tapes in tapepools {",".join(opt_tapepools)}, '
            f'{len(tapes)} tapes are eligible'
        )

    # filter tapes that have been verified more recently than --minage
    if int(args.minage) != 0:
        logger.info(f'--minage specified, selecting only tapes verified more than {args.minage} days ago')
        min_ts = get_ts_from_n_days_ago(int(args.minage), args.ts_format)
        tapes = list(filter(lambda tape: get_last_verification_ts_for_tape(tape) >= min_ts, tapes))
        logger.info(
            f'After selecting only tapes verified more than {args.minage} days ago, '
            f'{len(tapes)} are eligible'
        )

    # filter tapes with less than --min_data_on_tape bytes written
    if args.min_data_on_tape:
        logger.info(f'--min_data_on_tape specified, selecting only tapes with at least {args.min_data_on_tape} bytes written')
        tapes = list(filter(lambda tape: int(tape["masterDataInBytes"]) >= args.min_data_on_tape, tapes))
        logger.info(
            f'After selecting only tapes with at least {args.min_data_on_tape} bytes written, '
            f'{len(tapes)} are eligible'
        )

    # filter tapes less than --min_relative_capacity percentage filled
    if args.min_relative_capacity:
        logger.info(
            f'--min_relative_capacity specified, selecting only tapes '
            f'which are at least {args.min_relative_capacity}% full'
        )
        tapes = list(filter(lambda tape: 100 * int(tape["masterDataInBytes"]) / int(tape["capacity"]) >= args.min_relative_capacity, tapes))
        logger.info(
            f'After selecting only tapes which are at least ' \
            f'{args.min_relative_capacity}% full '
            f'{len(tapes)} are eligible')

    # ignore tapes that are already queued
    queued_tapes = {tape["vid"] for tape in queues}
    tapes = list(filter(lambda tape: tape["vid"] not in queued_tapes, tapes))

    # ignore tapes that are being repacked
    cmd_call = cmd_utils.run_cmd(
        tapeadmin.cta_admin_json_repack_ls,
        logger = logger
    )
    if cmd_call.returncode != 0:
        log_utils.log_and_exit(
            logger,
            "Could not get list of tapes being repacked"
        )
    repacking = json.loads(cmd_call.stdout)
    repacking_tapes = {tape["vid"] for tape in repacking}
    tapes = list(filter(lambda tape: tape["vid"] not in repacking_tapes, tapes))

    if len(tapes) == 0:
        msg = 'Options specified result in 0 tapes being verified'
        log_utils.log_and_exit(logger, msg
                )

    tapes.sort(key=verify_policies[args.verify_policy])

    tapes = tapes[:jobs_to_start]
    jobs_to_start = len(tapes)
    logger.info(
        f'Based on policy: {args.verify_policy}, following new tapes have ' \
        f'been selected for verification: ' \
        f'{", ".join([tape["vid"] for tape in tapes])}'
    )
    exit_code = 0
    for i, tape in enumerate(tapes):
        vid = tape['vid']
        cmd = f'{args.verify_path} --vid {vid} {args.verify_options}'
        if args.noaction:
            logger.info(f"'noaction' specified, not running command {cmd}")
        else:
            logger.info(
                f"Submitting verification for tape {vid} (" \
                f"media type: {tape['mediaType']}, " \
                f"logical library: {tape['logicalLibrary']}, " \
                f"tape pool: {tape['tapepool']}, " \
                f"total files: {tape['nbMasterFiles']}, " \
                f"total bytes: {tape['masterDataInBytes']}) using command: {cmd}"
            )
            cmd_call = cmd_utils.run_cmd(cmd, logger=logger)
            if cmd_call.returncode != 0:
                exit_code = 1
                logger.warning(
                    f"Verification for tape {vid} could not be submitted. " \
                    f"{log_utils.format_stderr(cmd_call.stderr)}"
                )
            else:
                logger.info(f'Tape {vid} successfully submitted for verification.')
            if i != jobs_to_start - 1:
                logger.info(f'Waiting {args.sleep_time} seconds to start the next verification job...')
                sleep(args.sleep_time)
    if not exit_code:
        logger.info('All verification jobs submitted successfully')
    sys.exit(exit_code)

if __name__ == '__main__':
    main()
