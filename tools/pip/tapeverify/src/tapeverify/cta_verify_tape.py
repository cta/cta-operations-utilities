#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
Verify-tape verifies a single tape by reading back parts of the data it contains.
"""

import sys
import json
import time
import random
import argparse
import tempfile
import ijson

import tapeadmin

from sortedcontainers import SortedKeyList
from ctautils import log_utils, cmd_utils
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from tapeadmin import tape_medium, tape_drive
from tapeverify import TOOL_NAME

###############
# Configuration
###############
# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]
config, logger = None, None

###########
# Functions
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description = "A script to verify that a tape is functioning correctly",
    )

    required_named = parser.add_argument_group('required arguments')

    required_named.add_argument(
        "-v",
        "--vid",
        required = True,
        type = str,
        help = "The volume ID"
    )

    parser.add_argument(
        "--read_time",
        type = int, help = "The minimum time it should take to read the tape. "
            "If not specified 0 minutes will be taken as default"
    )
    parser.add_argument(
        "--data_size",
        type = str,
        help = "The minimum amount of data that should be read from the tape. "
            "This should be just a number of bytes or a number followed by a "
            "unit (K, M, G, T, P, E)"
    )
    parser.add_argument(
        "--first",
        type = int,
        help = "How many files to verify from the beginning of the tape. "
            "If not specified defaults to 10"
    )

    parser.add_argument(
        "--last",
        type = int,
        help = "How many files to verify from the end of the tape. "
            "If not specified defaults to 10"
    )

    parser.add_argument(
        "--random",
        type = int,
        help = "Minimum number of files to verify from the middle of the tape. "
            "If not specified defaults to 10"
    )

    parser.add_argument(
        "--all",
        action = 'store_true',
        help = "If present, will verify all files on tape."
    )

    parser.add_argument(
        "-o",
        "--options",
        type = str,
        help = "Options to pass into cta-verify-file"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    # Set up config
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    args.data_size = args.data_size or config.get(my_name, 'default_read_data_size')
    args.read_time = args.read_time or config.get(my_name, 'default_read_time')
    args.first = args.first or config.get(my_name, 'default_first', default=10)
    args.random = args.random or config.get(my_name, 'default_random', default=10)
    args.last = args.last or config.get(my_name, 'default_last', default=10)

    if args.read_time < 0:
        msg = 'Minimum time to read tape must be at least 0'
        log_utils.log_and_exit(logger, msg)

    if cmd_utils.get_size_from_format(args.data_size) is None:
        msg = 'Invalid format for data_size ' \
            '(valid format: [0-9]+(\.[0-9]+)?([BKMGTP](i[BKMGTP])?)?'
        log_utils.log_and_exit(logger, msg)

    if args.first < 0:
        msg = 'Files to read at the beginning of the tape must be at least 0'
        log_utils.log_and_exit(logger, msg)

    if args.last < 0:
        msg = 'Files to read at the end of the tape must be at least 0'
        log_utils.log_and_exit(logger, msg)

    if args.random < 0:
        msg = 'Minimum number of files to read at the middle of the tape ' \
            'must be at least 0'
        log_utils.log_and_exit(logger, msg)

    return args


def get_read_data_size_from_time(drive_read_speed:int, desired_time:int):
    """
    Returns the amount of data that must be read from a tape drive with a given read speed
    so that the read takes the desired time
    :param drive_read_speed: The read speed of the tape drive in MBytes/s
    :param desired_time: The desired time to finish the reading of the tape in minutes
    :return: The size of data to be read in bytes
    """
    return drive_read_speed * desired_time * 60 * 1024 * 1024


def update_tape_verification_status(vid:str, files_submitted:int):
    """
    Update the verification status to ongoing of the tape specified
    :param vid: The volumeID of the desired tape
    :param files_submitted: The number of files submitted to verification
    """
    # update tape verification status
    verification_status = {
        "date": int(time.time()),
        "status": "ongoing",
        "files_submitted": files_submitted,
        "files_verified": 0,
        "files_failed": 0
    }

    # dump twice to escape ""
    change_command = f'{tapeadmin.cta_admin_tape_ch} --vid {vid} ' \
            f'--verificationstatus {json.dumps(json.dumps(verification_status))}'
    cmd_call = cmd_utils.run_cmd(change_command, logger=logger)
    if cmd_call.returncode != 0:
        msg = f"Unable to update verification status of tape {vid} to ongoing"
        log_utils.log_and_exit(logger, msg)


def safe_get_files_to_verify(args, read_data_size:int,
                             file_size_sum:int, n_on_tape:int):
    """
    Returns the list of files to be read based on the command line parameters,
    ensuring a desired amount of data is read if possible.
    This function is intended to work with large tapes, where the number
    of files may require a lot of memory.
    :param args: Parsed argparse args
    :param read_data_size: The desired amount of data to be read in bytes
    :param file_size_sum: The amount of data present on the tape
    :param n_on_tape: The total number of files on the tape
    :return: Tuple of the iterable of files to be read,
    a bool indicating wether the full tape is read or not,
    and the total file amount of bytes of these files as an integer.
    """
    def whole_tape_scan(tapefile_ls_fp):
        """
        Scan the full tape, just submit the whole iterable
        """
        logger.info("Performing whole tape scan.")
        tf_iter = get_iter(tapefile_ls_fp)
        return (tf_iter, True, file_size_sum)

    def partial_tape_scan(tapefile_ls_fp):
        """
        Perform a partial tape scan, selecting files at start/end/random.
        """
        logger.info("Performing partial tape scan.")

        # Lists permanently ordered by the files' fSeq value
        start_of_tape_tf_list = SortedKeyList(key=lambda f: int(f['tf']['fSeq']))
        end_of_tape_tf_list = SortedKeyList(key=lambda f: int(f['tf']['fSeq']))
        random_tf_list = SortedKeyList(key=lambda f: int(f['tf']['fSeq']))
        max_fseq = 0

        # Iterate once over the complete set of files for selecting the first/last
        # Try to fulfill request according to parameters
        logger.debug("Selecting files from start and end of tape")

        for tf_item in get_iter(tapefile_ls_fp):
            # Update list of first X files
            start_of_tape_tf_list.add(tf_item)
            if len(start_of_tape_tf_list) > args.first:
                start_of_tape_tf_list.pop()

            # Update list of last Y files
            end_of_tape_tf_list.add(tf_item)
            if len(end_of_tape_tf_list) > args.last:
                end_of_tape_tf_list.pop(index=0)

            # While we're at it, find the highest fseq present on the tape
            if int(tf_item['tf']['fSeq']) > max_fseq:
                max_fseq = int(tf_item['tf']['fSeq'])

        logger.debug(
            f"ArchiveId of files at start of tape to verify: " \
            f"{','.join([tf_item['af']['archiveId'] for tf_item in start_of_tape_tf_list])}"
        )

        logger.debug(
            f"ArchiveId of files at end of tape to verify: " \
            f"{','.join([tf_item['af']['archiveId'] for tf_item in end_of_tape_tf_list])}"
        )

        # Calculate size total thus far
        current_size = 0
        for tf_item in start_of_tape_tf_list + end_of_tape_tf_list:
            current_size += int(tf_item['af']['size'])

        logger.debug("Selecting random file sample")
        # Pre-select the random fseqs to pick.
        # Terribly simple/stupid, but it will have to do.
        random_pick_fseqs = random.choices(range(0, max_fseq + 1), k=args.random)
        finished_picking = False
        while not finished_picking:
            did_replacement = False
            for tf_item in start_of_tape_tf_list + end_of_tape_tf_list:
                if int(tf_item['tf']['fSeq']) in random_pick_fseqs:
                    random_pick_fseqs.remove(int(tf_item['tf']['fSeq']))
                    random_pick_fseqs.append(random.choice(range(0, max_fseq)))
                    did_replacement = True
            if not did_replacement:
                finished_picking = True

        # We iterate again to be sure that we don't select some files twice.
        # Update list of random Z files
        n_samples = 0
        tf_iter = get_iter(tapefile_ls_fp)
        for tf_item in tf_iter:
            if current_size >= read_data_size and n_samples >= args.random:
                break

            if tf_item not in start_of_tape_tf_list and \
                tf_item not in end_of_tape_tf_list and \
                tf_item not in random_tf_list:
                # Random decision if file is picked
                if int(tf_item['tf']['fSeq']) in random_pick_fseqs:
                    random_tf_list.add(tf_item)
                    current_size += int(tf_item['af']['size'])
                    n_samples += 1

        logger.debug(
            f"ArchiveId of random files to verify: " \
            f"{','.join([tf_item['af']['archiveId'] for tf_item in random_tf_list])}"
        )

        read_files = start_of_tape_tf_list + random_tf_list + end_of_tape_tf_list
        logger.debug(
            f"Selected {len(read_files)} files in total, " \
            "{cmd_utils.convert_tape_unit(current_size)}"
        )
        return (read_files, False, current_size)

    def get_iter(tapefile_ls_fp):
        """
        Get an iterator for the json list in fp, starting at the beginning.
        :return: ijson iterator
        """
        tapefile_ls_fp.seek(0)
        return ijson.items(tapefile_ls_fp, 'item')

    # Set up temporary file for `tapefile ls output`.
    # This file is automatically deleted upon close
    max_in_mem_size = cmd_utils.get_size_from_format('50M')
    with tempfile.SpooledTemporaryFile(max_size=max_in_mem_size) as tmp_fp:
        tape_medium.safe_get_tape_files(
            vid = args.vid,
            buffer_file = tmp_fp,
            logger = logger
        )
        if args.all:
            return whole_tape_scan(tmp_fp)

        # Sanity checks
        if  n_on_tape <= args.first + args.random + args.last:
            logger.info(
                "Number of files requested exceeds number of files on tape."
            )
            return whole_tape_scan(tmp_fp)

        if read_data_size > file_size_sum:
            logger.info("Size of data requested exceeds size of data on tape.")
            return whole_tape_scan(tmp_fp)

        # If sane, default to a partial tape scan
        return partial_tape_scan(tmp_fp)


def verify_files(vid:str, tf_iterable):
    """
    Verify a selected set of files on a tape.
    :param vid: The volumeID of the desired tape
    :param tf_iterable: List of tape file dicts to verify
    """
    logger.info(
        f"ArchiveId of files to verify: " \
        f"{','.join([tf_item['af']['archiveId'] for tf_item in tf_iterable])}"
    )

    for tf_item in tf_iterable:
        archive_id = tf_item['af']['archiveId']
        cmd_call = cmd_utils.run_cmd(
            f'{tapeadmin.cta_verify_file_cmd} {archive_id} --vid {vid}',
            logger=logger
        )
        if cmd_call.returncode != 0:
            msg = "Could not submit verification request for archiveId " \
                f"{archive_id} of tape {vid} " \
                f"{log_utils.format_stderr(cmd_call.stderr)}"
            log_utils.log_and_exit(logger, msg)

    logger.info(f"All file verifications queued for tape {vid}")


def verify_tape(args):
    """
    Verify a single tape in accordance with the selected parameters.
    :param args: Parsed argparse argse
    """
    read_speed = tape_drive.get_drive_read_speed()
    read_data_size = max(
        cmd_utils.get_size_from_format(args.data_size),
        get_read_data_size_from_time(read_speed, args.read_time)
    )

    logger.info(
        f"Running verify-tape for tape with vid {args.vid}, " \
        f"read speed of {read_speed} MB/s, " \
        f"and data size: {cmd_utils.convert_byte_unit(read_data_size)}"
    )

    n_on_tape, file_size_sum = tape_medium.get_tape_occupancy(args.vid, logger)

    if file_size_sum is None:
        msg = f"Could not get occupancy of tape {args.vid}"
        log_utils.log_and_exit(logger, msg)

    sel_tf_iterable, whole_tape, verify_file_size = safe_get_files_to_verify(
        args,
        read_data_size,
        file_size_sum,
        n_on_tape
    )

    to_verify_file_count = n_on_tape
    if not whole_tape:
        to_verify_file_count = len(sel_tf_iterable)
        if to_verify_file_count == 0:
            msg = "User specified parameters result in zero files being verified"
            log_utils.log_and_exit(logger, msg)

    logger.info(
        f"Verifying {to_verify_file_count} files and " \
        f"{cmd_utils.convert_byte_unit(verify_file_size)} from tape {args.vid}"
    )

    verify_files(args.vid, sel_tf_iterable)

    update_tape_verification_status(args.vid, to_verify_file_count)


def main():
    """
    Read user/config parameters for the verification,
    then verify a single tape.
    """
    args = load_args()

    # Configure tapeadmin
    tapeadmin.configure(config, logger)

    # Verify the selected tape
    verify_tape(args)


if __name__ == "__main__":
    main()
