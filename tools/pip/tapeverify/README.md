# CTA Tape Verify

The CTA Tape Verify framework monitors tapes in the CTA system and alerts in case of potential data loss/corruption.

The framework is composed by a set of scripts (namely `cta-verify-file`, `cta-verify-tape`, `cta-verification-feeder`), a monitoring flume agent that parses the CTA logs and extracts logs related to verification and related periodic rundeck jobs.

### cta-verify-file

This command submits a verification request to the CTA frontend. A verification request is a retrieve request with the `isVerifyOnly` flag set to true. CTA handles this requests specially:

* On queuing time, the mount policy n attributed to the request is hard-coded in the `/etc/cta/cta-frontend-xrootd.conf` file (configuration options `cta.verification.mount_policy`). This is done so that verification jobs run with the lowest possible priority in the cta scheduler. The verification mount policy should be associated to a specific verification virtual organization to allow tracking ongoing verification jobs in `cta-admin sq` 

* When a verification retrieve job is executed, the tape file is streamed to `/dev/null`.

* When a recall tape session is finished, verified jobs and bytes are accounted separately from user or repack jobs in the "Tape session finished message".

The options for this command are:

* **request.user**: The user submitting the request. This will be "verification"
* **request.group**: The group of the user submitting the request. This will be "it"
* **instance**: The disk instance of the retrieve request. This can be any disk instance since verification jobs don't write the file anywhere.
* **vid**: Optional. The vid of the tape the file belongs to.

### cta-verify-tape

The `cta-verify-tape` command submits a tape for verification by selecting some files from it and issuing a `cta-verify-file` command for each of them.

When a tape is submitted for verification, it's `verification_status` is updated to `{date: <current_date>, status: ongoing, files_submitted: <number of files>, files_verified: 0, files_failed: 0}`.

The options for this command are:

* **vid**: The vid of the tape to be verified.
* **read_time**: The minimum amount of time it should take to read a tape (assuming a read speed of 300 MB/s).
* **data_size**: The minimum amount of data that should be read from the tape.
* **first**: The number of files that should be read from the beginning of the tape (fseq order)
* **last**: The number of files that should be read from the end of the tape (fseq order)
* **random**: The number of files that should be read from the middle of the tape
* **all**: If present, all tape files will be verified
* **options**: Options to pass to the `cta-verify-file` commands.

The `cta-verify-tape` command should be run from the cta frontend. It's logs are written in `/var/log/cta/verification/cta-verify-tape.log`

### Verification monitoring

#### Verification job submission on a tape

File submission is caught on `cta-frontend` MSG.

```sh
May 11 14:57:36.567479 ctafrontend cta-frontend: LVL="INFO" PID="210" TID="800" MSG="Queued retrieve request" user="ctaadmin1@ctafrontend" fileId="10026" instanceName="ctaeos" diskFilePath="dummy" diskFileOwnerUid="0" diskFileGid="0" dstURL="file://dummy" errorReportURL="" creationHost="ctafrontend" creationTime="1652273856" creationUser="ctaeos" requesterName="verification" requesterGroup="it" criteriaArchiveFileId="10026" criteriaCreationTime="1652273277" criteriaDiskFileId="10016" criteriaDiskFileOwnerUid="0" criteriaDiskInstance="ctaeos" criteriaFileSize="407" reconciliationTime="1652273277" storageClass="ctaStorageClass" checksumType="ADLER32" checksumValue="20ce7e60" tapeTapefile0="(vid=V01007 fSeq=10006 blockId=100051 fileSize=407 copyNb=1 creationTime=1652273277)" selectedVid="V01007" verifyOnly="1" catalogueTime="0.004542" schedulerDbTime="0.008317" policyName="verification" policyMinAge="1" policyPriority="1" retrieveRequestId="RetrieveRequest-Frontend-ctafrontend-210-20220511-14:35:58-0-30326" 
```



#### Verification job finished

End of verification job is caught in the `Tape session finished` MSG from `cta-taped`:
```
[1651668122.508010000] May  4 14:42:02.508010 tpsrv327.cern.ch cta-taped: LVL="INFO" PID="29084" TID="29084" MSG="Tape session finished" tapeVid="L86417" mountType="Retrieve" mountId="1409" volReqId="1409" tapeDrive="S1L91024" vendor="IBM-SONY" vo="VERIFICATION" mediaType="LTO9" tapePool="verification" logicalLibrary="SPC1L9" capacityInBytes="18000000000000" wasTapeMounted="1" mountTime="24.315550" positionTime="292.720636" waitInstructionsTime="0.225751" waitFreeMemoryTime="0.003217" waitDataTime="0.000000" waitReportingTime="0.018863" checksumingTime="0.000000" readWriteTime="28.752113" flushTime="0.000000" unloadTime="140.028687" unmountTime="16.560684" encryptionControlTime="0.084526" transferTime="28.999944" totalTime="502.537895" deliveryTime="346.121073" drainingTime="0.000000" dataVolume="6146917032" filesCount="32" headerVolume="15360" payloadTransferSpeedMBps="12.231748" driveTransferSpeedMBps="12.231779" repackFilesCount="0" userFilesCount="0" verifiedFilesCount="32" repackBytesCount="0" userBytesCount="0" verifiedBytesCount="6146917032" status="success" 
```

Here 32 files have been verified on tape `L86417`.

This message allows to mix user jobs and verification jobs, is this something we need?

Indeed: if a tape is mounted for a read it is verified by user reads by definition...

## cta-verification-feeder

This command is responsible for selecting which tapes are to be verified according to user supplied parameters. It is run periodically from rundeck and handles the following:

* Check the tapes that have an ongoing verification status. If the verification has finished (`cta-admin --json sq` does now show the `verification` mount policy for the queue) change the verification status to `finished`).
* Compare the number of ongoing verifications with a user supplied maximum. If it is less than the maximum, submit new tapes for verification until the limit is reached.

The feeder can use three parameters to choose which tape to submit for verification:
* **last_read**: Choose the tape that has not been read in the longest time.
* **last_write**: Choose the tape that has not been written in the longest time.
* **last_verified**: Choose the tape that has not been verified in the longest time.

The options for the `cta-verification-feeder` are as follows:
* **filter**: Filter out tapes matching the states passed.
* **tapepool**: Only verify tapes in the specified tapepool
* **min_data_on_tape**: Minimum number of bytes on tape for it to be verified
* **min_relative_capacity**: Minimum relative filled capacity of tape for it to be verified
* **verify_path**: Location of `cta-verify-tape` executable.
* **verify_policy**: One of `last_read`, `last_written`, `last_verified`.
* **full_tape**: Verify only tapes that are full.
* **noaction**: Dry run, do not submit the tapes for verification.
* **minage**: Verify only tapes which have not been verified for so many days.
* **maxverify**: maximum number verify processes to run concurrently.
* **verify_options**: Options to pass to `cta-verify-tape`.
* **logfile**: Log file path.

The feeder calls the `cta-verify-tape` script for each tape it decides to verify.
