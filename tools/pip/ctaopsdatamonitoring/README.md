# CTA Operations Data Monitoring

Provides high-level tools for monitoring CTA infrastructure. For now, two measurement have a complete data worflow:
- tape_volume 
- tape_namespace


## General Workflow

Each measurement have an upstream workflow to populate a main storage location on EOS with json files:`/eos/project/t/tape-operations/monitoring-long-term/`,respectively `cta-ops-generate-volume` and `cta-ops-generate-namespace` for tape_volume and tape_namspace.  

Then there is a generic script `cta-ops-measurement-to-influx` to populate influxDB backend from EOS for any measurement.

```mermaid
flowchart TB

  subgraph Upstream["Upstream workflow"]
     generate_data["cta-ops-generate-my_measurement"]
  end

  subgraph EOS["/eos/project/t/tape-operations/monitoring-long-term"]
      subgraph tape_measurement["./my_measurement"]
        measurement_files
      end
  end

  subgraph dbod-cta-downsample
    subgraph long_term_cta_monitoring
      downsampled_1d{{long_term_1d.my_measurement }}
      downsampled_1w{{long_term_1d.my_measurement }}
      downsampled_1mo{{long_term_1d.my_measurement }}
    end
  end

  subgraph Grafana
    monitgrafana[Monit Grafana Dashboard]
  end

generate_data --> measurement_files -->|measurement_to_influx| downsampled_1d & downsampled_1w & downsampled_1mo
downsampled_1d & downsampled_1w & downsampled_1mo --> monitgrafana
```

## Configuration

The tools can be configured in the standard config file, under the `tools` section, and `cta-ops-data-monitoring` subsection.

### Measurements config

For each measurement, `cta-ops-config.yaml` file is the only one place to define metadata of measurements.
Under `cta-ops-data-monitoring` tool key, here the schema config definition for measurement metadata:

```yaml
measurements: # primary key to define measurement metadata
  <my_measurement:str>: # abstract name of the measurement (recommended to be the same as eos_name)
    eos_name: <eos_root_dir_of_the_measurement:str> # name of the src dir of the measurement on EOS
    validate_schema: <true|false:bool> # optional (default:true)
    schema_path: <schema_path_on_host:str> # optional (default: <dir_package_path>/json-schemas/<eos_name>_shema.json)
    influx: # optional (mandatory when using influxConnector: interaction with influxDB backend)
      name: <influx_measurement:str> # name of the influx measurement
      tags: <["tag1","tag2"]:list[str]> # list of tags to use for influx (have to belong to the corresponding json_schema)
      fields: <["field1","field2"]:list[str]> # list of fields to use for influx (have to belong to the corresponding json_schema)
      aggregate_func: <LAST|SUM|MAX> # aggregation func for Week and Month aggregations
```

For each measurement, there is only one way to define data schema. We chose json-schema files to define the measurement schema. Each measurement have a fixed json schema file under `json-schemas` dir inside the package. The name on this file is `<eos_name>_schema.json`.
This file follow [json-schema](https://json-schema.org/) spec. It is possible to define this json schemas file somewherelse on the host and use this other location by specifying using `schema_path` key under `measurements` section.

### Scripts config
Each script can be configured under `scripts` key by specifying the name of measurement. Please see complete example below.
For script interacting with EOS, a kerberos ticket has to be present and valid for the user of the script to access to desired directory.


### Complete yaml example of the tool

``` yaml
  # -------------------------------
  # CTA Data Monitoring
  # -----------------------------Measurements--
  cta-ops-data-monitoring:
    measurements:
      tape_namespace:
        eos_name: tape_namespace
        validate_schema: true
        # schema_path: To ovewrite the default schema_path packaged inside the pip package
        influx:
          name: tape_namespace
          tags: ["storageSystem","vo"]
          fields: ["fileCount","fileSize","segmentsCount","usedSize"]
          aggregate_func: "LAST"
      tape_volume:
        eos_name: tape_volume
        validate_schema: true
        # schema_path: To ovewrite the default schema_path packaged inside the pip package
        influx:
          name: tape_volume
          tags: ["mountType","vo","instance"]
          fields: ["dataVolume"]
          aggregate_func: "SUM"
    scripts:
      cta-ops-measurement-to-influx:
        influx_host: "dbod-cta-downsample.cern.ch"
        influx_port: 8080
        influx_username: "rundeck"
        inlfux_password: "*******"
        influx_database: "monitoring_prod"
      
      cta-ops-generate-volume:
        influx_host: "dbod-rp-stat0.cern.ch"
        influx_port: 8095
        influx_username: "fluent"
        influx_password: "*******"
        influx_database: "fluenttest1"
        # influx_retention: "3months" optional already define by default

      # cta-ops-generate-namespace: no need of configuration for this script 
```




## Usage


### cta-ops-measurement-to-influx

``` sh
cta-ops-measurement-to-influx -h
usage: cta-ops-measurement-to-influx [-h] [-d DAYS_BACK | -s START_DATE] [-D] [-W] [-M] [-C CONFIG_FILE] [--verbose] measurement

Data Volume Monitoring - Push JSON measurement from EOS to InfluxDB backend

positional arguments:
  measurement           Name of measurement selected

options:
  -h, --help            show this help message and exit
  -d DAYS_BACK, --days-back DAYS_BACK
                        Number of days back in past from today to process (default: 7) eg: 1 will generate data for yesterday
  -s START_DATE, --start-date START_DATE
                        Define a bottom limit date You have to use iso format str YYYY-MM-DD The start-date is include in generated data
  -D, --daily           Upload daily aggregate data points
  -W, --weekly          Upload weekly aggregate data points
  -M, --monthly         Upload monthly aggregate data points

settings:
  Configure how this script runs

  -C CONFIG_FILE, --config-file CONFIG_FILE
                        The path for the config file (file has to be in YAML format)
  --verbose             Generate more detailed logs

```

### cta-ops-generate-namespace

``` sh 
cta-ops-generate-namespace -h
usage: cta-ops-generate-namespace [-h] [-d DAYS_BACK | -s START_DATE] [-e END_DATE] [-C CONFIG_FILE] [--verbose]

Generate namespace measurement on EOS from summary files

options:
  -h, --help            show this help message and exit
  -d DAYS_BACK, --days-back DAYS_BACK
                        Number of days back in past from today to process (default: 7) eg: 1 will generate data for yesterday
  -s START_DATE, --start-date START_DATE
                        Define a bottom limit date You have to use iso format str YYYY-MM-DD The start-date is include in generated data
  -e END_DATE, --end-date END_DATE
                        To use with days_back to define a upper limit date (default: today) You have to use iso format str YYYY-MM-DD The end-date is not include in
                        generated data

settings:
  Configure how this script runs

  -C CONFIG_FILE, --config-file CONFIG_FILE
                        The path for the config file (file has to be in YAML format)
  --verbose             Generate more detailed logs

```

### cta-ops-generate-volume

``` sh
cta-ops-generate-namespace -h
usage: cta-ops-generate-namespace [-h] [-d DAYS_BACK | -s START_DATE] [-e END_DATE] [-C CONFIG_FILE] [--verbose]

Generate volume measurement on EOS from tapeSessionFinished measurement on Influx

options:
  -h, --help            show this help message and exit
  -d DAYS_BACK, --days-back DAYS_BACK
                        Number of days back in past from today to process (default: 7) eg: 1 will generate data for yesterday
  -s START_DATE, --start-date START_DATE
                        Define a bottom limit date You have to use iso format str YYYY-MM-DD The start-date is include in generated data
  -e END_DATE, --end-date END_DATE
                        To use with days_back to define a upper limit date (default: today) You have to use iso format str YYYY-MM-DD The end-date is not include in
                        generated data

settings:
  Configure how this script runs

  -C CONFIG_FILE, --config-file CONFIG_FILE
                        The path for the config file (file has to be in YAML format)
  --verbose             Generate more detailed logs

```