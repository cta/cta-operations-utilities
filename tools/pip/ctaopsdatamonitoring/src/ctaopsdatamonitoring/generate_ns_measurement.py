import argparse
from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctaopsdatamonitoring import TOOL_NAME
from ctaopsdatamonitoring.ctajson import JSON_DAY,CtaJsonMeasurement
from ctaopsdatamonitoring.config import MeasurementInfo
from XRootD.client import File
from XRootD.client.flags import OpenFlags
import json
from datetime import date,timedelta,timezone,datetime
import os
import re
import sys
from ctautils import log_utils,str_utils

ROOT_SERVER = "root://eoshome.cern.ch"
MULTI_COPY_POOL_NAMES = re.compile(".*_([2-9]|0)+")
my_name = sys.argv[0].split("/")[-1]

config, logger = None, None

def generate_eos_path(file_date: date) -> str:
    """Function to generate tape-summary EOS path from date"""
    root_eos_dir = "/eos/project/t/tape-operations/tape-summary"
    return os.path.join(
        root_eos_dir,
        f"tape_summary_{file_date.isoformat()}.json",
    )

def load_summary_file(file_date:date)->JSON_DAY:
    """
    Load summary file from EOS based on date
    """

    eos_path = generate_eos_path(file_date)

    with File() as f:
        xrstatus, _ = f.open(f"{ROOT_SERVER}/{eos_path}", OpenFlags.READ)
        if not xrstatus.ok:
            raise ConnectionError(
                f"Error occured during {eos_path} file loading: {xrstatus}"
            )
        xrstatus, raw_data = f.read()
        if not xrstatus.ok:
            raise ConnectionError(
                f"Error occured during {eos_path} file reading: {xrstatus}"
            )

    return JSON_DAY(file_date, [json.loads(l) for l in raw_data.splitlines()])

def fetch_summaries(days:list[date]):
    """Generator to load summary files from EOS"""
    logger.info(
        f"Loading summary files from /eos/project/t/tape-operations/tape-summary EOS directory between {min(days)} and {max(days)} dates"
    )
    for date in days:
        yield load_summary_file(date)

def summary_to_namespace(jday: JSON_DAY) -> JSON_DAY:
    """Convert a json_day representing a summary file to a namespace json_day measurement"""
    logger.info(f"Converting {generate_eos_path(jday.date)} file to namespace json file")

    ns_lines = []
    # Dict of data point lists, organized by VO
    VOs = {}

    logger.debug("Cleaning data and group by VO")
    for line in jday.lines:
        VO_name = line["vo"]
        VO_name = str_utils.to_all_caps(VO_name)
        hardcoded_vo = {
            "LHCB":"LHCb",
            "NTOF":"nTOF",
        }
        VO_name = line["vo"] = hardcoded_vo.get(VO_name, VO_name)
        if VO_name not in VOs:
            VOs[VO_name] = []
        VOs[VO_name].append(line)

    # Calculate aggregate for each VO
    logger.debug("Doing the sum aggregations for each VO")
    for VO_name, summary_list in VOs.items():
        timestamp = int(datetime.combine(
            jday.date, datetime.min.time(), timezone.utc
        ).timestamp())
        file_count, file_size, segments_count, used_size = 0, 0, 0, 0
        for summary in summary_list:
            pool = summary["tapepool"]
            if not MULTI_COPY_POOL_NAMES.match(pool):
                file_count += summary["nbMasterFilesSum"]
                file_size += summary["masterDataInBytesSum"]
            used_size += summary["masterDataInBytesSum"]
            # CTA has no concept of segmentation, 1 file = 1 segment
            segments_count = file_count

        # Add data point
        ns_lines.append(
            {
            "storageSystem": 'CTA',
            "vo": VO_name,
            "timestamp": timestamp,
            "fileCount": file_count,
            "fileSize": file_size,            
            "segmentsCount": segments_count,    
            "usedSize": used_size
            }
        )

    return JSON_DAY(jday.date,ns_lines)

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description="Generate namespace measurement on EOS from summary files",
    )
    # Time frame and data sources
    origin_time_group = parser.add_mutually_exclusive_group(required=False)
    origin_time_group.add_argument(
        "-d",
        "--days-back",
        type=int,
        default=7,
        help="Number of days back in past from today to process (default: 7) \
            eg: 1 will generate data for yesterday ",
    )
    origin_time_group.add_argument(
        "-s",
        "--start-date",
        type=str,
        help="Define a bottom limit date \
            You have to use iso format str YYYY-MM-DD \
            The start-date is include in generated data",
    )

    parser.add_argument(
        "-e",
        "--end-date",
        type=str,
        default=date.today().isoformat(),
        help="To use with days_back to define a upper limit date (default: today) \
            You have to use iso format str YYYY-MM-DD \
            The end-date is not include in generated data",
    )
    # Settings
    settings_group = parser.add_argument_group(
        "settings", "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type=str,
        default=DEFAULT_CONFIG_PATH,
        help="The path for the config file (file has to be in YAML format)",
    )
    settings_group.add_argument(
        "--verbose", action="store_true", help="Generate more detailed logs"
    )
    args = parser.parse_args()

    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)


    if date.fromisoformat(args.end_date) > date.today():
        raise argparse.ArgumentError("The end_date can't be in the future")

    if args.start_date:
        args.days_back = (date.fromisoformat(args.end_date) - date.fromisoformat(args.start_date)).days
        if args.days_back <= 0:
            raise argparse.ArgumentError("The start_date has to be strictly anterior to the end_date")

    if args.days_back <= 0:
        raise argparse.ArgumentError("days_back has to be a stricly positive integer")

    return args

def main():

    args = load_args()

    days_to_fetch = frozenset(
        date.fromisoformat(args.end_date) - timedelta(days=i)
        for i in range(1, args.days_back + 1)
    )

    measurement = CtaJsonMeasurement(MeasurementInfo.from_dict(config.get("measurements")["tape_namespace"]),logger=logger)

    for day,summary in zip(days_to_fetch,fetch_summaries(days_to_fetch)):
        measurement[day] =  summary_to_namespace(summary)

    logger.info(f"Pushing {measurement.info.eos_name} json days to EOS dir")
    measurement.push_data()

if __name__ == "__main__":
    main()
