from collections import namedtuple
from jsonschema import validate
import json

from datetime import date,datetime
from XRootD.client import File, FileSystem
from XRootD.client.flags import OpenFlags, AccessMode
import os
import re
from itertools import groupby
from ctaopsdatamonitoring.config import MeasurementInfo
import logging

"""
Core Module of CTA long term monitoring. 
The goal is to provide an abstraction to manage our long term measurements.
For now the decision is to use EOS for our main storage, and send data (duplicate data) to backend solutions
as InfluxDB to use them in practice (eg. Grafana)
"""

JSON_DAY = namedtuple("JSON_DAY", ["date", "lines"])
JSON_DAY.__doc__ = "Container for one json file entry in EOS: (date of the file, json lines as python dict)"
JSON_SAMPLE = namedtuple("JSON_SAMPLE", ["date", "line"])
JSON_SAMPLE.__doc__ = "Container for one json entry in EOS: (date of the parent file, json line as python dict)"

class CtaJsonMeasurement:
    """
    Main class to manage CTA measurement stored on EOS.
    The measurement are stored following this tree structure on EOS:
        /eos/project/t/tape-operations/monitoring-long-term
        ├── measurement1
        │   ├── 2024
        │   ├── 2023
        │   └── 2022
        │       ├── 01
        │       └── 02
                    ├── versions
                        ├── measurement1_2022-02-01_1731581835.json
                        └── measurement1_2022-02-01_1731443298.json
                    ├── measurement1_2022-02-01.json
                    └── measurement1_2022-02-02.json
    When a new file is writing a new version is created under versions directory. 
    And the last version is set in the main month directory.
    If the main version is broken, an operator can change it by `cp` the version he wants from versions dir. 
    """
    ROOT_EOS_DIR = "/eos/project/t/tape-operations/monitoring-long-term"
    ROOT_SERVER = "root://eoshome.cern.ch"
    ACCESS_MODE = AccessMode.UR | AccessMode.UW | AccessMode.GR | AccessMode.OR
    VERSIONS_SUBDIR = "versions"

    def __init__(self, m_info: MeasurementInfo, logger:logging.Logger = None) -> None:
        """
        Initialize measurement with MeasuremenInfo metadata
        """
        self._info = m_info
        self._schema = None
        self._data = {}
        self._fs = FileSystem(self.ROOT_SERVER)
        self._logger = logger or logging.getLogger(self.__class__.__name__)

    def __getitem__(self, key:date)-> JSON_DAY:
        """Return the JSON_DAY with the corresponding matching date"""
        if not isinstance(key,date):
            raise TypeError(f"key {key} has to be of type date")
        return self._data[key]

    def __setitem__(self,key:date,value:JSON_DAY):
        """Set JSON_DAY to corresponding date key in the internal dict storage"""
        if key in self._data:
            raise KeyError(f"Value for this date {key} already exist")
        if not isinstance(key,date):
            raise TypeError(f"key {key} has to be of type date")
        if not isinstance(value,JSON_DAY):
            raise TypeError(f"value {value} has to be of type JSON_DAY")
        self._data[key] = value

    @property
    def data(self)->dict[date,JSON_DAY]:
        """
        Return the current loaded data. The data format is dict[date,JSON_DAY].
        """
        return self._data

    @data.setter
    def data(self, data: dict[date, JSON_DAY]):
        """Set data into internal dict. The old internal dict is completly removed by this operation.
        The provided data has to be in this format dict[date,JSON_DAY]"""
        self._data = {}
        for k,v in data.items():
            self[k] = v

    @property
    def schema(self)-> dict:
        """Return the jsonchema of the measurment as a python dict"""
        if self._schema is None:
            self._schema = self._load_schema()
        return self._schema

    def _load_schema(self) -> dict:
        """Internal method to load the jsonschema file into memory"""
        schema_path = os.path.join(self.info.schema_path)
        with open(schema_path, "r") as f:
            schema = json.load(f)
        return schema

    def generate_eos_path(self, file_date: date, timestamp: int = None) -> str:
        """
        Generate EOS path of the meausurement for specified date
        When timestamp is not provided the main measurement file path is return
        else a specific version path under 'versions' sub dir is return
        """
        paths = [self.ROOT_EOS_DIR,self.info.eos_name,f"{file_date.year:04d}",f"{file_date.month:02d}"]

        if not timestamp:
            paths.append(f"{self.info.eos_name}_{file_date.isoformat()}.json")
        else:
            paths.extend([self.VERSIONS_SUBDIR,f"{self.info.eos_name}_{file_date.isoformat()}_{timestamp}.json"])

        return os.path.join(*paths)

    def validate_file(self, jd: JSON_DAY) -> None:
        """
        Validate json day data based on jsonchema measurement 
        If data is not valid raise a Exception.
        """
        validate(jd.lines, self.schema)

    def get_json_file(self, file_date: date, validate_schema:bool = None) -> JSON_DAY:
        """
        Get JSON file from EOS for specified date. JSON_DAY is returned
        but not loaded inside internal memory. To directly load data inside internal memory use load_data()
        """
        eos_path = self.generate_eos_path(file_date)
        self._logger.debug(f"Getting json file {eos_path} from EOS")
        validate_schema = validate_schema if validate_schema is not None else self.info.validate_schema
        raw_data = self._read_data(eos_path)
        jd = JSON_DAY(file_date, [json.loads(l) for l in raw_data.splitlines()])

        if validate_schema:
            self.validate_file(jd)

        return jd

    def _read_data(self, eos_path: str)-> str:
        """Secure method to read data from eos file, return raw_data"""
        with File() as f:
            xrstatus, _ = f.open(f"{self.ROOT_SERVER}/{eos_path}", OpenFlags.READ)
            if not xrstatus.ok and xrstatus.errno == 3011:
                raise FileNotFoundError(f"File {eos_path} does not exist")

            if not xrstatus.ok:
                raise ConnectionError(
                    f"Error occured during {eos_path} file loading: {xrstatus}"
                )

            xrstatus, raw_data = f.read()
            if not xrstatus.ok:
                raise ConnectionError(
                    f"Error occured during {eos_path} file reading: {xrstatus}"
                )

            return raw_data

    def load_data(self,*files_date: date) -> None:
        """
        Load files into memory
        :param files_date an arbitrary numbers of file_date to retrieve
        and load them inside memory
        """
        self._logger.info(
            f"Getting json files from {self.ROOT_EOS_DIR}/{self.info.eos_name} EOS dir between {min(files_date)} and {max(files_date)} dates"
        )
        for file_date in files_date:
            self[file_date] = self.get_json_file(file_date)

    def _write_data(self, data: bytes, eos_path: str, overwrite = False):
        """
        Method use internally to write data to EOS.Prevent file overwrite by default.
        """
        OPEN_FLAGS = (
            OpenFlags.DELETE if overwrite else OpenFlags.NEW
        ) | OpenFlags.MAKEPATH
        with File() as f:
            xrstatus, _ = f.open(
                f"{self.ROOT_SERVER}/{eos_path}", OPEN_FLAGS, self.ACCESS_MODE
            )
            if not xrstatus.ok:
                raise ConnectionError(
                    f"Error occured during {eos_path} file opening: {xrstatus}"
                )
            xrstatus, _ = f.write(data)
            if not xrstatus.ok:
                raise ConnectionError(
                    f"Error occured during writing data to {eos_path} file: {xrstatus}"
                )

    def push_json_file(self, json_day: JSON_DAY) -> None:
        """
        Push JSON day to EOS by default if this day is already present (with a different data)
        raise an exception and write the data to 'versions' subdir. 
        """
        self._logger.debug(
            f"Pushing json file {self.generate_eos_path(json_day.date)} to EOS"
        )

        data = (
            "\n".join(json.dumps(l, separators=(",", ":")) for l in json_day.lines)
            + "\n"
        ).encode("utf-8")

        try:
            old_jd = self.get_json_file(json_day.date,validate_schema=False)
        except FileNotFoundError as f:
            if self.info.validate_schema:
                self.validate_file(json_day)
            self._write_data(
                data,
                self.generate_eos_path(json_day.date),
            )
            return

        # Compare if the data are the same
        if not set(frozenset(d.items()) for d in old_jd.lines) == set(
            frozenset(d.items()) for d in json_day.lines
        ):
            timestamp = int(datetime.now().timestamp())
            self._write_data(
                data,
                self.generate_eos_path(json_day.date, timestamp=timestamp),
            )
            raise DataChangedError(
                f"File {self.generate_eos_path(json_day.date)} already exist with different data than {self.generate_eos_path(json_day.date, timestamp=timestamp)} new file version"
            )

    def push_data(self):
        """
        Push internal memory data (all json days loaded) to their EOS location.
        """
        self._logger.info(
            f"Pushing json files to {self.ROOT_EOS_DIR}/{self.info.eos_name} EOS dir between {min(self._data.keys())} and {max(self._data.keys())} dates"
        )
        for json_day in self._data.values():
            self.push_json_file(json_day)

    def get_others_versions(self,*files_date:date)-> dict[date,list[int]]:
        """
        Return a dict with file_date for keys an list of versions found for values
        """
        files_date = set(files_date)

        def dir_grouping(file_date):
            return (file_date.year, file_date.month)

        files_versions = {file_date: [] for file_date in files_date}

        for dir, file_dates in groupby(sorted(files_date,key=dir_grouping),key=dir_grouping):
            dir_path = os.path.join(self.ROOT_EOS_DIR,self.info.eos_name,f"{dir[0]:04d}",f"{dir[1]:02d}",self.VERSIONS_SUBDIR)
            xrstatus, files = self._fs.dirlist(dir_path)
            if not xrstatus.ok:
                raise ConnectionError(f"Impossible to list file in {dir_path} directory due to: {xrstatus}")

            for file_date in file_dates:
                pattern = re.compile(rf"^{self.info.eos_name}_{file_date.isoformat()}"+r"_([1-9]\d*)\.json$")
                files_versions[file_date].extend([ int(m[1]) for file in files if (m:=re.match(pattern,file.name))])

        return files_versions

    @property
    def info(self):
        """Getter for the MeasurementInfo property"""
        return self._info


class DataChangedError(Exception):
    """Error indicate an inconsistency in checksum of same meausurement file"""
    pass
