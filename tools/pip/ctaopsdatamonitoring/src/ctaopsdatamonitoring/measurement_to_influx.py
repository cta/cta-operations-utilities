from ctaopsdatamonitoring.ctajson import CtaJsonMeasurement
from ctaopsdatamonitoring.connectors import InfluxConnector
from ctaopsdatamonitoring.config import Resolution,MeasurementInfo
from ctaopsdatamonitoring import TOOL_NAME
from datetime import date,timedelta

from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctautils import log_utils

import argparse
from influxdb import InfluxDBClient
import sys

my_name = sys.argv[0].split("/")[-1]

config, logger = None, None

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description="Data Volume Monitoring - Push JSON measurement from EOS to "
        "InfluxDB backend",
    )
    # Time frame and data sources
    parser.add_argument(
        "measurement",
        type=str,
        help=f"Name of measurement selected",
    )
    # Time frame and data sources
    origin_time_group = parser.add_mutually_exclusive_group(required=False)
    origin_time_group.add_argument(
        "-d",
        "--days-back",
        type=int,
        default=7,
        help="Number of days back in past from today to process (default: 7) \
            eg: 1 will generate data for yesterday ",
    )
    origin_time_group.add_argument(
        "-s",
        "--start-date",
        type=str,
        help="Define a bottom limit date \
            You have to use iso format str YYYY-MM-DD \
            The start-date is include in generated data",
    )

    parser.add_argument(
        "-e",
        "--end-date",
        type=str,
        default=date.today().isoformat(),
        help="To use with days_back to define a upper limit date (default: today) \
            You have to use iso format str YYYY-MM-DD \
            The end-date is not include in generated data",
    )
    
    parser.add_argument(
        "-D",
        "--daily",
        action="append_const",
        const=Resolution.DAY,
        dest="resolutions",
        help="Upload daily aggregate data points",
    )
    parser.add_argument(
        "-W",
        "--weekly",
        action="append_const",
        const=Resolution.WEEK,
        dest='resolutions',
        help="Upload weekly aggregate data points",
    )
    parser.add_argument(
        "-M",
        "--monthly",
        action="append_const",
        const=Resolution.MONTH,
        dest="resolutions",
        help="Upload monthly aggregate data points",
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings", "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type=str,
        default=DEFAULT_CONFIG_PATH,
        help="The path for the config file (file has to be in YAML format)",
    )
    settings_group.add_argument(
        "--verbose", action="store_true", help="Generate more detailed logs"
    )
    args = parser.parse_args()

    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    if date.fromisoformat(args.end_date) > date.today():
        raise argparse.ArgumentError("The end_date can't be in the future")

    if args.start_date:
        args.days_back = (
            date.fromisoformat(args.end_date) - date.fromisoformat(args.start_date)
        ).days
        if args.days_back <= 0:
            raise argparse.ArgumentError(
                "The start_date has to be strictly anterior to the end_date"
            )

    if args.days_back <= 0:
        raise argparse.ArgumentError("days_back has to be a stricly positive integer")

    if not args.resolutions:
        parser.error("At least one of --D, --W, or --M is required")

    return args

def main():

    args = load_args()

    days_to_fetch = frozenset(
            date.fromisoformat(args.end_date) - timedelta(days=i)
            for i in range(1,args.days_back + 1)
        )

    m_info = MeasurementInfo.from_dict(config.get("measurements")[args.measurement])
    if args.resolutions:
        m_info.influx.resolutions = args.resolutions

    measurement = CtaJsonMeasurement(m_info,logger=logger)

    influx_conn = InfluxConnector(InfluxDBClient(
        host=config.get("scripts", my_name, "influx_host"),
        port=config.get("scripts", my_name, "influx_port"),
        username=config.get("scripts", my_name, "influx_username"),
        password=config.get("scripts", my_name, "influx_password"),
        database=config.get("scripts", my_name, "influx_database"),
        timeout=300,
        ssl=True,
        verify_ssl=False,
    ),logger=logger)

    logger.info(f"Fetching {measurement.info.eos_name} measurement data from EOS")
    measurement.load_data(*days_to_fetch)

    logger.info(
        f"Pushing {measurement.info.eos_name} measurement data to influxDB backend\nhost:{influx_conn.client._host},port:{influx_conn.client._port},user:{influx_conn.client._username},db:{influx_conn.client._database}"
    )
    influx_conn.push_measurement(measurement)


if __name__ == "__main__":
    main()
