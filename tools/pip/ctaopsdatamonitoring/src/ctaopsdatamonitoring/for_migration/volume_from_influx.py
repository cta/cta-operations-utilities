from ctaopsdatamonitoring.ctajson import CtaJsonMeasurement,JSON_DAY
from ctaopsdatamonitoring.connectors import InfluxConnector
from ctaopsdatamonitoring.config import MeasurementInfo, InfluxMeasurementInfo
from ctaopsdatamonitoring import TOOL_NAME
from datetime import date, timedelta

from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctautils import log_utils, dict_utils, str_utils
from itertools import groupby

import argparse
from influxdb import InfluxDBClient
import sys

my_name = sys.argv[0].split("/")[-1]

config, logger = None, None


def transform_to_tape_volume(jd: JSON_DAY):
    for d in jd.lines:
        d["mountType"] = str_utils.to_pascal_case(d["mountType"])
        d["instance"] = d["instance"].lower()
        VO_name = str_utils.to_all_caps(d["vo"])
        hardcoded_vo = {
            "LHCB": "LHCb",
            "NTOF": "nTOF",
        }
        d["vo"] = hardcoded_vo.get(VO_name, VO_name)
        d["dataVolume"] = int(d.pop("dataVolumeSum"))
    # find a way to deal with mountType = 'Archive'

    def tags_grouping(json_line: dict):
        return (json_line["vo"], json_line["instance"], json_line["mountType"])

    new_jd = JSON_DAY(jd.date, [])
    for _, json_lines in groupby(
        sorted(jd.lines, key=tags_grouping), key=tags_grouping
    ):
        new_jd.lines.append(
            dict_utils.AggFunctions.SUM([jl for jl in json_lines], ["dataVolume"])
        )

    return new_jd


def transform(jdays):
    for jday in jdays:
        yield transform_to_tape_volume(jday)


def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description="Data Volume Monitoring - Push JSON data to EOS from "
        "InfluxDB backend",
    )
    # Time frame and data sources

    parser.add_argument(
        "-d",
        "--days-back",
        type=int,
        default=1,
        help="Number of days back in past from today to process (default: 1) \
            eg: 1 will generate data for yesterday ",
    )
    parser.add_argument(
        "-e",
        "--end-date",
        type=str,
        default=date.today().isoformat(),
        help="To use with days_back to define a limit date (default: today) \
            You have to use iso format str YYYY-MM-DD \
            The end-date is not include in generated data",
    )
    # Settings
    settings_group = parser.add_argument_group(
        "settings", "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type=str,
        default=DEFAULT_CONFIG_PATH,
        help="The path for the config file (file has to be in YAML format)",
    )
    settings_group.add_argument(
        "--verbose", action="store_true", help="Generate more detailed logs"
    )
    args = parser.parse_args()

    global config
    config = CtaOpsConfig(
        tool_name=TOOL_NAME,
        config_path=args.config_file,
    )

    if args.verbose:
        config.set("debug", True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return args

def main():

    args = load_args()

    days_to_fetch = frozenset(
        date.fromisoformat(args.end_date) - timedelta(days=i)
        for i in range(1, args.days_back + 1)
    )

    m_info = MeasurementInfo.from_dict(config.get("measurements")["tape_volume"])
    measurement = CtaJsonMeasurement(m_info)

    influx_client = InfluxDBClient(
        host=config.get("scripts", my_name, "influx_host"),
        port=config.get("scripts", my_name, "influx_port"),
        username=config.get("scripts", my_name, "influx_username"),
        password=config.get("scripts", my_name, "influx_password"),
        database=config.get("scripts", my_name, "influx_database"),
        timeout=300,
        ssl=True,
        verify_ssl=False,
    )
    influx_conn = InfluxConnector(influx_client)

    request_info = InfluxMeasurementInfo(
        "downsampled_ctataped_tapeSessionFinished_prod",
        ["mountType", "instance", "vo"],
        ["dataVolumeSum"],
        aggregate_func=dict_utils.AggFunctions.LAST,
        retention_policy="long_term_1d",
    )

    jdays = influx_conn.get_measurement_data(
        min(days_to_fetch), max(days_to_fetch), request_info
    )

    measurement.data = {jd.date:jd for jd in transform(jdays)}

    measurement.push_data()


if __name__ == "__main__":
    main()
