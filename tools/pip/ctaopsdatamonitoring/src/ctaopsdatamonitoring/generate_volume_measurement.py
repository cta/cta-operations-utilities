from ctaopsdatamonitoring.ctajson import CtaJsonMeasurement,JSON_DAY
from ctaopsdatamonitoring.connectors import InfluxConnector
from ctaopsdatamonitoring.config import MeasurementInfo, InfluxMeasurementInfo
from ctaopsdatamonitoring import TOOL_NAME
from datetime import date, timedelta

from ctautils.config_holder import CtaOpsConfig, DEFAULT_CONFIG_PATH
from ctautils import log_utils, dict_utils, str_utils

import argparse
from influxdb import InfluxDBClient
import sys

from itertools import groupby

my_name = sys.argv[0].split("/")[-1]

config, logger = None, None

def transform_to_tape_volume(jd: JSON_DAY):
    """Convert a json_day representing a tapeSessionFinished measurement to a volume json_day measurement"""
    logger.info(
        f"Converting tapeSessionFinshed measurement of {(jd.date)} to volume json file"
    )

    logger.debug("Cleaning data")
    for d in jd.lines:
        d["mountType"] = str_utils.to_pascal_case(d["mountType"])
        d["instance"] = d["instance"].lower()
        VO_name = str_utils.to_all_caps(d["vo"])
        hardcoded_vo = {
            "LHCB": "LHCb",
            "NTOF": "nTOF",
        }
        d["vo"] =  hardcoded_vo.get(VO_name, VO_name)
        d["dataVolume"] = int(d["dataVolume"])

    def tags_grouping(json_line:dict):
        return (json_line["vo"], json_line["instance"], json_line["mountType"])

    logger.debug("Doing the sum aggregations for each (VO,instance,mountType)")
    new_jd = JSON_DAY(jd.date,[])
    for _, json_lines in groupby(
                sorted(jd.lines, key=tags_grouping), key=tags_grouping
            ):
        new_jd.lines.append(
            dict_utils.AggFunctions.SUM([jl for jl in json_lines], ["dataVolume"])
        )

    return new_jd

def transform(jdays:list[JSON_DAY]):
    """Generator to transform json days data from tapeSession measurement to tape_volume measurement"""
    for jday in jdays:
        yield transform_to_tape_volume(jday)


def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description="Generate volume measurement on EOS from tapeSessionFinished measurement on Influx",
    )
    # Time frame and data sources
    origin_time_group = parser.add_mutually_exclusive_group(required=False)
    origin_time_group.add_argument(
        "-d",
        "--days-back",
        type=int,
        default=7,
        help="Number of days back in past from today to process (default: 7) \
            eg: 1 will generate data for yesterday ",
    )
    origin_time_group.add_argument(
        "-s",
        "--start-date",
        type=str,
        help="Define a bottom limit date \
            You have to use iso format str YYYY-MM-DD \
            The start-date is include in generated data",
    )

    parser.add_argument(
        "-e",
        "--end-date",
        type=str,
        default=date.today().isoformat(),
        help="To use with days_back to define a upper limit date (default: today) \
            You have to use iso format str YYYY-MM-DD \
            The end-date is not include in generated data",
    )
    # Settings
    settings_group = parser.add_argument_group(
        "settings", "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type=str,
        default=DEFAULT_CONFIG_PATH,
        help="The path for the config file (file has to be in YAML format)",
    )
    settings_group.add_argument(
        "--verbose", action="store_true", help="Generate more detailed logs"
    )
    args = parser.parse_args()

    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    if date.fromisoformat(args.end_date) > date.today():
        raise argparse.ArgumentError("The end_date can't be in the future")

    if args.start_date:
        args.days_back = (
            date.fromisoformat(args.end_date) - date.fromisoformat(args.start_date)
        ).days
        if args.days_back <= 0:
            raise argparse.ArgumentError(
                "The start_date has to be strictly anterior to the end_date"
            )

    if args.days_back <= 0:
        raise argparse.ArgumentError("days_back has to be a stricly positive integer")

    return args

def main():

    args = load_args()

    days_to_fetch = frozenset(
        date.fromisoformat(args.end_date) - timedelta(days=i)
        for i in range(1, args.days_back + 1)
    )

    measurement = CtaJsonMeasurement(MeasurementInfo.from_dict(config.get("measurements")["tape_volume"]),logger=logger)

    influx_conn = InfluxConnector(InfluxDBClient(
        host=config.get("scripts", my_name, "influx_host"),
        port=config.get("scripts", my_name, "influx_port"),
        username=config.get("scripts", my_name, "influx_username"),
        password=config.get("scripts", my_name, "influx_password"),
        database=config.get("scripts", my_name, "influx_database"),
        timeout=300,
        ssl=True,
        verify_ssl=False,
    ),logger=logger)

    request_info = InfluxMeasurementInfo(
        "ctataped_tapeSessionFinished",
        ["vo", "mountType", "instance"],
        ["dataVolume"],
        aggregate_func=dict_utils.AggFunctions.SUM,
        retention_policy=f'"{config.get("scripts", my_name, "influx_retention", default="3months")}"',
    )

    logger.info(
        f"Loading {request_info.retention_policy}.{request_info.name} from  {min(days_to_fetch)} to  {min(days_to_fetch)} from Influx\nhost:{influx_conn.client._host},port:{influx_conn.client._port},user:{influx_conn.client._username},db:{influx_conn.client._database}"
    )
    jdays = influx_conn.get_measurement_data(
        min(days_to_fetch), max(days_to_fetch), request_info
    )

    logger.info(
        f"Tansform tapeSession measurement from Influx between {min(days_to_fetch)} and {max(days_to_fetch)} dates to tape_volume json format"
    )
    measurement.data = {jd.date:jd for jd in transform(jdays)}

    logger.info(f"Pushing {measurement.info.eos_name} json days to EOS dir")
    measurement.push_data()

if __name__ == "__main__":
    main()
