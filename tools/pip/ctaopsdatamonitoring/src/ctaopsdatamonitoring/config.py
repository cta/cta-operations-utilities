from jsonschema import validate
from ctautils.dict_utils import AggFunctions
from enum import Enum
from dataclasses import dataclass,field
import os 
"""
Module to define what are the metadata of a CtaJsonmeasurement and 
the metadata of avaiable backends (only influx for now)
"""

JSON_SCHEMAS_DIR = os.path.join(os.path.dirname(__file__),"json-schemas")


class Resolution(Enum):
    """
    Enum to define retention policies used for influx 
    """
    DAY = "1d"
    WEEK = "1w"
    MONTH = "1mo"


@dataclass
class InfluxMeasurementInfo:
    """
    Dataclass defining the influx metadata required to interact with influxDB
    backend for CtaJsonMeasurement
    """
    name: str
    tags: list[str]
    fields: list[str]
    resolutions: list[Resolution] = field(default_factory=list)
    aggregate_func: AggFunctions = None
    retention_policy: str = None # Just to override default resolution for import data 

    def __post_init__(self):
        """
        Check consistency of metadata
        """
        if not self.resolutions:
            self.resolutions.append(Resolution.DAY)

        if (Resolution.WEEK in self.resolutions or Resolution.MONTH in self.resolutions) and not self.aggregate_func:
            raise ValueError(f"aggregate_func has to be provided for {Resolution.WEEK} or {Resolution.MONTH} resolutions")

    @classmethod
    def from_dict(cls, data):
        """
        Classmethod to instanciate InfluxMeasurementInfo metadata from a dictionnary
        Recommended to use this if you want to verify user entry consistency (cli and config file).
        The data is validated through a jsonshema validation 
        """
        _schema = {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "tags": {
                    "type": "array",
                    "items": {"type": "string"},
                    "uniqueItems": True,
                    "minItems": 1,
                },
                "fields": {
                    "type": "array",
                    "items": {"type": "string"},
                    "uniqueItems": True,
                    "minItems": 1,
                },
                "resolutions": {
                    "type": "array",
                    "items": {
                        "type": "string",
                        "enum": [res.value for res in Resolution],
                    },
                    "uniqueItems": True,
                },
                "aggregate_func": {
                    "type": "string",
                    "enum": AggFunctions._functions,
                }
            },
            "required": ["name", "tags", "fields"],
        }

        validate(data, _schema)

        resolutions = [Resolution(r) for r in res] if (res := data.get("resolutions")) else []
        agg_func = getattr(AggFunctions, func_name) if (func_name := data.get("aggregate_func")) else None

        return InfluxMeasurementInfo(
            data["name"], data["tags"], data["fields"], resolutions, agg_func
        )


@dataclass
class MeasurementInfo:
    """
    Dataclass defining the ctajsonmeasurement metadata required to interact with EOS,
    and for data validation
    """
    eos_name: str
    schema_path: str = None
    validate_schema: bool = True
    influx: InfluxMeasurementInfo = None

    def __post_init__(self):
        """
        Check consistency of metadata
        """
        if not self.schema_path:
            self.schema_path = os.path.join(JSON_SCHEMAS_DIR,f"{self.eos_name}_schema.json")

        if self.validate_schema and not os.path.exists(self.schema_path):
            raise FileNotFoundError(f"File {self.schema_path} containing the jsonschema data validator does not exist")

    @classmethod
    def from_dict(cls, data):
        """
        Classmethod to instanciate MeasurementInfo metadata from a dictionnary
        Recommended to use this if you want to verify user entry consistency (cli and config file).
        The data is validated through a jsonshema validation (and submetadata also)
        """
        _schema = {
            "type": "object",
            "properties": {
                "eos_name": {"type": "string"},
                "schema_path": {"type": "string"},
                "validate_schema": {
                    "type": "boolean",
                },
                "influx": {"type": "object"},
            },
            "required": ["eos_name"],
        }

        validate(data, _schema)

        influx_info = InfluxMeasurementInfo.from_dict(influx) if (influx := data.get("influx")) else None

        return MeasurementInfo(
            data["eos_name"],
            data.get("schema_path"),
            data.get("validate_schema", True),
            influx_info,
        )
