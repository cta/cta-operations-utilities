from itertools import groupby
from influxdb import InfluxDBClient
from datetime import date, datetime, timezone, timedelta
from ctaopsdatamonitoring.ctajson import (
    JSON_DAY,
    JSON_SAMPLE,
    CtaJsonMeasurement,
)
from ctaopsdatamonitoring.config import Resolution,InfluxMeasurementInfo
import logging
"""
Module defining bakends for ctajsonmeasurement. For now only Influx Backend
"""

class InfluxConnector:
    """
    Connector for CtaJsonMeasurement to interact with InfluxDB as a backend. 
    """
    WRITE_BATCH_SIZE = 5000

    def __init__(
        self, influx_instance: InfluxDBClient, logger: logging.Logger = None
    ):
        """
        Initializes InfluxConnector by providing a influxDB client to interact with
        """
        self.client = influx_instance
        self._logger = logger or logging.getLogger(self.__class__.__name__)

    @staticmethod
    def _check_measurement_conf(measurement: CtaJsonMeasurement):
        """
        Method to test if the measurement contains Influx information required for using the connector
        """
        if not isinstance(measurement.info.influx, InfluxMeasurementInfo):
            raise ValueError(
                f"Measurement {measurement.info.eos_name} has to have InfluxMeasurementInfo to be used with the InfluxConnector"
            )
        return measurement

    def _json_sample_to_influx_point(
        self, js: JSON_SAMPLE, measurement_name: str, tags: list[str], fields: list[str]
    ) -> dict:
        """ Translate JSON_SAMPLE object to influx data point dict format"""
        influx_point = {
            "measurement": measurement_name,
            "tags": {t: js.line[t] for t in tags},
            "time": int(
                datetime.combine(js.date, datetime.min.time(), timezone.utc).timestamp()
            ),  # Timestamp in s for efficiency enough for our needs
            "fields": {f: js.line[f] for f in fields},
        }
        return influx_point

    def _json_day_to_influx_points(
        self, jd: JSON_DAY, influx_info: InfluxMeasurementInfo
    ):
        """
        Generator to convert JSON_DAY object into data points in influx format 
        """
        self._logger.debug(f"Convert json day:{jd.date} measurement: {influx_info.name} to influx json format")
        for jl in jd.lines:
            yield self._json_sample_to_influx_point(
                JSON_SAMPLE(jd.date, jl),
                influx_info.name,
                influx_info.tags,
                influx_info.fields,
            )

    def measurement_to_influx_points(
        self, measurement: CtaJsonMeasurement, resolution = Resolution.DAY
    ):
        """
        Generator to convert data of CtaJsonMeasurement object to influx points.
        By choosing the resolution, the method can downsampled the data by making aggregations.
        Resolutions choices: DAY, WEEK, MONTH. The data is aggregate the first day of the WEEK/MONTH
        """
        self._logger.info(f"Convert measurement {measurement.info.eos_name} to influx points for {resolution} resolution")
        self._check_measurement_conf(measurement)

        if resolution == Resolution.DAY:
            json_days = measurement.data.values()
        elif resolution == Resolution.WEEK:
            json_days = self._weekly_aggregates(measurement)
        elif resolution == Resolution.MONTH:
            json_days = self._monthly_aggregates(measurement)
        else:
            raise TypeError(
                "resolution has to be one of these values: {}".format(
                    ", ".join(f"{res.name}:{res.value}" for res in Resolution)
                )
            )

        for jd in json_days:
            yield from self._json_day_to_influx_points(jd, measurement.info.influx)

    def push_measurement(self, measurement: CtaJsonMeasurement, resolutions:list[Resolution]= None):
        """
        Push CtaJsonMeasurement data to influx backend.
        The user can provide a list of Resolution to populate different retention policies
        Resolutions choices: DAY, WEEK, MONTH. The data is aggregate the first day of the WEEK/MONTH
        """

        m_resolutions = self._check_measurement_conf(measurement).info.influx.resolutions

        if m_resolutions is None:
            m_resolutions = resolutions if resolutions is not None else [Resolution.DAY]

        self._logger.info(f"Push measurement {measurement.info.eos_name} for resolutions {m_resolutions} to InfluxDB")

        for resolution in m_resolutions:
            retention_policy = f"long_term_{resolution.value}"
            self.client.write_points(
                self.measurement_to_influx_points(measurement, resolution),
                batch_size=self.WRITE_BATCH_SIZE,
                protocol="json",
                retention_policy=retention_policy,
                time_precision='s'
            )

    def push_measurements(self, measurements: list[CtaJsonMeasurement],resolutions:list[Resolution]= None, fail_at_end=False):
        """
        Push a list a several CtaJsonMeasurement with push_measurement method
        The user can provide a list of Resolution to populate different retention policies
        Resolutions choices: DAY, WEEK, MONTH. The data is aggregate the first day of the WEEK/MONTH
        By default if a error is encountered for a measurement, method fails directly.
        Choose fail at end for raising adter trying all measurements  
        """
        self._logger.info(f"Push measurements {[m.info.eos_name for m in measurements]} to influx points")

        failed_measurements = []
        for m in measurements:
            try:
                self.push_measurement(m, resolutions)
            except Exception as e:
                if fail_at_end:
                    failed_measurements.append(m, resolutions)
                else:
                    raise e

        if failed_measurements:
            raise Exception("Error when pushing these measurements: "+ ", ".join(m.info.eos_name for m in failed_measurements))

    def _weekly_aggregates(self, measurement: CtaJsonMeasurement) -> list[JSON_DAY]:
        """
        Aggregates CtaJsonMeasurement data to list of JSON_DAY (one for each week) 
        JSON_DAY contains all aggregate data of the week at the first day of the week (monday)
        Caution: The data is supposes wihtout holes bettween days. The data has to be complete for each week.
            The only verification is on the oldest and latest date. The dates taken into account include only complete week (monday to sunday).
            An incomplete week is allowed if it is the current one. 
        """
        self._logger.info(f"Doing week grouping for {measurement.info.eos_name} measurement")

        if measurement.info.influx.aggregate_func is None:
            raise Exception(
                "No config aggregations function information to make weekly aggregations"
            )

        influx_info = measurement.info.influx

        oldest_date = min(measurement.data.keys())
        latest_date = max(measurement.data.keys()) 

        date_min_cutoff = (
            oldest_date
            if oldest_date.weekday() == 0
            else  oldest_date + timedelta(days=7 - oldest_date.weekday()) # get next monday
        )

        date_max_cutoff = (
            latest_date
            if latest_date.weekday() == 6 or date.today().isocalendar()[:2] == latest_date.isocalendar()[:2] # allow incomplete week if it is the current one
            else latest_date - timedelta(days= latest_date.weekday()+1) # get previous sunday
        )

        sample_gen = (
            JSON_SAMPLE(jd.date, line)
            for jd in measurement.data.values()
            for line in jd.lines
            if jd.date >= date_min_cutoff and jd.date <= date_max_cutoff
        ) # take into into account only days forming a complete week

        def week_grouping(js: JSON_SAMPLE):
            days_since_monday = js.date.weekday()
            first_day_of_week = js.date - timedelta(days=days_since_monday)

            return (first_day_of_week, *[js.line[k] for k in influx_info.tags])

        return self._time_aggregate(
            sample_gen,
            week_grouping,
            influx_info.aggregate_func,
            influx_info.fields,
        )

    def _monthly_aggregates(self, measurement: CtaJsonMeasurement) -> list[JSON_DAY]:
        """
        Aggregates CtaJsonMeasurement data to list of JSON_DAY (one for each month)
        JSON_DAY contains all aggregate data of the month at the first day of the month
        Caution: The data is supposes wihtout holes bettween days. The data has to be complete for each month.
            The only verification is on the oldest and latest date. The dates taken into account include only complete month.
            An incomplete month is allowed if it is the current one.
        """
        self._logger.info(f"Doing month grouping for {measurement.info.eos_name} measurement")

        if measurement.info.influx.aggregate_func is None:
            raise Exception(
                "No config aggregations function information to make monthly aggregations"
            )

        influx_info = measurement.info.influx

        oldest_date = min(measurement.data.keys())
        latest_date = max(measurement.data.keys())

        if oldest_date.day == 1:
            date_min_cutoff = oldest_date
        else:
            date_min_cutoff = (
                date(oldest_date.year + 1, 1, 1)
                if oldest_date.month == 12
                else date(oldest_date.year, oldest_date.month + 1, 1)
            )

        today = date.today()
        date_max_cutoff = (
            latest_date
            if (latest_date + timedelta(days=1)).day == 1 or (latest_date.year, latest_date.month) == (today.year, today.month) # allow incomplete month if it is the current one
            else  date(oldest_date.year, oldest_date.month, 1) - timedelta(days=1) # get last day of previous month
        )

        sample_gen = (
            JSON_SAMPLE(jd.date, line)
            for jd in measurement.data.values()
            for line in jd.lines
            if jd.date >= date_min_cutoff and jd.date <= date_max_cutoff
        )

        def month_grouping(js: JSON_SAMPLE):
            first_day_of_month = date(
                year=js.date.year,
                month=js.date.month,
                day=1,
            )
            return (first_day_of_month, *[js.line[k] for k in influx_info.tags])

        return self._time_aggregate(
            sample_gen,
            month_grouping,
            influx_info.aggregate_func,
            influx_info.fields,
        )

    def _time_aggregate(
        self, sample_gen, group_func, aggregate_func, fields: list[str]
    ) -> list[JSON_DAY]:
        """
        Generic functio_aggregaten to to aggregations based on a generator of dict,
        a grouping function (eg. keys of the dict), a aggregate function (eg. LAST,MAX,SUM),
        a list of fields to process with the aggregate function
        """
        self._logger.debug(
            f"Doing aggregation: group_func:{group_func.__name__},aggregate_func:{aggregate_func.__name__} for fields:{fields}"
        )

        aggregate_data = {}
        for key, jsamples in groupby(
            sorted(sample_gen, key=group_func), key=group_func
        ):
            agg_dict = aggregate_func([js.line for js in sorted(jsamples,key=lambda js: js.date)], fields)

            first_day_of_group = key[0]

            if first_day_of_group not in aggregate_data:
                aggregate_data[first_day_of_group] = JSON_DAY(
                    first_day_of_group, [agg_dict]
                )
            else:
                aggregate_data[first_day_of_group].lines.append(agg_dict)

        return aggregate_data.values()

    def get_measurement_data(
        self,
        min_date: date,
        max_date: date,
        request_info: InfluxMeasurementInfo,
    ):
        """
        Request measurement data from influxDB from min_date at 00:00 to max_date at 23:59:59.999
        and return a generator of json days.
        The request_info is use to know the fields to request, tags and function to use for aggregation  
        Timestamps are collasped to 00:00 utc for each day.
        """
        if request_info.aggregate_func is None:
            raise Exception(
                "No config aggregations function information"
            )

        min_timestamp = int(datetime.combine(min_date, datetime.min.time(), timezone.utc).timestamp()*1e9)
        max_timestamp = int(
            datetime.combine(max_date + timedelta(days=1), datetime.min.time(), timezone.utc).timestamp()
            * 1e9
        )

        fields_str = ", ".join(
            f"{request_info.aggregate_func.__name__}({field}) AS {field}"
            for field in request_info.fields
        )
        tags_str = ', '.join(request_info.tags)
        retention_str = request_info.retention_policy if request_info.retention_policy else f"long_term_{Resolution.DAY.value}"
        query_str = (
            f"SELECT  {fields_str} "
            f"FROM {retention_str}.{request_info.name} "
            f"WHERE time >= {min_timestamp} and time < {max_timestamp} "
            f"GROUP BY time(1d), {tags_str} "
        )
        self._logger.info(
            f"Doing InfluxQL request: {query_str}"
        )
        r = self.client.query(query_str)

        def day_grouping(point:dict):    
            return date.fromtimestamp(point['timestamp'])

        def get_points():
            for series in r.raw['series']:  # Access the raw series to get tags separately
                for point in series['values']:
                    if any(v is None for v in point) or any(
                        not tagvalue for tagvalue in series["tags"].values()
                    ):
                        continue
                    # Convert each point to a dictionary with field names
                    point_dict = dict(zip(series['columns'], point))
                    # Include tags in the point dictionary

                    point_dict.update(series["tags"])
                    time = datetime.strptime(point_dict.pop("time"), "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)
                    point_dict['timestamp']= int(time.timestamp())
                    yield point_dict

        self._logger.info(
            f"Grouping by day {sum(len(s['values']) for s in r.raw['series'])} points divided into {len(r.raw['series'])} "
        )
        for day, points in groupby(sorted(get_points(), key=day_grouping), key=day_grouping):
            yield JSON_DAY(day,[p for p in points])
