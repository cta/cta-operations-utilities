#!/usr/bin/env python3

# @project      The CERN Tape Archive (CTA)
# @copyright    Copyright © 2024 CERN
# @license      This program is free software, distributed under the terms of
#               the GNU General Public Licence version 3 (GPL Version 3),
#               copied verbatim in the file "LICENSE".
#               You can redistribute it and/or modify it under the terms of the
#               GPL Version 3, or (at your option) any later version.
#
#               This program is distributed in the hope that it will be useful,
#               but WITHOUT ANY WARRANTY; without even the implied warranty of
#               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#               See the GNU General Public License for more details.
#
#               In applying this licence, CERN does not waive the privileges and
#               immunities granted to it by virtue of its status as an
#               Intergovernmental Organization or submit itself to any
#               jurisdiction.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright © 2024 CTA CERN

"""
A tool for collecting environmental (temperature/humidity) info from drive sensors.
"""

import sys
import time
import json
import argparse

from ctautils import log_utils
from ctautils.config_holder import CtaOpsConfig,DEFAULT_CONFIG_PATH
from ctaopsdrvenv import TOOL_NAME
from tapeadmin import tape_drive


###############
#
# CONFIGURATION
#
###############

# Get rid of "/usr/local/bin" and take only the final part
my_name = sys.argv[0].split('/')[-1]

config, logger = None, None

###########
#
# FUNCTIONS
#
###########

def load_args():
    """
    Loads the options for all possible script arguments
    """
    parser = argparse.ArgumentParser(
        description="Tape Environmental Generator - " \
        "Gathers data about the humidity and temperature levels " \
        "in the tapeserver and writes it to stdout or a logfile",
    )
    parser.add_argument(
        "-o",
        "--output-file",
        type = str,
        help = "The path for the json output report file"
    )

    # Settings
    settings_group = parser.add_argument_group(
        "settings",
        "Configure how this script runs"
    )
    settings_group.add_argument(
        "-C",
        "--config-file",
        type = str,
        default = DEFAULT_CONFIG_PATH,
        help = "The path for the config file (file has to be in YAML format)"
    )
    settings_group.add_argument(
        "-V",
        "--verbose",
        action = 'store_true',
        help = "Generate more detailed logs"
    )

    args = parser.parse_args()

    # Set up config holder
    global config
    config = CtaOpsConfig(
        tool_name = TOOL_NAME,
        config_path = args.config_file,
    )

    if args.verbose:
        config.set('debug', True)

    if args.output_file:
        config.set('output_file', args.output_file)

    # Set up logger
    global logger
    logger = log_utils.init_logger(my_name, config)

    return args


def main():
    """
    Locate local drives, gather environmental data for each and write
    resulting json to a file. This in turn should be read by something
    like Fluentd.
    """
    args = load_args()

    now = int(time.time())
    local_drives = tape_drive.get_local_drives(config, logger)

    drive_names = ', '.join([d['DriveName'] for d in local_drives])
    logger.debug(
        f"Found local drives: {drive_names}"
    )

    if len(local_drives) < 1:
        log_utils.log_and_exit(
            logger,
            "No local tape drives found.",
            return_code = 0
        )

    for drive_dict in local_drives:
        device = drive_dict['DriveDevice']
        model = tape_drive.get_drive_model(device, logger)

        # Blacklist drive models which do not support the environmental stats gathering
        model_blacklist = config.get('model_blacklist', default=[])
        if model in model_blacklist:
            logger.warning(
                f"Drive model {model} not supported. Skipping."
            )
            continue

        temperature = tape_drive.get_drive_temperature(device, logger)
        humidity = tape_drive.get_drive_humidity(device, logger)
        library, tapedrive, tapeserver = tape_drive.get_drive_info(logger)

        output_map = {
            "timestamp": int(now),
            "command": my_name,
            "date": now,
            "library": library,
            "tapeserver": tapeserver,
            "tapedrive": tapedrive,
            "temperature": temperature,
            "humidity": humidity
        }

        output_file = config.get('output_file', default=None)
        if not output_file:
            # Write to stdout
            sys.stdout.write(json.dumps(output_map) + "\n")
        else:
            with open(output_file, 'a', encoding='utf-8') as f:
                f.write(json.dumps(output_map) + "\n")


if __name__ == "__main__":
    main()
