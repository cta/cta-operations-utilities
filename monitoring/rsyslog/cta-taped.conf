# Example config for using rsyslog to read a cta-taped json log file and send the content to a log collector.

# Read logs from text file using the imfile input module
# https://www.rsyslog.com/using-the-text-file-input-module/
module(load="imfile")

# Send logs using RELP Output Module
# https://www.rsyslog.com/files/temp/doc-indent/configuration/modules/omrelp.html
module(load="omrelp")

# JSON log line parser
module(load="mmjsonparse")

# Output template, tapelogs, json
# Docs for the 'all-json' property: https://www.rsyslog.com/json-elasticsearch/
template(name="ctatapelog-json" type="list") {
    property(name="$!all-json")
}

# Define actions to take
ruleset(name="tapeserver-send-tapelog") {
    # Parse json log message and populate <$!> fields
    action(type="mmjsonparse" cookie="")
    # Send to ctatapelog
    action(
        name="tapeserver-to-ctatapelog"   # Name replaces the raw action number in logs, see /var/log/messages
        type="omrelp"
        template="ctatapelog-json"
        target="changeme"
        port="changeme"
        queue.filename="ctatapelog"
        queue.maxdiskspace="1g"           # spool space limit (use as much as possible)
        queue.saveonshutdown="on"         # save messages to disk on shutdown
        queue.type="LinkedList"           # run asynchronously
        action.resumeRetryCount="-1"      # infinety retries if host is down
    )
}

# Read log lines from the cta-taped log file.
input(
    type="imfile"
    File="/var/log/cta/cta-taped.log"
    ruleset="tapeserver-send-tapelog"
    tag="cta-taped"
)

