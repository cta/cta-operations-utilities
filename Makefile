##
# CTA Operations tools
#
# @file
# @version 0.1

SHELL = /bin/sh
# Relative to pkg dir, aka ./build
DEST := ../../../build

# Libs
ctautils := tools/pip/ctautils
tapeadmin := tools/pip/tapeadmin

# Tools
cta-ops-repack-automation := tools/pip/atresys
cta-ops-eos               := tools/pip/ctaopseos
cta-ops-admin             := tools/pip/ctaopsadmin
cta-ops-tape-verify       := tools/pip/tapeverify
cta-ops-pool-supply       := tools/pip/poolsupply
cta-ops-drive-config-generate := tools/pip/ctaopsdrvconfgen
cta-ops-tape-alerting-system  := tools/pip/tapealerting
cta-ops-drive-environmentals  := tools/pip/ctaopsdrvenv
cta-ops-data-monitoring  := tools/pip/ctaopsdatamonitoring

# Group them
libs  := $(ctautils) $(tapeadmin)
tools := $(cta-ops-repack-automation) $(cta-ops-eos) $(cta-ops-admin) $(cta-ops-tape-verify) $(cta-ops-pool-supply) $(cta-ops-drive-config-generate) $(cta-ops-tape-alerting-system) $(cta-ops-drive-environmentals) $(cta-ops-data-monitoring)

.PHONY: all clean $(libs) $(tools)
all: build
build: $(libs) $(tools)

$(libs):
	DEST=${DEST} $(MAKE) --directory=$@ build

$(tools):
	DEST=${DEST} $(MAKE) --directory=$@ build

clean:
	rm -r ./build

# end
